@brand-lifebouy  @locale-za	@cxx @2020																																																																																																																																												
Feature: lifebouy - za
	
		@rr @p2
		Scenario: lifebouy za - VerifyReviewform - SubmitReview 
			Given the "lifebouy" brand "productpage" of "za" locale has been built 
			When i click on Write a Review link 
			Then i should be able to see write a review dialog 
			And i should be able to see product image in write a review dialog
			And i select overall rating as 4 
			And i give review title as "Lorem ipsom" 
			And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
			And i enter random value in nickname 
			And i enter random email id for notification purposes 
#			And i check age checkbox 
			And i agree to terms and conditions 
			And i submit the review 
			#Then i should see that review is submitted with message "THANK YOU. YOUR REVIEW HAS BEEN SUBMITTED." 
			
		@bin  @p2
		Scenario: lifebouy za - VerifyBuyInNow - BIN_Flow 
			Given the "lifebouy" brand "productpage" of "za" locale has been built 
			When i see the structure of the "productpage" 
			Then i should be able to see Buy It Now functionality 
			When i click on Buy It Now Button 
			Then i should be able to see online store list 
		
@faq @p2
  Scenario: Global Footer - FAQs Validation
  
    Given the "lifebouy" brand "homepage" of "za" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded	
    
@cta @p1
Scenario: LIFEBUOY - articledetailpage - Related Article 
	Given the "lifebouy" brand "articledetailpage" of "za" locale has been built 
	When i see the structure of the "articledetailpage"
    And I verify that count of component "article" on article page is 1
    And i verify that listing article component image is rendering
    And i verify that listing article component heading is appearing
    And i click on the listing article image
    And I verify that count of component "article" on article page is 1
    Then i verify that listing article component image is rendering    
    
@cross @p1
  Scenario: Home page - related products v2 component
    Given the "lifebouy" brand "productpage" of "za" locale has been built
    When i see the structure of the "productpage"
    And I expect to see "Lifebouy_Heading" as visible
    And I expect to see "Lifebouy_Img" as visible
    And I click on a "Lifebouy_Img"
    And I expect to see "Lifebouy_Heading" as visible
    
 #	Then i verify that following components "component-content" exist on the "productpage" 
    #And i verify that related products are present on the productpage   
    #And i verify that related product component image is rendering
    #And i verify that related product component heading is appearing
    #When I click on the related-product component
#	And i verify that related products are present on the productpage   
    #And i verify that related product component image is rendering
    #And i verify that related product component heading is appearing    
    #
@signup @p2     
Scenario: SignUp verification - Sign Up -  numerical validations						
    Given the "lifebouy" brand "homepage" of "za" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page       
    
    	