@brand-vaseline @cxx @locale-us	@us @2020
Feature: vaseline - us/en 

@rr @p1
Scenario: vaseline - VerifyReviewsubmit - ProductDetailPage 
	Given the "vaseline" brand "productpage" of "us/en" locale has been built 
	When i click on Write a Review link 
	Then i should be able to see write a review dialog 
	And i should be able to see product image in write a review dialog
	When i select overall rating as 4 
	And i give review title as "Lorem ipsom" 
	And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
	And i recommend this product to friend 
	And i enter random value in nickname 
	And i enter random email id for notification purposes 
	And i enter zipcode as "99501" 
	And i select "March" as birth month 
	And i select "1980" as birth year 
	And i select "Male" as gender 
	And i select "Grocery Store" as purchase location 
	And i select "Once a week" as Frequency 
	And i select "2-5 Years" as how long you have been purchasing 
	And i select "Yes" as incentive for reviews 
	And i select "Yes" as sign up for exciting offers 
	And i select 9 option for recommend brand to a friend 
	And i agree to terms and conditions 
	And i submit the review
	
@bin @p1
Scenario: vaseline - VerifyAddToBag - AddProductToCart 
	Given the "vaseline" brand "productpage" of "us/en" locale has been built 
	When i see the structure of the "productpage" 
	And I wait for 10 seconds
	And I click on a "BIN_Button" 
	And I expect to see "BIN_Widget" as visible
	#Then i should be able to see online store list 
	
	
@store 
Scenario: vaseline - VerifyStoreLocator - SearchforStore 
	Given the "vaseline" brand "storelocatorpage" of "us/en" locale has been built 
	When i see the structure of the "storelocatorpage" 
	Then i select "Hair" category for store locator 
	And i select "Hair Styling" sub categry for store locator 
	Then i should be able to see products for select category and sub category 
	When i select any of the random product for seleted category and subcategory 
	And i enter "10001" zip code 
	And i select "50 Miles" as find with store range 
	And i click on find in store button 
	Then i should be able to see map for searched stores 
	And i should be able to see stores list 
	
@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "vaseline" brand "homepage" of "us/en" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "component-content" exist on the "faqpage"	
    
        
@cta @p1
Scenario: VASELINE - articledetailpage - Related Article 
	  Given the "vaseline" brand "articledetailpage" of "us/en" locale has been built 
	  When i see the structure of the "articledetailpage"
    And I verify that count of component "article" on article page is 1
    Then i verify that listing article component image is rendering
    Then i verify that listing article component heading is appearing
    Then i verify that listing article component description is appearing
    And i click on the listing article image
    Then i verify that listing article component image is rendering
    Then i verify that listing article component heading is appearing
    Then i verify that listing article component description is appearing 
   
   @cross @p1 @p3
  Scenario: Home page - related products v2 component
    Given the "vaseline" brand "productpage" of "us/en" locale has been built
    And i verify that box-listing are present on the productpages   
    And i verify that box-listing component image is rendering
    And i verify that box-listing component heading is appearing
    And I scroll published page down by 2000
    And I click on a "VaselineRelatedProduct_Heading"
    #When I click on the related-product component by indexing
	  And i verify that box-listing are present on the productpages   
    And i verify that box-listing component image is rendering
    And i verify that box-listing component heading is appearing	
    
     @cp @p2
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "vaseline" brand "homepage" of "za" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains ""	  
    
		@signup @p1     
Scenario: SignUp verification Scenario - Sign Up -  numerical validations						
    Given the "vaseline" brand "homepage" of "us/en" locale has been built						
    When i see the structure of the "homepage"									
    Then i should see the sign up link in header						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"		
    And I enter Postal Code as "free"														
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page	
    Then I verify Postal Code Error Message is visible on Forms page						
    