@brand-vaseline @cxx @locale-th	@th @2020																																																																																																																						
Feature: vaseline - th
	
		@rr @p2
		Scenario: vaseline - VerifyReviewform - SubmitReview 
			Given the "vaseline" brand "productpage" of "th" locale has been built 
			When i click on Write a Review link 
			Then i should be able to see write a review dialog 
			And i should be able to see product image in write a review dialog
			And i select overall rating as 4 
			And i give review title as "Lorem ipsom" 
			And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
			And i enter random value in nickname 
			And i enter random email id for notification purposes 
#			And i check age checkbox 
			And i agree to terms and conditions 
			And i submit the review 
			#Then i should see that review is submitted with message "THANK YOU. YOUR REVIEW HAS BEEN SUBMITTED." 
			
		@bin @p2
		Scenario: vaseline - VerifyBuyInNow - BIN_Flow 
			Given the "vaseline" brand "productpage" of "th" locale has been built 
			When i see the structure of the "productpage" 
			Then i should be able to see Buy It Now functionality 
			When i click on Buy It Now Button 
			Then i should be able to see online store list 
		
@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "vaseline" brand "homepage" of "th" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "component-content" exist on the "faqpage"	
    
    @cta @p1 @p3
Scenario: VASELINE - articledetailpage - Related Article 
		Given the "vaseline" brand "articledetailpage" of "th" locale has been built 
	  When i see the structure of the "articledetailpage"
	  And I scroll published page down by 3000
	  And I expect to see "Vaseline_Heading" as visible
	  And I expect to see "Vaseline_Image" as visible
	  And I expect to see "Vaseline_CTA" as visible
	  And I click on a "Vaseline_CTA"
	  And I expect to see "Vaseline_Heading" as visible
    #And I verify that count of component "article" on article page is 1
    #Then i verify that listing article component image is rendering
    #Then i verify that listing article component heading is appearing
    #Then i verify that listing article component description is appearing
    #And i click on the listing article image
    #Then i verify that listing article component image is rendering
    #Then i verify that listing article component heading is appearing
    #Then i verify that listing article component description is appearing 
    
    @cross @p1
  Scenario: Home page - related products v2 component
    Given the "vaseline" brand "productpage" of "th" locale has been built
    When i see the structure of the "productpage"
    #Then i verify that following components "product" exist on the "productpage1" 
    And i verify that listing item component image is rendering
    And i verify that listing item  component heading is appearing
    When I click on the listing item image
	#Then i verify that following components "listing-items" exist on the "productpage" 
    And i verify that listing item component image is rendering
    And i verify that listing item  component heading is appearing 	
    
     @cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "vaseline" brand "homepage" of "th" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "https://www.unilever.co.th/cookie-policy.html"	
    
    @signup @p2     
Scenario: SignUp verification Sign Up -  numerical validations						
    Given the "vaseline" brand "homepage" of "th" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page							
    Then I verify Age Consent Error Message is visible on Forms page