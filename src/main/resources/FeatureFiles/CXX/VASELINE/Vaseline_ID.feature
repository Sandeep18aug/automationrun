@brand-vaseline @cxx @locale-id	@2020																																																																																																																													
Feature: vaseline - id
	
		@rr @p1
		Scenario: vaseline - VerifyReviewform - SubmitReview 
			Given the "vaseline" brand "productpage" of "id" locale has been built 
			When i click on Write a Review link 
			Then i should be able to see write a review dialog 
			And i should be able to see product image in write a review dialog
			And i select overall rating as 4 
			And i give review title as "Lorem ipsom" 
			And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
			And i enter random value in nickname 
			And i enter random email id for notification purposes 
#			And i check age checkbox 
			And i agree to terms and conditions 
			And i submit the review 
			#Then i should see that review is submitted with message "THANK YOU. YOUR REVIEW HAS BEEN SUBMITTED." 
			
		@bin  @p1
		Scenario: vaseline - VerifyBuyInNow - BIN_Flow 
			Given the "vaseline" brand "productpage" of "id" locale has been built 
			When i see the structure of the "productpage" 
			And I expect to see "BIN_Button_ID" as visible
			And I click on a "BIN_Button_ID"
			And I expect to see "BIN_Widget_ID" as visible
		
@faq @p2
  Scenario: Global Footer - FAQs Validation
  
    Given the "vaseline" brand "homepage" of "id" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "component-content" exist on the "faqpage"		
    
    @cross @p1
  Scenario: Home page - related products v2 component
    Given the "vaseline" brand "productpage" of "id" locale has been built
    When i see the structure of the "productpage"
    Then i verify that following components "component-content" exist on the "productpage" 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    When I click on the related-product component
	And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    
    @cta @p1 @p3
    Scenario: VASELINE - articledetailpage - Related Article 
	Given the "vaseline" brand "articledetailpage" of "id" locale has been built 
	When i see the structure of the "articledetailpage"
    And I verify that count of component "article" on article page is 1
    Then i verify that listing article component image is rendering
    Then i verify that listing article component heading is appearing
    Then i verify that listing article component description is appearing
    And i click on the listing article image
    Then i verify that listing article component image is rendering
    Then i verify that listing article component heading is appearing
    Then i verify that listing article component description is appearing 
    
    
    @cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "vaseline" brand "homepage" of "id" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 1
    Then i expect page url contains "https://www.unilevernotices.com/indonesia/bahasa/cookie-notice/notice.html"   
    
    @signup @p2     
Scenario: SignUp verification -  numerical validations						
    Given the "vaseline" brand "homepage" of "id" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page							
    Then I verify Age Consent Error Message is visible on Forms page  
    