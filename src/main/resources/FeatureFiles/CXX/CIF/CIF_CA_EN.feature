@brand-cif @cxx @locale-ca @2020																																																																																																																																												
Feature: cif - ca
	
	@rr @p1
	Scenario: cif ca - VerifyReviewform - SubmitReview 
	Given the "cif" brand "productpage" of "ca" locale has been built
	And i close cookie policy for canada locale
    When i click on Write a Review link
    Then i should be able to see write a review dialog
    And i should be able to see product image in write a review dialog
    And i select overall rating as 4
    And i give review title as "Lorem ipsom"
    And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing"
    And i recommend this product to friend
    And i enter random value in nickname
    And i enter random email id for notification purposes
    And i select "Yes" Are you over the age of majority
    And i enter zipcode as "99501"
    And i select "Male" as gender
    And i select "Spray" as usaully purchased vim product
    And i select "Tile" as surface typically used
    And i select "Yes" for first time buying the product
    And i select "Walmart" store for purchasing vim product
    And i select "6-10 years" as how long you have been purchasing
    And i select "Yes" as incentive for reviews
    And i select "Yes" as sign up for exciting offers
    And i select 9 option for recommend brand to a friend
    And i agree to terms and conditions
    And i submit the review
    Then i should see that review is submitted with message "Your review was submitted!"
			
		@bin @p1  
		Scenario: cif ca - VerifyBuyInNow - BIN_Flow 
		Given the "cif" brand "productpage" of "ca" locale has been built 
		When i see the structure of the "productpage" 
		And I wait for 5 seconds
		Then i should be able to see Buy It Now functionality 
		When i click on Buy It Now Button 
		#Then i should be able to see online store list 
		 
@faq @p2
  Scenario: Global Footer - FAQs Validation
  
    Given the "cif" brand "homepage" of "ca" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded
    
@cross @p1
  Scenario: Home page - related products v2 component
    Given the "cif" brand "productpage" of "ca" locale has been built
    When i see the structure of the "productpage"
    #Then i verify that following components "related-products" exist on the "productpage" 
    #And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading for cif is rendering
    When I click on the related-product component
	#And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading for cif is rendering
    
@cta @p2
Scenario: CIF - articledetailpage - Related Article 
	Given the "cif" brand "articledetailpage" of "ca" locale has been built 
	When i see the structure of the "articledetailpage"
    Then i verify that following components "related-articles" exist on the "homepage"
    And I verify that count of component "related-articles" on this page is 1
    And i verify that related article component image is rendering
    And i verify that related article component heading is appearing
    And i click on the article image
    Then i verify that following components "related-articles" exist on the "homepage"
    And i verify that related article component image is rendering	 

 @cp @p2
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "cif" brand "homepage" of "ca" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer    
    
   @signup @p2     
 Scenario: SignUp verification- Sign Up -  numerical validations						
    Given the "cif" brand "homepage" of "ca" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page          