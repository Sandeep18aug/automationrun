@brand-clear @cxx @locale-ph @2020
Feature: clear - PH

@bin @p1
Scenario: clear Ph - VerifyShopNow - BuyNowonProductPage 

	Given the "clear" brand "productpage" of "ph" locale has been built 
	When i see the structure of the "productpage" 
	And I wait for 5 seconds
	Then i should be able to see Buy It Now functionality 
	When i click on Buy It Now Button 
	Then i should be able to see online store list 		
		
@rr @p1
Scenario: clear Ph - VerifyProductReview - ProductDetail 
		
		Given  the "clear" brand "productpage" of "ph" locale has been built 
		When  i click on Write a Review link 
		Then  i should be able to see write a review dialog 
		And i should be able to see product image in write a review dialog
		And  i select overall rating as 4 
		And  i give review title as "Lorem ipsom" 
		And  i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
		And  i enter random value in nickname 
		And  i enter random email id for notification purposes 
		And  i agree to terms and conditions 
		And  i submit the review 
		
@faq @p2
  Scenario: Global Footer - FAQs Validation
  
    Given the "clear" brand "homepage" of "ph" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    #Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    #And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 1
    #And i expect that accordion panel with index 2 is expanded		
		

 @cross @p1
  Scenario: Home page - related products v2 component
    Given the "clear" brand "productpage" of "ph" locale has been built
    When i see the structure of the "productpage"
    Then i verify that following components "related-products" exist on the "productpage"
    And I verify that count of component "related-products" on this page is 2 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    #When I click on the related-product component by indexing
    And I click on a "RelatedProduct_Link"
		And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    
@cta @p1
Scenario: CLEAR - articledetailpage - Related Article 
	Given the "clear" brand "articlepage" of "ph" locale has been built 
	When i see the structure of the "articledetailpage"
    Then i verify that following components "related-articles" exist on the "homepage"
    And I verify that count of component "related-articles" on this page is 1
    And i verify that related article component image is rendering
    And i verify that related article component heading is appearing
    #And i click on the article image of clear
    And I scroll published page down by 700
    And I click on a "RelatedArticle_Link"
    Then i verify that following components "related-articles" exist on the "homepage"
    And i verify that related article component image is rendering    
			
			
  @cp 
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "clear" brand "homepage" of "ph" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "https://www.unilevernotices.com/brazil/portuguese/cookie-notice/notice.html"		
    
     @signup     
Scenario: SignUp verification Scenario - Sign Up -  numerical validations						
    Given the "clear" brand "homepage" of "ph" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page 
    
   @contact 
  Scenario: Contact Us - Blank field validation
    Given the "clear" brand "homepage" of "ph" locale has been built						
    When i see the structure of the "homepage"				
    When i click on the contact us link of global footer
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page  	