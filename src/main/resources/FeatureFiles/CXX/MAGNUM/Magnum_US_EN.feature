@brand-magnum @cxx @locale-us
Feature: magnum - us/en

@rr @p1
Scenario: MAGNUM US/EN - VerifyReviewForm - WriteAreviewsubmit_Flow 
	Given the "magnum" brand "productpage" of "us/en" locale has been built 
	And i click on "magnum.signup.popup.close" if present
	#When I scroll published page down by 230
	When i click on Write a Review link 
	Then i should be able to see write a review dialog 
	And i should be able to see product image in write a review dialog
	When i select overall rating as 4 
	And i give review title as "Lorem ipsom" 
	And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
	And i recommend this product to friend 
	And i enter random value in nickname 
	And i enter random email id for notification purposes 
	And i enter zipcode as "99501" 
	And i select "March" as birth month 
	And i select "1980" as birth year 
	And i select "Male" as gender 
	And i select "Grocery Store" as purchase location 
	And i select "Once a week" as Frequency 
	And i select "2-5 Years" as how long you have been purchasing 
	And i select "Yes" as incentive for reviews 
	And i select "Yes" as sign up for exciting offers 
	And i select 9 option for recommend brand to a friend 
	And i agree to terms and conditions 
	And i submit the review 
	Then i should see that review is submitted with message "Your review was submitted!" 

@bin @p1
Scenario: Magnum US/EN - VerifyShopNow - ShopnowFeature 
	Given the "magnum" brand "productpage" of "us/en" locale has been built 
	And I wait for 5 seconds
	#And i click on "magnum.signup.popup.close" if present
	#And i click on "magnum.delivery.option" if present
	When i see the structure of the "productpage" 
	Then i should be able to see Buy It Now functionality 
	When i click on Buy It Now Button
	#Then i should be able to see online store list
	#Then i should be able to see add to bag dialog
	#When i choose "Peapod" retailer in add to bag dialog 
	#And i choose "2" quantity in add to bag dialog
	#And i enter zip code as "10001" in checkout form
	#And i click on add to bag button in add to bag dialog
	#Then i should be able to see shopping bag updated 
	
@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "magnum" brand "homepage" of "us/en" locale has been built
    And i click on "magnum.signup.popup.close" if present
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    Then i close email signup popup
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed	
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded				

@cta @p1 @p3
Scenario: MAGNUM - articledetailpage - Page listing 
		Given the "magnum" brand "articledetailpage" of "us/en" locale has been built 
		And i click on "magnum.signup.popup.close" if present
	  When i see the structure of the "articledetailpage"
    Then i verify that following components "page-listing-v2" exist on the "homepage"
    And I verify that count of component "page-listing-v2" on this page is 1
    And i verify that Page Listing V2 component image is appearing
    And i verify that Page Listing V2 component heading is appearing
    And i verify that Page Listing V2 component Sub-heading is appearing
    And I wait for 10 seconds
    And I click on a "RealtedArticle_Link"
    #And i click on any of the items on Page Listing V2
    #Then it should take me to the corresponding page of Page Listing V2 Image
    Then i verify that following components "page-listing-v2" exist on the "homepage"
    And i verify that Page Listing V2 component image is appearing

@cross @p2
  Scenario: Home page - related products v2 component
    Given the "magnum" brand "productpage" of "us/en" locale has been built
    And i click on "magnum.signup.popup.close" if present
    When i see the structure of the "productpage" 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    When I click on the related-product component by indexing
		And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing	
   
   @cp @p2
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "magnum" brand "homepage" of "us/en" locale has been built
    When i see the structure of the "homepage"
    And i close email signup popup
    Then i click on the Cookie Policy link of global footer
  

	 @signup @p1    
Scenario: SignUp verification Sign Up -  numerical validations						
    Given the "magnum" brand "homepage" of "us/en" locale has been built						
    When i see the structure of the "homepage"
    And i click on "magnum.signup.popup.close" if present						
    Then i should see the sign up link in header					
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"
    And I enter Postal Code as "free"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page    
    Then I verify Postal Code Error Message is visible on Forms page
	
   @contact 
  Scenario: Contact Us - Blank field validation
    Given the "magnum" brand "homepage" of "us/en" locale has been built						
    When i see the structure of the "homepage"
    And i click on "magnum.signup.popup.close" if present
    When i click on the contact us link of global footer
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page 