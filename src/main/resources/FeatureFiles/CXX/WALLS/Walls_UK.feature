@brand-walls @uk @cxx @locale-uk
Feature: Walls - UK
			
@store 
Scenario: Walls - VerifyStoreLocator - SearchforStore 
	Given the "walls" brand "storelocatorpage" of "uk" locale has been built 
	When i see the structure of the "storelocatorpage" 
	Then i select "cornetto-range" category for store locator 
	#And i select "Anti-perspirant" sub categry for store locator 
	Then i should be able to see products for select category and sub category 
	When i select any of the random product for seleted category and subcategory 
	And i enter "SW1A 1AA" zip code 
	And i select "100 Miles" as find with store range 
	And i click on find in store button 
	Then i should be able to see map for searched stores 
	And i should be able to see stores list			
	
@bin @p1
		Scenario: walls - VerifyBuyInNow - BIN_Flow 
			Given the "walls" brand "productpage" of "uk" locale has been built 
			When i see the structure of the "productpage" 
			And I wait for 5 seconds
			Then i should be able to see Buy It Now functionality 
			When i click on Buy It Now Button 
			Then i should be able to see online store list 
	
	@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "walls" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded	
    
 @rr @p2
		Scenario: walls - VerifyReviewform - SubmitReview 
			Given the "walls" brand "productpage" of "uk" locale has been built 
			When i click on Write a Review link 
			Then i should be able to see write a review dialog 
			And i should be able to see product image in write a review dialog
			And i select overall rating as 4 
			And i give review title as "Lorem ipsom" 
			And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
			And i enter random value in nickname 
			And i enter random email id for notification purposes 
#			And i check age checkbox 
			And i agree to terms and conditions 
			And i submit the review 
			
			@cross @p1
  Scenario: Home page - related products v2 component
    Given the "walls" brand "productpage" of "uk" locale has been built
    When i see the structure of the "productpage"
    Then i verify that following components "related-products" exist on the "productpage" 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    When I click on the related-product component
		And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    
    @cta @p1
Scenario: WALLS - articledetailpage - Related Article 
		Given the "walls" brand "articledetailpage" of "uk" locale has been built 
	  When i see the structure of the "articledetailpage"
    Then i verify that following components "related-articles" exist on the "homepage"
    And I verify that count of component "related-articles" on this page is 1
    And i verify that related article component image is rendering
    And i verify that related article component heading is appearing
    And i click on the article img
    Then i verify that following components "related-articles" exist on the "homepage"
    And i verify that related article component image is rendering
    And i verify that related article component heading is appearing
			
@signup @p2     
Scenario: SignUp verification Sign Up -  numerical validations						
    Given the "walls" brand "homepage" of "uk" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page	
    
     @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "walls" brand "homepage" of "uk" locale has been built						
    When i see the structure of the "homepage"		 			
    When i click on the contact us link of global footer
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page
    Then I verify Age Consent Error Message is visible on Forms page		