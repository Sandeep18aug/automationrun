@cxx @dqm @p1
Feature: DQM

  @broken @internal @prod @testx
  Scenario: dqm - broken internal links
  Given the "dqm" brand "homepage" of "uk" locale has been built
  When i login into dqm site with userid "tgupta25@sapient.com" and password "Password12345" 
	Then i should be able to login successfully in dqm site 
	And i click on "dqm.website.select" if present
	And i enter website name as "axe.com/ar"
	And i click on "dqm.selected.website" if present
	And I wait for 05 seconds
	And i click on "dqm.total.issues" if present
	And i click on "dqm.links" if present
	When I scroll published page down by 700
	And I wait for 10 seconds
	And i click on "broken.internal.links" if present
	Then i click on all the links 
	And I wait for 10 seconds
																																																																																																																																																													
