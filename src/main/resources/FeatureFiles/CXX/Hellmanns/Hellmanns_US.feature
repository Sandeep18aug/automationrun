@brand-hellmanns @cxx @locale-us 
Feature: hellmanns - us/en

   @rr @p1
  Scenario: Hellmanns - VerifyReviewForm - ReviewOnProductPage
	Given the "hellmanns" brand "productpage" of "us/en" locale has been built
	When I scroll published page down by 580
	When i click on Write a Review link 
	Then i should be able to see write a review dialog 
	And i should be able to see product image in write a review dialog
	When i select overall rating as 4 
	And i give review title as "Lorem ipsom" 
	And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
	And i recommend this product to friend
	And i enter random value in nickname 
	And i enter random email id for notification purposes
	And i enter zipcode as "99501"
	And i select "March" as birth month
	And i select "1980" as birth year 
	And i select "Male" as gender
	And i select "Grocery Store" as purchase location
	And i select "Once a week" as Frequency
	And i select "2-5 Years" as how long you have been purchasing
	And i select "Yes" as incentive for reviews
	And i select "Yes" as sign up for exciting offers
	And i select 9 option for recommend brand to a friend 	 	 
	And i agree to terms and conditions 
	And i submit the review 
	Then i should see that review is submitted with message "Your review was submitted!" 

  @bin  @p1
  Scenario: Hellmanns - VerifyAddToBag - AddToBagFunctiononProductPage
	Given the "hellmanns" brand "productpage" of "us/en" locale has been built	
	When I scroll published page down by 650
	When i see the structure of the "productpage"
	And I wait for 5 seconds
	Then i should be able to see Buy It Now functionality 
	When i click on Buy It Now Button 
	Then i should be able to see target store list	
	#Then i verify that following components "buy-it-now" exist on the "homepage"
	#When i click on add to bag button
	#Then i should be able to see add to bag dialog
	#When i choose "Target" retailer in add to bag dialog
	#And i choose "20 oz" size in add to bag dialog 
	#And i choose "2" quantity in add to bag dialog
	#And i click on add to bag button in add to bag dialog
	#Then i should be able to see shopping bag updated 

 @store  
  Scenario: Hellmanns - VerifyStoreLocator - StoreLocationOnsite
	  Given the "hellmanns" brand "storelocatorpage" of "us/en" locale has been built	
	  When i see the structure of the "storelocatorpage"
	 #Then i enter "10001" zip code
    Then i select "Mayonnaise" category for store locator
    #And i select "Shampoo" sub categry for store locator
    Then i should be able to see products for select category and sub category
    When i select any of the random product button for seleted category and subcategory 
   	And i enter "10001" zip code 
    And i select "50 Miles" as find with store range
    And i click on find in store button
    Then i should be able to see map for searched stores
    And i should be able to see stores list

@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "hellmanns" brand "homepage" of "us/en" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded

@cross @p1
  Scenario: Home page - related products v2 component
    Given the "hellmanns" brand "productpage" of "us/en" locale has been built
    When i see the structure of the "productpage"
    Then i verify that following components "related-products" exist on the "productpage" 
    #And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    When I scroll published page down by 400
    #When I click on the related-product component
    And I click on a "RelatedProductLink"
	  And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing    
    
@cta @p1 @p3
Scenario: HELLMANNS - articledetailpage - Related Article 
		Given the "hellmanns" brand "articledetailpage" of "us/en" locale has been built 
	  When i see the structure of the "articledetailpage"
    Then i verify that following components "tags" exist on the "homepage"
    And I expect to see "Tag_Heading" as visible
    And I expect to see "Tag_List" as visible
    And I click on a "Tag_List"
    And I expect to see "Article_Heading" as visible
    And I expect to see "Article_Image" as visible
    And I wait for 10 seconds
    And I click on a "Article_Image"
    And I expect to see "Tag_Heading" as visible
    And I expect to see "Tag_List" as visible
    
    
@cp @p2
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "hellmanns" brand "homepage" of "us/en" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "www.unilevercookiepolicy.com/en_gb/policy.aspx"    
    
 @signup @p1     
Scenario: SignUp verification  Sign Up -  numerical validations						
    Given the "hellmanns" brand "homepage" of "us/en" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in header						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"		
    And I enter Postal Code as "free"    														
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page	
    Then I verify Postal Code Error Message is visible on Forms page
    Then I verify DOB Error Message is visible on Forms page
      
    @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "hellmanns" brand "homepage" of "us/en" locale has been built						
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When i click on contact us form link on helpcenter
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page       