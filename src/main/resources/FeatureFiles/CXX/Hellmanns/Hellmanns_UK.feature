@brand-hellmanns @cxx @locale-uk @hellmanns
Feature: hellmanns - uk

   @rr @p1
  Scenario: Hellmanns - VerifyReviewForm - ReviewOnProductPage
		Given  the "hellmanns" brand "productpage" of "uk" locale has been built 
		#And i click on "hellmanns.signup.popup" if present
		When I scroll published page down by 450
		When I click on a "WriteReview_Lnk"
		Then  i should be able to see write a review dialog 
		And i should be able to see product image in write a review dialog
		And  i select overall rating as 4 
		And  i give review title as "Lorem ipsom" 
		And  i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
		And  i enter random value in nickname 
		And  i enter random email id for notification purposes 
		And i check age checkbox
		And  i agree to terms and conditions 
		

  @bin  @p1
  Scenario: Hellmanns - VerifyAddToBag - AddToBagFunctiononProductPage
	Given the "hellmanns" brand "productpage" of "uk" locale has been built	
	#And i click on "hellmanns.signup.popup" if present
	And I wait for 10 seconds
	When i see the structure of the "productpage" 
	When I scroll published page down by 900
	And I switch to frame by object Iframe_Hellmann_UK
	And I click on a "pdp.butitnow.button.uk"
	And I switch to default
	And I switch to frame by object Iframe_BIN_Widget
	And I expect to see "BIN_Widget" as visible
  #Then i should be able to see Buy It Now functionality 
  #When i click on Buy It Now Button 
  #Then i should be able to see online store list

 @store  
  Scenario: Hellmanns - VerifyStoreLocator - StoreLocationOnsite
	Given the "hellmanns" brand "storelocatorpage" of "uk" locale has been built	
	#And i click on "hellmanns.signup.popup" if present
	When i see the structure of the "storelocatorpage"
    Then i should be able to see products for select category and sub category
    When i select any of the random product button for seleted category and subcategory 
   	And i enter "SW1A 1AA" zip code 
    And i select "100 Miles" as find with store range
    And i click on find in store button
    Then i should be able to see map for searched stores
    And i should be able to see stores list

@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "hellmanns" brand "homepage" of "uk" locale has been built
    And i click on "hellmanns.signup.popup" if present
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded
    
@cross @p1 @p3
  Scenario: Home page - related products v2 component
    Given the "hellmanns" brand "productpage" of "uk" locale has been built
    And i click on "hellmanns.signup.popup" if present
    When i see the structure of the "productpage"
    When I scroll published page down by 400
    Then i verify that following components "related-products" exist on the "productpage" 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    #When I click on the related-product component
    And I click on a "RelatedProductLink"
		And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing    
    
 @cta @p1 @p3
Scenario: HELLMANNS - articledetailpage - Related Article 
		Given the "hellmanns" brand "articledetailpage" of "uk" locale has been built 
		And i click on "hellmanns.signup.popup" if present
	  When i see the structure of the "articledetailpage"
	  Then i verify that following components "tags" exist on the "homepage"
    And I expect to see "Tag_Heading" as visible
    And I expect to see "Tag_List" as visible
    And I click on a "Tag_List"
    And I expect to see "Article_Heading" as visible
    And I expect to see "Article_Image" as visible
    And I wait for 10 seconds
    And I click on a "Article_SubHeading"
    Then i verify that following components "tags" exist on the "homepage"
    
    
@cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "hellmanns" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "www.unilevercookiepolicy.com/en_gb/policy.aspx" 
    
    @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "hellmanns" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    When i click on the contact us link of header
    When i click on contact us form link on helpcenter
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page 
    Then I verify Age Consent Error Message is visible on Forms page    
    