@brand-tresemme @cxx @locale-us @us
Feature: tresemme - us/en 

@rr @p1
Scenario: Tresemme - VerifyReviewsubmit - ReviewOnProductPage 
	Given the "tresemme" brand "productpage" of "us/en" locale has been built
	And i click on "tresemme.campaign.popup" if present
	When i click on Write a Review link 
	Then i should be able to see write a review dialog 
	And i should be able to see product image in write a review dialog
	When i select overall rating as 4 
	And i give review title as "Lorem ipsom" 
	And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
	And i recommend this product to friend
	And i enter random value in nickname 
	And i enter random email id for notification purposes
	And i enter zipcode as "99501"
	And i select "March" as birth month
	And i select "1980" as birth year 
	And i select "Male" as gender
	And i select "Grocery Store" as purchase location
	And i select "Once a week" as Frequency
	And i select "2-5 Years" as how long you have been purchasing
	And i select "Yes" as incentive for reviews
	And i select "Yes" as sign up for exciting offers
	And i select 9 option for recommend brand to a friend 	 	 
	And i agree to terms and conditions 
	And i submit the review 
	

@bin @p1
Scenario: Tresemme - VerifyAddToBag - AddToBagFunctiononProductPage
	Given the "tresemme" brand "productpage" of "us/en" locale has been built	
	And i click on "tresemme.campaign.popup" if present
	And I wait for 10 seconds
	When i see the structure of the "productpage"
	Then i verify that following components "buy-it-now" exist on the "homepage"
	When i click on add to bag button
#	Then i should be able to see add to bag dialog
#	When i choose "Target" retailer in add to bag dialog
#	And i choose "22 fl oz" size in add to bag dialog 
#	And i choose "2" quantity in add to bag dialog
#	And i click on add to bag button in add to bag dialog
#	Then i should be able to see shopping bag updated


@Store  
Scenario: TRESEM - VerifyStoreLocator - StoreLocationOnsite 
		Given the "tresemme" brand "storelocatorpage" of "us/en" locale has been built	
		And i click on "tresemme.campaign.popup" if present
		When i see the structure of the "storelocatorpage"
    Then i select "Shampoo" category for store locator
    Then i should be able to see products for select category and sub category
    When i select any of the random product for seleted category and subcategory
    And i enter "10001" zip code
    And i select "50 Miles" as find with store range
    And i click on find in store button
    Then i should be able to see map for searched stores
    And i should be able to see stores list 

@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "tresemme" brand "homepage" of "us/en" locale has been built
    And i click on "tresemme.campaign.popup" if present
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded
    
     @cross @p1
  Scenario: Home page - related products v2 component
    Given the "tresemme" brand "productpage" of "us/en" locale has been built
    And i click on "tresemme.campaign.popup" if present
    When i see the structure of the "productpage"
    And i close email signup popup
    Then i verify that following components "related-products" exist on the "productpage" 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
   # When I click on the related-product component
    And I click on the related-product component of tresemme
		And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    
    @cta @p1
Scenario: TRESEMME - articledetailpage - Related Article 
		Given the "tresemme" brand "articledetailpage" of "us/en" locale has been built 
		And i click on "tresemme.campaign.popup" if present
	  When i see the structure of the "articledetailpage"
    Then i verify that following components "related-articles" exist on the "homepage"
    And I verify that count of component "related-articles" on this page is 1
    And I scroll published page down by 1800
    And I wait for 10 seconds
    And i verify that related article component image is rendering
    And i verify that related article component heading is appearing
    #And i click on the article image
    And I click on a "RelatedArticle_ImageLink"
    Then i verify that following components "rich-text-v2" exist on the "homepage"
    And i verify rich text heading is rendering on page
    
     @cp @p2
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "tresemme" brand "homepage" of "us/en" locale has been built
    And i click on "tresemme.campaign.popup" if present
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "https://www.unilevernotices.com/united-kingdom/english/cookie-notice/notice.html"

     @signup @p1     
Scenario: SignUp verification Scenario - Sign Up -  numerical validations						
    Given the "tresemme" brand "homepage" of "us/en" locale has been built	
    And i click on "tresemme.campaign.popup" if present					
    When i see the structure of the "homepage"						
    Then i should see the sign up link in header						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page	
    
    @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "tresemme" brand "homepage" of "us/en" locale has been built	
    And i click on "tresemme.campaign.popup" if present					
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When i click on contact us form link on helpcenter
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page