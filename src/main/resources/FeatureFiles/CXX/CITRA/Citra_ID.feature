@citra @id @cxx @brand-citra @locale-id
Feature: Citra - id

  @rr @p2
  Scenario: Citra id - VerifyReviewform - SubmitReview
    Given the "citra" brand "productpage" of "id" locale has been built
    When i click on Write a Review link
    Then i should be able to see write a review dialog
    And i should be able to see product image in write a review dialog
    And i select overall rating as 4
    And i give review title as "Lorem ipsom"
    And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing"
    And i enter random value in nickname
    And i enter random email id for notification purposes
    #And i check age checkbox
    And i agree to terms and conditions
    And i submit the review

  #Then i should see that review is submitted with message "THANK YOU. YOUR REVIEW HAS BEEN SUBMITTED."
  @bin @p1
  Scenario: Citra id - VerifyBuyInNow - BIN_Flow
    Given the "citra" brand "productpage" of "id" locale has been built
    When i see the structure of the "productpage"
    Then i should be able to see Buy It Now functionality
    When i click on Buy It Now Button
    Then i should be able to see online store list

  @faq @p1
  Scenario: Global Footer - FAQs Validation
    Given the "citra" brand "homepage" of "id" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    And i close email signup popup
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "component-content" exist on the "faqpage"
     When I scroll published page down by 600
    And I click on a "Accordion_Last"
    And I expect to see "AccordionPannel_Last" as visible
    #And i expect that accordion panel with index 2 is collapsed
    #When i expand accordion panel 2
    #And i expect that accordion panel with index 2 is expanded
    
@cross @p1
  Scenario: Home page - related products v2 component
    Given the "citra" brand "productpage" of "id" locale has been built
    When i see the structure of the "productpage"
    Then I verify that citra related product component image is rendering
    #Then i verify that citra related product component CTA is appearing
    And I click on the citra product image   
    Then I verify that citra related product component image is rendering
   # Then I verify that citra related product component CTA is appearing  
    
@cta @p1 @p3
Scenario: CITRA - articledetailpage - Related Article 
	  Given the "citra" brand "articledetailpage" of "id" locale has been built 
	  When i see the structure of the "articledetailpage"
	  And I scroll published page down by 1900
    Then i verify that following components "component-content " exist on the "homepage"
    And I expect to see "Citra_RelatedArticleImage" as visible
    And I expect to see "Citra_RelatedArticleHeading" as visible
    And I click on a "Citra_RelatedArticleImage"
    And I expect to see "RelatedArticleHeading" as visible  


     @signup @p2     
Scenario: SignUp verification - Sign Up -  numerical validations						
    Given the "citra" brand "homepage" of "id" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						 
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page 

 @cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "citra" brand "homepage" of "id" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "https://www.unilevercookiepolicy.com/en_gb/policy.aspx"    

