@brand-simple @cxx @locale-in @in																																																																																																																																														
Feature: simple - in
	
		@rr @p2
		Scenario: simple - VerifyReviewform - SubmitReview 
			Given the "simple" brand "productpage" of "in" locale has been built 
			When i click on Write a Review link 
			Then i should be able to see write a review dialog 
			And i should be able to see product image in write a review dialog
			And i select overall rating as 4 
			And i give review title as "Lorem ipsom" 
			And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
			And i enter random value in nickname 
			And i enter random email id for notification purposes 
#			And i check age checkbox 
			And i agree to terms and conditions 
			And i submit the review 
			#Then i should see that review is submitted with message "THANK YOU. YOUR REVIEW HAS BEEN SUBMITTED." 
			
		@bin @p2
		Scenario: simple - VerifyBuyInNow - BIN_Flow 
			Given the "simple" brand "productpage" of "in" locale has been built 
			When i see the structure of the "productpage" 
			Then i should be able to see Buy It Now functionality 
			When i click on Buy It Now Button 
			Then i should be able to see online store list 
		
	@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "simple" brand "homepage" of "in" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And I expect "class" attribute of "accordian.panel" object is having string "is-active"
    When I click on "accordian.panel.link" by actions class
    Then I expect "class" attribute of "accordian.panel" object is not having string "is-active"
    
    @cta @p1
Scenario: SIMPLE - articledetailpage - Related Article 
	Given the "simple" brand "articledetailpage" of "in" locale has been built 
	When i see the structure of the "articledetailpage"
    And I verify that count of component "article" on article page is 1
    And i verify that listing article component image is rendering
    And i verify that listing article component heading is appearing
    And i click on the listing article image
    And I verify that count of component "article" on article page is 1
    Then i verify that listing article component image is rendering
    
     @cross @p1
  Scenario: Home page - related products v2 component
    Given the "simple" brand "productpage" of "in" locale has been built
   	When i see the structure of the "productpage"
    #Then i verify that following components "product" exist on the "productpage" 
    And i verify that box-listing are present on the productpages   
    And i verify that box-listing component image is rendering
    And i verify that box-listing component heading is appearing
    When I click on the listing item image
	And i verify that box-listing are present on the productpages   
    And i verify that box-listing component image is rendering
    And i verify that box-listing component heading is appearing
    
     @signup @p2     
Scenario: SignUp verification - Sign Up -  numerical validations						
    Given the "simple" brand "homepage" of "in" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page

@cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "simple" brand "homepage" of "in" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 1
    Then i expect page url contains "https://www.unilevernotices.com/india/english/cookie-notice/notice.html" 
        
 @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "simple" brand "homepage" of "in" locale has been built
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When i click on contact us form link on helpcenter
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page
