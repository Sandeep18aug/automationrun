@brand-simple @cxx @locale-tr @2020																																																																																																																																														
Feature: simple - tr
	
		@rr @p1
		Scenario: simple - VerifyReviewform - SubmitReview 
			Given the "simple" brand "productpage" of "tr" locale has been built 
			And I wait for 5 seconds
			And I click on a "WriteReview_LNK"
			#When i click on Write a Review link 
			Then i should be able to see write a review dialog 
			And i should be able to see product image in write a review dialog
			And i select overall rating as 4 
			And i give review title as "Lorem ipsom" 
			And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
			And i enter random value in nickname 
			And i enter random email id for notification purposes 
			And I click on a "AgeConsent_CHK"
			And i agree to terms and conditions 
			And i submit the review 
			And I expect to see "ThankYou_MSG_popup" as visible
			#Then i should see that review is submitted with message "Yorumun onay için gönderildi." 
			
		@bin @p1
		Scenario: simple - VerifyBuyInNow - BIN_Flow 
			Given the "simple" brand "productpage" of "tr" locale has been built 
			When i see the structure of the "productpage" 
			Then i should be able to see Buy It Now functionality 
			When i click on Buy It Now Button 
			Then i should be able to see online store list 
		
	@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "simple" brand "homepage" of "tr" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    And i verify that following components "component-content" exist on the "faqpage"
    And i expect that accordion panel with index 2 is expanded
    	
    
    @cta @p1 @p4
Scenario: SIMPLE - articledetailpage - Related Article 
	  Given the "simple" brand "articledetailpage" of "tr" locale has been built 
	  When i see the structure of the "articledetailpage"
    And I verify that count of component "article" on article page is 1
    And i verify that listing article component image is rendering
    And i verify that listing article component heading is appearing
    And i click on the listing article image
    And I verify that count of component "article" on article page is 1
    Then i verify that listing article component image is rendering  
    
      @cross @p1 @p3
  Scenario: Home page - box-listing  component
    Given the "simple" brand "productpage" of "tr" locale has been built
    When i see the structure of the "productpage"
    And i verify that box-listing are present on the productpages   
    And i verify that box-listing component image is rendering
    And i verify that box-listing component heading is appearing
    And I scroll published page down by 1000
    And I click on a "RelatedProduct_Link_TR"
    #When I click on the related-product component by indexing
	  And i verify that box-listing are present on the productpages   
    And i verify that box-listing component image is rendering
    And i verify that box-listing component heading is appearing	
    
     @signup @p2     
Scenario: SignUp verification  Sign Up -  numerical validations						
    Given the "simple" brand "homepage" of "tr" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page

@cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "simple" brand "homepage" of "tr" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 1
    Then i expect page url contains "https://www.unilever.com.tr/kullanim-kosullari.html"    
#@contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "simple" brand "homepage" of "tr" locale has been built
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When i click on contact us form link on helpcenter
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page
    
    