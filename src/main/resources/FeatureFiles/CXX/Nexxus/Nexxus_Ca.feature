@brand-nexxus @cxx @locale-ca @2020
Feature: nexxus - ca_en
	
@rr @p1
Scenario: nexxus ca - VerifyReviewForm - ReviewOnProductPage 
	Given the "nexxus" brand "productpage" of "ca" locale has been built
    When i click on Write a Review link
    Then i should be able to see write a review dialog
    And i should be able to see product image in write a review dialog
    And i select overall rating as 4
    And i give review title as "Lorem ipsom"
    And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing"
    And i recommend this product to friend
    And i enter random value in nickname
    And i enter random email id for notification purposes
    And i enter zipcode as "99501"
    And I wait for 10 seconds
    And i select "Yes" Are you over the age of majority
    And i select "25-34" as Age Range
    And i select "Male" as gender
    #And i select "3 - 5 years" as how long you have been purchasing
    And i select "Yes" as incentive for reviews
    #And i select "Yes" as sign up for exciting offers
    And i select 9 option for recommend brand to a friend
    And i agree to terms and conditions
    And i submit the review
    Then i should see that review is submitted with message "Your review was submitted!" 
			
	@bin @p1
	Scenario: nexxus ca - VerifyBuyInNow - BIN_Flow 
			Given the "nexxus" brand "productpage" of "ca" locale has been built 
			When i see the structure of the "productpage" 
			And I wait for 10 seconds	
			Then i should be able to see Buy It Now functionality 
			And I click on a "BIN_Button"
			And I switch to frame by object BIN_Frame
			And I expect to see "BIN_widget" as visible
#			When i click on Buy It Now Button 
#			Then i should be able to see online store list 
			
@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "nexxus" brand "homepage" of "ca" locale has been built
    When i see the structure of the "homepage"
    And I wait for 10 seconds
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    #And I click on a "Accordion_01"
    #And I expect to see "AccordionPannel_01" as visible
   And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded
    
@cta @p1
Scenario: nexxus - articledetailpage - Related Article 
	Given the "nexxus" brand "articledetailpage" of "ca" locale has been built 
	When i see the structure of the "articledetailpage"
    Then i verify that following components "related-articles" exist on the "homepage"
    And I scroll published page down by 2000
    And I verify that count of component "related-articles" on this page is 1
    And i verify that related article component image is rendering
    And i verify that related article component heading is appearing
    And i click on the article image
    Then i verify that following components "related-articles" exist on the "homepage"
    And i verify that related article component image is rendering

@cross @p1
  Scenario: Home page - related products v2 component
    Given the "nexxus" brand "productpage" of "ca" locale has been built
    When i see the structure of the "productpage"
    Then i verify that following components "related-products" exist on the "productpage" 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    #When I click on the related-product component
    And I click on a "RelatedProducts_Link"
	  And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing


@signup @p1     
Scenario: SignUp verification  Sign Up -  numerical validations						
    Given the "nexxus" brand "homepage" of "ca" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page     			

@cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "nexxus" brand "homepage" of "ca" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "https://www.unilevernotices.com/united-kingdom/english/cookie-notice/notice.html"    			
   
   @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "nexxus" brand "homepage" of "ca" locale has been built						
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page
    Then I verify Age Consent Error Message is visible on Forms page
