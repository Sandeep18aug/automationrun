@brand-nexxus @cxx @locale-us @2020 
Feature: nexxus - us

@rr @p1
Scenario: nexxus US/EN - VerifyReviewForm - WriteAreviewsubmit_Flow 
	Given the "nexxus" brand "productpage" of "us" locale has been built 
	And i close email signup popup
	When i click on Write a Review link 
	Then i should be able to see write a review dialog 
	And i should be able to see product image in write a review dialog
	When i select overall rating as 4 
	And i give review title as "Lorem ipsom" 
	And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
	And i recommend this product to friend 
	And i enter random value in nickname 
	And i enter random email id for notification purposes 
	And i enter zipcode as "99501" 
	And i select "March" as birth month 
	And i select "1980" as birth year 
	And i select "Male" as gender 
	And i select "Grocery Store" as purchase location 
	And i select "Once a week" as Frequency 
	And i select "2-5 Years" as how long you have been purchasing 
	And i select "Yes" as incentive for reviews 
	And i select "Yes" as sign up for exciting offers 
	#And i select 9 option for recommend brand to a friend 
	And i agree to terms and conditions 
	And i submit the review 
			
	@bin @p1
	Scenario: nexxus us - VerifyBuyInNow - BIN_Flow 
			Given the "nexxus" brand "productpage" of "us" locale has been built 
			When i see the structure of the "productpage" 
			And I wait for 15 seconds
			When I click on a "nexxus.addtobag.button"
			#Then i should be able to see add to bag dialog
    	Then i should be able to see target store list
			
@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "nexxus" brand "homepage" of "us" locale has been built
    When i see the structure of the "homepage"
    And I wait for 15 seconds
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And I click on a "Accordion_01"
    And I expect to see "AccordionPannel_01" as visible
    #And i verify that following components "accordion" exist on the "faqpage"
    #And i expect that accordion panel with index 2 is collapsed
    #When i expand accordion panel 2
    #And i expect that accordion panel with index 2 is expanded   
    
@cta @p1 @p3
Scenario: nexxus - articledetailpage - Related Article 
	  Given the "nexxus" brand "articledetailpage" of "us" locale has been built 
	  When i see the structure of the "articledetailpage"
    Then i verify that following components "c-editorial-card__media" exist on the "homepage"
    And I verify that count of component "c-editorial-card__media" on this page is 4
    And i verify that Page Listing V2 component image is appearing
    And i verify that Page Listing V2 component heading is appearing
    And i verify that Page Listing V2 component Sub-heading is appearing
    And i click on any of the items on Page Listing V2
    Then it should take me to the corresponding page of Page Listing V2 Image
    Then i verify that following components "page-listing-v2" exist on the "homepage"
    And i verify that Page Listing V2 component image is appearing


@cross @p1 @p3
  Scenario: Home page - related products v2 component
    Given the "nexxus" brand "productpage" of "us" locale has been built
    When i see the structure of the "productpage"
    And I scroll published page down by 1800
    And I expect to see "RelatedProduct_Heading_US" as visible
    And I expect to see "RelatedProduct_Image_US" as visible
    And I click on a "RelatedProduct_Image_US"
    And I scroll published page down by 1500
    And I expect to see "RelatedProduct_Heading_US" as visible
    And I expect to see "RelatedProduct_Image_US" as visible
    
    #Then i verify that following components "related-products" exist on the "productpage" 
    #And i verify that related products are present on the productpage   
    #And i verify that related product component image is rendering
    #And i verify that related product component heading is appearing
   #When I click on the related-product component
    #When I click on the related-product component by indexing
#	  And i verify that related products are present on the productpage   
    #And i verify that related product component image is rendering
    #And i verify that related product component heading is appearing 

@signup @p1     
Scenario: SignUp verification- Sign Up -  numerical validations						
    Given the "nexxus" brand "homepage" of "us" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page     			

 
 @cp @p2
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "nexxus" brand "homepage" of "us" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer   			

   @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "nexxus" brand "homepage" of "us" locale has been built						
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When i click on contact us form link on helpcenter
    And i close email signup popup
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page
