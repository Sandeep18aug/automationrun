@brand-lux @cxx @locale-bd @2020
Feature: Lux - bd

  @rr @p2
  Scenario: Lux bd - VerifyReviewForm - ReviewOnProductPage
  Given the "lux" brand "productpage" of "bd" locale has been built 
	When i click on Write a Review link 
	Then i should be able to see write a review dialog
	And i should be able to see product image in write a review dialog 
	And i select overall rating as 4 
	And i give review title as "Lorem ipsom" 
	And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
	And i enter random value in nickname 
	And i enter random email id for notification purposes 
	#And i check age checkbox 
	And i agree to terms and conditions 
	And i submit the review  


@bin @p2
  Scenario: Lux bd - VerifyAddToBag - AddToBagProductPage
 Given the "lux" brand "productpage" of "bd" locale has been built 
	When i see the structure of the "productpage" 
	Then i should be able to see Buy It Now functionality 
	When i click on Buy It Now Button 
	Then i should be able to see online store list 

@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "lux" brand "homepage" of "bd" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "rich-text-v2" exist on the "faqpage"
    
@cta @p2
Scenario: LUX - articledetailpage - Related Article 
		Given the "lux" brand "articledetailpage" of "bd" locale has been built 
	  When i see the structure of the "articledetailpage"
    Then i verify that following components "media-gallery-v2" exist on the "homepage"
    And I verify that count of component "media-gallery-v2" on this page is 1
    And i verify that media gallery component image is rendering
    And i click on the media gallery image
    #Then it should take me to the corresponding page of media gallery image
    Then i expect page url contains "https://www.instagram.com"     
   
@cross @p1
  Scenario: Home page - related products v2 component
    Given the "lux" brand "productpage" of "bd" locale has been built
    When i see the structure of the "productpage"
    Then i verify that following components "related-products" exist on the "productpage" 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    When I click on the related-product component
		And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing    

@cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "lux" brand "homepage" of "bd" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "https://www.unilevernotices.com/brazil/portuguese/cookie-notice/notice.html"   
    
     @signup @p2     
Scenario: SignUp verification Scenario - Sign Up -  numerical validations						
    Given the "lux" brand "homepage" of "bd" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page
    
  @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "lux" brand "homepage" of "bd" locale has been built						
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
      