@cxx @brand-comfort  @locale-br	@cxx			
Feature: comfort - BR

@bin @p1
Scenario: comfort Br - VerifyShopNow - BuyNowonProductPage 

	Given the "comfort" brand "productpage" of "br" locale has been built 
	When i see the structure of the "productpage" 
	Then i should be able to see Buy It Now functionality 
	When i click on Buy It Now Button 
	Then i should be able to see online store list	
		
@rr @p1
Scenario: comfort Br - VerifyProductReview - ProductDetail 
		
		Given  the "comfort" brand "productpage" of "br" locale has been built 
		When I scroll published page down by 300 
		When  i click on Write a Review link for comments section
		Then  i should be able to see write a review dialog 
		And i should be able to see product image in write a review dialog
		And  i select overall rating as 4 
		And  i give review title as "Lorem ipsom" 
		And  i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
		And  i enter random value in nickname 
		And  i enter random email id for notification purposes 
		And  i agree to terms and conditions 
		And  i submit the review 
		
@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "comfort" brand "homepage" of "br" locale has been built
    When i see the structure of the "homepage"
    And I wait for 10 seconds
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded		
		
@cross @p1
  Scenario: Home page - related products v2 component
    Given the "comfort" brand "productpage" of "br" locale has been built
    When i see the structure of the "productpage"
    Then i verify that following components "related-products" exist on the "productpage" 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    When I click on the related-product component
  	And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    
@cta @p1
Scenario: COMFORT - articledetailpage - Related Article 
	  Given the "comfort" brand "articledetailpage" of "br" locale has been built 
	  When i see the structure of the "articledetailpage"
	  Then i verify that following components "related-articles" exist on the "homepage"
	  And I verify that count of component "related-articles" on this page is 1
	  And i verify that related article component image is rendering
    And i verify that related article component heading is appearing
    And I scroll published page down by 700
    And I click on a "RelatedArticle_Lnk"
    Then i verify that following components "related-articles" exist on the "homepage"
    And i verify that related article component image is rendering   
    #And I verify that count of component "bws-sublayout-s2" on bwsLayout page is 1
    #And i verify that sublayout component image is rendering
    #And i verify that sublayout component heading is appearing
    #And i click on the sublayout image
    #And i verify that sublayout component heading is appearing    
			