@brand-comfort  @locale-tr 			
Feature: comfort - TR

@bin 
Scenario: comfort TR - VerifyShopNow - BuyNowonProductPage 

	Given the "comfort" brand "productpage" of "tr" locale has been built 
	When i see the structure of the "productpage" 
	Then i should be able to see Buy It Now functionality 
	When i click on Buy It Now Button 
	Then i should be able to see online store list 		
		
@rr 
Scenario: comfort TR - VerifyProductReview - ProductDetail 
		
		Given  the "comfort" brand "productpage" of "tr" locale has been built 
		When  i click on Write a Review link 
		Then  i should be able to see write a review dialog 
		And i should be able to see product image in write a review dialog
		And  i select overall rating as 4 
		And  i give review title as "Lorem ipsom" 
		And  i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
		And  i enter random value in nickname 
		And  i enter random email id for notification purposes 
		And  i agree to terms and conditions 
		And  i submit the review 
		
@faq
  Scenario: Global Footer - FAQs Validation
  
    Given the "comfort" brand "homepage" of "tr" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded		
		

			