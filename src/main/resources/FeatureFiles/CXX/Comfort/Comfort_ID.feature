@brand-comfort  @locale-id @cxx		
Feature: comfort - id

@bin @p1
Scenario: comfort id - VerifyShopNow - BuyNowonProductPage 

	Given the "comfort" brand "productpage" of "id" locale has been built 
	When i see the structure of the "productpage" 
	Then i should be able to see Buy It Now functionality 
	When i click on Buy It Now Button 
	Then i should be able to see online store list 		
		
@rr 
Scenario: comfort id - VerifyProductReview - ProductDetail 
		
		Given  the "comfort" brand "productpage" of "id" locale has been built 
		When i click on Write a Review link for comments section
		Then i should be able to see write a review dialog 
		And i should be able to see product image in write a review dialog
		And i select overall rating as 4 
		And i give review title as "Lorem ipsom" 
		And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
		And i enter random value in nickname 
		And i enter random email id for notification purposes 
		And i agree to terms and conditions 
		And i submit the review 
		
@faq
  Scenario: Global Footer - FAQs Validation
  
    Given the "comfort" brand "homepage" of "id" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded		
		
@cross @p1
  Scenario: Home page - related products v2 component
    Given the "comfort" brand "productpage" of "id" locale has been built
    When i see the structure of the "productpage"
    Then i verify that following components "related-products" exist on the "productpage" 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    When I click on the related-product component
		And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    
    @cta
Scenario: COMFORT - articledetailpage - Related Article 
		Given the "comfort" brand "articledetailpage" of "id" locale has been built 
	  When i see the structure of the "articledetailpage"
    And I verify that count of component "bws-sublayout-s2" on bwsLayout page is 1
    And i verify that sublayout component image is rendering
    And i verify that sublayout component heading is appearing
    And i click on the sublayout image
    And i verify that sublayout component heading is appearing	
    
   @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "comfort" brand "homepage" of "id" locale has been built
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When i click on contact us form link on helpcenter
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page
			