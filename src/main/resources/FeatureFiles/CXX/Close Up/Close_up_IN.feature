@brand-closeup @cxx @locale-in
Feature: closeup - in

@rr @p2 @p4
Scenario: closeup in - VerifyReviewForm - WriteAreviewsubmit_Flow
	Given the "closeup" brand "productpage" of "in" locale has been built 
	When i click on Write a Review link 
	Then i should be able to see write a review dialog 
	And i should be able to see product image in write a review dialog
	And i select overall rating as 4 
	And i give review title as "Lorem ipsom" 
	And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
	And i enter random value in nickname 
	And i enter random email id for notification purposes 
	#And i check age checkbox 
	And i agree to terms and conditions 
	And i submit the review 
	Then i should see that review is submitted with message "Your review has been sent for Approval." 



@bin @p1
Scenario: closeup in - VerifyShopNow - ShopnowFeature 
	Given the "closeup" brand "productpage" of "in" locale has been built 
	When i see the structure of the "productpage" 
	Then i should be able to see Buy It Now functionality 
	And I click on a "BIN_Button"
	Then i should be able to see online store list 
	
@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "closeup" brand "homepage" of "in" locale has been built
    When i see the structure of the "homepage"
    And I scroll published page down by 2750
    And I click on a "FAQ_Link"
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And I expect to see "Accordion_Collapsed_02" as visible
    And I click on a "Accordion_Collapsed_02"
    And I expect to see "Accordion_Expanded_02" as visible
    
    #And i verify that following components "accordion-v2" exist on the "faqpage"
    #And i expect that accordion panel with index 2 is collapsed
    #When i expand accordion panel 2
    #And i expect that accordion panel with index 2 is expanded		
    
@cta @p1
Scenario: CLOSEUP - articledetailpage - Related Article 
	Given the "closeup" brand "articledetailpage" of "in" locale has been built 
	When i see the structure of the "articledetailpage"
    And I verify that count of component "article" on article page is 1
    And i verify that listing article component image is rendering
    And i verify that listing article component heading is appearing
    And i click on the listing article image
    And I verify that count of component "article" on article page is 1
    Then i verify that listing article component image is rendering	    	

@cross @p1
  Scenario: Home page - related products v2 component
    Given the "closeup" brand "productpage" of "in" locale has been built
    When i see the structure of the "productpage"
    And i verify that listing item component image is rendering
    And i verify that listing article component heading is appearing
    When I click on the listing item image
    And i verify that listing item component image is rendering
    And i verify that listing article component heading is appearing 


@signup @p1     
Scenario: SignUp verification  Sign Up -  numerical validations						
    Given the "closeup" brand "homepage" of "in" locale has been built						
    When i see the structure of the "homepage"	
    When i click on site map link in global footer					
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page

@cp @p2
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "closeup" brand "homepage" of "in" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer    
    
    @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "closeup" brand "homepage" of "in" locale has been built						
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When i click on contact us form link on helpcenter
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page 
    

