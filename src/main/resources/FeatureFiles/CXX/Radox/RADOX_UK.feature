@brand-radox @cxx @locale-uk
Feature: radox - uk

@rr @p1
Scenario: radox - VerifyReviewForm - WriteAreviewsubmit_Flow
	Given the "radox" brand "productpage" of "uk" locale has been built 
	When i click on Write a Review link 
	Then i should be able to see write a review dialog 
	And i should be able to see product image in write a review dialog
	And i select overall rating as 4 
	And i give review title as "Lorem ipsom" 
	And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
	And i enter random value in nickname 
	And i enter random email id for notification purposes 
	#And i check age checkbox 
	And i agree to terms and conditions 
	And i submit the review 
	#Then i should see that review is submitted with message "Your review has been sent for Approval." 



@bin @p2
Scenario: radox - VerifyShopNow - ShopnowFeature 
	Given the "radox" brand "productpage" of "uk" locale has been built 
	When i see the structure of the "productpage" 
	Then i should be able to see Buy It Now functionality 
	When i click on Buy It Now Button 
	Then i should be able to see online store list  
	
@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "radox" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    And I scroll published page down by 3000
    And I click on a "FAQ_Link"
    #When i click on the FAQs link of global footer
    #Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    And i click on "SignupCloseButton" if present
    And I scroll published page down by 500
    Then I expect to see "FAQ_Heading" as visible
    #And i verify that following components "accordion-v2" exist on the "faqpage"
    #And i expect that accordion panel with index 2 is collapsed
    #When i expand accordion panel 2
    #And i expect that accordion panel with index 2 is expanded			
    
    @cta @p1
Scenario: RADOX - articledetailpage - Related Article 
		Given the "radox" brand "articledetailpage" of "uk" locale has been built 
	  When i see the structure of the "articledetailpage"
	  And I scroll published page down by 1600
	  And I expect to see "RelatedArticle_Heading" as visible
	  #And I scroll published page down by 650
	  And I expect to see "RelatedArticle_Link" as visible
	  And I click on a "RelatedArticle_Link"
	  And I expect to see "Heading" as visible
	  And I scroll published page down by 1000
	  And I expect to see "RelatedArticle_Link" as visible
	  
     
 
   @cross @p1
  Scenario: Home page - related products v2 component
    Given the "radox" brand "productpage" of "uk" locale has been built
    When i see the structure of the "productpage"
    Then i verify that following components "component-content" exist on the "productpage" 
    And i verify that related products are present on the productpage 
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    When I click on the related-product component
	  And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing   
    
@cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "radox" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "https://www.unilevernotices.com/united-kingdom/english/cookie-notice/notice.html"
