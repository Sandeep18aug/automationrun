@brand-knorr @cxx @locale-mx
Feature: knorr - mx

@rr @p1
		Scenario: KNORR MX - VerifyReviewform - SubmitReview 
			Given the "knorr" brand "productpage" of "mx" locale has been built 
			And i click on "knorr.popup.close" if present
			When I scroll published page down by 170
			And i click on "knorr.popup.close" if present
			When I click on a "writeReview_Lnk"
			Then i should be able to see write a review dialog 
			And i should be able to see product image in write a review dialog
			And i select overall rating as 4 
			And i give review title as "Lorem ipsom" 
			And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
			And i enter random value in nickname 
			And i enter random email id for notification purposes 
			#And i check age checkbox 
			And i agree to terms and conditions 
			And i submit the review 
			 
							
@bin @p1
   Scenario: KNORR MX- VerifyBuyInNow - BIN_Flow 
			Given the "knorr" brand "productpage" of "mx" locale has been built 
			And i click on "knorr.popup.close" if present
			When i see the structure of the "productpage" 
			And I wait for 10 seconds
			#Then i should be able to see Buy It Now functionality 
			And I switch to frame by object Iframe_KnorrMX_Bin
			When I click on a "pdp.knorrMX.BIN"
			#And I expect to see "Online_store" as visible
			#Then i should be able to see online store list
			

@faq  @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "knorr" brand "homepage" of "mx" locale has been built
    And i click on "knorr.popup.close" if present
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded	
    
@cta @p1
Scenario: Knorr - articledetailpage - Related Article 
	Given the "knorr" brand "articledetailpage" of "mx" locale has been built 
	And i click on "knorr.popup.close" if present
	When i see the structure of the "articledetailpage"
    And I verify that count of component "media-gallery" on this page is 1
    And I expect to see "Article_Img" as visible
    And I expect to see "Article_Heading" as visible
    And I click on a "Article_Img"
    And I expect to see "RelatedArticle_Heading" as visible
    And I scroll published page down by 1800
    And I expect to see "RelatedArticle_Image" as visible
    And I expect to see "RelatedArticle_SubHeading" as visible
    
    
@cross @p1 @p3
  Scenario: Home page - related products v2 component
    Given the "knorr" brand "productpage" of "mx" locale has been built
    And i click on "knorr.popup.close" if present
    When i see the structure of the "productpage"
    When I scroll published page down by 1700
    And I expect to see "Listing_Heading" as visible
    And I expect to see "Listing_Img" as visible
    And I click on a "Listing_Img"
    And I expect to see "Listing_Heading" as visible
    
    #Then i verify that following components "related-products" exist on the "productpage" 
    #And i verify that related products are present on the productpage   
    #And i verify that related product component image is rendering
    #And i verify that related product component heading is appearing
    #And I click on the related-product component of knorr
#	And i verify that related products are present on the productpage   
    #And i verify that related product component image is rendering
    #And i verify that related product component heading is appearing    
    
     @cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "knorr" brand "homepage" of "mx" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "https://www.unilevercookiepolicy.com/es_ES/policy.aspx"
    
     @signup @p1     
Scenario: SignUp verification Sign Up -  numerical validations						
    Given the "knorr" brand "homepage" of "mx" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page	
    
    @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "knorr" brand "homepage" of "mx" locale has been built						
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When i click on contact us form link on helpcenter
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page		