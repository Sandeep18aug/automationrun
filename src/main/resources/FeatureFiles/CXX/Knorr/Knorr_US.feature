@brand-knorr @cxx @locale-us
Feature: knorr - us 
			
@rr @p1
Scenario: Knorr-us - VerifyReviewform - SubmitReview 
	Given the "knorr" brand "productpage" of "us/en" locale has been built 
	When i click on Write a Review link 
	Then i should be able to see write a review dialog 
	#And i should be able to see product image in write a review dialog
	When i select overall rating as 4 
	And i give review title as "Lorem ipsom" 
	And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
	And i recommend this product to friend 
	And i enter random value in nickname 
	And i enter random email id for notification purposes 
	And i enter zipcode as "99501" 
	And i select "March" as birth month 
	And i select "1980" as birth year 
	And i select "Grocery stores" as purchase location 
	And i select "Once a week" as Frequency 
	And i select "2-5 years" as how long you have been purchasing 
	And i select "Yes" as incentive for reviews 
	And i select "Yes" as sign up for exciting offers 
	And i select 9 option for recommend brand to a friend 
	And i agree to terms and conditions 
	And i submit the review 
	 
								
@STORE
Scenario: Knorr-us - StoreLocator - Store 
	Given the "knorr" brand "storelocatorpage" of "us/en" locale has been built 
	When i see the structure of the "storelocatorpage" 
	Then i select "SAUCES" category for store locator 
	Then i should be able to see products for select category and sub category 
	When i select any of the random product for seleted category and subcategory 
	And i enter "10001" zip code 
	And i select "50 Miles" as find with store range 
	And i click on find in store button 
	Then i should be able to see map for searched stores 
	And i should be able to see stores list 
	
	
@bin @p1
Scenario: Knorr-us - AddToBag - Add 
	Given the "knorr" brand "productpage" of "us/en" locale has been built 
	When i see the structure of the "productpage" 
	Then i should be able to see Buy It Now functionality 
	When i click on Buy It Now Button 
	Then i should be able to see online store list

@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "knorr" brand "homepage" of "us/en" locale has been built
    And I click on "knorr.signup.button" if present
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    #Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded			

@cta @p1
Scenario: Knorr - articledetailpage - Related Article 
		Given the "knorr" brand "articledetailpage" of "us/en" locale has been built 
	  When i see the structure of the "articledetailpage"
    Then i verify that following components "page-listing-v2" exist on the "homepage"
    And I verify that count of component "page-listing-v2" on this page is 1
    And i verify that Page Listing V2 component image is appearing
    And i verify that Page Listing V2 component heading is appearing
    And i verify that Page Listing V2 component Sub-heading is appearing
    And i click on any of the items on Page Listing V2
    Then it should take me to the corresponding page of Page Listing V2 Image
    Then i verify that following components "page-listing-v2" exist on the "homepage"
    And i verify that Page Listing V2 component image is appearing
    
@cross @p1 @p3
  Scenario: Home page - related products v2 component
    Given the "knorr" brand "productpage" of "us/en" locale has been built
    When i see the structure of the "productpage"
    When I scroll published page down by 2000
    And I expect to see "Product_Heading" as visible
    And I expect to see "Product_Img" as visible
    And I click on a "Product_Img"
    And I expect to see "Product_Heading" as visible
    
    #Then i verify that following components "related-products" exist on the "productpage" 
    #And i verify that related products are present on the productpage   
    #And i verify that related product component image is rendering
    #And i verify that related product component heading is appearing
    #And I click on the related-product component of knorr
#		And i verify that related products are present on the productpage   
    #And i verify that related product component image is rendering
    #And i verify that related product component heading is appearing    
    
   @cp @p2
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "knorr" brand "homepage" of "us/en" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    
     @signup @p1     
Scenario: SignUp verification Scenario - Sign Up -  numerical validations						
    Given the "knorr" brand "homepage" of "us/en" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in header						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"				
    And I enter Postal Code as "free"												
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page	
    Then I verify Postal Code Error Message is visible on Forms page
    
    @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "knorr" brand "homepage" of "us/en" locale has been built						
    When i see the structure of the "homepage"
    And i click on "knorr.popup.close" if present
    When i click on the contact us link of global footer
    When i click on contact us form link on helpcenter
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page 
    	