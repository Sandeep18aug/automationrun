@brand-knorr @cxx @locale-au
Feature: knorr - au

@rr @p1
		Scenario: KNORR AU - VerifyReviewform - SubmitReview 
			Given the "knorr" brand "productpage" of "au" locale has been built 
			And I wait for page to load
		  And I wait for 10 seconds
			When i click on Write a Review link 
			Then i should be able to see write a review dialog 
			And i should be able to see product image in write a review dialog
			And i select overall rating as 4 
			And i give review title as "Lorem ipsom" 
			And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing" 
			And i enter random value in nickname 
			And i enter random email id for notification purposes 
			#And i check age checkbox 
			And i agree to terms and conditions 
			And i submit the review 
			 
							
@bin @p1
   Scenario: KNORR AU - VerifyBuyInNow - BIN_Flow 
			Given the "knorr" brand "productpage" of "au" locale has been built 
			When i see the structure of the "productpage" 
			Then i should be able to see Buy It Now functionality 
			When i click on Buy It Now Button 
			Then i should be able to see online store list

@faq @p1
  Scenario: Global Footer - FAQs Validation
  
    Given the "knorr" brand "homepage" of "au" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    And i expect that accordion panel with index 2 is collapsed
    When i expand accordion panel 2
    And i expect that accordion panel with index 2 is expanded
    
@cta @p2
Scenario: Knorr - articledetailpage - Related Article 
	  Given the "knorr" brand "articledetailpage" of "au" locale has been built 
	  When i see the structure of the "articledetailpage"
	  Then i verify that following components "page-listing-v2" exist on the "homepage"
    And I verify that count of component "page-listing-v2" on this page is 1
    And i verify that Page Listing V2 component image is appearing
    And i verify that Page Listing V2 component heading is appearing
    And i verify that Page Listing V2 component Sub-heading is appearing
    And i click on any of the items on Page Listing V2
    Then it should take me to the corresponding page of Page Listing V2 Image
    Then i verify that following components "page-listing-v2" exist on the "homepage"
    And i verify that Page Listing V2 component image is appearing
    
    @cross @p1
  Scenario: Home page - related products v2 component
    Given the "knorr" brand "productpage" of "au" locale has been built
    When i see the structure of the "productpage"
    Then i verify that following components "related-products" exist on the "productpage" 
    And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing
    And I click on the related-product component of knorr
	  And i verify that related products are present on the productpage   
    And i verify that related product component image is rendering
    And i verify that related product component heading is appearing		  
    
     @cp @p1
  Scenario: Global Footer - Cookie Policy Validation
  
    Given the "knorr" brand "homepage" of "au" locale has been built
    When i see the structure of the "homepage"
    Then i click on the Cookie Policy link of global footer
    And I switch to the newly opened tab 2
    Then i expect page url contains "https://www.unilevercookiepolicy.com/en_gb/policy.aspx"    
    
 @signup @p2     
Scenario: SignUp verification Scenario - Sign Up -  numerical validations						
    Given the "knorr" brand "homepage" of "au" locale has been built						
    When i see the structure of the "homepage"						
    Then i should see the sign up link in the global navigation						
    When i click on the sign up link in the global navigation						
    Then it should take me to sign up page						
    And i enter email address as "6689"											
    And i enter first name as "456"						
    And i enter last name as "345"																
    And I click on the submit button on Forms Page						
    And I wait for 5 seconds						
    Then I verify Email Address Error Message is visible on Forms page						
    Then I verify First Name Error Message is visible on Forms page						
    Then I verify Last Name Error Message is visible on Forms page	    
       
   @contact @p1
  Scenario: Contact Us - Blank field validation
    Given the "knorr" brand "homepage" of "au" locale has been built						
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    When I click on the submit button on Forms Page
    Then I verify Enquiry Error Message is visible on Forms page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page			