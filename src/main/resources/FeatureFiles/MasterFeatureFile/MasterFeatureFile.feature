@master
Feature: master

  #***********************************************************************************************************************************************************************************
  @tag1 @homepage
  Scenario: Home page - components present
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i verify that only one of the following components "simple-header;global-navigation" exists on the "homepage"
    And i verify that following components "search-input-v2;hero-v2;back-to-top-cta;global-footer-v2;" exist on the "homepage"
    And i verify that global search is present on the page
    And i verify that one of the following components "related-articles;call-to-action;featured-content;page-listing-v2" exists on the "homepage"
    And i verify that either all or few of the following components exist on page:
      """
      "simple-header;global-navigation;social-media-wall;search-input-v2;search-input-v2;hero-v2;back-to-top-cta;global-footer-v2;related-articles;call-to-action;featured-content;page-listing-v2;miappi"
      """

  #***********************************************************************************************************************************************************************************
  @tag2 @homepage
  Scenario: Home page - Sign Up Link
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i should see the sign up link in the global navigation
    When i click on the sign up link in the global navigation
    Then it should take me to sign up page

  #***********************************************************************************************************************************************************************************
  @tag3 @homepage
  Scenario: HomePage - Global Search to Search Result page - Product
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i search "Shampoo" text in global search
    Then it should take me to the search listing page
    And i should be able to see search listing tabs
    When i select "Product" search listing tab
    And i should be able to see search list items
    And i expect search list item name is present
    And i expect search list item description is present
    And i expect search list item image is present
    When i click on any of the items of search listing page
    Then it should take me to corresponding page
    And I should see breadcrumb is present on the page

  #***********************************************************************************************************************************************************************************
  @tag4 @homepage
  Scenario: HomePage - global navigation tabs
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    And I verify that the brand logo is present in the global navigation
    Then i expect the global navigation to have the following "PRODUCTS;HAIR NEEDS;COLLECTIONS;HAIR CONCIERGE;IN THE PRESS;HOW TO" tabs

  #***********************************************************************************************************************************************************************************
  @tag5 @homepage
  Scenario: HomePage - Global navigation Tabs
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i hover over the "Products" tab of global navigation
    Then i expect to see following submenus "SHAMPOO;CONDITIONER;MASQUE;TREATMENT" under "Products" tab

  #***********************************************************************************************************************************************************************************
  @tag6 @homepage
  Scenario: HomePage - Global navigation Tabs - HAIR NEEDS tab submenu
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i hover over the "HAIR NEEDS" tab of global navigation
    Then i expect to see following submenus "NORMAL TO DRY HAIR;DAMAGED HAIR;FRIZZY HAIR;FINE OR AGEING HAIR" under "HAIR NEEDS" tab

  #***********************************************************************************************************************************************************************************
  #@tag7 @homepage
  #Scenario: HomePage - Global navigation Tabs - Collections tab submenu ----- no tab submenu for Collection
  #Given the "nexxus" brand "homepage" of "uk" locale has been built
  #When i hover over the "COLLECTIONS" tab of global navigation
  #Then i expect to see following submenus "Browse All;Nutritive;Emergencee;Oil Infininte;Youth Renewal" under "COLLECTIONS" tab
  #***********************************************************************************************************************************************************************************
  @tag8 @homepage
  Scenario: HomePage - Global navigation Tabs - HAIR CONCIERGE
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    Then i expect the global navigation to have the following "PRODUCTS;HAIR NEEDS;COLLECTIONS;HAIR CONCIERGE;IN THE PRESS;HOW TO" tabs
    When i click on the "HAIR CONCIERGE" tab of global navigation
    Then i expect page url contains "concierge.html"

  #***********************************************************************************************************************************************************************************
  @tag9 @homepage
  Scenario: HomePage - Global Navigation to PLP to PDP navigation
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i hover over the "Products" tab of global navigation
    And i click on any of the submenu links of tab "Products"
    Then it should take me to a product listing page
    And i should see product list items on product listing page
    When i click on any of the items of product listing page
    Then it should take me to a product detail page

  #***********************************************************************************************************************************************************************************
  @tag10 @homepage
  Scenario: Home page - hero v2 component
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i verify that following components "hero-v2" exist on the "homepage"
    And i verify that hero image is present
    And i verify that hero heading is present
    And i verify that hero cta is rendering
    When i click on cta of hero component
    Then it should take me to the corresponding page of hero component cta

  #***********************************************************************************************************************************************************************************
  @tag11 @homepage
  Scenario: Home page - related articles v2 component
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i verify that following components "related-articles" exist on the "homepage"
    And i verify that related articles are present on the homepage
    And i verify that related article component image is rendering
    And i verify that related article component heading is appearing
    And i verify that cta link/button of related-articles component is present on the page
    When I click on the cta of the related-articles component
    Then it should take me to the corresponding page of related articles cta
    And I click on any of the items on Page Listing V2
    Then it should take me to the corresponding page of Page Listing V2 Image

  #***********************************************************************************************************************************************************************************
  @tag12 @homepage
  Scenario: Home page - featured content component
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i verify that following components "featured-content" exist on the "homepage"
    And I verify that count of component "featured-content" on this page is 3

  #***********************************************************************************************************************************************************************************
  @tag13 @homepage
  Scenario: Home page - page listing v2 component
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i verify that following components "page-listing-v2" exist on the "homepage"
    And I verify that Page Listing V2 component heading is appearing
    And I verify that Page Listing V2 component image is appearing
    And I verify that Page Listing V2 component description is appearing
    And i verify that page listing v2 component has cta link/button present on the page
    When I click on the cta of the page listing v2 component
    Then it should take me to the corresponding page of page listing cta

  #***********************************************************************************************************************************************************************************
  @tag14 @homepage
  Scenario: Home page - olapic/miappi/social media integration
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    And I verify that one of the following components "social-media-wall" exists on the "homepage"
    And i verify that I am able to interact with the integration

  #***********************************************************************************************************************************************************************************
  @tag15 @homepage
  Scenario: Home page - back to top component
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i verify that following components "back-to-top-cta" exist on the "homepage"
    When I scroll published page to Bottom
    And I click on the back to top CTA

  #Then I am taken back to the top of the same page
  #***********************************************************************************************************************************************************************************
  @tag16 @homepage
  Scenario: Home page - global footer component
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i verify that following components "global-footer-v2" exist on the "homepage"
    And I verify that the brand logo is present in the footer
    And I verify that social share links are present
    When I clicking on any social share link
    Then it takes me to the corresponding social site page

  #***********************************************************************************************************************************************************************************
  @tag17 @homepage
  Scenario: Home page - global footer component
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    And i verify that following links "Site map;Contact Us;Terms of use;Cookie Policy;FAQs;Privacy Policy" are present in the global footer
    And I verify that country selector image is present in the footer
    And I verify that unilever logo is appearing in the footer
    And I verify that copyright icon is appearing in the footer
    And I verify that disclaimer is appearing in the footer

  #***********************************************************************************************************************************************************************************
  @tag18 @homepage
  Scenario: Nexxus - VerifyCountrySelector - CountrySelector
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i should be able to see country selector link in global footer
    When i click on country selector link in global footer
    Then i should be taken to country selector page
    When i select any country from country selector page
    Then i should be taken to corresponding page

  #***********************************************************************************************************************************************************************************
  @tag19 @homepage @SubMenu
  Scenario: Product Listing Page (PLP) of the brand website
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i hover over the "HAIR NEEDS" tab of global navigation
    And i click on any of the submenu links of tab "HAIR NEEDS"
    Then it should take me to a product listing page
    When i see the structure of the "productlistingpage"
    And i verify that following components "global-navigation;search-input-v2;hero-v2;back-to-top-cta;global-footer-v2" exist on the "product listing page"
    And i verify that one of the following components "section-navigation-v2;product-listing-v2;accordion" exists on the "product listing page"
    And i verify that one of the following components "video-player;featured-content;related-articles" exists on the "product listing page"

  #***********************************************************************************************************************************************************************************
  @tag20 @homepage
  Scenario: Product Description Page (PDP) of the brand website
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i hover over the "HAIR NEEDS" tab of global navigation
    And i click on any of the submenu links of tab "HAIR NEEDS"
    Then it should take me to a product listing page
    When i click on any of the items of product listing page
    Then it should take me to a product detail page
    And i verify that following components "global-navigation;search-input-v2;product-images;breadcrumb;product-rating-overview;back-to-top-cta;global-footer-v2" exist on the "product definition page"
    And i verify that one of the following components "section-navigation-v2;search-input-v2;accordion" exists on the "product definition page"

  #***********************************************************************************************************************************************************************************
  @tag21 @alp
  Scenario Outline: Article Listing Page (ALP) of the brand website for different pages
    Given the "nexxus" brand "<pagename>" of "uk" locale has been built
    When i see the structure of the "<pagename>"
    And i verify that following components "call-to-action;search-input-v2;global-navigation;hero-v2;page-listing-v2;back-to-top-cta;page-listing-v2" exist on the "article listing page"

    Examples: 
      | pagename            |
      | articlelistingpage1 |
      | articlelistingpage2 |

  #***********************************************************************************************************************************************************************************
  @tag22
  Scenario: Article Description Page - Hero - Image/Video (ADPHV) of the brand website
    Given the "nexxus" brand "articlelistingpage" of "uk" locale has been built
    When i see the structure of the "articlelistingpage"
    Then i should see article list items on article listing page
    When i click on any of the items of article listing page
    Then it should take me to the article detailed page
    And i verify that following components "search-input-v2;global-navigation;rich-text-v2;product-listing-v2;social-sharing;tags;related-articles;global-footer-v2;ordered-list" exist on the "article listing page"
    And i verify that following components "search-input-v2;global-navigation;page-divider-v2;rich-text-v2;product-listing-v2;social-sharing;tags;related-articles;global-footer-v2" exist on the "article listing page"

  #***********************************************************************************************************************************************************************************
  @tag23 @homepage
  Scenario: Hygiene Page of the brand website
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i navigate to wrong url
    Then it should take me to the error message page
    And I verify that the brand logo is present in the global navigation
    And I verify that the brand logo is present in the footer
    When i click on redirect home page button
    Then i expect page url contains "home.html"

  #***********************************************************************************************************************************************************************************
  @tag24 @homepage
  Scenario: Contact us page
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    And i verify that following links "Site map;Contact Us;Terms of use;Cookie Policy;FAQs;Privacy Policy" are present in the global footer
    And I click on the back to top CTA
    And I scroll published page to Bottom
    When i click on the contact us link of global footer
    Then it should take me to contact us form page
    And i verify that only one of the following components "simple-header;global-navigation" exists on the "contact-us"
    And i verify that following components "search-input-v2;back-to-top-cta;global-footer-v2" exist on the "contact-us"
    And i verify that either all or few of the following components exist on page:
      """
      "page-divider-v2;formElementV2;simple-header;global-navigation;search-input-v2;search-input-v2;breadcrumb;rich-text-v2;accordion-v2;form-v2;call-to-action;country-selector;store-search;store-search-results;checkout-shoppable;purchase-order-confirmation;sitemap;back-to-top-cta;global-footer-v2"
      """

  #***********************************************************************************************************************************************************************************
  @tag25 @plp
  Scenario: Nexxus - VerifyProductListFilter - FilterOnRecipeProduct
    Given the "nexxus" brand "productlistingpage" of "uk" locale has been built
    When i apply any random filter on product listing page
    Then i should be able to see that filter is successfully applied

  #***********************************************************************************************************************************************************************************
  @tag26 @pdp
  Scenario: Nexxus - VerifyReviewForm - ReviewOnProductPage
    Given the "nexxus" brand "productpage" of "uk" locale has been built
    When i click on Write a Review link
    Then i should be able to see write a review dialog
    And i select overall rating as 4
    And i give review title as "Lorem ipsom"
    And i give detailed review as "This is Lorem ipsom description. Please ignore this as this is being used for just testing"
    And i enter random value in nickname
    And i enter random email id for notification purposes
    And i check age checkbox
    And i agree to terms and conditions
    And i submit the review
    Then i should see that review is submitted with message "Your review has been sent for Approval."

  #***********************************************************************************************************************************************************************************
  #@tag27
  #Scenario: nexxus - VerifyShopNow - BuyNowonProductPage ------- Out of Scope
  #Given the "nexxus" brand "productpage" of "uk" locale has been built
  #When i see the structure of the "productpage"
  #Then i should be able to see Buy It Now functionality
  #When i click on Buy It Now Button
  #Then i should be able to see online store list
  #***********************************************************************************************************************************************************************************
  #@tag28 @pdp
  #Scenario: nexxus - VerifyTurnTo - TurnToFunctionality  ------- Out of Scope
  #Given the "nexxus" brand "productpage" of "uk" locale has been built
  #When i see the structure of the "productpage"
  #Then i should be able to expand turn to section
  #When i submit question "Would you like to send details about this product?"
  #Then i should be able to see that question is successfully submitted
  #***********************************************************************************************************************************************************************************
  @tag29 @homepage
  Scenario: HomePage - Global Search to Search Result page - Article
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i search "Shampoo" text in global search
    Then it should take me to the search listing page
    And i should be able to see search listing tabs
    When i select "Article" search listing tab
    And i should be able to see search list items
    And i expect search list item name is present
    And i expect search list item description is present
    And i expect search list item image is present
    And i verify that search listing component has cta link/button present on the page
    When i click on the cta of the search listing component
    And i should be able to see search list items

  #**********************************************************************************************************************************************************************************
  @tag30 @homepage
  Scenario: HomePage - Global Search to Search Result page - Search Listing Pagination
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i search "Shampoo" text in global search
    Then it should take me to the search listing page
    And i should be able to see search listing tabs
    When i select "Article" search listing tab
    And I verify show more functionality is working on Search Listing Page

  #***********************************************************************************************************************************************************************************
  @tag31 @homepage
  Scenario: Home page - page listing v2 component - Page Listing V2 Pagination
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i verify that following components "page-listing-v2" exist on the "homepage"
    And i verify that page listing v2 component has cta link/button present on the page
    When i click on the cta of the page listing v2 component
    Then it should take me to the corresponding page of page listing cta
    And I verify show more functionality is working on Page Listing Page

  #***********************************************************************************************************************************************************************************
  @tag32 @homepage
  Scenario: Global Footer - Site Map Validation
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    And i click on the site map link of global footer
    Then it should take me to site map page

  #***********************************************************************************************************************************************************************************
  @tag33 @homepage
  Scenario: Global Footer - Contact us Validation
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    And i click on the contact us link of global footer
    Then it should take me to contact us form page

  #***********************************************************************************************************************************************************************************
  @tag34 @homepage
  Scenario: Global Footer - Terms of use Validation
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    And i click on the terms of use link of global footer
    Then it should take me to terms of use page

  #***********************************************************************************************************************************************************************************
  @tag35 @homepage
  Scenario: Global Footer - Cookie Policy Validation
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    And i click on the Cookie Policy link of global footer
    And it should take me to Cookie Policy page

  #***********************************************************************************************************************************************************************************
  @tag36 @homepage
  Scenario: Global Footer - FAQs Validation
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    When i click on the FAQs link of global footer
    Then it should take me to FAQs page

  #***********************************************************************************************************************************************************************************
  @tag37 @homepage
  Scenario: Global Footer - Privacy Policy Validation
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    And i click on the Privacy Policy link of global footer
    Then it should take me to Privacy Policy page

  #***********************************************************************************************************************************************************************************
  @tag38 @homepage
  Scenario: SignUp - Sign Up Link validation
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i should see the sign up link in the global navigation
    When i click on the sign up link in the global navigation
    Then it should take me to sign up page
    When I click on the submit button on Forms Page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Address Error Message is visible on Forms page
    Then I verify City Error Message is visible on Forms page
    Then I verify Age Consent Error Message is visible on Forms page

  #***********************************************************************************************************************************************************************************
  @tag39 @homepage
  Scenario: Hero V2 video validation
    Given the "nexxus" brand "herov2videopage" of "uk" locale has been built
    When i see the structure of the "herov2videopage"
    Then i should see video link is present on the page
    When i click on video link
    Then i should see video is playing
    When i click on video
    Then i should see video is not playing

  #***********************************************************************************************************************************************************************************
  @tag40 @pdp
  Scenario: Home Page - Validating filters , rating and reviews
    Given the "nexxus" brand "productratingpage" of "uk" locale has been built
    When I see the structure of the "productratingpage"
    And I click on Write a Review link
    Then I should be able to see write a review dialog
    Then I should be able to see write a review dialog
    Then I select overall rating as 4
    And I give review title as "Please review the product and let us know your thoughts"
    And I give detailed review as "Conditioner is crafted to leave hair feeling balanced and flexible. Suitable for both straight and curly hair, use it as a frizz-controlling conditioner for thick hair or as a conditioner for curly hair to smooth waves and enjoy glossy locks."
    And i enter random value in nickname
    And i enter random email id for notification purposes
    And i check age checkbox
    And i agree to terms and conditions
    And i submit the review
    And i should see that review is submitted with message "Your review has been sent for Approval."

  #***********************************************************************************************************************************************************************************
  @tag41 @articlepage
  Scenario: HomePage - Articlepage re-direct to Search Listing page with Tag name
    Given the "nexxus" brand "articlepage" of "uk" locale has been built
    And I click on article tag of article listing page
    Then it should take me to the search listing page

  #***********************************************************************************************************************************************************************************
  @tag42 @articlepage
  Scenario: HomePage - Productpage re-direct to Search Listing page with Tag name
    Given the "nexxus" brand "productratingpage" of "uk" locale has been built
    And I click on product tag of article listing page
    Then it should take me to the search listing page

  #***********************************************************************************************************************************************************************************
  @tag43 @homepage @plp
  Scenario Outline: Verify product listing page for different pages
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i click on the "<Tab>" tab of global navigation
    And i click on "<SubTab>" submenu link of tab "<Tab>"
    Then it should take me to a product listing page
    When i see the structure of the "<page>"
    Then i verify that following components "hero-v2" exist on the "<page>"
    And i verify that herov2 component image is rendering
    And i verify filter is rendering on Product Page
    And i verify that related article component image is rendering
    And i verify that following links "Site map;Contact Us;Terms of use;Cookie Policy;FAQs;Privacy Policy" are present in the global footer

    Examples: 
      | Tab      | SubTab      |
      | PRODUCTS | SHAMPOO     |
      | PRODUCTS | CONDITIONER |
      | PRODUCTS | MASQUE      |
      | PRODUCTS | TREATMENT   |

  #***********************************************************************************************************************************************************************************
  @tag44
  Scenario: Form Page - brand logo in header and footer
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i should see the sign up link in the global navigation
    And I verify that the brand logo is present in the global navigation
    And I verify that the brand logo is present in the footer

  #***********************************************************************************************************************************************************************************
  @tag45 @homepage
  Scenario: HomePage - Global navigation Tabs - close button and brand logo
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i hover over the "Products" tab of global navigation
    Then i click on close button in global nav tab
    And I verify that the brand logo is present in the footer
    And i click on brand logo in global footer
    Then i expect page url contains "home.html"

  #***********************************************************************************************************************************************************************************
  @tag46 @homepage
  Scenario: HomePage - FAQ
    Given the "nexxus" brand "faqpage" of "uk" locale has been built
    When i see the structure of the "faqpage"
    Then i verify FAQ heading
    And i verify that following components "accordion-v2" exist on the "faqpage"
    When i expand first accordion panel
    Then i expect that first accordion panel should be expanded
    When i collapse first accordion panel
    Then i expect that first accordion panel should be collapsed

  #
  #
  #And i click on the FAQ Question link
  #And it should show me to the FAQ rich text
  #And I verify expand and collapse functionality of Accordian
  #***********************************************************************************************************************************************************************************
  @tag47 @homepage
  Scenario: Conyry Selector - search box validations
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    Then i should be able to see country selector link in global footer
    When i click on country selector link in global footer
    Then i should be taken to country selector page
    When i see the structure of the "countryselectorpage"
    And i verify that following components "simple-header;country-selector" exist on the "countryselectorpage"
    When i search country with text "uni"
    Then i should be able to see country suggestion list
    When i search country with text "yyy"
    Then i should not be able to see country suggestion list
    When i search country with text "un"
    Then i should not be able to see country suggestion list
    And I verify that the brand logo is present in the footer

  #***********************************************************************************************************************************************************************************
  #@tag48 @homepage
  #Scenario: HomePage - reset button for search Page , close button overlay
  #
  #Given the "nexxus" brand "homepage" of "uk" locale has been built
  #When i search "Shampoo" text in global search
  #And i should see close button overlay on search listing page
  #And i click on reset button on search listing page
  #Then i should not be able to see search result
  #***********************************************************************************************************************************************************************************
  @tag49 @homepage
  Scenario: HomePage - incorrect search result
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i search "yfduyugd" text in global search
    And i should see sorry message is diaplyed as "SORRY WE HAVE NOTHING TO SHOW YOU, PERHAPS YOU COULD SEARCH FOR SOMETHING ELSE."
    Then i should be able to see search list items

  #***********************************************************************************************************************************************************************************
  @tag50
  Scenario: Article Listing Page - ALP Page Ordered List
    Given the "nexxus" brand "articlelistingpage" of "uk" locale has been built
    When i see the structure of the "articlelistingpage"
    And I should see breadcrumb is present on the page
    Then i should see article list items on article listing page
    When i click on any of the items of article listing page
    And i verify that following components "ordered-list;rich-text-v2" exist on the "article listing page"
    And I verify that Article Listing component description is appearing

  #***********************************************************************************************************************************************************************************
  @tag51 @homepage
  Scenario: HomePage - Global navigation Tabs - Collections Detail Page (CDP) - Accordian
    Given the "nexxus" brand "collectiondetailpage" of "uk" locale has been built
    When i see the structure of the "collectiondetailpage"
    And i verify that following components "hero-v2;accordion-v2;featured-content;related-articles;back-to-top-cta;product-listing-v2" exist on the "article listing page"
    And i should be able to see Accordian on Collection Detail Page
    And I verify expand and collapse functionality of Accordian

  #***********************************************************************************************************************************************************************************
  @tag52 @homepage
  Scenario: HomePage - Global navigation Tabs - Collections Detail Page (CDP) - Accordian
    Given the "nexxus" brand "collectiondetailpage" of "uk" locale has been built
    When i see the structure of the "collectiondetailpage"
    And i should be able to see Accordian on Collection Detail Page
    And I verify expand and collapse functionality of Accordian

  #***********************************************************************************************************************************************************************************
  @tag53 @homepage
  Scenario: Sign Up - Successfull form submisssion -man/non manselection
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i should see the sign up link in the global navigation
    When i click on the sign up link in the global navigation
    Then it should take me to sign up page
    And i enter random email id
    And I select title as "Mr"
    And i enter first name as "Automation"
    And i enter last name as "User"
    And I enter address line 1 as "Address 001"
    And I enter address line 2 as "Street 11"
    And I enter City as "OVERLAND PARK"
    And I enter Postal Code as "SW1A 1AA"
    And I select Country as "United Kingdom"
    And I enter Contact Number as "9876541231"
    And I check age Consent checkbox
    And I check Brand Opt-in checkbox
    And I check Corporate Opt-in checkbox
    And I check Mail Opt-in checkbox
    And I click on the submit button on Forms Page

  #***********************************************************************************************************************************************************************************
  @tag54 @homepage
  Scenario: Sign Up -  numerical validations
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i should see the sign up link in the global navigation
    When i click on the sign up link in the global navigation
    Then it should take me to sign up page
    And i enter email address as "6689"
    And I select title as "Mr"
    And i enter first name as "456"
    And i enter last name as "345"
    And I enter address line 1 as "Address 001 Street 11"
    And I enter City as "1234"
    And I enter Postal Code as "free"
    And I select Country as "United Kingdom"
    And I enter Contact Number as "free"
    And I click on the submit button on Forms Page
    And I wait for 10 seconds
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Address Error Message is visible on Forms page
    Then I verify City Error Message is visible on Forms page
    Then I verify Postal Code Error Message is visible on Forms page
    Then I verify Age Consent Error Message is visible on Forms page

  #***********************************************************************************************************************************************************************************
  @tag55 @homepage
  Scenario: Sign Up -  space , alpha - numerical and special char validations
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i should see the sign up link in the global navigation
    When i click on the sign up link in the global navigation
    Then it should take me to sign up page
    And i enter email address as "werf6689"
    And I select title as "Mr"
    And i enter first name as " wer456"
    And i enter last name as "wer345#$"
    And I enter address line 1 as "Address 001 Street 11"
    And I enter City as "wer1234"
    And I enter Postal Code as "33free"
    And I select Country as "United Kingdom"
    And I enter Contact Number as "456free"
    And I click on the submit button on Forms Page
    And I wait for 10 seconds
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify City Error Message is visible on Forms page
    Then I verify Postal Code Error Message is visible on Forms page
    Then I verify Contact No Error Message is visible on Forms page
    Then I verify Age Consent Error Message is visible on Forms page

  #***********************************************************************************************************************************************************************************
  @tag56 @homepage
  Scenario: Sign Up - Successfull form submisssion -mandatory selection
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i should see the sign up link in the global navigation
    When i click on the sign up link in the global navigation
    Then it should take me to sign up page
    And i enter random email id
    And i enter first name as "Automation"
    And i enter last name as "User"
    And I enter address line 1 as "Address 001 Street 11"
    And I enter City as "OVERLAND PARK"
    And I select Country as "United Kingdom"
    And I check age Consent checkbox
    And I check Brand Opt-in checkbox
    And I check Corporate Opt-in checkbox
    And I check Mail Opt-in checkbox
    And I click on the submit button on Forms Page

  #***********************************************************************************************************************************************************************************
  @tag57 @homepage
  Scenario: Contact Us - Successfull form submisssion -
    1.Enquiry Type as "Concern"
    2.Bar Code as "Yes"
    3.Manufacturing code Availability as "Yes"

    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    Then it should take me to contact us form page
    And i select enquiry type as "Concern"
    And i enter random email id
    And i select gender as "Male"
    And i enter first name as "Automation"
    And I select title as "Mr"
    And i enter last name as "User"
    And I select Country as "United Kingdom"
    And I enter address line 1 as "Address 001 Street 11"
    And I enter City as "OVERLAND PARK"
    And I enter Postal Code as "SW1A 1AA"
    And I select County as "Aberdeen"
    And I enter Contact Number as "9876541231"
    #Product Details
    And I select UPC Details as "Yes"
    And I enter bar code as "58987"
    And I enter Product as "Shampoo"
    And I enter Size as "1"
    And I enter expiry date as "02-06-1990"
    And I enter Manufacturing Code as "868768"
    And I enter Purchased Location as "India"
    And I select Manufacturing Code Availability as "Yes"
    And I enter Store Name as "PetStore"
    And I enter date of purchase as "02-06-2000"
    And I enter Query as "how does this system works ?"
    And I check age Consent checkbox
    And I check Brand Opt-in checkbox
    And I check Corporate Opt-in checkbox
    And I check Mail Opt-in checkbox
    And i check By Email Brand checkbox
    And i check By Email All checkbox
    And i check By Sms Brand checkbox
    And i check By Sms All checkbox
    And i check By Post Brand checkbox
    And i check By Post All checkbox
    And I click on the submit button on Forms Page

  #***********************************************************************************************************************************************************************************
  @tag58 @homepage
  Scenario: Contact Us - Successfull form submisssion -
    1.Enquiry Type as "Question"
    2.Bar Code as "Yes"
    3.Manufacturing code Availability as "Yes"

    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    Then it should take me to contact us form page
    And i select enquiry type as "Concern"
    And i enter random email id
    And i select gender as "Male"
    And i enter first name as "Automation"
    And I select title as "Mr"
    And i enter last name as "User"
    And I select Country as "United Kingdom"
    And I enter address line 1 as "Address 001 Street 11"
    And I select County as "Aberdeen"
    And I enter Postal Code as "SW1A 1AA"
    And I enter Contact Number as "9876541231"
    #Product Details
    And I select UPC Details as "Yes"
    And I enter bar code as "58987"
    And I enter Product as "Shampoo"
    And I enter Size as "1"
    And I enter expiry date as "02-06-1990"
    And I enter Manufacturing Code as "868768"
    And I enter Purchased Location as "India"
    And I select Manufacturing Code Availability as "Yes"
    And I enter Store Name as "PetStore"
    And I enter date of purchase as "02-06-1990"
    And I enter Query as "how does this system works ?"
    And I check age Consent checkbox
    And I check Brand Opt-in checkbox
    And I check Corporate Opt-in checkbox
    And I check Mail Opt-in checkbox
    And I click on the submit button on Forms Page

  #***********************************************************************************************************************************************************************************
  @tag59 @homepage
  Scenario: Contact Us - Blank field validation
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    Then it should take me to contact us form page
    When I click on the submit button on Forms Page
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Address Error Message is visible on Forms page
    Then I verify City Error Message is visible on Forms page
    Then I verify Postal Code Error Message is visible on Forms page
    #Then I verify Bar Code Error Message is visible on Forms page
    #Then I verify Product Error Message is visible on Forms page
    #Then I verify Manufacturing Code Error Message is visible on Forms page
    Then I verify Query Error Message is visible on Forms page
    Then I verify Age Consent Error Message is visible on Forms page

  #***********************************************************************************************************************************************************************************
  @tag60 @homepage
  Scenario: Contact us -  numerical validations
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    Then it should take me to contact us form page
    And i enter email address as "6689"
    And I select title as "Mr"
    And i enter first name as "456"
    And i enter last name as "345"
    And I enter address line 1 as "Address 001 Street 11"
    And I enter City as "1234"
    And I enter Postal Code as "free"
    And I select Country as "United Kingdom"
    And I enter Contact Number as "free"
    And I select bar code on product type as "Yes"
    And I enter bar code as "qwerty"
    And I click on the submit button on Forms Page
    And I wait for 10 seconds
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Address Error Message is visible on Forms page
    Then I verify City Error Message is visible on Forms page
    Then I verify Contact No Error Message is visible on Forms page
    Then I verify Postal Code Error Message is visible on Forms page
    Then I verify Bar Code Error Message is visible on Forms page
    Then I verify Age Consent Error Message is visible on Forms page

  #***********************************************************************************************************************************************************************************
  @tag61 @homepage
  Scenario: Contact Us -  space , alpha - numerical and special char validations
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    When i click on the contact us link of global footer
    Then it should take me to contact us form page
    When i see the structure of the "contactuspage"
    And i enter email address as "werf6689"
    And I select title as "Mr"
    And i enter first name as " wer456"
    And i enter last name as "wer345#$"
    And I enter address line 1 as "Address 001 Street 11"
    And I enter City as "wer1234"
    And I enter Postal Code as "33free"
    And I select Country as "United Kingdom"
    And I enter Contact Number as "456free"
    And I click on the submit button on Forms Page
    And I wait for 10 seconds
    Then I verify Email Address Error Message is visible on Forms page
    Then I verify First Name Error Message is visible on Forms page
    Then I verify Last Name Error Message is visible on Forms page
    Then I verify Address Error Message is visible on Forms page
    Then I verify City Error Message is visible on Forms page
    Then I verify Postal Code Error Message is visible on Forms page
    And I verify that following components "rich-text-v2" exist on the "contactuspage"
    Then I verify Age Consent Error Message is visible on Forms page

  @tag62 @homepage
  Scenario: Home page - featured content heading and image
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    Then i verify that following components "featured-content" exist on the "homepage"
    And I verify that featured content component heading is appearing
    And I verify that featured content component image is appearing

  @tag63 @homepage
  Scenario: Home page - featured content cta
    Given the "nexxus" brand "homepage" of "uk" locale has been built
    When i see the structure of the "homepage"
    And i verify that each featured-content has cta link/button present on the page
    When I click on the cta of the featured-content component
    Then it should take me to the corresponding page of featured content cta

  @Shopify @personalizationInAddtoCart
  Scenario: enter personalization text on image
    Given the "showcase" brand "productpage" of "us/en" locale has been built
    When i see the structure of the "productpage"
    #Then i verify that following components "ecommerce-add-to-cart" exist on the "productpage"
    #And I expect to see Product image on page
    #And I verify that Add to basket button is disabled
    Then I enter 11 digit or less personalization text "testgift" in field and verify it appears on image component
    And I verify that Add to basket button is Enabled
    #Then I click on Add to Basket button
    Then i click on add to bag button
    And I verify that user is navigated to My basket page

  @faq
  Scenario: Verify FAQ
    Given the "knorr" brand "faqpage" of "in" locale has been built
    When I see the structure of the "faqpage"
    Then I verify FAQ exists on page
    And I verify that total number of questions in FAQ section is 5
    When I expand all FAQs on page
    Then I verify All FAQ have answers for them

  @quickview @plp
  Scenario: Product Listing Page - Quick View feature
    Given the "dove" brand "productlistingpage" of "us/en" locale has been built
    When i see the structure of the "productlistingpage"
    Then i verify that following components "product-listing" exist on the "productlistingpage"
    Then i click on the "quick-view" button
    Then quick-view panel should expand
    When I click on cross button quick view panel should close

  @socialsites @pdp @pdpluxsociallinks
  Scenario: Product Detail Page - Social sites
    Given the "lux" brand "productpage" of "in" locale has been built
    When i see the structure of the "productpage"
    And I click on the Share This button
    Then i verify that following components "social-sharing" exist on the "productpage2"
    And I verify that "social-sharing-facebook" icon is present
    And I verify that "social-sharing-twitter" icon is present
    And I verify that "social-sharing-email" icon is present
    When I click the "social-sharing-facebook" icon
    And I switch to the newly opened tab 2
    And I close the newly opened tab 2
    And I switch to the newly opened tab 1
    When I click the "social-sharing-email" icon
    And I switch to the newly opened tab 2
    Then i expect page url contains "addthis.com/tellfriend"
    And I close the newly opened tab 2
    And I switch to the newly opened tab 1
    When I click the "social-sharing-twitter" icon
    And I switch to the newly opened tab 2

  @socialsites @pdp @pdppondssociallinks
  Scenario: Product Detail Page - Social sites
    Given the "ponds" brand "productpage2" of "us/en" locale has been built
    When i see the structure of the "productpage2"
    Then i verify that following components "social-sharing" exist on the "productpage2"
    And I verify that "social-sharing-facebook" icon is present
    And I verify that "social-sharing-twitter" icon is present
    When I click the "social-sharing-facebook" icon
    And I switch to the newly opened tab 2
    And I close the newly opened tab 2
    And I switch to the newly opened tab 1
    When I click the "social-sharing-twitter" icon
    And I switch to the newly opened tab 2

  @tag01 @productdetailpage @storelocator
  Scenario: Product Detail Page - Validate store locator on PDP
    Given the "thegoodstuff" brand "productdetailpage" of "us/en" locale has been built
    Then i see the structure of the "productdetailpage"
    And I verify that following components "store-search-pdp" exist on the "productdetailpage"
    When I enter "10009" ZipCode for store locator PDP
    And I click on "StoreLocator_ClickGoButton"
    Then I expect page title contains "Store locator results"
    When I click on refresh button on store search result page
    And I select random category for store locator
    Then I should be able to see products for selected category

  @tag02 @carouselv2
  Scenario: Carousel V2 - Verify Carousel v2 functionality
    Given the "whitelabel" brand "carouselpage" of "us/en" locale has been built
    Then i see the structure of the "carouselpage"
    And I verify that following components "carousel-v2" exist on the "carouselpage"
    And I verify that Next and Previous buttons are appearing for carousel component
    And I verify Pause button appears for carousel component
    Then I verify that carousel item is getting changed by clicking next button

  @tag3 @shopalyst
  Scenario: Shopalyst - Verify flow till Retailers page
    Given the "whitelabel" brand "shopalystpage" of "us/en" locale has been built
    When I click on Buy It Now Button
    Then I should be able to see Shopalyst retailers list
    When I click on any of the shopalyst retailers
    Then it should take user to corresponding shopalyst retailer page

  @tag4 @constantcommerce
  Scenario: Shopalyst - Verify flow till Retailers page
    Given the "dove" brand "productpage" of "uk" locale has been built
    When I click on Buy It Now Button
    Then I should be able to see ConstantCommerce retailers list
    When I click on any of the ConstantCommerce retailers
    Then it should open ConstantCommerce retailer window

  @tag5 @cartwire
  Scenario: Shopalyst - Verify flow till Retailers page
    Given the "dove" brand "productpage" of "au" locale has been built
    When I click on Buy It Now Button
    Then I should be able to see Cartwire retailers list
    When I click on any of the Cartwire retailers
    Then it should take user to corresponding Cartwire retailer page
    
    @tag64
    Scenario: PDP - VerifyAddToBag - AddToBagFunctiononProductPage
	Given the "tresemme" brand "productpage" of "us/en" locale has been built	
	When i see the structure of the "productpage"
	When i click on add to bag button
	Then i should be able to see add to bag dialog
	When i choose "Target" retailer in add to bag dialog
	And i choose "22 fl oz" size in add to bag dialog 
	And i choose "15" quantity in add to bag dialog
	And i click on add to bag button in add to bag dialog
	Then i should be able to see shopping bag updated
	When i click on shopping bag icon at header
	Then i should be able to see viewbag overlay
	And i click on viewbag button on viewbag overlay
	Then it should take me to a view bag page
	And i click on edit link on viewbag page
	Then i should be able to see edit bag pop up
	And i choose "8" quantity in edit bag pop up
	And i click on add to bag button in add to bag dialog
	Then i should be able to see shopping bag updated on viewbag page
	And i click on checkout button on viewbag page
	Then it should take me to a checkout page
	And i should be able to see checkout form
	And i enter first name as "test" in checkout form
	And i enter last name as "test" in checkout form
	And i enter addressLine1 as "test" in checkout form
	And i enter addressLine2 as "test" in checkout form
	And i enter city as "test" in checkout form
	And i select State as "Alabama" in checkout form
	And i enter zip code as "11211" in checkout form
	And i enter phone number as "9999999999" in checkout form
	And i enter email address as "a@b.com" in checkout form
	And i enter card holder name as "test" in checkout form
	And i enter card number as "4111111111111111" in checkout form
	And i select expiry month as "11 - November" in checkout form
	And i select expiry year as "2022" in checkout form
	And i enter verification code as "211" in checkout form
	And i check information and offers checkbox
	And i click on place order button	
	Then I expect page url contains "shoppable/confirmation.html"
  
