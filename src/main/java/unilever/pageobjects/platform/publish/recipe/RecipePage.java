package unilever.pageobjects.platform.publish.recipe;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class RecipePage extends AbstractPublishPage {
	
	
	public RecipePage(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public WebElement getRecipeTitleObject() {
		return webElementFactory.getElement("recipepage.title.text");		
	}
	
	public WebElement getRecipeDescriptionObject() {
		return webElementFactory.getElement("recipepage.description.text");		
	}
	
	public WebElement getRecipeRatingReview() {
		return webElementFactory.getElement("recipepage.ratingandreview.div");		
	}
	public String getRecipeTitleText() {
		return getRecipeTitleObject().getText();
	}
	
	public String getRecipeDescriptionText() {
		return getRecipeDescriptionObject().getText();	
	}
	
	public WebElement getShowNutritionalFacts() {
		return webElementFactory.getElement("recipepage.shownutritionalfacts.link");
	}
	public WebElement getRecipePageNutrientsTableHeading() {
		return webElementFactory.getElement("recipepage.pagenutrients.table.heading.text");
	}
}
