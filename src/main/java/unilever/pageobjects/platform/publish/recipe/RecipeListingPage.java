package unilever.pageobjects.platform.publish.recipe;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class RecipeListingPage extends AbstractPublishPage {
	
	//By recipeListing = By.cssSelector("div[data-role='recipe-listing']");
	
	
	public RecipeListingPage(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public WebElement getRecipeListingHeadingObject() {
		return webElementFactory.getElement("recipelisting.heading");
	}
	
	public WebElement getRecipeListingFilterButtonObject() {
		return webElementFactory.getElement("recipelisting.filter.button");
	}
	
	public List<WebElement> getRecipeListingItems() {
		return webElementFactory.getElements("recipelisting.listitem");
	}
	
	public List<WebElement> getRecipeFilterOptions(){
		return webElementFactory.getElements("recipelisting.filter.options");
	}
	
	public WebElement getRecipeFilterApplyButton() {
		return webElementFactory.getElement("recipelisting.filter.apply.button");
	}
	
	public int getRecipeListingItemsCountInHeading() {
		String strItemsCount = getRecipeListingHeadingObject().findElement(By.xpath(".//span")).getText();
		return Integer.parseInt(strItemsCount.trim());
	}
	

}
