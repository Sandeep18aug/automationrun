package unilever.pageobjects.platform.publish.common;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.util.Strings;

import framework.exceptions.ObjectNotFoundInORException;
import io.netty.util.internal.StringUtil;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

/**
 * This class deals with the global navigation functionality of the brand site
 * @author avigorka
 *
 */
public class GlobalNavigation extends AbstractPublishPage {




	public GlobalNavigation(WebDriver driver,String brandName) {
		super(driver, brandName);
	}


	public WebElement getGlobalSearchIcon() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalnavigation.search.icon");
	}

	public WebElement getGlobalSearchIcon(String brandName) {
		if (brandName.toLowerCase().contains("ponds"))
			return webElementFactory.getElement(By.cssSelector(".o-btn.o-btn--search-handler"));
		else
			return null;
	}

	/**
	 * This method is used to fetch the global search text box webelement.
	 * @return returns global search textbox webelement
	 * @throws ObjectNotFoundInORException
	 */
	public WebElement getGlobalSearchTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalnavigation.search.textbox", false);
	}


	public WebElement getGlobalSearchSuggestions() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalnavigation.search.suggestions", false);
	}

	public WebElement getSearchInvalidInputMessage() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalnavigation.search.sorryMessage", false);
	}

	public WebElement getGlobalSearchButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalnavigation.search.button");
	}


	public WebElement getGlobalSearchClose() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalnavigation.search.close.link");
	}

	public List<WebElement> getNavigationTabsList() {
		waitForPageReady();
		return waitDriver.until(ExpectedConditions.presenceOfAllElementsLocatedBy(webElementFactory.getByLocator("globalnavigation.tab.list"))).stream()
				.filter((elem) -> !Strings.isNullOrEmpty(elem.getText().trim())).collect(Collectors.toList());
		//return webElementFactory.getElements("globalnavigation.tab.list");
	}

	public void clickOnNavigationTab(String tabText) {
		waitForPageReady();
		WebElement navigationTab = getNavigationTabByText(tabText);
		if (!navigationTab.getAttribute("class").toLowerCase().contains("active")) {
			navigationTab.click();
		}
	}

	public WebElement getNavigationTabByText(String tabText) {
		try {
			waitForPageReady();
			return getNavigationTabsList().stream().filter((tab) -> tab.findElement(By.xpath(
					"./*[contains(@class,'o-navbar-label')] | (//div[@class='tabMenuItem component section default-style item_0'])[1] | ./a"))
					.getText().trim().equalsIgnoreCase(tabText.trim())).findFirst().get();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("exception occured in getting navigation tab with text " + tabText);
			return null;
		}
	}

	public List<WebElement> getSubMenuListOfNavigationTab(String tabText) {
		WebElement navigationTab = getNavigationTabByText(tabText);
		if (!navigationTab.getAttribute("class").toLowerCase().contains("active")) {
			navigationTab.click();
		}
		return navigationTab.findElements(webElementFactory.getByLocator("globalnavigation.submenu.list"));
	}

	public void clickOnSubMenuOfNavigationTab(String tabText, String subMenuText) {
		waitForPageReady();
//		List<WebElement> subMenuList = getSubMenuListOfNavigationTab(tabText);
//		for (WebElement subMenu : subMenuList) {
//			if (subMenu.getText().trim().equalsIgnoreCase(subMenuText)) {
//				subMenu.findElement(By.xpath("//*[normalize-space(text())='" + subMenuText + "']")).click();
//				break;
//			}
//		}
		getSubMenuOfNavigationTab(tabText, subMenuText)
				.findElement(By.xpath("//*[normalize-space(text())='" + subMenuText + "']")).click();
	}

	public WebElement getSubMenuOfNavigationTab(String tabText, String subMenuText) {
		waitForPageReady();
		WebElement subMenuTab = null;
		List<WebElement> subMenuList = getSubMenuListOfNavigationTab(tabText);
		for (WebElement subMenu : subMenuList) {
			if (subMenu.getText().trim().equalsIgnoreCase(subMenuText)) {
				subMenuTab = subMenu;
				break;
			}
		}
		return subMenuTab;
	}

	public WebElement getTier3SubMenu(String tier1, String tier2, String tier3) {
		waitForPageReady();
		// WebElement subMenuTab = getSubMenuOfNavigationTab(tier1, tier2);
		List<WebElement> tier3ElementsList = getTier3SubMenuList(tier1, tier2);
		for (WebElement tier3Element : tier3ElementsList) {
			if (tier3Element.getText().trim().equalsIgnoreCase(tier3)) {
				return tier3Element;
			}
		}
		return null;
	}

	public WebElement getTier3SubMenu(WebElement tier2Element, String tier3) {
		waitForPageReady();
		// WebElement subMenuTab = getSubMenuOfNavigationTab(tier1, tier2);
		List<WebElement> tier3ElementsList = tier2Element
				.findElements(By.cssSelector(".o-navbar-item.o-navbar-tier3-item"));
		for (WebElement tier3Element : tier3ElementsList) {
			if (tier3Element.getText().trim().equalsIgnoreCase(tier3)) {
				return tier3Element;
			}
		}
		return null;
	}

	public List<WebElement> getTier3SubMenuList(String tier1, String tier2) {
		waitForPageReady();
		WebElement subMenuTab = getSubMenuOfNavigationTab(tier1, tier2);
		List<WebElement> tier3ElementsList = subMenuTab
				.findElements(By.cssSelector(".o-navbar-item.o-navbar-tier3-item"));
		return tier3ElementsList;
	}

	public String getShoppingBagText() throws ObjectNotFoundInORException {
		waitForPageReady();
		String shoppingBagNumber = webElementFactory.getElement("globalnavigation.shoppingBag.icon", false).getText().trim();
		return shoppingBagNumber.isEmpty() ? "0" : shoppingBagNumber;

	}

}
