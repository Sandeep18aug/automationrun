package unilever.pageobjects.platform.publish.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class GlobalFooter extends AbstractPublishPage {

	public GlobalFooter(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public WebElement getsocialItemLink(WebElement element) throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.socialItem.link");
	}

	public List<WebElement> getSocialIconList() {
		return webElementFactory.getElements("globalfooter.socialIcon.list");
	}

	public WebElement getcountryselectorimage() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.countrySelectorImage");
	}

	public WebElement getContactusHeaderLink() {
		return webElementFactory.getClickableElement(webElementFactory.getByLocator("header.contact.link"),
				false);
	}
	
	public WebElement getContactusLink() {
		return webElementFactory.getClickableElement(webElementFactory.getByLocator("globalfooter.contactUs.link"),
				false);
	}
	
	public WebElement getContactmenulink() {
		return webElementFactory.getClickableElement(webElementFactory.getByLocator("globalfooter.contactUs.menu.link"),
				false);
	}
	
	public WebElement getcontactlink() {
		return webElementFactory.getClickableElement(webElementFactory.getByLocator("contactus.form.link"),
				false);
	}

	public WebElement getHelpCenter() {
		return webElementFactory.getClickableElement(webElementFactory.getByLocator("globalfooter.HelpCenter.link"),
				false);
	}

	public WebElement getunilverlogo() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.unilverLogo");
	}

	public WebElement getcopyrighticon() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.copyrightIcon");
	}

	public WebElement getdisclaimer() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.disclaimer.link");
	}

	public WebElement getbrandlogo() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.brandLogo");
	}

	public WebElement getFBLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.socialshare.facebook.link");
	}

	public WebElement getInstagramLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.socialshare.instagram.link");
	}

	public WebElement getTwitterLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.socialshare.twitter.link");
	}

	public WebElement getYoutubeLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.socialshare.youtube.link");

	}

	public WebElement getPinterestLink() {
		return webElementFactory.getElement("globalfooter.socialshare.pinterest.link");
	}

	public WebElement getCountrySelector() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.countrySelector.link");
	}

	public WebElement getSiteMapLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.siteMap.link", false);
	}

	public WebElement gettermsofuseLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.termsOfUse.link", false);
	}

	public WebElement getSignUpLink() {

		return webElementFactory.getElement("globalfooter.signup.link", false);

	}

	public WebElement getcookiePolicyLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.cookiePolicy.link", false);
	}

	public WebElement getfaqSLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.faqS.link", false);
	}

	public WebElement getprivacyPolicyLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.privacyPolicy.link", false);
	}

	public WebElement getAdChoiceLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.adChoice.link", false);
	}

	public WebElement getAdChoicePopup() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.adChoicePopup", false);
	}

	public WebElement getStoreLocatorLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.storeLocator.link", false);
	}

	public WebElement getconsulationLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("globalfooter.consulation.link", false);
	}

	public WebElement getAssesibilityLinkFooter() {
		return webElementFactory.getElement("globalfooter.accessibilityTazo", false);
	}

	public void clickSiteMap() throws ObjectNotFoundInORException {

		getSiteMapLink().click();
	}

	public void clickCountrySelector() throws ObjectNotFoundInORException {
		getCountrySelector().click();
	}

	public List<WebElement> getFooterNavigationList() {
		return webElementFactory.getElements("globalfooter.footerNavList");
	}

	public WebElement getFooterNavigationLinkByText(String text) {
		try {
			return getFooterNavigationList().stream().filter(x -> x.getText().trim().equals(text)).findFirst().get();
		} catch (Exception e) {
			return null;
		}
	}

	public void selectFooterNavItemByText(String text) {
		getFooterNavigationList().stream().filter(x -> x.getText().toLowerCase().contains(text.toLowerCase()))
				.findFirst().get().click();
	}

}
