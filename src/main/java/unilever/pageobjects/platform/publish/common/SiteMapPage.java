package unilever.pageobjects.platform.publish.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class SiteMapPage extends AbstractPublishPage {

	private By DIV_SiteMapComponent = By.cssSelector("div[data-role='"+ComponentsDataRole.SITEMAP+"']");
	private By HeadingSiteMapComponent = By.xpath(".//h1");

	public SiteMapPage(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public boolean isPresent() {
		waitForPageReady();
		return webElementFactory.getElement(DIV_SiteMapComponent) != null;
	}
	
	public String getSiteMapHeading() {
		waitForPageReady();
		return webElementFactory.getElement(DIV_SiteMapComponent).findElement(HeadingSiteMapComponent).getText();
	}
}
