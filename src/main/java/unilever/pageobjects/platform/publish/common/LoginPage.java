package unilever.pageobjects.platform.publish.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class LoginPage extends AbstractPublishPage {

	public LoginPage(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public void loginAs(String email, String password) throws Throwable {
		waitForPageReady();
		acceptCookiesIfPresent();
		webElementFactory.getElement("loginpage.email").sendKeys(email);
		webElementFactory.getElement("loginpage.password").sendKeys(password);
		webElementFactory.getElement("loginpage.submit").click();
		waitForPageReady();
	}

	public void loginThroughGlobalNavLogicIconAs(String email, String password) throws Throwable {
		waitForPageReady();
		acceptCookiesIfPresent();
		waitForPageReady();
		webElementFactory.getElement("loginpage.header.loginicon").click();
		driver.switchTo().frame(webElementFactory.getElement("loginpage.header.secureloginiframe", false));
		webElementFactory.getElement("loginpage.header.emailAddress").sendKeys(email);
		webElementFactory.getElement("loginpage.header.password").sendKeys(password);
		webElementFactory.getElement("loginpage.header.secureloginiframe").click();
		waitForPageReady();
		driver.switchTo().defaultContent();
	}

	public WebElement getGetNewPasswordButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("loginpage.newpassword.button");
	}

	public WebElement getThankyouMessage() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("loginpage.thankyou");
	}
	
	public WebElement getEmailTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("loginpage.email");
	}

}
