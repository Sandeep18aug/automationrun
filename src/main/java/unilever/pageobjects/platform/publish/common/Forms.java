package unilever.pageobjects.platform.publish.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class Forms extends AbstractPublishPage {

	By submitButtonBy = By.cssSelector("button[class*='c-form-submit-btn input-submit']");
	By emailAddressErrMsg = By.cssSelector("input[placeholder='Email Address']+span");
	By firstNameErrMsg = By.cssSelector("input[id*='givenName']+span");
	By lastNameErrMsg = By.cssSelector("input[id*='familyName']+span");
	By addressErrMsg = By.cssSelector("input[id*='streetAddress1']+span");
	By cityErrMsg = By.cssSelector("input[id*='locality']+span");
	By ageConsentErrMsg = By.cssSelector("input[id*='legalAgeConfirmation']+span");
	By lnkSignUpBy = By.cssSelector("a[class='call-to-action__link']");
	By titleSignUpBy = By.cssSelector("h1[class='rich-text-heading']");
	By signUpHeading = By.cssSelector("div[class*='linkclick'] h1");
	By emailAddressTextBox = By.cssSelector("input[id='contact-email'],input#email");
	By enquiryType = By.cssSelector("select[id='contactUs-inquiryType']");
	By genderTextBox = By.cssSelector("select[id='contact-gender']");
	By firstNameTextBox = By.cssSelector("input[id='contact-givenName'],input[id='givenName']");
	By lastNameTextBox = By.cssSelector("input[id='contact-familyName'],input[id='familyName']");
	By prefixTextBox = By.cssSelector("#contact-honorificPrefix");
	By countryTextBox = By.cssSelector("select[id='contact-country'i]");
	By address1TextBox = By.cssSelector("input[id='contact-streetAddress1'i]");
	By address2TextBox = By.cssSelector("input[id='contact-streetAddress2'i]");
	By localityTextBox = By.cssSelector("input[id='contact-locality'i]");
	// Modified css selector for postalCodeTextBox for lhp by Abhishek on 21-Dec
//	By postalCodeTextBox = By.cssSelector("input[id='contact-postalcode']");
	By postalCodeTextBox = By.cssSelector("input[id='contact-postalcode'i],input[id='postalCode'i]");
	// added selectors for birthday and captcha text box
	By birthdayBox = By.cssSelector("input[id='contact-birthday'i]");
	By CHK_CaptchaTextBox = By.cssSelector("div[class*='recaptcha-checkbox-checkmark']");

	By regionTextBox = By.cssSelector("select[id='contact-region'i]");
	By selectStateDropdown = By.cssSelector("select[id='contact-region'i]");
	By phoneNumberBox = By.cssSelector("input[id='contact-phoneNumbers-0-value'i]");
	By datePicker = By.cssSelector("input[id='contactUs-datePurchased'i]");
	By DateOfBirth = By.cssSelector("input[id='contact-birthday'],input[id='birthday'i]");
	By ZipCodeTextBox = By.cssSelector("input[id='contact-postalCode'i]");

	// Product Details
	By barCodeAvailabilityBox = By.cssSelector("select[id='contactUs-upcCodeDetail']");
	By barCodeBox = By.cssSelector("input[id='contactUs-upcCode']");
	By productBox = By.cssSelector("input[id='contactUs-product']");
	By sizeBox = By.cssSelector("input[id='contactUs-size']");
	By expiryDate = By.cssSelector("input[id='contactUs-expiryDate']");
	By manufacturingCode = By.cssSelector("input[id='contactUs-manufacturingCode']");
	By locationPurchased = By.cssSelector("input[id='contactUs-townPurchasedFrom']");
	By manufacturingCodeAvailabilityBox = By.cssSelector("#contactUs-manufacturingCodeDetail");
	By storeName = By.cssSelector("input[id='contactUs-storeNamePurchasedFrom']");
	By dateOfPurchase = By.cssSelector("input[id='contactUs-datePurchased']");
	By queryTextArea = By.cssSelector("textarea[id='contactUs-comments']");
	By CHK_ageConsent = By.cssSelector("input[name='contact-legalAgeConfirmation']");
	By CHK_brandOptIn = By.cssSelector("input[name='optIn-brand']");
	By CHK_corporateOptIn = By.cssSelector("input[name='optIn-corporate']");
	By CHK_byEmailBrandOptin = By.cssSelector("input[name='optIn-onlineBrand']");
	By CHK_byEmailAllOptin = By.cssSelector("input[name='optIn-onlineAll']");
	By CHK_bySmsBrandOptin = By.cssSelector("input[name='optIn-smsBrand']");
	By CHK_bySmsAllOptin = By.cssSelector("input[name='optIn-smsAll']");
	By CHK_byMailBrandOptin = By.cssSelector("input[name='optIn-mailBrand']");
	By CHK_byMailAllOptin = By.cssSelector("input[name='optIn-mailAll']");
    By contactUsHeading = By.cssSelector("div[class*='linkclick'] h1");
    By SignupPopUpbtn= By.cssSelector("button[class='o-btn--close js-btn-close js-content-panel-close']"); 

    public Forms(WebDriver driver,String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}
	
	

	public WebElement getCloseButtonForms() {
		return webElementFactory.getElement(SignupPopUpbtn);
	}

	public void enterEmailAddress(String email) {
		webElementFactory.getElement(emailAddressTextBox).sendKeys(email);
		System.out.println("successfully entered email");

	}

	public void selectTitle(String title) {
		webElementFactory.getElement(prefixTextBox).isSelected();
		System.out.println("successfully selected Title");
	}

	public void selectPrefix(String prefix) {
		webElementFactory.getElement(prefixTextBox).sendKeys(prefix);
		System.out.println("successfully entered prefix");
	}

	public void selectRegionTextBox(String Country) {
		Select select = new Select(webElementFactory.getElement(regionTextBox));
		select.selectByVisibleText(Country);
		System.out.println("successfully selected Region");
	}

	public void selectGenderTextBox(String Gender) {
		Select select = new Select(webElementFactory.getElement(genderTextBox));
		select.selectByVisibleText(Gender);
		System.out.println("successfully selected Gender");
	}

	public void selectEnquiryType(String EnquirType) {
		// webElementFactory.getElement(enquiryType).isSelected();
		Select select = new Select(webElementFactory.getElement(enquiryType));
		select.selectByVisibleText(EnquirType);
		System.out.println("successfully selected enquiry Type");
	}

	public void selectUPCDeatils(String UPCAvailability) {
		Select select = new Select(webElementFactory.getElement(barCodeAvailabilityBox));
		select.selectByVisibleText(UPCAvailability);
		System.out.println("successfully selected enquiry Type");
	}

	public void selectState(String State) {
		Select select = new Select(webElementFactory.getElement(selectStateDropdown));
		select.selectByVisibleText(State);
		System.out.println("successfully selected enquiry Type");
	}

	public void enterFirstName(String name) {
		webElementFactory.getElement(firstNameTextBox).sendKeys(name);
		System.out.println("successfully entered first Name");
	}

	public void enterLastName(String name) {
		webElementFactory.getElement(lastNameTextBox).sendKeys(name);
		System.out.println("successfully entered last Name");
	}

	public void enterAddress1(String name) {
		webElementFactory.getElement(address1TextBox).sendKeys(name);
		System.out.println("successfully entered in address 01");
	}

	public void enterAddress2(String name) {
		webElementFactory.getElement(address2TextBox).sendKeys(name);
		System.out.println("successfully entered in address 02");
	}

	public void enterCity(String name) {
		webElementFactory.getElement(localityTextBox).sendKeys(name);
		System.out.println("successfully entered locality");
	}

	public void enterPostalCode(String num) {
		webElementFactory.getElement(postalCodeTextBox).sendKeys(num);
		System.out.println("successfully entered postal Code");
	}

	public void enterZipCode(String num) {
		webElementFactory.getElement(ZipCodeTextBox).sendKeys(num);
		System.out.println("successfully entered Zip Code");
	}

	public void selectCountry(String country) {
		Select select = new Select(webElementFactory.getElement(countryTextBox));
		select.selectByVisibleText(country);
		System.out.println("successfully selected country");
	}

	public void enterContactNumber(String num) {
		webElementFactory.getElement(phoneNumberBox).sendKeys(num);
		System.out.println("successfully entered phoneNumber");
	}

	public void enterDatePicker(String num) {
		webElementFactory.getElement(datePicker).sendKeys(num);
		System.out.println("successfully entered date");
	}

	public void enterDateOfBirth(String Date) {
		webElementFactory.getElement(DateOfBirth).sendKeys(Date);
		System.out.println("successfully entered date of birth");
	}

	public void enterExpiryDate(String ExpiryDate) {
		webElementFactory.getElement(expiryDate).sendKeys(ExpiryDate);
		System.out.println("successfully entered Expiry Date");
	}

	public void enterDateOfPurchase(String DateOfPurchase) {
		webElementFactory.getElement(dateOfPurchase).sendKeys(DateOfPurchase);
		System.out.println("successfully entered DateOfPurchase");
	}

	// added birthday and captch click code for lhp brand by Abhishek on 21st-Dec
	public void enterBirthDay(String num) {
		webElementFactory.getElement(birthdayBox).sendKeys(num);
		System.out.println("successfully entered birthday");
	}

	public void checkcapcthaCheckBox() {
		WebElement check = webElementFactory.getElement(CHK_CaptchaTextBox);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked captcha text box");
		} else {
			System.out.println("captcha text checkbox was already selected");
		}
	}

	public void checkAgeConsentCheckBox() {
		WebElement check = webElementFactory.getElement(CHK_ageConsent);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked age consent checkbox");
		} else {
			System.out.println("age consent checkbox was already selected");
		}
	}

	public void checkBrandOptInCheckBox() {
		WebElement check = webElementFactory.getElement(CHK_brandOptIn);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked brandOptIn checkbox");
		} else {
			System.out.println("brandOptIn checkbox was already selected");
		}
	}

	public void checkCorporateOptInCheckBox() {
		WebElement check = webElementFactory.getElement(CHK_corporateOptIn);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked corporateOptIn checkbox");
		} else {
			System.out.println("corporateOptIn checkbox was already selected");
		}

	}

	public void checkMailCheckBox() {
		WebElement check = webElementFactory.getElement(CHK_byMailAllOptin);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked all mail checkbox");
		} else {
			System.out.println("All Mail checkbox was already selected");
		}

	}

	public void checkByEmailBrandOptin() {
		WebElement check = webElementFactory.getElement(CHK_byEmailBrandOptin);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked Email Brand Optin checkbox");
		} else {
			System.out.println("Email Brand Optin checkbox was already selected");
		}

	}

	public void checkByEmailAllOptin() {
		WebElement check = webElementFactory.getElement(CHK_byEmailAllOptin);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked Email All Optin checkbox");
		} else {
			System.out.println("Email All Optin checkbox was already selected");
		}

	}

	public void checkbySmsBrandOptin() {
		WebElement check = webElementFactory.getElement(CHK_bySmsBrandOptin);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked SMS Brand Optin checkbox");
		} else {
			System.out.println("SMS Brand Optin checkbox was already selected");
		}

	}

	public void checkbySmsAllOptin() {
		WebElement check = webElementFactory.getElement(CHK_bySmsAllOptin);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked SMS all Optin checkbox");
		} else {
			System.out.println("SMS all Optin checkbox was already selected");
		}

	}

	public void checkByMailBrandOptin() {
		WebElement check = webElementFactory.getElement(CHK_byMailBrandOptin);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked Mail brand Optin checkbox");
		} else {
			System.out.println("Mail brand Optin checkbox was already selected");
		}

	}

	public void checkByMailAllOptin() {
		WebElement check = webElementFactory.getElement(CHK_byMailAllOptin);
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked Mail All Optin checkbox");
		} else {
			System.out.println("Mail All Optin checkbox was already selected");
		}

	}

	public WebElement SignUplinkvisible() {
		return webElementFactory.getElement(lnkSignUpBy);
	}

	public WebElement titleSignUplinkvisible() {
		return webElementFactory.getElement(lnkSignUpBy);
	}

	public WebElement signUpHeadingvisible() {
		return webElementFactory.getElement(signUpHeading);
	}

	public WebElement emailAddErrMsgvisible() {
		return webElementFactory.getElement(emailAddressErrMsg);
	}

	public WebElement firstnameErrMsgvisible() {
		return webElementFactory.getElement(firstNameErrMsg);
	}

	public WebElement lastnameErrMsgvisible() {
		return webElementFactory.getElement(lastNameErrMsg);
	}

	public WebElement addressErrMsgvisible() {
		return webElementFactory.getElement(addressErrMsg);
	}

	public WebElement cityErrMsgvisible() {
		return webElementFactory.getElement(cityErrMsg);
	}

	public WebElement ageConsentErrMsgvisible() {
		return webElementFactory.getElement(ageConsentErrMsg);
	}

	public WebElement getSubmitButton() {
		return webElementFactory.getElement(submitButtonBy);
	}

	public WebElement getContactUsHeading() {
		return webElementFactory.getElement(contactUsHeading);
	}

	public void selectBarCodeAvailabilityBox(String barCode) {
		webElementFactory.getElement(barCodeAvailabilityBox).isSelected();
		System.out.println("successfully selected bar Code Availability Box");
	}

	public void enterBarCodeBox(String code) {
		webElementFactory.getElement(barCodeBox).sendKeys(code);
		System.out.println("successfully entered bar/UPC code");

	}

	public void enterProductBox(String num) {
		webElementFactory.getElement(productBox).sendKeys(num);
		System.out.println("successfully entered productBox");

	}

	public void enterSizeBox(String num) {
		webElementFactory.getElement(sizeBox).sendKeys(num);
		System.out.println("successfully entered size Box");

	}

	public void enterManufacturingCode(String ManufacturingCode) {
		webElementFactory.getElement(manufacturingCode).sendKeys(ManufacturingCode);
		System.out.println("successfully entered manufacturing Code");

	}

	public void enterLocationPurchased(String num) {
		webElementFactory.getElement(locationPurchased).sendKeys(num);
		System.out.println("successfully entered location Purchased");

	}

	public void selectManufacturingCodeAvailability(String selectManufacturingCodeAvailability) {
		Select select = new Select(webElementFactory.getElement(manufacturingCodeAvailabilityBox));
		select.selectByVisibleText(selectManufacturingCodeAvailability);
		System.out.println("successfully selected Manufacturing Code Availability");

	}

	public void enterStoreName(String StoreName) {
		webElementFactory.getElement(storeName).sendKeys(StoreName);
		System.out.println("successfully entered store Name");

	}

	public void enterQuery(String name) {
		webElementFactory.getElement(queryTextArea).sendKeys(name);
		System.out.println("successfully entered query");

	}
}
