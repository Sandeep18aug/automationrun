package unilever.pageobjects.platform.publish.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class StoreLocatorPage extends AbstractPublishPage {

	By DIV_StoreSearch = By.cssSelector("div[data-role='"+ComponentsDataRole.BUY_IN_STORE_SEARCH+"']");
	By DIV_StoreSearchResult = By.cssSelector("div[data-role='"+ComponentsDataRole.STORE_SEARCH_RESULTS+"']");

	public StoreLocatorPage(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public List<WebElement> getCategoryListOfButtons() {
		return webElementFactory.getElements("storesearch.result.category.button");
	}

	public WebElement getStoreSearchComponent() {
		return webElementFactory.getElement(DIV_StoreSearch, false);
	}

	public Select getCategoryListbox() throws ObjectNotFoundInORException {
		return new Select(webElementFactory.getElement("storesearch.category.listbox", false));
	}

	public Select getSubCategoryListbox() throws ObjectNotFoundInORException {
		return new Select(webElementFactory.getElement("storesearch.subcategory.listbox", false));
	}

	public List<WebElement> getProductsList() {
		return webElementFactory.getElements("storesearch.list.items");

	}

	public WebElement getPostalCodeEditbox() throws ObjectNotFoundInORException {
		WebElement postalCode = webElementFactory.getElement("storesearch.postcode.textbox");
		return waitDriver.until(ExpectedConditions.visibilityOf(postalCode));

	}

	public Select getFindStoreWithInListbox() throws ObjectNotFoundInORException {
		return new Select(webElementFactory.getElement("storesearch.findstorewithin.listbox", false));
	}

	public WebElement getFindStoreButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("storesearch.findastore_button", false);
	}

	public WebElement getRefreshStoreButton() {
		By BTN_Refresh_StoreSearch = By.cssSelector(".c-buy-in-store-search-result__refresh__button");
		return webElementFactory.getElement(BTN_Refresh_StoreSearch, false);
	}
	
	public WebElement getStoreResultRefreshButton() {
		return webElementFactory.getElementWithInParent(DIV_StoreSearchResult, webElementFactory.getByLocator("storesearch.result.refresh.button"));
	}

	public List<WebElement> getStoreSearchResultList() {
		return webElementFactory.getElements("storesearch.result.list.item");
	}

	public WebElement getMap() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("storesearch.result.map.div", false);
	}

	public WebElement getStoreBrowseSearchTabByText(String tabText) {
		List<WebElement> tabList = webElementFactory.getElements("storesearch.result.browsetabs.list.item");
		return tabList.stream().filter(x -> x.getText().trim().equalsIgnoreCase(tabText.trim())).findFirst().get();
	}

	public void selectProductFromList(WebElement element) {
		WebElement elemToBeClicked = null;
		try {
			elemToBeClicked = element.findElement(By.xpath(".//label"));
			if (elemToBeClicked != null) {
				elemToBeClicked.click();
				return;
			}
		} catch (Exception ex) {
		}
		try {
			elemToBeClicked = element.findElement(By.xpath(".//input[contains(@class,'choose-variant')]"));
			if (elemToBeClicked != null) {
				elemToBeClicked.click();
				return;
			}
		} catch (Exception ex) {
		}
		try {
			elemToBeClicked = element.findElement(By.xpath(".//a"));
			if (elemToBeClicked != null) {
				elemToBeClicked.click();
				return;
			}
		} catch (Exception ex) {
		}

	}

}
