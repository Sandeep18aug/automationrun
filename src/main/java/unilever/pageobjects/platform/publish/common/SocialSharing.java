package unilever.pageobjects.platform.publish.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class SocialSharing extends AbstractPublishPage {

	private By socialsharingComponentDivBy = By
			.cssSelector("div[data-role='" + ComponentsDataRole.SOCIAL_SHARING + "']");

	
	public SocialSharing(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public boolean isPresent() {
		waitForPageReady();
		return webElementFactory.getElement(socialsharingComponentDivBy) != null;
	}

	public WebElement getsocialItemLink(WebElement element) {
		return webElementFactory.getElement(socialsharingComponentDivBy);
	}
	
	public WebElement socialsharingChannels() {
		waitForPageReady();
		return webElementFactory.getElementWithInParent(socialsharingComponentDivBy, webElementFactory.getByLocator("socialsharing.channels"));
	}
	
	public boolean isSocialMediaPageDisplayed(String baseURL) {
		boolean isDisplayed = false;
		if (baseURL.contains("facebook.com"))
			isDisplayed = webElementFactory.isElementPresentOnPage(webElementFactory.getByLocator("facebook.page.identifier"));
		else if (baseURL.contains("twitter.com"))
			isDisplayed = webElementFactory.isElementPresentOnPage(webElementFactory.getByLocator("twitter.page.identifier"));
		else if (baseURL.contains("instagram.com"))
			isDisplayed = webElementFactory.isElementPresentOnPage(webElementFactory.getByLocator("instagram.page.identifier"));
		else if (baseURL.contains("youtube.com"))
			isDisplayed = webElementFactory.isElementPresentOnPage(webElementFactory.getByLocator("youtube.page.identifier"));
		return isDisplayed;
	}
}
