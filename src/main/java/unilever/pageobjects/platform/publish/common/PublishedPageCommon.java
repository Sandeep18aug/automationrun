package unilever.pageobjects.platform.publish.common;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import framework.exceptions.ObjectNotFoundInORException;
import steps.generic.keywords.Click;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class PublishedPageCommon extends AbstractPublishPage {

	SoftAssert softAssert = null;


	public PublishedPageCommon(WebDriver driver, String brandName) {

		super(driver, brandName);
		softAssert = new SoftAssert();
	}
	
	public WebElement OlpaicSlidervisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("olapic.slider");
	}

	public WebElement getbreadCrumbLinkVisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("breadcrumb.link");
	}
	
	
	public WebElement getreturnHomepageButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("returntohome.button");
	}

	public WebElement DOBErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.birthday.error.message");
	}

	public WebElement enquiryMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.enquirymsg.error.message");
	}
	
	
	public WebElement emailAddErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.emailaddress.error.message");
	}
	
	public WebElement firstnameErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.firstname.error.message");
	}

	public WebElement lastnameErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.lastname.error.message");
	}

	public WebElement addressErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.address.error.message");
	}

	public WebElement cityErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.city.error.message");
	}

	public WebElement ageConsentErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.ageconsent.error.message");
	}

	public WebElement contactNumberErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.contactnumber.error.message");
	}

	public WebElement barCodeErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.barcode.error.message");
	}

	public WebElement postalCodeErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.postalcode.error.message");
	}

	public WebElement zipCodeErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.zipcode.error.message");
	}

	public WebElement birthDateErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.birthday.error.message");

	}

	public WebElement productErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.product.error.message");
	}

	public WebElement manufacturingCodeErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.manufacturingcode.error.message");
	}

	public WebElement queryErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.comment.error.message");
	}

	public WebElement questionErrMsgvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.enterQuestion.error.message");
	}

	public WebElement getSignUpLink() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("signup.link");
	}
	

	public boolean isSignUpLinkVisible() throws ObjectNotFoundInORException {
		return webElementFactory.isElementVisibleOnPage("signup.link");
	}
	

	public WebElement contactUslinkvisible() {

		return webElementFactory.getElement("globalnavigation.contactus.link");

	}
	public WebElement smartLabellinkvisible() {

		return webElementFactory.getElement("globalnavigation.smartlabel.link");
	}
	
	public WebElement VerifySmartLabelPage() {
		return webElementFactory.getElement("smartlabel.title");

	}


	public WebElement getSubmitButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("form.submit.button");
	}

	public WebElement titleSignUplinkvisible() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("signup.link");
	}

	public WebElement getVideoIconButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("video.icon",true);
	}
	
	public void clickVideoIconButton() throws ObjectNotFoundInORException {
		WebElement videoIcon = getVideoIconButton();
		Actions actions = new Actions(driver);
		actions.moveToElement(videoIcon).pause(Duration.ofSeconds(1)).click().build().perform();
	}

	public WebElement getVideoIFrame() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("video.frame", false);
	}

	public void switchToVideoIframe() throws ObjectNotFoundInORException {
		driver.switchTo().frame(getVideoIFrame());
	}

	public WebElement getVideoObject() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("video");
	}
//	public WebElement getVideoObjectForPersil() {
//		return webElementFactory.getElement("video.div");
//	}

	public WebElement getVideoObjectDiv() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("video.div");
	}

	public WebElement getCloseButtonForSignUpPopup() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("email.signup.close");

	}

	public WebElement getCloseButtonForFeedbackPopup() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("feedback.popup.cross.button");

	}
	
	public int getExpectedRowCount(int totalItem) {
		int rowCount = 0;
		if (totalItem % 3 == 0)
			rowCount = totalItem / 3;
		else
			rowCount = (totalItem / 3) + 1;
		return rowCount;
	}
	
	public int splitStringAndGetMyNumber(String largeStr, String splitCharacter, int nthString) {
		int number = 0;
		try {
			String[] strs = largeStr.split(splitCharacter);
			String myString = strs[nthString - 1];
			number = Integer.parseInt(String.valueOf(myString.charAt(myString.length() - 1)));
		} catch(NumberFormatException e) {
			Assert.fail("The character can't be converted to number.");
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		return number;
	}
	
	public int[] getCountOfRowColumn(List<WebElement> elements) {
		List<Integer> numbers = new ArrayList<Integer>();
		int rowCount = 0; 
		int columnCount = 0;
		if (elements.size() > 0) {
			for (WebElement item : elements)
				numbers.add(splitStringAndGetMyNumber(item.getAttribute("class").trim(), " ", 7));
			Map<Integer, Integer> map = new HashMap<Integer, Integer>();

			for (Integer i : numbers) { 
	            Integer j = map.get(i); 
	            map.put(i, (j == null) ? 1 : j + 1); 
	        }
		    rowCount = map.keySet().size();
		    int counter = 0;
		    boolean hasMaxColExceeded = false;
		    for (Map.Entry<Integer, Integer> val : map.entrySet()) {
		    	counter ++;
		    	if (val.getValue() > 3) {
		    		hasMaxColExceeded = true;
		    		softAssert.fail("There are more than 3 columns of items for row " + counter + " instead of maximum 3 columns in each row.");
		    	}		    		
		    }
		    if (hasMaxColExceeded)
		    	softAssert.assertAll();
		    else 
		    	columnCount = map.entrySet().iterator().next().getValue();
		} else 
			Assert.fail("There is no item in the list.");
		return new int[] {rowCount, columnCount};
	}
}
