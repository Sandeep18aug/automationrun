package unilever.pageobjects.platform.publish.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class LiveChat extends AbstractPublishPage {

	private By liveChatV2ComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.LIVE_CHAT_V2 + "']");

	public LiveChat(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public boolean isPresent() {
		waitForPageReady();
		return webElementFactory.getElement(liveChatV2ComponentDivBy) != null;
	}

	public WebElement getliveChatV2ComponentHeading() {
		waitForPageReady();
		return webElementFactory.getElementWithInParent(liveChatV2ComponentDivBy,
				webElementFactory.getByLocator("livechat.heading"));
	}

	public boolean getliveChatV2ComponentWindow() {
		waitForPageReady();
		webElementFactory
				.getElementWithInParent(liveChatV2ComponentDivBy, webElementFactory.getByLocator("livechat.heading"))
				.click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		return webElementFactory.getElementWithInParent(liveChatV2ComponentDivBy,
				webElementFactory.getByLocator("livechat.window")) != null;
	}

	public String getliveChatV2ComponentOnlineMessage() {
		waitForPageReady();
		return webElementFactory.getElementWithInParent(liveChatV2ComponentDivBy,
				webElementFactory.getByLocator("livechat.onlinemessage")).getText();
	}

	public String getliveChatV2ComponentOfflineMessage() {
		waitForPageReady();
		return webElementFactory.getElementWithInParent(liveChatV2ComponentDivBy,
				webElementFactory.getByLocator("livechat.offlinemessage")).getText();
	}

}
