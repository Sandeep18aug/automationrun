package unilever.pageobjects.platform.publish.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class CountrySelector extends AbstractPublishPage {

	By countrySelectorComponentDiv = By.cssSelector("div[data-role='" + ComponentsDataRole.COUNTRY_SELECTOR + "']");

	public CountrySelector(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public WebElement getCountrySelectorComponent() {
		return webElementFactory.getElement(countrySelectorComponentDiv, false);
	}

	public WebElement getCountrySelectorInput() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("countryselector.input", false);
	}

	public WebElement getCountrySelectorSearchButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("countryselector.search.button", false);
	}

	public WebElement getCountryObject(String countryText) {
		List<WebElement> countryList = getCountryList();
		WebElement country = null;
		try {
			// countryList.forEach(x-> System.out.println(x.getAttribute("textContent")));
			country = countryList.stream()
					.filter(x -> x.getAttribute("textContent").trim().equalsIgnoreCase(countryText)).findFirst().get();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return country;
	}

	public List<WebElement> getCountryList() {
		return webElementFactory.getElements("countryselector.list");

	}

	public List<WebElement> getCountrySuggestionList() {
		return webElementFactory.getElements("countryselector.suggestionList");

	}

	public void enterCountrySuggestion(String countrysuggestion) throws ObjectNotFoundInORException {
		WebElement countrySuggestion = webElementFactory.getElement("countryselector.input");
		countrySuggestion.click();
		countrySuggestion.sendKeys(countrysuggestion);
		System.out.println("successfully entered text");
	}

}
