package unilever.pageobjects.platform.publish.brandspecific;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class CWLoginAndRegistrationPage extends AbstractPublishPage {
	
	
	public CWLoginAndRegistrationPage(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public WebElement getLoginButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.loginpage.loginButton");
	}
	public WebElement getFirstNameTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.registrationpage.firstName");
	}
	
	public WebElement getLastNameTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.registrationpage.lastName");
	}
	public WebElement getEmailTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.loginpage.email");
	}
	public WebElement getPasswordTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.loginpage.password");
	}
	public WebElement getConfirmPasswordTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.registrationpage.confirmPassword");
	}
	public WebElement getRoleTypeListBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.registrationpage.roleType");
	}
	public WebElement getTermsOfUseCheckBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.registrationpage.termsOfUse");
	}
	public WebElement getSendRequestButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.registrationpage.sendRequest");
	}
	public WebElement getCancelButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.registrationpage.cancel");
	}
	
	public WebElement getGetNewPasswordButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.loginpage.newpassword.button");
	}
	
	public WebElement getThankyouMessage() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.loginpage.thankyou");
	}
	
	
}
