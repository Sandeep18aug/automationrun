package unilever.pageobjects.platform.publish.brandspecific;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class Dove extends AbstractPublishPage {
		
	public Dove(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

}
