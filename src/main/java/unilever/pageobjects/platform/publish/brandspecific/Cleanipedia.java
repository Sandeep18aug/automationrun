package unilever.pageobjects.platform.publish.brandspecific;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class Cleanipedia extends AbstractPublishPage {
		
	public Cleanipedia(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public WebElement getSearchTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cw.search");
	}
	
	public WebElement getCloseButtonForCleanipediaPopUp() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("cleanipedia.banner.close");
	}
	
	public WebElement getSearchListItemImagecleanipedia(WebElement element) {
		return element.findElement(By.cssSelector("div[class*='c-search-listing-v2__item-image']"));
	}


}
