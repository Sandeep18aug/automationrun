package unilever.pageobjects.platform.publish.brandspecific;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class Tresemme extends AbstractPublishPage {
		
	public Tresemme(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public WebElement getSearchListingTresemmeCTA() {
		return webElementFactory.getElement("searchlisting.tresemme.cta");
	}
}
