package unilever.pageobjects.platform.publish.brandspecific;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class DQMSite extends AbstractPublishPage {
		
	public DQMSite(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public WebElement getSearchTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.search");
	}
	
	public void searchText(String text) throws ObjectNotFoundInORException {
		WebElement search = getSearchTextBox();
		search.sendKeys(text);
		search.sendKeys(Keys.ENTER);
	}

}
