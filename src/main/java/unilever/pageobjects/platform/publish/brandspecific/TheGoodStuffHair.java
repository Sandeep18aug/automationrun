package unilever.pageobjects.platform.publish.brandspecific;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class TheGoodStuffHair extends AbstractPublishPage {
		
	public TheGoodStuffHair(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public List<WebElement> getRetailerList() {
		return webElementFactory.getElements("pdp.addtobag.retailer.list.item");
	}
	
	public void clickRetailerOptionByText(String retailer) {
		getRetailerList().stream().filter((option)-> option.getText().trim().equalsIgnoreCase(retailer)).findFirst().get().click();
		
	}
	
	public List<WebElement> getQtyList() {
		return webElementFactory.getElements("pdp.addtobag.qty.list.item");
	}
	
	public void clickQtyOptionByText(String qty) {
		getQtyList().stream().filter((option)-> option.getText().trim().equalsIgnoreCase(qty)).findFirst().get().click();
		
	}
	
}
