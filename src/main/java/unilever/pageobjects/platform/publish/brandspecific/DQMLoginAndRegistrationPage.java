package unilever.pageobjects.platform.publish.brandspecific;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class DQMLoginAndRegistrationPage extends AbstractPublishPage {
	
	
	public DQMLoginAndRegistrationPage(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public WebElement getLoginButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.loginpage.loginButton");
	}
	public WebElement getFirstNameTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.registrationpage.firstName");
	}
	public WebElement getLastNameTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.registrationpage.lastName");
	}
	public WebElement getEmailTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.loginpage.email");
	}
	public WebElement getCountryTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.loginpage.country");
	}
	public WebElement getPasswordTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.loginpage.password");
	}
	public WebElement getConfirmPasswordTextBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.registrationpage.confirmPassword");
	}
	public WebElement getRoleTypeListBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.registrationpage.roleType");
	}
	public WebElement getTermsOfUseCheckBox() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.registrationpage.termsOfUse");
	}
	public WebElement getSendRequestButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.registrationpage.sendRequest");
	}
	public WebElement getCancelButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.registrationpage.cancel");
	}
	
	public WebElement getGetNewPasswordButton() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.loginpage.newpassword.button");
	}
	
	public WebElement getThankyouMessage() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("dqm.loginpage.thankyou");
	}
	
	
}
