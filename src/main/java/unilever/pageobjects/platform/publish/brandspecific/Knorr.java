package unilever.pageobjects.platform.publish.brandspecific;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class Knorr extends AbstractPublishPage {
		
	public Knorr(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public void closeKnorrPollContainer() {
		WebElement pollClose = webElementFactory.getElement("recipelisting.knorr.poll.container.close",10);
		if(pollClose != null && pollClose.isDisplayed()) {
			pollClose.click();
		}
	}

}
