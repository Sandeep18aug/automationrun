package unilever.pageobjects.platform.publish.searchlistingpage;

import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import unilever.pageobjects.platform.publish.AbstractPublishPage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;

public class SearchListingPage extends AbstractPublishPage {

	By searchLisitngV2By = By.cssSelector("div[data-role='" + ComponentsDataRole.SEARCH_LISTING_V2 + "']");

	public SearchListingPage(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public String getSearchListingIncorrectSearchMsg() throws Throwable {
		return webElementFactory.getElement("searchlisting.incorrect.search.message").getAttribute("innerText");
	}

	public WebElement getSearchListingCloseButton() {
		return webElementFactory.getElement("searchlisting.close.button");
	}

	public WebElement getSearchListingPlaceholder() {
		return webElementFactory.getElement("searchlisting.placeholder");
	}

	public WebElement getSearchListingCta() {
		return webElementFactory.getElement("searchlisting.cta");
	}



	public WebElement getSearchListingResetButton() {
		return webElementFactory.getElement("searchlisting.reset.button");
	}

	public List<WebElement> getSearchListingPaginationItem() {
		return webElementFactory.getElements("searchlisting.pagination.item");
	}

	public WebElement getShowMoreButton() {
		return webElementFactory.getElement("searchlisting.pagination");
	}

	public void selectSearchListingTabItem(String tabText) throws Throwable {
		try {
			waitForLoaderToDisappear();
			getSearchListingTabItems().stream().filter(x -> x.getText().toLowerCase().contains(tabText.toLowerCase()))
					.findFirst().get().click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("search list tab with text " + tabText + " is not present");
		}
	}

	public void selectSearchListingFilter(String filterText) throws Throwable {
		try {
			waitForLoaderToDisappear();
			WebElement filter = webElementFactory.getElement("searchlistingv2.filter");
			Select searchListingV2Filter = new Select(filter);
			searchListingV2Filter.getOptions().stream()
					.filter(x -> x.getText().toLowerCase().contains(filterText.toLowerCase())).findFirst().get()
					.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("search list filter with text " + filterText + " is not present");
		}
	}

	public void selectSearchListingFilterUlItem(String filterText) {
		try {
			List<WebElement> filters = webElementFactory.getElements("searchlistingv2.filter.ul.items");
			filters.stream().filter(x -> x.getText().toLowerCase().contains(filterText.toLowerCase())).findFirst().get()
					.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("search list filter with text " + filterText + " is not present");
		}
	}

	public List<WebElement> getSearchListingTabItems() {
		return webElementFactory.getElements("searchlistingv2.tab.item");
	}

	public List<WebElement> getSearchListingFilters() {
		return webElementFactory.getElements("searchlistingv2.filter");
	}

	public List<WebElement> getSearchListingItems() {
		return webElementFactory.getElements("searchlistingv2.list.item");
		// return driver.findElements(searchListingV2ItemsBy);
	}


	public String getSearchListItemName(WebElement element) {
		return element.findElement(webElementFactory.getByLocator("searchlisting.item.name")).getText();
	}

	public String getSearchListItemDescription(WebElement element) {
		return element.findElement(webElementFactory.getByLocator("searchlistingv2.item.description"))
				.getText();
	}

	public WebElement getSearchListItemImage(WebElement element) {
		return element.findElement(webElementFactory.getByLocator("searchlistingv2.item.image"));
	}

//	public String getSearchListItemUrl(WebElement element) {
//		return element.findElement(webElementFactory.getByLocator("searchlistingv2.item.link"))
//				.getAttribute("href");
//	}

	public WebElement getSearchListItemLink(WebElement element) {
		return element.findElement(webElementFactory.getByLocator("searchlistingv2.item.link"));
	}
//	public List<WebElement> getPageListingV2ListItems() {
//		return driver.findElements(pageListingV2ItemsBy);
//	}
//
//	public WebElement getPageListingV2Component() {
//		return webElementFactory.getElement(pageLisitngV2By);
//	}
//
//	public WebElement getPageListingV2Filter() {
//		return webElementFactory.getElement(pageListingV2FilterBy);
//	}
//
//	public void selectFilterByText(String filterText) {
//		try {
//			Select filter = new Select(getPageListingV2Filter());
//			WebElement optionToBeSelected = null;
//			optionToBeSelected = filter.getOptions().stream().filter(x -> x.getText().contains(filterText)).findFirst()
//					.get();
//			optionToBeSelected.click();
//		} catch (NoSuchElementException e) {
//			System.out.println("there is no filter option having text ");
//			e.printStackTrace();
//		}
//	}

}
