package unilever.pageobjects.platform.publish.plp;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import unilever.pageobjects.platform.publish.AbstractPublishPage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;

public class ProductListingPage extends AbstractPublishPage {
	
	public By productListingV2ComponentDivBy = By.cssSelector("div[data-role='"+ComponentsDataRole.PRODUCT_LISTING_V2+"']");

	public ProductListingPage(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public List<WebElement> getProductListingV2ListItems() {
		try {
			return driver.findElements(webElementFactory.getByLocator("productlistingv2.item.listitem"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	public WebElement getProductTag() {
		return webElementFactory.getElement("productlistingv2.tag");
	}
	public WebElement getProductListingV2Component() {
		return webElementFactory.getElement(productListingV2ComponentDivBy);
	}
	public WebElement getRelatedArticleImagePLP() {
		return webElementFactory.getElement("relatedarticle.image");
	}
	
//	public WebElement getProductListingV2ComponentVisible() {
//			WebElement filter = waitDriver.until(ExpectedConditions.presenceOfNestedElementLocatedBy(productListingV2ComponentDivBy, productListingV2FilterBy));
//			return filter;
//		}
	
	public WebElement getPLPFilterLabel() {
		return webElementFactory.getElement("plp.FilterLabel");
	}	

	public Select getProductListingV2Filter() {
		return new Select(webElementFactory.getElement("productlistingv2.filter.listbox"));
	}

	public WebElement getProductListingV2UnorderedListFilter() {
		return webElementFactory.getElement("productlistingv2.filter.ul", false);
	}

	public void selectFilterByText(String filterText) {
		try {
			Select filter = getProductListingV2Filter();
			WebElement optionToBeSelected = null;
			optionToBeSelected = filter.getOptions().stream().filter(x -> x.getText().contains(filterText)).findFirst()
					.get();
			optionToBeSelected.click();
		} catch (NoSuchElementException e) {
			System.out.println("there is not filter option having text " + filterText);
			e.printStackTrace();
		}
	}

	public void selectFilterByIndex(int index) {
		try {
			Select filter = getProductListingV2Filter();
			filter.selectByIndex(index);
		} catch (NoSuchElementException e) {
			System.out.println("there is not filter option with index " + index);
			e.printStackTrace();
		}
	}

	public void selectFilterULByIndex(int index) {
		try {
			System.out.println("applying index = " +  index);
			WebElement ul = getProductListingV2UnorderedListFilter();
			ul.findElement(By.xpath("./parent::div/*[1]")).click();
				Thread.sleep(3000);
			ul.findElements(By.xpath("./li")).get(index).click();
		} catch (NoSuchElementException e) {
			System.out.println("there is not filter option with index " + index);
			e.printStackTrace();
		}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getProductListingFilterProductsCount() {
		try {
			String productCount = "";
			WebElement count = webElementFactory.getElement("productlistingv2.filter.cta.count",5);
			if(count == null) {
				productCount = Integer.toString(webElementFactory.getElements("productlistingv2.item.listitem").size());
			}else {
				productCount = webElementFactory.getElement("productlistingv2.filter.cta.count",5).getAttribute("innerHTML");
			}
			return productCount.trim();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public int getProductListingV2FilterOptionsCount() {
		return getProductListingV2Filter().getOptions().size();
	}

	public int getProductListingV2FilterUnorderedListOptionsCount() {
		return getProductListingV2UnorderedListFilter().findElements(By.xpath("./li")).size();
	}

	public List<String> getProductListingV2Names() {
		List <WebElement> productNameListElement = getProductListingV2ListItems();
		List <String> ProductNames = new ArrayList<>();
		for (WebElement individualName : productNameListElement) {
			String ProductName = individualName.getAttribute("data-product-name");
			ProductNames.add(ProductName);
		
		}
		return ProductNames;
	}
	
	public List<String> getArticleListingV2Names() {
		List <WebElement> articleNameListElement = webElementFactory.getElements("articleListingv2.item");
		List <String> articleNames = new ArrayList<>();
		for (WebElement individualName : articleNameListElement) {
			String articleName = individualName.getAttribute("title");
			articleNames.add(articleName);
		
		}
		return articleNames;
	}
	
	public WebElement getButton() {
		return webElementFactory.getElement("quickview.button");
	}
	
	public WebElement getQuickViewPanel() {
		return webElementFactory.getElement("quickview.panel");
	}
	
	public WebElement getClosePanel() {
		return webElementFactory.getElement("quickview.panel.close");
	}
	
	public WebElement getFirstFilterListBox() {
		return webElementFactory.getElement("plp.filter.first");
				
	}
	public WebElement getSecondFilterListBox() {
		return webElementFactory.getElement("plp.filter.second");
				
	}

	public List<WebElement> getPLPCollectionFilterItemAllProduct() {
		return webElementFactory.getElements("plp.FilterListCollectionAllProduct");
	}
	
	public List<WebElement> getPLPFlavorFilterItemAllProduct() {
		return webElementFactory.getElements("plp.FilterListFlavorAllProduct");
	}
	
	public List<WebElement> getPLPFlavor() {
		return webElementFactory.getElements("plp.FilterListFlavor");
	}
	
	public List<WebElement> getPLPSegment() {
		return webElementFactory.getElements("plp.FilterListSegment");
	}
	
	public List<WebElement> getPLPSegmentFilterItemAllProduct() {
		return webElementFactory.getElements("plp.FilterListSegmentAllProduct");
	}
}

