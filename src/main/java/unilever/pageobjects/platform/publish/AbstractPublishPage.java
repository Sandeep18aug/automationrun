package unilever.pageobjects.platform.publish;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.testng.util.Strings;

import framework.core.baseClasses.BasePage;
import unilever.pageobjects.platform.publish.common.GlobalFooter;
import unilever.pageobjects.platform.publish.common.GlobalNavigation;
import unilever.pageobjects.platform.publish.common.SocialSharing;

/**
 * @author avigorka
 *
 */
public abstract class AbstractPublishPage extends BasePage implements PublishPage {

	SoftAssert softAssert;

	public AbstractPublishPage(WebDriver driver, String brandName) {
		super(driver, brandName);
		softAssert = new SoftAssert();
	}

	public List<WebElement> getAccordianList() {
		return webElementFactory.getElements("accordian.panel");
	}

	public GlobalFooter getGlobalFooter() {
		return new GlobalFooter(driver, brandName);
	}

	public GlobalNavigation getGlobalNavigation() {
		return new GlobalNavigation(driver, brandName);
	}

	public WebElement getMenuIcon() {
		return webElementFactory.getElement("globalnavigation.menu.icon");
	}

	public SocialSharing getSocialSharing() {
		return new SocialSharing(driver, brandName);
	}

	public WebElement getGlobalNavLogo() {
		return webElementFactory.getElement("globalnavigation.logo");
	}

	public WebElement getfaqTitle() {
		return webElementFactory.getElement("faq.title");
	}

	public WebElement getfaqExpandButton() {
		return webElementFactory.getElement("faq.expand.button");
	}

	public WebElement getFaqRichText() {
		return webElementFactory.getElement("faq.richtext");
	}

	private WebElement getCookieFrame() {
		System.out.println("trying to find cookie frame");
		WebElement element = null;
		element = webElementFactory.getElement("cookie.frame", 5);
		return element;
	}

	public void acceptCookiesIfPresent() throws Throwable {
		System.out.println("clicking accept button of cookies pop-up");
		WebElement element = getCookieFrame();
		if (element != null) {
			driver.switchTo().frame(element);
			Thread.sleep(1000);
			WebElement btnAccept = webElementFactory.getClickableElement("cookie.accept.button", false);
			if (btnAccept != null) {
				btnAccept.click();
			}
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			Thread.sleep(1000);
		}
		else
		{	WebElement otcookiebtn = webElementFactory.getClickableElement("cookie.ot.accept", false);
			if (otcookiebtn !=null) {
				otcookiebtn.click();
				System.out.println("Successfully click on ot cookie button");
			}
			
		}
	}

	public void declineCookiesIfPresent() throws Throwable {
		System.out.println("clicking decline button of cookies pop-up");
		WebElement element = getCookieFrame();
		if (element != null) {
			driver.switchTo().frame(element);
			Thread.sleep(1000);
			WebElement btnAccept = webElementFactory.getClickableElement("cookie.accept.decline", false);
			if (btnAccept != null) {
				btnAccept.click();
			}
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			Thread.sleep(1000);
		}
	}

	public void acceptEvidonCookiesIfPresent() throws Throwable {

		WebElement btnAccept = getEvidonCookiesOkButton();
		if (btnAccept != null) {
			btnAccept.click();
			System.out.println("successfully accepted evidon cookies button");
		} else
			System.out.println("evidon cookies is not present, hence bypassing this step");
	}

	public void closeUnileverCookiePopupIfPresent() throws Throwable {

		WebElement btnAccept = getUnileverCookieePopupCloseButton();
		if (btnAccept != null) {
			btnAccept.click();
			System.out.println("successfully closed unilever cookies popup");
		} else
			System.out.println("unilever cookies pop up is not present, hence bypassing this step");
	}

	public WebElement getEvidonCookiesOkButton() throws Throwable {
		System.out.println("getting evidon cookies button");
		WebElement btnAccept = webElementFactory.getElement("cookie.evidon.btn", 5);
		return btnAccept;
	}

	public WebElement getUnileverCookieePopupCloseButton() throws Throwable {
		System.out.println("getting evidon cookies button");
		WebElement btnAccept = webElementFactory.getElement("unilever.cookie.popup.close.button", 5);
		return btnAccept;
	}

	public List<WebElement> getComponentList() {
		try {
			waitForPageReady();
			List<WebElement> componentList = driver.findElements(By.cssSelector("div[data-role]"));
			// waitDriver.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[@data-role]")));
			return componentList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public int getComponentCountOnPage(String componentName) {
		try {
			waitForPageReady();
			List<WebElement> componentList = driver.findElements(By.cssSelector("div[data-role='"+componentName+"' i],div[class='"+componentName+"']"));
			return componentList.size();
		} catch (Exception e) {
			return 0;
		}
	}

	public WebElement getComponent(String componentName) {
		try {
			waitForPageReady();
			return driver.findElement(By.cssSelector("div[data-role='"+componentName+"' i],div[class='"+componentName+"' i],section[class*='"+componentName+"' i]"));

		} catch (Exception e) {
			return null;
		}
	}

	public WebElement getIntegration(String integrationName) {
		try {
			waitForPageReady();
			return driver.findElement(By.cssSelector("div[data-role='" + integrationName + "' i]"));
		} catch (Exception e) {
			return null;
		}
	}

	public Boolean isComponentPresent(String componentName) {
		return getComponent(componentName) != null;
	}

	public Boolean isIntegrationPresent(String integrationName) {
		return getIntegration(integrationName) != null;
	}

	public String getComponentName(WebElement component) {
		return component.getAttribute("data-role");
	}

	public String getComponentJsonAttribute(WebElement component, String jsonAttribute) {
		Pattern pattern = Pattern.compile("\"" + jsonAttribute + "\"(\\s*|):(\\s*|)\"([^\"]+)\"");
		String componentName = null;
		if (component != null) {
			WebElement scriptTagElement = component.findElement(By.xpath("./script[@type='module/config']"));
			String scriptTagText = scriptTagElement.getAttribute("innerHTML");
			Matcher matcher = pattern.matcher(scriptTagText);
			if (matcher.find()) {
				String componentNameStringFromJson = matcher.group();
				componentNameStringFromJson = componentNameStringFromJson.replace(" ", "");
				componentNameStringFromJson = componentNameStringFromJson.replace("\"", "");
				componentNameStringFromJson = componentNameStringFromJson.split(":")[1];
				componentName = componentNameStringFromJson;
			}
		}
		return componentName;
	}

	public String getErrorMessage() {
		waitForPageReady();
		return getErrorObject().getText();
	}

	public WebElement getErrorObject() {
		waitForPageReady();
		return webElementFactory.getElement("errormesage");
	}

	public WebElement getCloseButtonGlobalNav() {
		waitForPageReady();
		return webElementFactory.getElement("globalnavigation.close.button");
	}

	public void waitForLoaderToDisappear() throws Throwable {
		waitForObjectToPerish("preloader.div");
	}

	public String getBaseURLAndSubPath(String completeURL) {
		String baseURL;
		if (completeURL.contains("www."))
			baseURL = completeURL.substring(completeURL.indexOf('.') + 1);
		else
			baseURL = completeURL.substring(completeURL.indexOf('/') + 2);
		return baseURL;
	}

	public WebElement getAccordianPanelByIndex(int index) {
		List<WebElement> accordianPanelList = webElementFactory.getElements("accordian.panel");
		return accordianPanelList.get(index);
	}
	
	public void closeCampaignPopup() {
		By campaignOverlay = webElementFactory.getByLocator("campaign.overlay");	
		if(webElementFactory.findElement(campaignOverlay) != null) {
			WebElement close = webElementFactory.getElement("campaign.overlay.close.button",5);
			if(close != null && close.isDisplayed()) {
				Actions actions = new Actions(driver);
				actions.moveToElement(close).pause(Duration.ofSeconds(1)).click().build().perform();
			}
		}
	}
	
	public void clickBackToTopCTA() {
		WebElement backToTopCta = webElementFactory.getClickableElement("backtotop.cta.link", true);
		backToTopCta.click();
	}
	
	public int getComponentCountOnPageById(String componentName) {
		try {
			waitForPageReady();
			List<WebElement> componentList = driver.findElements(By.cssSelector("div[data-id='"+componentName+"' i]"));
			return componentList.size();
		} catch (Exception e) {
			return 0;
		}
	}
	
	public int getComponentCountOnPageByClass(String componentName) {
		try {
			waitForPageReady();
			List<WebElement> componentList = driver.findElements(By.cssSelector("div[class*='"+componentName+"' i]"));
			return componentList.size();
		} catch (Exception e) {
			return 0;
		}
	}
	
	public int getComponentCountOnPageForVaseline(String componentName) {
		try {
			waitForPageReady();
			List<WebElement> componentList = driver.findElements(By.cssSelector("section[class*='"+componentName+"' i]"));
			return componentList.size();
		} catch (Exception e) {
			return 0;
		}
	}
}
