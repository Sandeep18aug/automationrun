package unilever.pageobjects.platform.publish.forms;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class ContactUsForm extends AbstractPublishPage {
	Objects objects = new Objects();

	public ContactUsForm(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public void selectSubjectByText(String subjectText) {
		WebElement divEnquiryType = waitDriver.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//select[@id='contactUs-inquiryType']/../div")));
		divEnquiryType.click();
		List<WebElement> items = divEnquiryType.findElements(By.xpath(".//li[contains(@class,'option')]"));
		items.stream().filter(x -> x.getText().trim().equalsIgnoreCase(subjectText)).findFirst().get().click();
	}

	public void selectSubjectByIndex(int subjectIndex) {
		WebElement divEnquiryType = waitDriver.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//select[@id='contactUs-inquiryType']/../div")));
		divEnquiryType.click();
		List<WebElement> items = divEnquiryType.findElements(By.xpath(".//li[contains(@class,'option')]"));
		items.get(subjectIndex - 1).click();
	}

	public void enterEmailAddress(String email) {
		WebElement emailAddress = webElementFactory.getElement(By.id("contact-email"));
		emailAddress.clear();
		emailAddress.sendKeys(email);
	}

	public void enterInitials(String initials) {
		WebElement objInitials = webElementFactory.getElement(By.id("contact-honorificPrefix"));
		objInitials.clear();
		objInitials.sendKeys(initials);
	}

	public void enterName(String name) {
		WebElement objName = webElementFactory.getElement(By.id("contact-givenName"));
		objName.clear();
		objName.sendKeys(name);
	}

	public void enterSurName(String surname) {
		WebElement objSurName = webElementFactory.getElement(By.id("contact-familyName"));
		objSurName.clear();
		objSurName.sendKeys(surname);
	}

	public void selectGenderByText(String gender) {
		WebElement divGender = webElementFactory.getElement(By.xpath("//select[@id='contact-gender']/../div"));
		divGender.click();
		List<WebElement> items = divGender.findElements(By.xpath(".//li[contains(@class,'option')]"));
		items.stream().filter(x -> x.getText().trim().equalsIgnoreCase(gender)).findFirst().get().click();
	}

	public void selectGenderByIndex(int index) {
		WebElement divGender = webElementFactory.getElement(By.xpath("//select[@id='contact-gender']/../div"));
		divGender.click();
		List<WebElement> items = divGender.findElements(By.xpath(".//li[contains(@class,'option')]"));
		items.get(index - 1).click();
	}

	public void enterAddressLine1(String address) {
		WebElement edtAddressline1 = webElementFactory.getElement(By.id("contact-streetAddress1"));
		edtAddressline1.clear();
		edtAddressline1.sendKeys(address);
	}

	public void enterAddressLine2(String address) {
		WebElement edtAddressline2 = webElementFactory.getElement(By.id("contact-streetAddress2"));
		edtAddressline2.clear();
		edtAddressline2.sendKeys(address);
	}

	public void enterCityLocality(String city) {
		WebElement edtCityLocality = webElementFactory.getElement(By.id("contact-locality"));
		edtCityLocality.clear();
		edtCityLocality.sendKeys(city);
	}

	public void selectRegionByText(String region) {
		WebElement divRegion = webElementFactory.getElement(By.xpath("//select[@id='contact-region']/../div"));
		divRegion.click();
		List<WebElement> items = divRegion.findElements(By.xpath(".//li[contains(@class,'option')]"));
		items.stream().filter(x -> x.getText().trim().equalsIgnoreCase(region)).findFirst().get().click();
	}

	public void selectRegionByIndex(int index) {
		WebElement divRegion = webElementFactory.getElement(By.xpath("//select[@id='contact-region']/../div"));
		divRegion.click();
		List<WebElement> items = divRegion.findElements(By.xpath(".//li[contains(@class,'option')]"));
		items.get(index - 1);
	}

	public void enterZipCode(String zipCode) {
		WebElement edtZipCode = webElementFactory.getElement(By.id("contact-postalcode"));
		edtZipCode.clear();
		edtZipCode.sendKeys(zipCode);
	}

	public void enterPhoneNumber(String zipCode) {

	}

	public void enterPhoneNumberExtension(String zipCode) {

	}

	public void enterPhoneNumber1(String zipCode) {

	}

	public void enterPhoneNumber1Extension(String zipCode) {

	}

	public void enterComments(String zipCode) {

	}
	

	private class Objects {
//		String ClickContactUs = "//a[@title='Contact us'] | //a[@title='Contact Us']| //a[@title='Fale Conosco'] | //a[contains(.,'Contact Us')]";
//		String ClickContactus = "(//*[@class='o-footer-links select'])[5]";
//		String ClickwebInquiry = "//a[contains (.,'Web Inquiry')] | .//*[@title='Web inquiry']";
//		String ClickSendInquiry = "(//*[@class='c-link-panel'])[1]";
//		String SelectEnquiryType = ".//*[@id='contactUs-inquiryType']/option[2] |  //*[@class='chosen-single'] ";
//		String ScrolltoMagnum = "//label[contains(.,'Enquiry Type*')]";
//		String SelectEnquiryTypeMagnum = ".//*[@id='contactUs_inquiryType_chosen']/a";
//		String SelectEnquiryValueMagnum = ".//*[@id='contactUs_inquiryType_chosen']/div/ul/li[3]";
//		String SelectGenderTypeMagnum = ".//*[@id='contact_gender_chosen']/a";
//		String SelectGenderValueMagnum = ".//*[@id='contact_gender_chosen']/div/ul/li[2]";
//		String SelectPrefixTypeMagnum = ".//*[@id='contact_honorificPrefix_chosen']/a";
//		String SelectPrefixValueMagnum = ".//*[@id='contact_honorificPrefix_chosen']/div/ul/li[2]";
//		String SelectUPCCodeDetailMagnum = ".//*[@id='contactUs_upcCodeDetail_chosen']/a/span";
//		String SelectUPCCodeValueMagnum = ".//*[@id='contactUs_upcCodeDetail_chosen']/div/ul/li[2]";
//		String EnterManufacturingCodeMagnum = ".//*[@id='contactUs_manufacturingCodeDetail_chosen']/a";
//		String SelectManufacturingvalueMagnum = ".//*[@id='contactUs_manufacturingCodeDetail_chosen']/div/ul/li[2]";
//		String SelectStateMagnum = ".//*[@id='contactUs_region_chosen']/a";
//		String SelectStatevalueMagnum = ".//*[@id='contactUs_region_chosen']/div/ul/li[2]";
//		String ClickSendemail = "//a[contains (.,'Send an email')]";
//		String ContactTitle = "//h1[contains (.,'Contact us')]";
//		String SelectInquiryType = ".//*[@id='contactUs-inquiryType']/option[3] | //option[contains(.,'Question')] | //option[contains(.,'Un conseil ou une information')]";
//		String ClcikSubject = "(//span[contains(.,'Por favor selecione')])[1]";
//		String Selectsubject = "//li[contains(.,'Dúvidas, sugestões ou elogios')]";
//		String EnterProductDiscuss = ".//*[@id='contactUs-product']";
//		String EnterSize = ".//*[@id='contactUs-size']";
//		String datePurchase = ".//input[@id='contactUs-datePurchased']";
//		String selectPurchaseDate = "(.//div[contains(@class,'datepicker-dropdown')]//td)[10]";
//		String ClickAgelimt = ".//*[@for='contact-legalAgeConfirmation']";
//		String EnterPrefix = ".//*[@id='contact-honorificPrefix']";
//		String ClickTitlePrefix = "(//span[contains(.,'Παρακαλώ επιλέξτε')])[1]";
//		String SelectTitlePrefix = ".//*[@id='contact-honorificPrefix']/option[2] | .//li[@data-value='Κος']";
//		String SelectPrefixUK = ".//*[@id='contact_country_chosen']/a/span";
//		String EnterFirstName = ".//*[@id='contact-givenName']";
//		String EnterLastName = ".//*[@id='contact-familyName']";
//		String SelectCountry1 = ".//*[@id='contact-country']";
//		String SelectCountry1UK = "";
//		String Sex = "(//span[contains(.,'Por favor selecione')])[2]";
//		String SelectSex = "//li[contains(.,'Masculino')]";
//		String SelectGender = ".//*[@id='contact-gender']/option[2] | .//*[@id='contact-gender'] | .//*[@id='contact_gender_chosen']/a | //option[contains(.,'Male')]";
//		String EnterEmailAddress = ".//*[@id='contact-email']";
//		String EnterMobile = ".//*[@id='contact-phoneNumbers-0-value']";
//		String BrandOptin = ".//*[@for='optIn-brand'] | .//*[@id='_content_dove_en_gb_home_secure_contact2_jcr_content_flexi_hero_par_formv2']/div[18] | .//*[@id='optIn-brand'] | (//*[@class='c-form-checkbox__label-copy'])[1]";
//		String CorporateOptin = ".//*[@for='optIn-corporate'] | .//*[@id='optIn-corporate'] | (//*[@class='c-form-checkbox__label-copy'])[1]";
//		String OnlineBrandOptin = ".//*[@for='optIn-onlineBrand']";
//		String OnlineAll = ".//*[@id='_content_dove_en_gb_home_secure_contact2_jcr_content_flexi_hero_par_formv2']/div[22]";
//		String SampleInfo = "(//*[@class='c-form-checkbox__label-copy'])[1]";
//		String LikeToContacted = "(//*[@class='c-form-checkbox__label-copy'])[3]";
//		String EnterAddress = ".//*[@id='contact-streetAddress1']";
//		String EnterAddress2 = ".//*[@id='contact-streetAddress2']";
//		String EnterCity = ".//*[@id='contact-locality']";
//		String SelectState = ".//*[@id='contactUs-region'] | .//*[@id='contact-region']/option[3] |.//*[@id='contactUs_region_chosen']/a/span | //li[contains(.,'Alagoas')]";
//		String SelectSunState = "//span[contains(.,'Selecione o Estado')]";
//		String SelectSelState = "//li[contains(.,'Alagoas')]";
//		String EnterZipCode = ".//*[@id='contact-postalCode'] | .//*[@id='contact-postalcode']| .//*[@id='Postcode']";
//		String DateOfBirth = ".//*[@id='contact-birthday']";
//		String ContactNumber = ".//*[@id='contact-phoneNumbers-value'] | .//*[@id='contact-phoneNumbers-0-value']";
//		String PhoneNumber = ".//*[@id='contact-phoneNumbers-0-value']";
//		String EnterDayTimeTelePhone = ".//*[@id='contact-phoneNumbers-0-value']";
//		String EnterDayTimeTelePhoneExtension = ".//*[@id='contact-phoneNumbers-0-ext']";
//		String EvenTimeTelePhone = ".//*[@id='contact-phoneNumbers-1-value']";
//		String EvenTimeTelePhoneExtension = ".//*[@id='contact-phoneNumbers-1-ext']";
//		String ClickProductInformation = ".//*[@id='product-information-link'] | //*[@class='contactus-product-accordian-link'] | //a[contains(.,'Product Information')] | //*[@class='productioninformation-link']";
//		String EnterUPCCode = ".//*[@id='contactUs-upcCode'] ";
//		String SelectUPCCodeDetail = ".//*[@id='contactUs-upcCodeDetail']/option[3] |.//*[@id='contactUs_upcCodeDetail_chosen']/a/span | .//*[@id='contactUs-upcCodeDetail']/option[2] ";
//		String EnterSkUPC = "(//span[contains(.,'Por favor selecione')])[3]";
//		String SelectSkUPC = "//li[contains(.,'cheio')]";
//		String EnterExpiryDate = ".//*[@id='contactUs-expiryDate']";
//		String EnterManufacturingCode = ".//*[@id='contactUs-manufacturingCode'] ";
//		String SelectManufacturingCodeDetail = ".//*[@id='contactUs-manufacturingCode']/option[2]  | .//*[@id='contactUs-manufacturingCodeDetail']/option[5] | .//*[@id='contactUs_manufacturingCodeDetail_chosen']/a/span";
//		String SelectManfCodeMagnum = ".//*[@id='contactUs_manufacturingCodeDetail_chosen']/div/ul/li[3]";
//		String EnterSkMFC = "(//span[contains(.,'Por favor selecione')])[4]";
//		String SelectSkMFC = "//li[contains(.,'Cheio')]";
//		String EnterStoreName = ".//*[@id='contactUs-storeNamePurchasedFrom']";
//		String EnterProductPurchaseFrom = "(.//*[@id='contactUs-townPurchasedFrom'])[1]";
//		String EnterTowntPurchaseFrom = "(.//*[@id='contactUs-townPurchasedFrom'])[2]";
//		String EnterComments = ".//*[@id='contactUs-comments'] |.//div[contains(@class,'nice-select')]//li[@data-value='Product Concern']";
//		String EnterMessage = ".//*[@id='contactUs-comments']";
//		String EnterProduct = ".//*[@id='contactUs-product']";
//		String SelectBarcode = ".//*[@id='contactUs-upcCodeDetail']";
//		String EnterBarCodeUK = ".//*[@id='contactUs-upcCode']";
//		String SelectEnquiryTypeMG = ".//*[@id='contactUs_inquiryType_chosen']";
//		String SelectConcernMG = ".//div[contains(@class,'chosen-drop')]//li[@data-option-array-index='2']";
//		String ClickGenderMG = ".//*[@id='contact_gender_chosen']/a";
//		String SelectGenderMG = "(.//div[contains(@class,'chosen-drop')]//li[@data-option-array-index='1'])[2]";
//		String ScrolltoSignal = "//h2[contains(.,'Votre message')]";
//		String SelectinquiryTypeWals = ".//li[@data-value='Ερώτηση']";
//		String selectInquiryTypesignl = ".//select[@id='contactUs-inquiryType']/following-sibling::div";
//		String selectConcernsignal = "//li[@data-value='Product Question']";
//		String message = ".//textarea[@id='contactUs-comments']";
//		String productName = ".//input[@id='contactUs-product']";
//		String productFormat = ".//input[@id='contactUs-size']";
//		String DatePurchase = ".//input[@id='contactUs-datePurchased']";
//		String SelectPurchaseDate = "(.//div[contains(@class,'datepicker-dropdown')]//td)[10]";
//		String selectBarCode = ".//select[@id='contactUs-upcCodeDetail']/following-sibling::div";
//		String selectBarYes = "(.//div[contains(@class,'nice-select')]//li[@data-value='Full'])[1]";
//		String enterBarCode = ".//input[@id='contactUs-upcCode']";
//		String dateExpiry = ".//input[@id='contactUs-expiryDate']";
//		String selectExpiryDate = "(.//div[contains(@class,'datepicker-dropdown')]//td)[30]";
//		String selectManufacturingCode = ".//select[@id='contactUs-manufacturingCodeDetail']/following-sibling::div";
//		String selectManfYes = "(.//div[contains(@class,'nice-select')]//li[@data-value='Full'])[2]";
//		String enterManufacturingCode = ".//input[@id='contactUs-manufacturingCode']";
//		String selectPrefixsignal = ".//select[@id='contact-honorificPrefix']/following-sibling::div";
//		String prefixClicksignal = ".//div[contains(@class,'nice-select')]//li[@data-value='Monsieur']";
//		String SelectInquiryTypeCaress = ".//select[@id='contactUs-inquiryType']";
//		String SelectStateCaress = ".//select[@id='contact-region']";
//		String SelectUPCdetails = ".//select[@id='contactUs-upcCodeDetail']";
//		String SelectManfCode = ".//select[@id='contactUs-manufacturingCodeDetail']";
//		String CaressThankYou = ".//p[contains(@class,'o-section__title')]";
//		String EnterDateOfBirth = ".//input[contains(@id,'contact-birthday')] ";
//		String ClickDateOfBirth = ".//*[@id='contact-birthday']";
//		String ClickYearHeadA = "(.//th[contains(@class,'datepicker-switch')])[1]";
//		String ClickYearA = "(.//th[contains(@class,'datepicker-switch')])[2] ";
//		String selectYearA = "(.//th[contains(@class,'datepicker-switch')])[3]";
//		String ClickBackA = "(.//th[contains(@class,'prev')])[2] ";
//		String ClickBirthYearA = ".//span[contains(@class,'year old')] ";
//		String ClickMonthA = "(.//span[contains(@class,'month')])[7] ";
//		String ClickDateA = "(.//td[contains(@class,'day')])[10] ";
//		String Capiframe = "//div[contains(@class,'g-recaptcha')]//iframe";
//		String ClickCapc = "//*[@id='recaptcha-anchor']";
//		String EnterPrefixLipton = ".//input[@id='contact-honorificPrefix']";
//		String LiptonThankYou = "//*[starts-with(@id,'rich-text-v2')]/div/div/div/div/h3";
//		String ClickSubmit = ".//button[contains(@class,'c-form-submit-btn') and @type='submit'] |.//*[@id='drg'] | .//*[@id='Enviar'] | //button[contains(.,'Submit')] | .//*[@id='3432-submit']";
//		String VerifySuccessmsg = "//*[@class='form-success-msg']";
//		String VerifyConThanksDov = ".//*[@class='description']";
//		String VerifyConThanksSeda = "(//*[@class='c-rich-text-v2__description js-ct-linkclick']/p)[3]";
//		String VerifyConThanksApo = "//*[@class='c-rich-text-v2__description js-ct-linkclick']/p";
//		String VerifyConThanksCare = "//*[@class='c-rich-text-v2__description js-ct-linkclick']/p[2]";
//		String VerifyTextDIG = ".//*[@id='form-554aebfe047af6eaecbe4f4642fd8937']/div/span/div/div[2]";
//		String VerifyTextKnorr = ".//*[@id='hero-v2-516b9e4db7352b1e24dc8ce0d2ab2dce']/div/div/div/div[2]/p";
//		String VerifyTextMagnum = ".//*[@id='form-1b2f249a3160196e94ad8eeacfb37743']/div/span/div/div[2]";
//		String VerifyTextSignal = "//*[starts-with(@id,'form-v2')]/div/span/div/div[3]";
//		String VerifyTextAxe = "//*[starts-with(@id,'form-v2')]/div/span/div[3]";
//		String VerifyTextDegree = ".//*[@id='wrapper']/section/div/div[4]/div/div/span/div[2]";
//		String VerifyTextDoveUK = "//*[starts-with(@id,'form-v2')]/div/span/span/div[2]";
//		String VerifyTextPonds = "//*[starts-with(@id,'form-v2')]/div/span/p/span";
//		String VerifyTextHellmanns = "//*[starts-with(@id,'form-v2')]/div/span/p[2]";
//		String VerifyTextTresemme = "//*[starts-with(@id,'rich-text-v2')]/div/div/div/div/p[1]";
//		String SuccessMessageAxe = "//*[@class='form-success-msg']//div[3]";
//		String SuccessMessageknr = "//*[@class='in-view'] ";
//		String SuccessMessage = "//*[@class='form-success-msg'] | //*[@class='c-rich-text-v2__description js-ct-linkclick']//p[1] | //*[@class='description'] | (//*[@class='c-rich-text-v2__description js-ct-linkclick'])[3]";
//		String SuccessMessagetext = "//*[@class='form-success-msg']//div[3] | (//*[@class='o-text__heading-2 c-form-copy-content__headline form form-horizontal'])[2] | (//*[@class='o-text__heading-2 c-form-copy-content__headline form form-horizontal'])[3]";

	}

}
