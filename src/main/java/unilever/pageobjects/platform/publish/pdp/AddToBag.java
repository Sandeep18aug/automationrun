package unilever.pageobjects.platform.publish.pdp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import framework.core.baseClasses.BasePage;

public class AddToBag extends BasePage {

	public AddToBag(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public WebElement getAddToBagDialog() {
		return webElementFactory.getElement("pdp.addtobag.popup");
	}

	public boolean isPresent() {
		return (getAddToBagDialog() != null);
	}

	public Select getRetailerListBox() {
		return new Select(getRetailerWebElement());

	}

	public WebElement getRetailerWebElement() {
		return webElementFactory.getElement("pdp.addtobag.popup.retailer.listbox");

	}

	public Select getSizeListBox() {
		return new Select(getSizeWebElement());

	}

	public WebElement getSizeWebElement() {
		return webElementFactory.getElement("pdp.addtobag.popup.size.listbox");

	}

	public Select getQuantityListBox() {
		return new Select(getQuantityWebElement());

	}

	public WebElement getQuantityWebElement() {
		return webElementFactory.getElement("pdp.addtobag.popup.quantity.listbox");

	}

	public WebElement getAddToBagButton() {
		return webElementFactory.getElement("pdp.addtobag.popup.button");

	}

	public void clickAddToBag() {
		getAddToBagButton().click();
	}

	public WebElement getViewBagOverlay() {
		return webElementFactory.getElement("pdp.addtobag.popup.viewbag.overlay");
	}

	public void clickViewBagButton() {
		webElementFactory
				.getClickableElement(webElementFactory.getByLocator("pdp.addtobag.popup.viewbag.button"), false)
				.click();
	}

	public void clickShoppingBagIcon() {
		webElementFactory
				.getClickableElement(webElementFactory.getByLocator("pdp.addtobag.popup.shoppingbag.icon"), false)
				.click();
	}

}
