package unilever.pageobjects.platform.publish.pdp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import framework.core.baseClasses.BasePage;

public class TurnTo extends BasePage {


	public TurnTo(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public WebElement getGmailIcon() {
		return webElementFactory.getElement("pdp.turnto.dialog.gmail.icon");
	}

	public WebElement getFacebookIcon() {
		return webElementFactory.getElement("pdp.turnto.dialog.facebook.icon");
	}

	public WebElement getFirstName() {
		return webElementFactory.getElement("pdp.turnto.dialog.firstname.textbox");
	}

	public WebElement getLastName() {
		return webElementFactory.getElement("pdp.turnto.dialog.lastname.textbox");
	}

	public WebElement getEmail() {
		return webElementFactory.getElement("pdp.turnto.dialog.email.textbox");
	}

	public WebElement getNickName() {
		return webElementFactory.getElement("pdp.turnto.dialog.nickname.textbox");
	}

	public WebElement getSubmitButton() {
		return webElementFactory.getElement("pdp.turnto.dialog.submit.button");
	}

	public WebElement getCancelButton() {
		return webElementFactory.getElement("pdp.turnto.dialog.cancel.button");
	}

	public WebElement getSubmitMessageObject() {
		return webElementFactory.getElement("pdp.turnto.dialog.submit.message.text");
	}

	public WebElement getSubmitOk() {
		return webElementFactory.getElement("pdp.turnto.dialog.submit.ok.button");
	}

	public void enterEmail(String email) {
		getEmail().sendKeys(email);
		System.out.println("turn to - successfully entered email as  - " + email);
	}

	public void enterFirstName(String firstName) {
		getFirstName().sendKeys(firstName);
		System.out.println("turn to - successfully entered firstname as  - " + firstName);
	}

	public void enterLastName(String lastName) {
		getLastName().sendKeys(lastName);
		System.out.println("turn to - successfully entered lastname as  - " + lastName);
	}

	public void enterNickName(String nickName) {
		getNickName().sendKeys(nickName);
		System.out.println("turn to - successfully entered nickname as  - " + nickName);
	}

	public void submit() {
		getSubmitButton().click();
		System.out.println("turn to - successfully clicked submit button");
	}

	public void cancel() {
		getCancelButton().click();
	}
	
	public String getSubmitMessageText() {
		return getSubmitMessageObject().getText();
	}
	
	public void clickOk() {
		getSubmitOk().click();
	}

}
