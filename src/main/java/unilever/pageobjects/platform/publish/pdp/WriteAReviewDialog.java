package unilever.pageobjects.platform.publish.pdp;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import framework.core.baseClasses.BasePage;
import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class WriteAReviewDialog extends BasePage {

	public WriteAReviewDialog(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public void selectRecommendScore(int recommendScore) {
		List<WebElement> recommendScoreElements = webElementFactory
				.getElements("pdp.writeareview.dialog.recommendscore.radio");
		recommendScoreElements.get(recommendScore - 1).click();
		System.out.println("selected review ratings start - " + recommendScoreElements);
	}

	public WebElement getPurchaseLocationListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.purchaselocationreview.listbox");
	}

	public WebElement getMarketingOptInListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.marketingoptionreview.listbox");
	}

	public WebElement getIncentivizedReviewListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.incentivizedreview.listbox");
	}

	public WebElement getHowLongHaveYouBeenListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.howlonghaveyoubeenpurchasing.listbox");
	}

	public WebElement getGenderListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.gender.listbox");
	}

	public WebElement getFrequencyListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.frequency.listbox");
	}

	public WebElement getFilterListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.filter.listbox");
	}

	public WebElement getBirthYearListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.birthyear.listbox");
	}

	public WebElement getMajorityListBox(){
		return webElementFactory.getElement("pdp.writeareview.dialog.majority.listbox");
	}
	
	public WebElement getBirthMonthListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.birthmonth.listbox");
	}

	public WebElement getAreYourOverTheYearListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.areyouovertheage.listbox");
	}

	public WebElement getUsuallyPurchasedVimProduct() {
		return webElementFactory.getElement("pdp.writeareiview.vimproduct.listbox");
	}
	
	public WebElement getSurfaceUsed() {
		return webElementFactory.getElement("pdp.writeareiview.surfaceused.listbox");
	}
	
	public WebElement getFirstTimeBuyingProduct() {
		return webElementFactory.getElement("pdp.writeareview.dialog.firsttimebuyingproduct.listbox");
	}
	
	public WebElement getVimProductPurchaseStore() {
		return webElementFactory.getElement("pdp.writeareview.dialog.ProductPurchaseStore.listbox");
	}
	
	public WebElement getAgeRangeListBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.agerange.listbox");
	}

	public WebElement getZipCodeEditBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.zip.textbox");
	}

	public WebElement getRecommendedNoObject() {
		return webElementFactory.getElement("pdp.writeareview.dialog.recommed.false.radio");
	}

	public WebElement getRecommendedYesObject() {
		return webElementFactory.getElement("pdp.writeareview.dialog.recommed.true.radio");
	}

	public WebElement getPostReviewMessageCloseLink() {
		return webElementFactory.getElement("pdp.writeareview.postreview.popup.cross.link");
	}

	public WebElement getPostReviewMessageObject() {
		return webElementFactory.getElement("pdp.writeareview.postreview.popup.message.text");
	}

	public WebElement getSubmitReviewButton() {
		return webElementFactory.getElement("pdp.writeareview.dialog.submit.button");
	}

	public WebElement getReviewDialogObject() {
		return webElementFactory.getElement("pdp.writeareview.dialog");
	}

	public List<WebElement> getReviewStarsList() {
		return webElementFactory.getElements("pdp.writeareview.dialog.stars");
	}

	public WebElement getTitleEditBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.title.textbox");
	}

	public WebElement getReviewDescriptTextArea() {
		return webElementFactory.getElement("pdp.writeareview.dialog.description.textbox");
	}

	public WebElement getNickNameEditBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.nickname.textbox");
	}

	public WebElement getEmailEditBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.email.textbox");
	}

	public WebElement getAgeCheckBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.age.checkbox");
	}

	public WebElement getTermsAndConditionsCheckBox() {
		return webElementFactory.getElement("pdp.writeareview.dialog.termsandconditions.checkbox");
	}

	public boolean isPresent() {
		return (getReviewDialogObject() != null);
	}

	
	
	public void selectRatingsStar(int starsIndex) {
        List<WebElement> starsElements = webElementFactory.getElements("pdp.writeareview.dialog.stars");
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        //jse.executeScript("arguments[0].scrollIntoView()", starsElements.get(starsIndex - 1));
        jse.executeScript("arguments[0].click();", starsElements.get(starsIndex - 1));
        //starsElements.get(starsIndex - 1).click();
        System.out.println("selected review ratings start - " + starsIndex);
    }

	public void enterReviewTitle(String title) {
		getTitleEditBox().sendKeys(title);
		System.out.println("successfully entered review title");
	}

	public void enterReviewDescriptionText(String description) {
		// reviewDescription.click();
		getReviewDescriptTextArea().sendKeys(description);
		System.out.println("successfully entered review descrption");
	}

	// rr-nckName
	public void enterNickName(String nickName) {
		getNickNameEditBox().sendKeys(nickName);
		System.out.println("successfully entered nickname");

	}

	// #rr-email

	public void enterEmail(String email) {
		getEmailEditBox().sendKeys(email);
		System.out.println("successfully entered email");

	}

	public void checkAgeCheckBox() {
		WebElement check = getAgeCheckBox();
		if (!check.isSelected()) {
			check.click();
			System.out.println("successfully checked age checkbo");
		} else {
			System.out.println("age checkbox was already selected");
		}
	}

	public void checkTermsAndConditions() throws Throwable {
		WebElement checkTerms = getTermsAndConditionsCheckBox();
		clickAt(checkTerms, 5, 5);// rr-termandconditions
		System.out.println("successfully checked terms and conditions");
	}

	public void submitReview() throws Throwable {
		WebElement submit = getSubmitReviewButton();
		webElementFactory.focusElement(submit);
		submit.click();
		System.out.println("successfully clicked on Submit Review Button");
		waitForObjectToPerish(webElementFactory.getByLocator("pdp.writeareview.dialog.submit.button"));
	}

	public String getReviewSubmitMessage() throws Throwable {
		return webElementFactory.getElementIfVisible(
				webElementFactory.getByLocator("pdp.writeareview.postreview.popup.message.text"), false).getText();
	}

	public WebElement getProductImage() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("pdp.writeareview.dialog.product.image", false);
	}

}
