package unilever.pageobjects.platform.publish.pdp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import unilever.pageobjects.platform.publish.AbstractPublishPage;



public class CheckOutPage extends AbstractPublishPage {

	
	public CheckOutPage(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public boolean isCheckoutPage() {
		return webElementFactory.getElement("checkoutpage.div.checkoutshoppable") != null;
	}
	
	public WebElement getCheckoutIframe() {
		return webElementFactory.getElement("checkoutpage.checkoutshoppable.iframe");
	}
	
	public void checkOutFormPresent() {
		if(getCheckoutIframe().isDisplayed()) {
			driver.switchTo().frame(getCheckoutIframe());
			System.out.println("Checkout form is available");
			}
		else {
			System.out.println("Checkout form is not available");
		}
	}
	
	public void enterFirstName(String name) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.firstname", false).sendKeys(name);
		System.out.println("successfully entered first name in checkout form");
	}
	
	public void enterLastName(String name) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.lastname", false).sendKeys(name);
		System.out.println("successfully entered last  name in checkout form");
	}
	
	public void enterAddressLine1(String address1) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.addressline1", false).sendKeys(address1);
		System.out.println("successfully entered address line 1 in checkout form");
	}
	
	public void enterAddressLine2(String address2) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.addressline2", false).sendKeys(address2);
		System.out.println("successfully entered address line 2 in checkout form");
	}
	
	public void enterCity(String city) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.city", false).sendKeys(city);
		System.out.println("successfully entered city in checkout form");
	}
	
	public void selectState(String State) {
		Select select = new Select(webElementFactory.getElement("checkoutpage.checkoutshoppable.state"));
		select.selectByVisibleText(State);
		System.out.println("successfully selected State in checkout form");
	}
	
	public void enterZipCode(String zipCode) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.zipcode", false).sendKeys(zipCode);
		System.out.println("successfully entered zip code in checkout form");
	}
	
	public void enterPhoneNumber(String num) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.phone", false).sendKeys(num);
		System.out.println("successfully entered phone number in checkout form");
	}
	
	public void enterEmail(String email) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.email", false).sendKeys(email);
		System.out.println("successfully entered zip code in checkout form");
	}
	
	public void enterCardHolderName(String cardName) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.cardholdername", false).sendKeys(cardName);
		System.out.println("successfully entered card holder name in checkout form");
	}
	
	public void enterCardNumber(String cardNum) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.cardnumber", false).sendKeys(cardNum);
		System.out.println("successfully entered Card number in checkout form");
	}
	
	public void selectExpiryMonth(String expMonth) {
		Select select = new Select(webElementFactory.getElement("checkoutpage.checkoutshoppable.expirymonth"));
		select.selectByVisibleText(expMonth);
		System.out.println("successfully selected expiry month in checkout form");
	}
	
	public void selectExpiryYear(String expYear) {
		Select select = new Select(webElementFactory.getElement("checkoutpage.checkoutshoppable.expiryyear"));
		select.selectByVisibleText(expYear);
		System.out.println("successfully selected expiry year in checkout form");
	}
	
	public void enterVerificationCode(String cardNum) {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.verificationcode", false).sendKeys(cardNum);
		System.out.println("successfully entered Card number in checkout form");
	}
	
	public WebElement getInfoAndOfferOptin() {
		
		return webElementFactory.getElement("checkoutpage.checkoutshoppable.infoandofferoptin");
	}
	
	public void clickInfoAndOfferOptin() throws Throwable {
	clickAt(getInfoAndOfferOptin(), 10, 10);
	System.out.println("successfully clicked on Information & Offers Optin");
	}
	
	public void clickPlaceOrder() {
		webElementFactory.getElement("checkoutpage.checkoutshoppable.placeorder.button", false).click();
		driver.switchTo().defaultContent();
		System.out.println("successfully clicked on Place order button");
	}

}
