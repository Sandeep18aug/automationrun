package unilever.pageobjects.platform.publish.pdp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class ProductCategoryPage extends AbstractPublishPage{

	public ProductCategoryPage(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}
	
	public WebElement getProductCategoryHeading() {
		return webElementFactory.getElement("productCategory.Heading");
	}

	public WebElement getProductCategorySubHeading() {
		return webElementFactory.getElement("productCategory.SubHeading");
	}
	
	public WebElement getProductCategory() {
		return webElementFactory.getElement("productCategory");
	}
	
	public WebElement getProductCategoryExploreButton() {
		return webElementFactory.getElement("productCategoryExplore");
	}
	
	

}
