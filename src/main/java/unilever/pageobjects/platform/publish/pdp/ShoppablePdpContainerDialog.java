package unilever.pageobjects.platform.publish.pdp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import framework.core.baseClasses.BasePage;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class ShoppablePdpContainerDialog extends BasePage {

	//By shoppablePdpContainerDialogBy = By.id("shoppable_magic_v1_pdp_container");
	By shoppableMerchantBy = By.id("shoppable_magic_select_merchant");
	By shoppableColorSize = By.id("shoppable_magic_select_color_size");
	By shoppableQuantity = By.id("shoppable_magic_select_qty");
	By shoppaleAddToBag = By.id("shoppable_magic_add_to_cart_btn");

	public ShoppablePdpContainerDialog(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public void selectRetailer(String retailerName) {
		WebElement retailerWE = webElementFactory.getElement("pdp.shoppable.dialog.merchant.list", false);
		Select retailer = new Select(retailerWE);
		retailer.selectByVisibleText(retailerName);
	}

	public void selectSize(String sizeText) {
		WebElement sizeWE = webElementFactory.getElement("pdp.shoppable.dialog.color.list", false);
		Select size = new Select(sizeWE);
		size.selectByVisibleText(sizeText);
	}

	public void selectQuantity(String quantityText) {
		WebElement quantityWE = webElementFactory.getElement("pdp.shoppable.dialog.quantity.list", false);
		Select quantity = new Select(quantityWE);
		quantity.selectByVisibleText(quantityText);
	}
	
	public void clickAddToBag() {
		webElementFactory.getElement("pdp.shoppable.dialog.addtobag.button", false).click();
	}

}
