package unilever.pageobjects.platform.publish.pdp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class ViewBagPage extends AbstractPublishPage {

	By DIV_ShoppableViewBag = By.cssSelector(".flexi_hero_par.parsys div[data-role='shoppable-view-bag']"); 
	
	public ViewBagPage(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public WebElement getContinueShoppingButton() {
		return webElementFactory.getElement("viewbagpage.continueshopping.button");
	}

	public boolean isPresentOnPage() {
		return webElementFactory.getElement(DIV_ShoppableViewBag) != null;
	}
	
	public WebElement getEditBagLink() {
		return webElementFactory.getElement("viewbagpage.editbag.button");
	}
	
	public void clickEditBagLink() {
		getEditBagLink().click();
	}
	
	public WebElement getEditBagPopUp() {
		return webElementFactory.getElement("viewbagpage.editbag.dialog");
	}
	

	public boolean isEditBagPopUpPresent() {
		return (getEditBagPopUp() != null);
	}
	
	public Select getQuantityEditBagPopUp() {
		return new Select(webElementFactory.getElement("viewbagpage.editbag.dialog.quantity"));

	}
	
	public WebElement getCheckoutButton() {
		return webElementFactory.getElement("viewbagpage.checkout.button");
	}
	
	public void clickCheckoutButton() {
		getCheckoutButton().click();
	}

}
