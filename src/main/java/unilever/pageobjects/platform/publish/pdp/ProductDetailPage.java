package unilever.pageobjects.platform.publish.pdp;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class ProductDetailPage extends AbstractPublishPage {

	private WriteAReviewDialog writeAReview = null;
	private AddToBag addToBag = null;
	private TurnTo turnTo = null;
	
	public ProductDetailPage(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public void clickWriteAReviewButton() {
		List<WebElement> listWriteAReview = webElementFactory.getElements("pdp.writeareview.link");
		if (listWriteAReview != null) {
			for (WebElement elem : listWriteAReview) {
				if (elem.isDisplayed()) {
					elem.click();
					return;
				}
			}
		}
		webElementFactory.getElement("pdp.ratingandreview.section", false).click();
		webElementFactory.getClickableElement(webElementFactory.getByLocator("pdp.writeareview.link"), false).click();
	}

	public void clickWriteAReviewButtonFromCommentSection() {
		// if()
		WebElement writeAReview = webElementFactory.getElement("pdp.writeareview.commentsection.link");
		//WebElement writeAReview = webElementFactory.findElement(By.cssSelector("textarea[class='rr-textarea']"));
		writeAReview.click();
	}

	public void clickAddToBagButton() {
		webElementFactory.getClickableElement("pdp.addtobag.button", false).click();
	}
	
		
	public void clickShareThisButton() {
		webElementFactory.getClickableElement("pdp.sharethis.button", false).click();
	}

	public WebElement getCloseButtonForDomestosPopUp() {
		return webElementFactory.getElement("domestos.cookie.popup");
	}
	
	public WebElement getCloseButtonForCleanipediaPopUp() {
		return webElementFactory.getElement("cleanipedia.banner.close");
	}
	
	public WebElement getCloseButtonForHellmannsSignupPopUp() {
		return webElementFactory.getElement("hellmanns.signup.popup");
	}
	
	public WebElement getBreezeCleanipediaPopUp()
	{
		return webElementFactory.getElement("breeze.cleanipedia.popup");
	}
	
	public WebElement getCanadacookiepopup()
	{
		return webElementFactory.getElement("canada.cookie.popup");
	}
	
	public WebElement getCloseButtonForquizPopUp() {
		return webElementFactory.getElement("close.quiz.popup");
	}
	
	public WebElement getomocookiepopup() {
		return webElementFactory.getElement("omo.cookie.popup");
	}
	
	public WebElement getmagnumcookiepopup() {
		return webElementFactory.getElement("magnum.cookie.popup");
	}
	
	public WebElement getevidonCookiepopup() {
		return webElementFactory.getElement("evidon.cookie.popup");
	}
	
	public WebElement getEvidonCookiepopup() {
		return webElementFactory.getElement("Evidon.cookie.popup");
	}
	
	public WebElement getsitecookiepopup() {
		return webElementFactory.getElement("site.cookie.popup");
	}
	
	public WriteAReviewDialog getWriteAReviewDialog() {
		if (this.writeAReview == null) {
			this.writeAReview = new WriteAReviewDialog(driver,brandName);
		}
		return this.writeAReview;
	}

	public TurnTo getTurnToDialog() {
		if (this.turnTo == null) {
			this.turnTo = new TurnTo(driver,brandName);
		}
		return this.turnTo;
	}

	public AddToBag getAddToBag() {
		if (this.addToBag == null) {
			this.addToBag = new AddToBag(driver,brandName);
		}
		return this.addToBag;
	}

	public WebElement getBuyItNowButton() {
		WebElement binButton = null;
		if(webElementFactory.findElement("pdp.buyitnow.iframe.constantcowidget") != null) {
			//this case is for knorr bin
			driver.switchTo().frame(webElementFactory.getElement("pdp.buyitnow.iframe.constantcowidget",false));
			binButton = webElementFactory.getElement("pdp.buyitnow.choosesupermarket.button", false);
		}else
			//this case is for axe
			binButton = webElementFactory.getClickableElement("pdp.buyitnow.button", false);
		return binButton;
	}

	public String getLocationMessage() throws Throwable {
		driver.switchTo().frame(getBuyItNowIFrame());
        return webElementFactory.getElementIfVisible(
                      webElementFactory.getByLocator("pdp.location.popup.message.text"), false).getText();
 }

	
	public void clickBuyItNowButton() {
		getBuyItNowButton().click();
		driver.switchTo().defaultContent();
	}

	public WebElement getBuyItNowIFrame() {
		return webElementFactory.getElement("pdp.bin.iframe", false);
	}

	public WebElement getShopNowButton() {
		return webElementFactory.getElement("pdp.buyitnow.iframe.constantcowidget", false);
	}

	public WebElement getBuyItNowIFrame(By frameBy) {
		return webElementFactory.getElement(frameBy, false);
	}

	public List<WebElement> getBuyItNowOnlineStoreList() throws Throwable {
		driver.switchTo().defaultContent();
		if (waitForObjectToLoad(webElementFactory.getByLocator("pdp.storelist.dialog"), 15)) {
			return webElementFactory.getElements("pdp.storelist.dialog.list");
		}
		driver.switchTo().frame(getBuyItNowIFrame());
		return webElementFactory.getElements("pdp.bin.online.storelist");
	}
	
	public List<WebElement> getBuyItNowPricespiderStoreList() throws Throwable {
        driver.switchTo().defaultContent();
        if(waitForObjectToLoad(webElementFactory.getByLocator("pdp.target.dialog"), 15)) {
               return webElementFactory.getElements("pdp.target.list");
        }
        driver.switchTo().frame(getBuyItNowIFrame());
        return webElementFactory.getElements("pdp.bin.online.storelist");
 }

	
	
	public List<WebElement> getBuyItNowOnlineStoreList1() throws Throwable 
	{
		driver.switchTo().frame(getBuyItNowIFrame());
		return webElementFactory.getElements("pdp.location.popup.message.text");
	}
	
	

	public List<WebElement> getBuyItNowOnlineStoreListForSmartProductBasketFrame() throws Throwable {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(webElementFactory.findElement("pdp.buyitnow.iframe.smartproductbasket"));
		return webElementFactory.getElements("pdp.bin.online.storelist");
	}

	public List<WebElement> getTabList() {
		return webElementFactory.getElements("pdp.tabbedcontent.tab");
	}

	public WebElement getQuestionAndAnswerTab() {
		List<WebElement> tabList = getTabList();
		return tabList.stream().filter(x -> x.getText().trim().equalsIgnoreCase("Questions And Answers")).findFirst()
				.get();
	}

	public WebElement getRatingsAndReviewsTab() {
		List<WebElement> tabList = getTabList();
		return tabList.stream().filter(x -> x.getText().trim().equalsIgnoreCase("RATINGS & REVIEWS")).findFirst().get();
	}

	public WebElement getTab(String tabText) {
		List<WebElement> tabList = getTabList();
		System.out.println("there are " + tabList.size() + " tabs found");
		return tabList.stream().filter((x) -> x.getText().trim().equalsIgnoreCase(tabText.trim())).findFirst().get();
	}

	public WebElement getTabSection() {
		return webElementFactory.getElement("pdp.tabbedcontent");
	}

	public void expandTurnToSection() {
		webElementFactory.getElement("pdp.turnto.section.link").click();
	}

	public WebElement getTurnToInputText() {
		return webElementFactory.getElement("pdp.turnto.textarea");
	}

	public WebElement getFirstTurnToQuestionSubmitButton() {
		return webElementFactory.getElement("pdp.turnto.submit.first.button");
	}

	public WebElement getSecondTurnToQuestionSubmitButton() {
		return webElementFactory.getElement("pdp.turnto.submit.second.button");
	}

	public WebElement getStoreSearchPDP() {
		return webElementFactory.getElement("pdp.storesearch");
	}
	
	public TurnTo submitTurnToQuestion(String questionText) {
		this.getTurnToInputText().click();

		this.getTurnToInputText().sendKeys(questionText);
		System.out.println("successfully entered question text - " + questionText);
		this.getFirstTurnToQuestionSubmitButton().click();
		System.out.println("succesfully clicked first submit");
		this.getSecondTurnToQuestionSubmitButton().click();
		System.out.println("succesfully clicked second submit");
		return new TurnTo(driver,brandName);
	}

	public List<WebElement> getShopalystOnlineStoreList() throws Throwable {
		return webElementFactory.getElements("pdp.shopalystretailers");
	}

	public List<WebElement> getConstantCommerceOnlineStoreList() throws Throwable {
		return webElementFactory.getElements("pdp.constantcommerce.retailers");
	}

	public List<WebElement> getCartwireOnlineStoreList() throws Throwable {
		return webElementFactory.getElements("pdp.cartwire.reatilers");
	}

	public WebElement pdpPersonalisationText() {
		return webElementFactory.getElement("pdp.personalisation.text");
	}

	public void selectFilterByText(String value) {
		try {
			Select filter = getProductQuantityFilter();
			// filter.selectByIndex(index);
			filter.selectByValue(value);
		} catch (NoSuchElementException e) {
			System.out.println("there is not filter option with value:  " + value);
			e.printStackTrace();
		}
	}

	public Select getProductQuantityFilter() {
		return new Select(webElementFactory.getElement("pdp.productquantity.cartfilter"));
	}
	
}
