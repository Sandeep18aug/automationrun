package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class MultipleRelatedArticlesSidebar extends AbstractComponent {
	
	By MultipleRelatedArticlesSidebarItemBy = By.cssSelector("div[class*='clearfix component multiple-related-articles-sidebar']");
	By MultipleRelatedArticlesSidebarComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.MULTIPLE_RELATED_ARTICLES_SIDEBAR + "']");
	By MultipleRelatedArticlesSidebarHeading = By.cssSelector("a[class='c-multiple-related-articles__adaptive-image']+a[data-ct-action='relatedArticle'] h4");
	By MultipleRelatedArticlesSidebarDescription = By.cssSelector("div[class='c-multiple-related-articles__description'] a[class='o-text__tight js-dotdotdot-truncation is-truncated']");
	By MultipleRelatedArticlesSidebarImageBy = By.cssSelector("a[class='c-multiple-related-articles__adaptive-image']");

	public MultipleRelatedArticlesSidebar(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public List<WebElement> RelatedArticleItemvisible() {
		List<WebElement> multiplemelatedmrticlessidebarItems = webElementFactory.getElements(MultipleRelatedArticlesSidebarImageBy);
		return multiplemelatedmrticlessidebarItems;
	}
	

	public WebElement getMultipleRelatedArticleImages() {
		WebElement relatedArticleImages = webElementFactory.getElementWithInParent(MultipleRelatedArticlesSidebarComponentDivBy,
				MultipleRelatedArticlesSidebarImageBy);
		return relatedArticleImages;
	}
	
	
	public List<WebElement> getMultipleRelatedArticleItems() {
		try {
			return driver.findElements(MultipleRelatedArticlesSidebarItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	

	public WebElement getMultipleRelatedArticleHeading() {
		return webElementFactory.getElement(MultipleRelatedArticlesSidebarHeading);

	}
	
	public WebElement getMultipleRelatedArticleDescription() {
		return webElementFactory.getElementWithInParent(MultipleRelatedArticlesSidebarComponentDivBy, MultipleRelatedArticlesSidebarDescription);
	}


	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
