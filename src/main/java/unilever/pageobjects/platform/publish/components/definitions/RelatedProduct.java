package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class RelatedProduct extends AbstractComponent {

	By RelatedProductsComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.RELATED_PRODUCTS + "']");

	public RelatedProduct(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public List<WebElement> getRelatedProductsVisibleItems() {
		return webElementFactory.getElements("relatedproduct.image");
	}
	
	public WebElement getRelatedProductsVisibleItem() {
		return webElementFactory.getElement("relatedproduct.images"); 
	}
	
	public WebElement getRelatedProductsVisibleItemCif() {
		return webElementFactory.getElement("relatedproductcif.heading"); 
	}

	public WebElement getRelatedProductHeading() {
		return webElementFactory.getElement("relatedproduct.heading");
	}
	
	public WebElement getRelatedProductHeadingAxe() {
		return webElementFactory.getElement("relatedproductaxe.heading");
	}

    public WebElement getRelatedProductbyindexing() {
        return webElementFactory.getElement("relatedproduct.indexing");
 }

    public WebElement getRelatedListingItemImages() {
        WebElement relatedArticleImages = webElementFactory.getElement("relateditem.image");
        return relatedArticleImages;
 }

    public WebElement getRelatedListingItemHeading() {
        return webElementFactory.getElement("relateditem.heading");

 }
 
    public WebElement getRelatedArticleImagePLP() {
        return webElementFactory.getElement("relatedarticles.image");
 }

    public WebElement getProductListingHeadingPLP() {
        return webElementFactory.getElement("productlisting.heading");
 }
	
    public WebElement getRelatedArticleImages() {
        WebElement relatedArticleImages = webElementFactory.getElement("relatedarticles.image");
        return relatedArticleImages;
 }
    public WebElement getRelatedArticleHeading() {
        return webElementFactory.getElement("productlisting.heading");

 }

    public WebElement getRelatedArticleImage() {
        WebElement relatedArticleImages = webElementFactory.getElement("relatedproduct.images");
        return relatedArticleImages;
 }
     
    public List<WebElement> getBoxListingItemvisible() {
        List<WebElement> relatedArticleItems = webElementFactory.getElements("relatedarticles.image");
        return relatedArticleItems;
 }

    public WebElement getBoxListingImages() {
    	WebElement relatedArticleImages = webElementFactory.getElement("relatedarticles.image");
   return relatedArticleImages;
    }
    
    public WebElement getBoxListingHeading() {
        return webElementFactory.getElement("relateditem.heading");

 }

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(RelatedProductsComponentDivBy);
	}
	
}
