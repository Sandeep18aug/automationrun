package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class BwsLayoutWordpress extends AbstractComponent {

	
	By BwsLayoutWordpressItemBy = By.cssSelector("div[class*='bws-sublayout-s2']");
	By BwsLayoutWordpressComponentDivBy = By.cssSelector("div[class*='" + ComponentsDataRole.ARTICLE_NON_D2 + "']");
	By BwsLayoutWordpressHeading = By.cssSelector("h3[class*='bws-caption-title']");
	By BwsLayoutWordpressDescription = By.cssSelector("p[class='bws-caption-desc']");
	By BwsLayoutWordpressImageBy = By.cssSelector("div[class='bws-teaser-image']");
	By BwsLayoutWordpressImgBy = By.cssSelector("div[class='bws-teaser-body']");

	public BwsLayoutWordpress(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public List<WebElement> RelatedArticleItemvisible() {
		List<WebElement> relatedArticleItems = webElementFactory.getElements(BwsLayoutWordpressImageBy);
		return relatedArticleItems;
	}

	public WebElement getBwsLayoutWordpressImages() {
		WebElement bwsLayoutWordpressImages = webElementFactory.getElementWithInParent(BwsLayoutWordpressComponentDivBy,
				BwsLayoutWordpressImageBy);
		return bwsLayoutWordpressImages;
	}
	
	public WebElement getBwsLayoutWordpressImg() {
		WebElement bwsLayoutWordpressImg = webElementFactory.getElementWithInParent(BwsLayoutWordpressComponentDivBy,
				BwsLayoutWordpressImgBy);
		return bwsLayoutWordpressImg;
	}

	
	public List<WebElement> getBwsLayoutWordpressItems() {
		try {
			return driver.findElements(BwsLayoutWordpressItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	

	public WebElement getBwsLayoutWordpressHeading() {
		return webElementFactory.getElement(BwsLayoutWordpressHeading);
	}

	public WebElement getRelatedArticleDescription() {
		return webElementFactory.getElementWithInParent(BwsLayoutWordpressComponentDivBy, BwsLayoutWordpressDescription);
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
