package unilever.pageobjects.platform.publish.components.definitions;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class DiagnosticTool extends AbstractComponent {

	By DTComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.DIAGNOSTIC_TOOL + "']");

	public DiagnosticTool(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public WebElement getStartButton() {
		WebElement startbutton = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.start.button");
		return startbutton;
	}

	public WebElement getWomenRadioButton() {
		WebElement womenrdobtn = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.women.radio");
		return womenrdobtn;
	}

	public WebElement getNextButtonGender() {
		WebElement nxtgenderbtn = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.next.radio");
		return nxtgenderbtn;
	}

	public WebElement getAgeTransitionRadioButton() {
		WebElement ageTransRdoBtn = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.age.radio.first");
		return ageTransRdoBtn;
	}

	public WebElement getNextButtonAgeFactor() {
		WebElement nxtAgeFactorbtn = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.age.next.radio");
		return nxtAgeFactorbtn;
	}

	public WebElement getAgeOfChildren() {
		WebElement childrenAgeRdoBtn = webElementFactory.getElementWithInParent(DTComponentDivBy,
				"dt.children.radio.first");
		return childrenAgeRdoBtn;
	}

	public WebElement getNextButtonChildrenAgeFactor() {
		WebElement nxtChildrenAgeBtn = webElementFactory.getElementWithInParent(DTComponentDivBy,
				"dt.children.age.next.radio");
		return nxtChildrenAgeBtn;
	}

	public WebElement getMiddleAgeChildren() {
		WebElement middleAgeChldrn = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.middle.age.checkbox");
		return middleAgeChldrn;
	}

	public WebElement getFinalAgeChildren() {
		WebElement finalAgeChldrn = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.final.age.checkbox");
		return finalAgeChldrn;
	}

	public WebElement getNextButtonMultipleChildren() {
		WebElement nxtMultipleChildren = webElementFactory.getElementWithInParent(DTComponentDivBy,
				"dt.multiplechildren.age.next.button");
		return nxtMultipleChildren;
	}

	public WebElement getInterest_01() {
		WebElement interestChildren1 = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.dove.interest.01");
		return interestChildren1;
	}

	public WebElement getInterest_02() {
		WebElement interestChildren2 = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.dove.interest.02");
		return interestChildren2;
	}

	public WebElement getNextButtonAllResult() {
		WebElement nxtButtonAllResult = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.allresult.button");
		return nxtButtonAllResult;
	}

	public WebElement getThankYouMsg() {
		WebElement thnkyouMsg = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.thankyou.message");
		return thnkyouMsg;
	}

	public WebElement getRelationShipMsg() {
		WebElement rltnMsg = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.relation.message");
		return rltnMsg;
	}

	public WebElement getFemaleStereotypeMsg() {
		WebElement fmlMsg = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.fml.stereotype.message");
		return fmlMsg;
	}

	public WebElement getPersonalGoalMsg() {
		WebElement persoanlMsg = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.personalgoal.message");
		return persoanlMsg;
	}

	public WebElement getRelationShipCTA() {
		WebElement rltnCTA = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.relationship.message.cta.link");
		return rltnCTA;
	}

	public WebElement getfmlstereotypeCTA() {
		WebElement fmlCTA = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.fml.cta.link");
		return fmlCTA;
	}

	public WebElement getPersoanlGoalCTA() {
		WebElement personalgoalCTA = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.personalgoal.cta.link");
		return personalgoalCTA;
	}

	public WebElement getTwitterIcon() {
		WebElement twitterIcon = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.socialshare.facebook");
		return twitterIcon;
	}

	public WebElement getFBIcon() {
		WebElement fbIcon = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.socialshare.twitter");
		return fbIcon;
	}

	public WebElement getStartAgainButton() {
		WebElement startAgain = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.startagain.button");
		return startAgain;
	}

	public void enterEmailAddressDT(String email) throws ObjectNotFoundInORException {
		webElementFactory.getElement("dt.result.email").sendKeys(email);
		System.out.println("successfully entered email");

	}

	public WebElement getSubmitButtonDT() {
		WebElement submitButton = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.submit.button");
		return submitButton;
	}

	public WebElement getSuccessMsgDT() {
		WebElement successMessage = webElementFactory.getElementWithInParent(DTComponentDivBy, "dt.success.message");
		return successMessage;
	}

	public void getDTOptionListItems(String optionText) {
		// return webElementFactory.getElements(optionsListBy);

		try {
			List<WebElement> filters = webElementFactory.getElements("dt.gender.list.heading");
			filters.stream().filter(x -> x.getText().toLowerCase().contains(optionText.toLowerCase()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Given" + optionText + " is not present");
		}
	}

	public int getRelationShipArticleCount() {
		try {
			waitForPageReady();
			List<WebElement> rltnarticleList = driver.findElements(webElementFactory.getByLocator("dt.relationship.article.list"));
			return rltnarticleList.size();
		} catch (Exception e) {
			return 0;
		}
	}

	public int getFemaleStereoArticleCount() {
		try {
			waitForPageReady();
			List<WebElement> fmlarticleList = driver.findElements(webElementFactory.getByLocator("dt.fml.article.list"));
			return fmlarticleList.size();
		} catch (Exception e) {
			return 0;
		}
	}

	public int getPersonalGoalArticleCount() {
		try {
			waitForPageReady();
			List<WebElement> personalGoalarticleList = driver.findElements(webElementFactory.getByLocator("dt.personalgoal.article.list"));
			return personalGoalarticleList.size();
		} catch (Exception e) {
			return 0;
		}
	}

	public int getOptionsCountOnPage() {
		try {
			waitForPageReady();
			List<WebElement> optionsCountList = driver.findElements(webElementFactory.getByLocator("dt.gender.list.heading"));
			return optionsCountList.size();
		} catch (Exception e) {
			return 0;
		}
	}

	public void getDT_QuestionText(String questionText) {

		try {
			List<WebElement> filters = webElementFactory.getElements(By.cssSelector("h2[class='o-text__heading-2']"));
			filters.stream().filter(x -> x.getText().toLowerCase().contains(questionText.toLowerCase()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Given" + questionText + " is not present");
		}
	}

	public void getDTGender(String optionText) {
		// return webElementFactory.getElements(optionsListBy);

		try {
			List<WebElement> filters = webElementFactory.getElements("dt.gender");
			filters.stream().filter(x -> x.getText().trim().contentEquals(optionText)).findFirst().get().click();
			// return
			// filters.stream().filter(x->x.getText().contentEquals(optionText)).findFirst().get();
			// return
			// filters.stream().filter(x->x.getText().trim().equals(optionText)).findFirst().get();
			// return
			// filters.stream().filter(x->x.getText().matches(optionText)).findFirst().get();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Given " + optionText + " is not present");
			// return null;
		}
	}

	public void getDTAgeSelection(String ageText) {
		// return webElementFactory.getElements(optionsListBy);

		try {
			List<WebElement> filters = webElementFactory.getElements("dt.ageselection.list");
			filters.stream().filter(x -> x.getText().trim().contentEquals(ageText)).findFirst().get().click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Given " + ageText + " is not present");
			// return null;
		}
	}

	public void getDTInterestSelection(String interestText) {

		try {
			List<WebElement> filters = webElementFactory.getElements("dt.interestselection.list");
			filters.stream().filter(x -> x.getText().trim().contentEquals(interestText)).findFirst().get().click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Given " + interestText + " is not present");
			// return null;
		}
	}

	public void getDTAgeFactorSelection(String agefactorText) {

		try {
			List<WebElement> filters = webElementFactory.getElements("dt.agefactor.selection.list");
			filters.stream().filter(x -> x.getText().trim().contentEquals(agefactorText)).findFirst().get().click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Given " + agefactorText + " is not present");
			// return null;
		}
	}

	public void getDTChildrenAgeSelection(String childrenageText) {

		try {
			List<WebElement> filters = webElementFactory.getElements("dt.childrenageselection.list");
			filters.stream().filter(x -> x.getText().trim().contentEquals(childrenageText)).findFirst().get().click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Assert.fail("Given " + childrenageText + " is not present");
			// return null;
		}
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
