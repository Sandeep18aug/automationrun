package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class RichTextV2 extends AbstractComponent {
	public By richtextV2ComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.RICH_TEXT_V2 + "']");

	public RichTextV2(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public String getRichTextV2heading() {
		WebElement heading = waitDriver.until(
				ExpectedConditions.presenceOfNestedElementLocatedBy(richtextV2ComponentDivBy, webElementFactory.
						getByLocator("richtextv2.heading")));
		String h2Heading = heading.getText();
		return h2Heading;
	}

	public WebElement getSmallHeadingInRichText(WebElement richTextComponent) {
		return webElementFactory.getNestedElement(richTextComponent, webElementFactory.
				getByLocator("richtextv2.smallheading"));
	}
	
	public WebElement getDescriptionInRichText(WebElement richTextComponent) {
		return webElementFactory.getNestedElement(richTextComponent, webElementFactory.
				getByLocator("richtextv2.description"));
	}
	
	public WebElement getRichTextV2Description() {
		WebElement Description = waitDriver.until(
				ExpectedConditions.presenceOfNestedElementLocatedBy(richtextV2ComponentDivBy, webElementFactory.
						getByLocator("richtextv2.description")));
		return Description;
	}

	public List<WebElement> getRichTextV2Components() {
		return webElementFactory.getElements(richtextV2ComponentDivBy);
	}
	
	public WebElement getRichTextV2ComponentWithHeading(String heading) {
		List<WebElement> richTextCompList = webElementFactory.getElements(richtextV2ComponentDivBy);
		for (WebElement richTextComp : richTextCompList) {
			if (webElementFactory.getElementWithInParent(richTextComp, By.xpath(".//h2|.//h1")).getText()
					.equalsIgnoreCase(heading)) {
				return richTextComp;
			}
		}
		return null;
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(richtextV2ComponentDivBy);
	}
}
