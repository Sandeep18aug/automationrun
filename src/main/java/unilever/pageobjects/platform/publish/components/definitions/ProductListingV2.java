package unilever.pageobjects.platform.publish.components.definitions;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class ProductListingV2 extends AbstractComponent {
	
	public By productListingV2ComponentDivBy = By.cssSelector("div[data-role='"+ComponentsDataRole.PRODUCT_LISTING_V2+"'], div[data-role='"+ComponentsDataRole.PRODUCT_LISTING+"']");
	
	
	public ProductListingV2(WebDriver driver,String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}
	
	
	public WebElement getProductListingV2Image() {
		return webElementFactory.getElementWithInParent(productListingV2ComponentDivBy, "productlistingv2.image");
	}
	
	public String getProductListingV2Heading() {
		WebElement heading = waitDriver.until(ExpectedConditions.presenceOfNestedElementLocatedBy(productListingV2ComponentDivBy, webElementFactory.getByLocator("productlistingv2.heading")));
		return heading.getText();
	}
	
	public String getProductListingV2SubHeading() {
		WebElement heading = waitDriver.until(ExpectedConditions.presenceOfNestedElementLocatedBy(productListingV2ComponentDivBy, webElementFactory.getByLocator("productlistingv2.subheading")));
		return heading.getText();
	}
	 public WebElement getProductListingV2CtaButton() {
		 return webElementFactory.getElementWithInParent(productListingV2ComponentDivBy, webElementFactory.getByLocator("productlistingv2.cta.link"));
	 }
	 

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(productListingV2ComponentDivBy);
	}

}
