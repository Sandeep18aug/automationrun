package unilever.pageobjects.platform.publish.components.definitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import framework.core.baseClasses.BasePage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class ProductRatingOverview extends AbstractComponent {
	
	public By productRatingOverviewComponentDivBy = By.cssSelector("div[data-role='" +ComponentsDataRole.PRODUCT_RATING_OVERVIEW+"']");
	
	
	public ProductRatingOverview(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(productRatingOverviewComponentDivBy);
	}

}
