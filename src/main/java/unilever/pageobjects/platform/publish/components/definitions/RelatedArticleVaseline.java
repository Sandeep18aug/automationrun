package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class RelatedArticleVaseline extends AbstractComponent {

	
	By RelatedArticleVaselineItemBy = By.cssSelector("section[class='might-like']");
	By RelatedArticleVaselineComponentDivBy = By.cssSelector("section[class='" + ComponentsDataRole.RELATED_ARTICLES_VASELINE + "']");
	By RelatedArticleVaselineHeading = By.cssSelector("li[class*='col-lg-6 col-md-6 col-sm-6 col-xs-12'] div[class*=data]");
	By RelatedArticleVaselineDescription = By.cssSelector("li[class*='col-lg-6 col-md-6 col-sm-6 col-xs-12'] div span[class*=subData]");
	By RelatedArticleVaselineImageBy = By.cssSelector("li[class*='col-lg-6 col-md-6 col-sm-6 col-xs-12'] a figure img");

	public RelatedArticleVaseline(WebDriver driver,String brandName) {
		super(driver, brandName);
	}
	public List<WebElement> RelatedArticleVaselineItemvisible() {
		List<WebElement> relatedArticleVaselineItems = webElementFactory.getElements(RelatedArticleVaselineImageBy);
		return relatedArticleVaselineItems;
	}
	

	public WebElement getRelatedArticleVaselineImages() {
		WebElement relatedArticleVaselineImages = webElementFactory.getElementWithInParent(RelatedArticleVaselineComponentDivBy,
				RelatedArticleVaselineImageBy);
		return relatedArticleVaselineImages;
	}

	
	public List<WebElement> getRelatedArticleVaselineItems() {
		try {
			return driver.findElements(RelatedArticleVaselineItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	

	public WebElement getRelatedArticleVaselineHeading() {
		return webElementFactory.getElement(RelatedArticleVaselineHeading);
	}

	public WebElement getRelatedArticleVaselineDescription() {
		return webElementFactory.getElementWithInParent(RelatedArticleVaselineComponentDivBy, RelatedArticleVaselineDescription);
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
