package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class RelatedArticleCitra extends AbstractComponent {

	By RelatedArticleItemBy = By.cssSelector("div[class*='artikel-side']");
	By RelatedArticleComponentDivBy = By.cssSelector("div[class*='" + ComponentsDataRole.RELATED_ARTICLES_CITRA + "']");
	By RelatedArticleDescription = By.cssSelector("div[class='txt'] h");
	By RelatedArticleImageBy = By.cssSelector("div[class='gi item'] a, div[class*='component-content left']");

	public RelatedArticleCitra(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public List<WebElement> RelatedArticleItemvisible() {
		List<WebElement> relatedArticleItems = webElementFactory.getElements(RelatedArticleImageBy);
		return relatedArticleItems;
	}


	public WebElement getRelatedArticleImages() {
		WebElement relatedArticleImages = webElementFactory.getElementWithInParent(RelatedArticleComponentDivBy,
				RelatedArticleImageBy);
		return relatedArticleImages;
	}
	
	public WebElement getRelatedArticleDescription() {
		return webElementFactory.getElementWithInParent(RelatedArticleComponentDivBy, RelatedArticleDescription);
	}


	
	public List<WebElement> getRelatedArticleItems() {
		try {
			return driver.findElements(RelatedArticleItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
