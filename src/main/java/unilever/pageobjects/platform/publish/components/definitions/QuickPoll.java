package unilever.pageobjects.platform.publish.components.definitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import framework.core.baseClasses.BasePage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class QuickPoll extends AbstractComponent {
	
	public By quickPollComponentDiv = By.cssSelector("div[data-role='" +ComponentsDataRole.QUICK_POLL +"']");
	
	
	public QuickPoll(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}


	@Override
	public Boolean isPresent() {
		return webElementFactory.isElementPresentOnPage(quickPollComponentDiv);
	}
	
	public void selectAnswerByIndex(int index) {
		WebElement answer = webElementFactory.getElements("quickpoll.answer.list").get(index-1);
		answer.click();
	}
	
	public WebElement getAnswerObjectByIndex(int index) {
		WebElement ans =  webElementFactory.getElements("quickpoll.answer.list").get(index-1);
		webElementFactory.focusElement(ans);
		return ans;
	}
	
	public String getQuickPollSubmitMessage() {
		return webElementFactory.getElement("quickpoll.response.heading").getText();
	}
	
	

}
