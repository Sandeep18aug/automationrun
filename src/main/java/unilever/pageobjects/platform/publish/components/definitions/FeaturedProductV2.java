package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;



public class FeaturedProductV2 extends AbstractComponent {

	
	By RelatedArticleItemBy = By.cssSelector("div[class*='featured-product-v2']");
	By FeaturedProductV2ComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.FEATURED_PRODUCT_V2 + "']");
	By FeaturedProductV2Heading = By.cssSelector("h4[class*=o-text__heading-2]");
	By FeaturedProductV2eDescription = By.cssSelector("div[class*='c-text__body']");
	By FeaturedProductV2ImageBy = By.cssSelector("div[class*=c-featured-product-v2__image] a");
	
	public FeaturedProductV2(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public List<WebElement> getFeaturedProdcutV2Items() {
		List<WebElement> relatedArticleItems = webElementFactory.getElements("featuredproductv2.item");
		return relatedArticleItems;
	}
	

	public WebElement getFeaturedProductV2Image() {
		WebElement relatedArticleImages = webElementFactory.getElementWithInParent(FeaturedProductV2ComponentDivBy,
				"featuredproductv2.item.image.link");
		return relatedArticleImages;
	}
	

	public WebElement getFeaturedProductV2Heading() {
		return webElementFactory.getElement("featuredproductv2.item.heading");

	}

	public WebElement getRelatedArticleDescription() {
		return webElementFactory.getElementWithInParent(FeaturedProductV2ComponentDivBy, "featuredproductv2.item.description");
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(FeaturedProductV2ComponentDivBy);
	}

}
