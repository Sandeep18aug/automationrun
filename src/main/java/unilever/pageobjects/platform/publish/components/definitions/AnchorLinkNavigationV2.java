package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.AbstractPublishPage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class AnchorLinkNavigationV2 extends AbstractComponent {
	
	public By anchorLinkNavigationV2 = By.cssSelector("div[data-role='"+ComponentsDataRole.ANCHOR_LINK_NAVIGATION_V2+"']");

    
	public AnchorLinkNavigationV2(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public void selectAnchorLinkNavigationV2Tab(String tabText) {
		
		try {
			WebElement tab = getTabs().stream().filter(x -> x.getText().trim().equalsIgnoreCase(tabText.trim())).findFirst().get();
			tab.click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<WebElement> getTabs(){
		return driver.findElements(webElementFactory.getByLocator("anchorlinknavigationv2.tab.li"));
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(anchorLinkNavigationV2);
	}
}
