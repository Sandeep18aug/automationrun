package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class WhatsNewNonD2 extends AbstractComponent {

	
	By WhatsNewNonD2ItemBy = By.cssSelector("div[class*='whats-new-recent']");
	By WhatsNewNonD2ComponentDivBy = By.cssSelector("div[class*='" + ComponentsDataRole.ARTICLE_PAGE_NON_D2 + "']");
	By WhatsNewNonD2Heading = By.cssSelector("span[class*='h6span-whatsNew']>a");
	By WhatsNewNonD2Description = By.cssSelector("span[class*='h6span-whatsNew']+p+p");
	By WhatsNewNonD2ImageBy = By.cssSelector("section[class*='post']>img");

	public WhatsNewNonD2(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public List<WebElement> RelatedArticleItemvisible() {
		List<WebElement> whatsNewNonD2Items = webElementFactory.getElements(WhatsNewNonD2ImageBy);
		return whatsNewNonD2Items;
	}

	public WebElement getWhatsNewNonD2Images() {
		WebElement whatsNewNonD2Images = webElementFactory.getElementWithInParent(WhatsNewNonD2ComponentDivBy,
				WhatsNewNonD2ImageBy);
		return whatsNewNonD2Images;
	}

	
	public List<WebElement> getWhatsNewNonD2Items() {
		try {
			return driver.findElements(WhatsNewNonD2ItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	

	public WebElement getWhatsNewNonD2Heading() {
		return webElementFactory.getElement(WhatsNewNonD2Heading);
	}

	public WebElement getWhatsNewNonD2Description() {
		return webElementFactory.getElementWithInParent(WhatsNewNonD2ComponentDivBy, WhatsNewNonD2Description);
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
