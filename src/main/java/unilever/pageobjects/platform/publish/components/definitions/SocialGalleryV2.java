package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class SocialGalleryV2 extends AbstractComponent {

	
	By SocialGalleryItemBy = By.cssSelector("div[class*='social-gallery-v2']");
	By SocialGalleryComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.SOCIAL_GALLERY_V2 + "']");
	By SocialGalleryBy = By.cssSelector("div[class*='c-social-gallery-v2-header__cta'] a");

	public SocialGalleryV2(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	
	public WebElement getRelatedArticleCTA() {
		return webElementFactory.getElement(SocialGalleryBy);
	}

	
	public List<WebElement> getRelatedArticleItems() {
		try {
			return driver.findElements(SocialGalleryItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}