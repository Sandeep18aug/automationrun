package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.AbstractPublishPage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class OrderedListDefinition  extends AbstractComponent{
	
	public By orderListComponentBy = By.cssSelector("div[data-role='"+ComponentsDataRole.ORDERED_LIST+"']");
//	public By listItemsInOrderedListBy = By.cssSelector("ul.c-ordered-list-list>li");
//	public By orderedListTitleBy = By.cssSelector(".c-ordered-list-heading");
//	public By orderedListdescriptionBy = By.cssSelector(".c-ordered-list-longdescription");
	
	public OrderedListDefinition(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(orderListComponentBy);
	}
	
	public List<WebElement> getOrderListComponents() {
		return webElementFactory.getElements(orderListComponentBy);
	}
	
	public WebElement getOrederedListHeading(WebElement orderedListComponent) {
		return webElementFactory.getNestedElement(orderedListComponent, webElementFactory.
				getByLocator("orderedlist.title"));
	}
	
	public WebElement getOrederedListDescription(WebElement orderedListComponent) {
		return webElementFactory.getNestedElement(orderedListComponent, webElementFactory.
				getByLocator("orderedlist.description"));
	}
	
	public List<WebElement> getOrederedListItems(WebElement orderedListComponent) {
		return webElementFactory.getNestedElements(orderedListComponent, webElementFactory.
				getByLocator("orderedlist.list"));
	}
}
