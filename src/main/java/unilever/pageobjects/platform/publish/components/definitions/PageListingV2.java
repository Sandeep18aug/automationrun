package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class PageListingV2 extends AbstractComponent {

	By pageListingV2ComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.PAGE_LISTING_V2 + "']");

	//public By PageListingV2PaginationBy = By.cssSelector("button[title='LOAD MORE' i]");

	public PageListingV2(WebDriver driver,String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public List<WebElement> getPageListingItems() {
		return webElementFactory.getElements("pagelistingv2.item");
	}

	public WebElement getShowMoreButtonPageListing() throws Throwable {
		return webElementFactory.getElement("pagelistingv2.pagination.loadmore");
	}

	public WebElement pagelistingv2heading() {
		WebElement heading = waitDriver.until(ExpectedConditions
				.presenceOfNestedElementLocatedBy(pageListingV2ComponentDivBy, webElementFactory.getByLocator("pagelistingv2.pagination.loadmore")));
		return heading;

	}

	public WebElement pagelistingv2image() {
		WebElement image = waitDriver.until(
				ExpectedConditions.presenceOfNestedElementLocatedBy(pageListingV2ComponentDivBy, webElementFactory.getByLocator("pagelistingv2.image")));
		return image;
	}

	public WebElement pagelistingv2suheading() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("pagelistingv2.subheading");
	}

	public WebElement pagelistingv2description() {
		WebElement description = waitDriver.until(ExpectedConditions
				.presenceOfNestedElementLocatedBy(pageListingV2ComponentDivBy, webElementFactory.getByLocator("pagelistingv2.description")));
		return description;

	}

	public WebElement pagelistingv2CTA() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("pagelistingv2.cta");
	}

	public WebElement pagelistingv2pagination() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("pagelistingv2.pagination.loadmore");
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
