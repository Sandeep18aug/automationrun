package unilever.pageobjects.platform.publish.components.definitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class CarouselV2 extends AbstractComponent {
	
	private By carouselV2ComponentDivBy = By.cssSelector("div[data-role='"+ComponentsDataRole.CAROUSEL_V2+"']");
	
	public CarouselV2(WebDriver driver,String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}
	
	
	public WebElement getCarouselV2PreviousButton() {
		return webElementFactory.getElementWithInParent(carouselV2ComponentDivBy, "carouselv2.previous.button");
		 
	}
	
	public WebElement getCarouselV2NextButton() {
		return webElementFactory.getElementWithInParent(carouselV2ComponentDivBy, "carouselv2.next.button");
		 
	}
	
	public WebElement getCarouselV2PauseButton() {
		return webElementFactory.getElementWithInParent(carouselV2ComponentDivBy, "carouselv2.pause.button");
		 
	}
	
	public WebElement getCarouselV2FirstItem() {
		return webElementFactory.getElementWithInParent(carouselV2ComponentDivBy, "carouselv2.item.first");
		 
	}
	
	public WebElement getCarouselV2SecondItem() {
		return webElementFactory.getElementWithInParent(carouselV2ComponentDivBy, "carouselv2.item.second");
		 
	}
	
	public WebElement getCarouselV2CurrentItem() {
		return webElementFactory.getElementWithInParent(carouselV2ComponentDivBy, "carouselv2.item.active");
		 
	}
	
	

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(carouselV2ComponentDivBy);
	}

}
