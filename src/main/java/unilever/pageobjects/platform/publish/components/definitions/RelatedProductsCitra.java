package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class RelatedProductsCitra extends AbstractComponent {

	By RelatedProductsComponentDivBy = By.cssSelector("div[class*='" + ComponentsDataRole.RELATED_PRODUCTS_CITRA + "']");

	public RelatedProductsCitra(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}


    public WebElement getRelatedProductCTA() {
        return webElementFactory.getElement("relatedproductscitra.cta");

 }

    public WebElement getRelatedProductImages() {
        WebElement relatedArticleImages = webElementFactory.getElement("relatedproductscitra.images");
        return relatedArticleImages;
 }
     
    public List<WebElement> getBoxListingItemvisible() {
        List<WebElement> relatedArticleItems = webElementFactory.getElements("relatedarticles.image");
        return relatedArticleItems;
 }

    public WebElement getBoxListingImages() {
    	WebElement relatedArticleImages = webElementFactory.getElement("relatedarticles.image");
   return relatedArticleImages;
    }
    
 

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(RelatedProductsComponentDivBy);
	}

}
