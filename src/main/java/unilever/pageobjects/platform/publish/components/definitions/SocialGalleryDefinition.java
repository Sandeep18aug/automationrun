package unilever.pageobjects.platform.publish.components.definitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import unilever.pageobjects.platform.publish.AbstractPublishPage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class SocialGalleryDefinition extends AbstractComponent {

	public By socialGalleryComponentBy = By.cssSelector("div[data-role='"+ComponentsDataRole.SOCIAL_GALLERY_TAB+"']");
	
	public SocialGalleryDefinition(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(socialGalleryComponentBy);
	}
}
