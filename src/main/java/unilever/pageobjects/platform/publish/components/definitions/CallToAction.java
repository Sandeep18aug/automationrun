package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class CallToAction extends AbstractComponent {

	
	By CTAItemBy = By.cssSelector("div[class*='call-to-action']");
	By CTAComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.CALL_TO_ACTION + "']");
	By CTABy = By.cssSelector("button[data-ct-action='calltoaction']");
	By CTABy1 = By.cssSelector("button[data-target='_blank']"); 

	public CallToAction(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	
	public WebElement getRelatedArticleCTA() {
		return webElementFactory.getElement(CTABy);
	}
	
	public WebElement getRelatedArticleCTA1() {
		return webElementFactory.getElement(CTABy1);
	}
	
	public List<WebElement> getRelatedArticleItems() {
		try {
			return driver.findElements(CTAItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
