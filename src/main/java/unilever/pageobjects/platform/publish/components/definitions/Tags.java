package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class Tags extends AbstractComponent {
	

	public By tagsComponentDivBy = By.cssSelector("div[data-role='"+ComponentsDataRole.TAGS+"']");
	//public By tags = By.cssSelector("li[class='c-tags-list-item']");

	public Tags(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}
	
	public List<WebElement> getTagsList(){
		return webElementFactory.getElements("tag.item");
	}
	
	
	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
