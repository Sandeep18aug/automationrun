package unilever.pageobjects.platform.publish.components.definitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class BackToTopPage extends AbstractComponent {
	

	//CTABackToTopBy Updated by Abhishek for brand LHP on 19-Dec
	//public By CTABackToTopBy = webElementFactory.getByLocator(object)

	public BackToTopPage(WebDriver driver,String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}
	
	public WebElement CTABackToTopvisible() {
		return webElementFactory.getClickableElement(webElementFactory.getByLocator("backtotop.cta.link"), true);
	}
	
	public void clickCTABackToTop() {
		CTABackToTopvisible().click();
	}
	
	
	@Override
	public Boolean isPresent() {
		return webElementFactory.isElementPresentOnPage(webElementFactory.getByLocator("backtotop.cta.link"));
	}
	
	
}
