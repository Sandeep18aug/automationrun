package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class HomeStory extends AbstractComponent {

	
	By HomeStoryItemBy = By.cssSelector("div[class*='home-story']");
	By HomeStoryComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.HOME_STORY + "']");
	By HomeStoryHeading = By.cssSelector("h3[class*='home-story-heading']");
	By HomeStoryDescription = By.cssSelector("p[class*='home-story-copy']");
	By HomeStoryImageBy = By.cssSelector("div[class='home-story-col-image']");
	By HomeStoryCTABy = By.cssSelector("a[class*='home-story-button']");


	public HomeStory(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public List<WebElement> RelatedArticleItemvisible() {
		List<WebElement> homeStoryItems = webElementFactory.getElements(HomeStoryImageBy);
		return homeStoryItems;
	}
	
	public WebElement getHomeStoryCTA() {
		return webElementFactory.getElement(HomeStoryCTABy);
	}

	public WebElement getHomeStoryImages() {
		WebElement homeStoryImages = webElementFactory.getElementWithInParent(HomeStoryComponentDivBy,
				HomeStoryImageBy);
		return homeStoryImages;
	}
	
	
	public List<WebElement> getHomeStoryItems() {
		try {
			return driver.findElements(HomeStoryItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	

	public WebElement getHomeStoryHeading() {
		return webElementFactory.getElement(HomeStoryHeading);
	}

	public WebElement getHomeStoryDescription() {
		return webElementFactory.getElementWithInParent(HomeStoryComponentDivBy, HomeStoryDescription);
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}
}
