package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.AbstractPublishPage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class FeaturedContent extends AbstractComponent {
	

	public By featuredContentComponentDivBy = By.cssSelector("div[data-role='"+ComponentsDataRole.FEATURED_CONTENT+"'],div[class='content paragraphSystem']");



    
	public FeaturedContent(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public WebElement featuredcontentheading() {
		//WebElement heading = waitDriver.until(ExpectedConditions.presenceOfNestedElementLocatedBy(featuredContentComponentDivBy, FeaturedContentHeadingBy));
		WebElement heading = webElementFactory.getElementWithInParent(featuredContentComponentDivBy, "featuredcontent.heading");
		return heading;
	}
	
	public WebElement getFeaturedContentHeading(WebElement featuredContent) {
		//WebElement heading = waitDriver.until(ExpectedConditions.presenceOfNestedElementLocatedBy(featuredContentComponentDivBy, FeaturedContentHeadingBy));
		WebElement heading = webElementFactory.getNestedElement(featuredContent, webElementFactory.getByLocator("featuredcontent.heading"));
		return heading;
	}
	
	public WebElement featuredcontentimage() {
		WebElement image = webElementFactory.getElementWithInParent(featuredContentComponentDivBy, "featuredcontent.image" );
		return image;
	}
	
	public List<WebElement> getFeaturedContentImages(WebElement featuredContent) {
		return webElementFactory.getNestedElements(featuredContent, webElementFactory.getByLocator("featuredcontent.image"));
	}
	
	public WebElement featuredcontentdescription() {
		WebElement description = webElementFactory.getElementWithInParent(featuredContentComponentDivBy, "featuredcontent.description");
		return description;
	}
	
	public WebElement getFeaturedContentDescription(WebElement featuredContent) {
		return webElementFactory.getNestedElement(featuredContent, webElementFactory.getByLocator("featuredcontent.description"));
	}
	
	public WebElement getFeaturedContentVideo(WebElement featuredContent) {
		return webElementFactory.getNestedElement(featuredContent, webElementFactory.getByLocator("featuredcontent.video.play"));
	}
	
	public List<WebElement> getfeaturedcontentCTA() {
		return webElementFactory.getElements("featuredcontent.cta");
	}
	
	public List<WebElement> getFeaturedContentCTAs(WebElement featuredContent) {
		return webElementFactory.getNestedElements(featuredContent, webElementFactory.getByLocator("featuredcontent.cta"));
	}
	
	public WebElement getfeaturedcontentCTA1() {
		WebElement description = webElementFactory.getElementWithInParent(featuredContentComponentDivBy, "featuredcontent.cta.first");
		return description;
	}
	
	public List<WebElement> getFeacturedContentList(){
		return webElementFactory.getElements("featuredcontent.div");
		//return driver.findElements(searchListingV2ItemsBy);
	}
	
	public List<WebElement> getFeacturedContents(){
		return webElementFactory.getElements(featuredContentComponentDivBy);
	}
	
	public WebElement getfeaturedcontentCTALink(WebElement element) {
		return element.findElement(By.cssSelector("a[data-ct-action='clicktoaction']"));
	}
	
	
	public WebElement getaTbbedContentByIndex(int index) {
		return webElementFactory.getElements("tabbedcontent.link").get(index);
	}
	
	public WebElement gettabbedcontent1() {
		return webElementFactory.findElement(webElementFactory.getByLocator("tabbedcontent.first"));
	}
	public WebElement gettabbedcontent2() {
		return webElementFactory.findElement(webElementFactory.getByLocator("tabbedcontent.second"));
	}
	public WebElement gettabbedcontent3() {
		return webElementFactory.findElement(webElementFactory.getByLocator("tabbedcontent.third"));
	}
	public WebElement gettabbedcontent4() {
		return webElementFactory.findElement(webElementFactory.getByLocator("tabbedcontent.fourth"));
	}
	public WebElement gettabbedcontent5() {
		return webElementFactory.findElement(webElementFactory.getByLocator("tabbedcontent.fifth"));
	}
	public WebElement gettabbedcontent6() {
		return webElementFactory.findElement(webElementFactory.getByLocator("tabbedcontent.sixth"));
	}
	
	public WebElement getFeaturedContentText() {
		return webElementFactory.getElement("featuredcontent.text");
	}
	
	public WebElement getFeaturedContentImage() {
		return webElementFactory.getElement("featuredcontent.image");
	}
	
	public WebElement getFeaturedContentHeading() {
		return webElementFactory.getElement("featuredcontent.heading");	
		
	}
	
	public WebElement getFeaturedContentCTA() {
		return webElementFactory.getElement("featuredcontent.cta");
	}


	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(featuredContentComponentDivBy);
	}
}
