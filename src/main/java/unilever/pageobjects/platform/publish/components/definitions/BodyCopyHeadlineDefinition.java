package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class BodyCopyHeadlineDefinition extends AbstractComponent {
	
	public By bodyCopyHeadlineComponentBy = By.cssSelector("div[data-role='"+ComponentsDataRole.BODY_COPY_HEADLINE+"']");
	
	public BodyCopyHeadlineDefinition(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(bodyCopyHeadlineComponentBy);
	}
	
	public List<WebElement> getBodyCopyHeadlineElements() throws ObjectNotFoundInORException {
		return webElementFactory.getElement(bodyCopyHeadlineComponentBy).findElements(webElementFactory.getByLocator("bodycopy.headline"));
	}
	
	public List<WebElement> getBodyCopyArticleHeadline() throws ObjectNotFoundInORException {
		return webElementFactory.getElement(bodyCopyHeadlineComponentBy).findElements(webElementFactory.getByLocator("bodycopy.article.headline"));
	}
	
	public List<WebElement> getBodyCopySocialSharingHeadline() throws ObjectNotFoundInORException {
		return webElementFactory.getElement(bodyCopyHeadlineComponentBy).findElements(webElementFactory.getByLocator("bodycopy.socialsharing.headline"));
	}
}
