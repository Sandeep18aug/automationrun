package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class BwsLayoutRelatedArticleRexona extends AbstractComponent {

	
	By RelatedArticleItemBy = By.cssSelector("div[class*='bws-body-layout-l']");
	By RelatedArticleComponentDivBy = By.cssSelector("div[id='" + ComponentsDataRole.RELATED_ARTICLES_REXONA + "']");
	By RelatedArticleHeading = By.cssSelector("div[class='bws-caption'] h3");
	By RelatedArticleDescription = By.cssSelector("div[class='bws-caption'] h3 + p");
	By RelatedArticleImageBy = By.cssSelector("div[class='bws-teaser-body'] a div img");
	
	public BwsLayoutRelatedArticleRexona(WebDriver driver,String brandName) {
		super(driver, brandName);
	} 
		
	

	public List<WebElement> RelatedArticleItemvisible() {
		List<WebElement> relatedArticleItems = webElementFactory.getElements(RelatedArticleImageBy);
		return relatedArticleItems;
	}
	
	public WebElement getRelatedArticleImages() {
		WebElement relatedArticleImages = webElementFactory.getElementWithInParent(RelatedArticleComponentDivBy,
				RelatedArticleImageBy);
		return relatedArticleImages;
	}

	
	public List<WebElement> getRelatedArticleItems() {
		try {
			return driver.findElements(RelatedArticleItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	

	public WebElement getRelatedArticleHeading() {
		return webElementFactory.getElement(RelatedArticleHeading);

	}

	public WebElement getRelatedArticleDescription() {
		return webElementFactory.getElementWithInParent(RelatedArticleComponentDivBy, RelatedArticleDescription);
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
