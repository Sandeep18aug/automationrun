package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class MediaGalleryV2 extends AbstractComponent {

	
	By MediaGalleryV2ItemBy = By.cssSelector("div[class*='gallery']");
	By MediaGalleryV2ImgBy = By.cssSelector("div[class*='thumbnail-img'] img[itemprop*='image']");
	By MediaGalleryV2ComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.MEDIA_GALLERY_V2 + "']");
	By MediaGalleryV2ImageBy = By.cssSelector("div[class*='c-media-gallery-v2__thumbnail-img'] p a");
	//By MediaGalleryV2ImageBy = By.xpath("(//div[contains(@class,'c-media-gallery-v2__thumbnail-img')])[1]");
	public MediaGalleryV2(WebDriver driver,String brandName) {
		super(driver, brandName);
	}


	public WebElement getMediaGalleryV2Images() {
		WebElement mediaGalleryV2Images = webElementFactory.getElementWithInParent(MediaGalleryV2ComponentDivBy,
				MediaGalleryV2ImageBy);
		return mediaGalleryV2Images;
	}

	

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
