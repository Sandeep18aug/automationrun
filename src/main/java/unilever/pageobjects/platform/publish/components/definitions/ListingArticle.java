package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class ListingArticle extends AbstractComponent {

	
	By ListingArticleItemBy = By.cssSelector("div[class*='composite-related_articles'],div[class*='composite-variation-filtered-articles']");
	By ListingArticleComponentDivBy = By.cssSelector("div[data-id='" + ComponentsDataRole.LISTING_ARTICLE + "']");
	By ListingArticleHeading = By.cssSelector("div[class='richText-content']>h3");
	By ListingArticleDescription = By.cssSelector("div[class='richText component section default-style even last initialized'],div[class*='richText-content']>h3+p");
	By ListingArticleImageBy = By.cssSelector("div[class*='image component section image-small first odd initialized'],div[class*='image component section default-style first odd initialized'],div[class='image component section image-medium first odd initialized'], div[class*='image component section image-small first odd grid_6 initialized'],RletaedArticle_Img=xpath=(//div[@class='image component section image-small first odd grid_6 initialized'])[last()]");

	public ListingArticle(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public List<WebElement> ListingArticleItemvisible() {
		List<WebElement> listingArticleItems = webElementFactory.getElements(ListingArticleImageBy);
		return listingArticleItems;
	}

	public WebElement getListingArticleImages() {
		WebElement listingArticleImages = webElementFactory.getElementWithInParent(ListingArticleComponentDivBy,
				ListingArticleImageBy);
		return listingArticleImages;
	}

	
	public List<WebElement> getListingArticleItems() {
		try {
			return driver.findElements(ListingArticleItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	

	public WebElement getListingArticleHeading() {
		return webElementFactory.getElement(ListingArticleHeading);

	}

	public WebElement getListingArticleDescription() {
		return webElementFactory.getElementWithInParent(ListingArticleComponentDivBy, ListingArticleDescription);
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
