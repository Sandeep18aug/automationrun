package unilever.pageobjects.platform.publish.components.definitions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class RelatedArticle extends AbstractComponent {

	
	By RelatedArticleItemBy = By.cssSelector("div[class*='articles']");
	//By RelatedArticleImgBy = By.cssSelector("div[class*='item-image'] img[class*='lazyloaded']");
	By RelatedArticleComponentDivBy = By.cssSelector("div[data-role='" + ComponentsDataRole.RELATED_ARTICLES + "']");
	By RelatedArticleHeading = By.cssSelector("div[class*='c-related-articles-heading'],h3[class='c-related-articles-item-content__heading'],h3[class='text-xl font-program font-extrabold px-6 pb-6']");
	By RelatedArticleDescription = By.cssSelector("div[class*='content__desc");
	By RelatedArticleImageByAxe = By.xpath("(//div[contains(@class,'c-related-articles-item-image')])[1]");
	By RelatedArticleImageByClear = By.xpath("(//div[@class='js-related-articles-carousel enable-related-articles-carousel slick-initialized slick-slider']//div[@class='c-related-articles-item-image '])[4]");
	By RelatedArticleImageBy = By.cssSelector("a[class*='c-related-articles-item-image__link'],div[class*='c-related-articles-item-image'],div[class='related-article'],div[class*='c-related-articles-item-image '],div[class*='rounded-tl rounded-tr gatsby-image-wrapper']");
	By RelatedArticleCTABy = By.cssSelector("a[data-ct-action='clicktoaction'][tabindex='0'],button[data-ct-action='calltoaction'],a[class*='c-related-articles-item-content__link'],a[class*='c-related-articles-item-image__link']");

	public RelatedArticle(WebDriver driver,String brandName) {
		super(driver, brandName);
	}

	public List<WebElement> RelatedArticleItemvisible() {
		List<WebElement> relatedArticleItems = webElementFactory.getElements(RelatedArticleImageBy);
		return relatedArticleItems;
	}
	
	public WebElement getRelatedArticleCTA() {
		return webElementFactory.getElement(RelatedArticleCTABy);
	}

	public WebElement getRelatedArticleImages() {
		WebElement relatedArticleImages = webElementFactory.getElementWithInParent(RelatedArticleComponentDivBy,
				RelatedArticleImageBy);
		return relatedArticleImages;
	}
	
	public WebElement getRelatedArticleImagesAxe() {
		WebElement relatedArticleImages = webElementFactory.getElementWithInParent(RelatedArticleComponentDivBy,
				RelatedArticleImageByAxe);
		return relatedArticleImages;
	}
	
	public WebElement getRelatedArticleImagesClear() {
		WebElement relatedArticleImages = webElementFactory.getElementWithInParent(RelatedArticleComponentDivBy,
				RelatedArticleImageByClear);
		return relatedArticleImages;
	}

	
	public List<WebElement> getRelatedArticleItems() {
		try {
			return driver.findElements(RelatedArticleItemBy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	

	public WebElement getRelatedArticleHeading() {
		return webElementFactory.getElement(RelatedArticleHeading);

		/*
		 * WebElement heading =
		 * waitDriver.until(ExpectedConditions.presenceOfNestedElementLocatedBy(
		 * RelatedArticleComponentDivBy, RelatedArticleHeading)); return
		 * heading.getText();
		 */
	}

	public WebElement getRelatedArticleDescription() {
		return webElementFactory.getElementWithInParent(RelatedArticleComponentDivBy, RelatedArticleDescription);
	}

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return null;
	}

}
