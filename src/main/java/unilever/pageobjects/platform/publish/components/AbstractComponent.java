package unilever.pageobjects.platform.publish.components;

import org.openqa.selenium.WebDriver;

import framework.core.baseClasses.BasePage;

public abstract class AbstractComponent extends BasePage implements Component {

	public AbstractComponent(WebDriver driver, String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public abstract Boolean isPresent();
	
}
