package unilever.pageobjects.platform.publish.components.definitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import framework.core.baseClasses.BasePage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.AbstractComponent;

public class HeroV2 extends AbstractComponent {
	




	public By heroV2ComponentDivBy = By.cssSelector("div[data-role='"+ComponentsDataRole.HERO_V2+"'],div[data-role='"+ComponentsDataRole.HERO+"'],.carousel-content");



	

	
	public HeroV2(WebDriver driver,String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}
	
	
	public WebElement getHeroV2Image() {
		WebElement image = waitDriver.until(ExpectedConditions.presenceOfNestedElementLocatedBy(heroV2ComponentDivBy, webElementFactory.getByLocator("heroV2.image")));
		return image;
	}
	
	public String getHeroV2Heading() {
		WebElement heading = waitDriver.until(ExpectedConditions.presenceOfNestedElementLocatedBy(heroV2ComponentDivBy, webElementFactory.getByLocator("heroV2.heading")));
		return heading.getText();
	}
	
	public String getHeroV2BodyContent() {
		try {
			WebElement content = waitDriver.until(ExpectedConditions.presenceOfNestedElementLocatedBy(heroV2ComponentDivBy, webElementFactory.getByLocator("heroV2.body.content")));
			return content.getText();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	 public WebElement getHeroV2CtaButton() {
		 return webElementFactory.getElementWithInParent(heroV2ComponentDivBy, "heroV2.cta");
	 }
	 
	 public WebElement getHeroV2ReadMoreLink() {
		 return webElementFactory.getElementWithInParent(heroV2ComponentDivBy, "heroV2.readMore");
	 }

	@Override
	public Boolean isPresent() {
		// TODO Auto-generated method stub
		return webElementFactory.isElementPresentOnPage(heroV2ComponentDivBy);
	}

}
