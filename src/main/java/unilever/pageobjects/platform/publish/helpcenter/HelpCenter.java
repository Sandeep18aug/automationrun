package unilever.pageobjects.platform.publish.helpcenter;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import unilever.pageobjects.platform.publish.AbstractPublishPage;

public class HelpCenter extends AbstractPublishPage {

	// By EmailUsBy = By.cssSelector("//a[@title='EMAIL US']");
	
	public HelpCenter(WebDriver driver, String brandName) {
		super(driver, brandName);
	}

	public WebElement getemailus() {
		return webElementFactory.getElement("helpcenter.emailus.link");

	}

	public List<WebElement> getFaqHeadings() {
		try {
			return driver.findElements(webElementFactory.getByLocator("helpcenter.panel.heading"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public List<WebElement> getFaqPanelAnswers() {
		try {
			return driver.findElements(webElementFactory.getByLocator("helpcenter.panel.faq.answers"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

}
