package unilever.pageobjects.platform.publish.alp;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import framework.exceptions.ObjectNotFoundInORException;
import unilever.pageobjects.platform.publish.AbstractPublishPage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;

public class ArticleListingPage extends AbstractPublishPage {

	// By pageListingV2ItemsBy = By.cssSelector("ul[class*='page-listing-v2-listing__list']>li");
	// By pageLisitngV2By = By.cssSelector("div[data-role='" + ComponentsDataRole.PAGE_LISTING_V2 + "']");
	// By articleListingV2ItemsBy = By
	// 		.cssSelector(".js-multiple-related-carousel-placeholder .o-text__heading-2,"
	// 				+ "div[data-role='related-articles'] div[class*='c-related-articles-item slick-slide'],"
	// 				+ ".js-multiple-related-articles-carousel>.c-related-article-wrapper");
	// By pageListingV2FilterBy = By.cssSelector("select.o-filter-dropdown.c-listing-v2-filter__dropdown,"
	// 		+ ".c-product-listing__overlay-static__filter .o-filter-dropdown");
	// By pageListingV2FilterCtaCount = By.cssSelector(".c-page-listing-v2-filter-cta span.js-filter-count,"
	// 		+ ".c-product-listing__overlay-static__filter .c-product-listing__filter-block .js-filter-count");
	// By pageListingV2FilterBlock = By.cssSelector("div.c-page-listing-v2-filter__block");
	// By articleListingTagItem = By.cssSelector("ul[class='c-tags-list']");
	// By articleListingTagItemLink = By.cssSelector("li[class='c-tags-list-item' a]");
	// By articleListingTag = By.xpath("//a[@title='article']");
	// By articleListingDescription = By.cssSelector("div[class*='linkclick'] h1");
	// By articleListingOrderedList = By.cssSelector("ul[class='c-ordered-list  ']");
	// By articleListingOrderedListLink = By.cssSelector("li[class*='c-ordered-list__item']");
	// By anchorlink1By = By.xpath("(.//span[@class='c-navigation-sticky__button'])[1]");
	// By anchorlink2By = By.xpath("(.//span[@class='c-navigation-sticky__button'])[2]");
	// By anchorlink3By = By.xpath("(.//span[@class='c-navigation-sticky__button'])[3]");
	// By anchorlink4By = By.xpath("(.//span[@class='c-navigation-sticky__button'])[4]");
	// By anchorlink5By = By.xpath("(.//span[@class='c-navigation-sticky__button'])[5]");

	// public ArticleListingPage(WebDriver driver,String brandName) {
	// 	super(driver, brandName);
	// 	// TODO Auto-generated constructor stub
	// }

	// public WebElement getArticleListItemLink(WebElement element) {
	// 	return element.findElement(By.cssSelector("articleListingTagItemLink"));
	// }
	
	// public List<WebElement> getArticleListingListItems() {
	// 	return driver.findElements(articleListingTagItem);
	// }

	// public WebElement getArticleListingOrderedListLink(WebElement element) {
	// 	return element.findElement(By.cssSelector("articleListingOrderedListLink"));
	// }

	// public List<WebElement> getArticleListingOrderedList() {
	// 	return driver.findElements(articleListingOrderedList);
	// }

	// public WebElement getArticlelistingdescription() {
	// 	return webElementFactory.getElement(articleListingDescription);
	// }

	// public WebElement getFilterContainerBlock() {
	// 	return webElementFactory.getElement(pageListingV2FilterBlock);
	// }

	// public WebElement getPageListingV2UnorderedListFilter() {
	// 	return webElementFactory.getElementWithInParent(getFilterContainerBlock(), By.cssSelector("ul.list"));
	// }

	// public List<WebElement> getPageListingV2ListItems() {
	// 	return driver.findElements(pageListingV2ItemsBy);
	// }

	// public WebElement getArticleTag() {
	// 	return webElementFactory.getElement(articleListingTag);
	// }
	// public List<WebElement> getArticleListingV2ListItems() {
	// 	return driver.findElements(articleListingV2ItemsBy);
	// }

	// public WebElement getPageListingV2Component() {
	// 	return webElementFactory.getElement(pageLisitngV2By);
	// }
	
	// public WebElement getanchorlink1() {
	// 	return webElementFactory.getElement(anchorlink1By);
	// }
	
	// public WebElement getanchorlink2() {
	// 	return webElementFactory.getElement(anchorlink2By);
	// }
	
	// public WebElement getanchorlink3() {
	// 	return webElementFactory.getElement(anchorlink3By);
	// }
	
	// public WebElement getanchorlink4() {
	// 	return webElementFactory.getElement(anchorlink4By);
	// }
	
	// public WebElement getanchorlink5() {
	// 	return webElementFactory.getElement(anchorlink5By);
	// }

	// public Select getPageListingV2Filter() {
	// 	return new Select(webElementFactory.getElement(pageListingV2FilterBy));
	// }

	// public void selectFilterByText(String filterText) {
	// 	try {
	// 		Select filter = getPageListingV2Filter();
	// 		WebElement optionToBeSelected = null;
	// 		optionToBeSelected = filter.getOptions().stream().filter(x -> x.getText().contains(filterText)).findFirst()
	// 				.get();
	// 		optionToBeSelected.click();
	// 	} catch (NoSuchElementException e) {
	// 		System.out.println("there is no filter option having text ");
	// 		e.printStackTrace();
	// 	}
	// }

	// public void selectFilterByIndex(int filterIndex) {
	// 	try {
	// 		Select filter = getPageListingV2Filter();
	// 		filter.selectByIndex(filterIndex);
	// 	} catch (NoSuchElementException e) {
	// 		System.out.println("there is no filter option having index " + filterIndex);
	// 		e.printStackTrace();
	// 	}
	// }

	// public String getPageListingFilterCtaCount() {
	// 	try {
	// 		return webElementFactory.getElement(pageListingV2FilterCtaCount).getAttribute("innerHTML").trim();
	// 	} catch (Exception e) {
	// 		// TODO Auto-generated catch block
	// 		return null;
	// 	}
	// }

	// public int getPageListingV2FilterOptionsCount() {
	// 	return getPageListingV2Filter().getOptions().size();
	// }

	// public void selectFilterULByIndex(int index) {
	// 	try {
	// 		WebElement ul = getPageListingV2UnorderedListFilter();
	// 		webElementFactory.getElementWithInParent(ul, By.xpath("./parent::div/span")).click();
	// 		ul.findElements(By.xpath("./li")).get(index).click();
	// 	} catch (NoSuchElementException e) {
	// 		System.out.println("there is not filter option with index " + index);
	// 		e.printStackTrace();
	// 	}
	// }

	// public int getPageListingV2FilterUnorderedListOptionsCount() {
	// 	return getPageListingV2UnorderedListFilter().findElements(By.xpath("./li")).size();
	// }

	//By pageListingV2ItemsBy = By.cssSelector("ul[class*='page-listing-v2-listing__list']>li");
	By pageLisitngV2By = By.cssSelector("div[data-role='" + ComponentsDataRole.PAGE_LISTING_V2 + "']");
	
//	By articleListingV2ItemsBy = By
//			.cssSelector(".js-multiple-related-carousel-placeholder .o-text__heading-2,"
//					+ "div[data-role='related-articles'] div[class*='c-related-articles-item slick-slide'],"
//					+ ".js-multiple-related-articles-carousel>.c-related-article-wrapper");
//	By pageListingV2FilterBy = By.cssSelector("select.o-filter-dropdown.c-listing-v2-filter__dropdown,"
//			+ ".c-product-listing__overlay-static__filter .o-filter-dropdown");
	
//	By pageListingV2FilterCtaCount = By.cssSelector(".c-page-listing-v2-filter-cta span.js-filter-count,"
//			+ ".c-product-listing__overlay-static__filter .c-product-listing__filter-block .js-filter-count");
//	By pageListingV2FilterBlock = By.cssSelector("div.c-page-listing-v2-filter__block");
//	By articleListingTagItem = By.cssSelector("ul[class='c-tags-list']");
//	By articleListingTagItemLink = By.cssSelector("li[class='c-tags-list-item' a]");
//	By articleListingTag = By.xpath("//a[@title='article']");
//	By articleListingDescription = By.cssSelector("div[class*='linkclick'] h1");
//	By articleListingOrderedList = By.cssSelector("ul[class='c-ordered-list  ']");
//	By articleListingOrderedListLink = By.cssSelector("li[class*='c-ordered-list__item']");
	By anchorlink1By = By.xpath("(.//span[@class='c-navigation-sticky__button'])[1]");
	By anchorlink2By = By.xpath("(.//span[@class='c-navigation-sticky__button'])[2]");
	By anchorlink3By = By.xpath("(.//span[@class='c-navigation-sticky__button'])[3]");
	By anchorlink4By = By.xpath("(.//span[@class='c-navigation-sticky__button'])[4]");
	By anchorlink5By = By.xpath("(.//span[@class='c-navigation-sticky__button'])[5]");

	public ArticleListingPage(WebDriver driver,String brandName) {
		super(driver, brandName);
		// TODO Auto-generated constructor stub
	}

	public WebElement getArticleListItemLink(WebElement element) {
		return element.findElement(By.cssSelector("articleListingTagItemLink"));
	}
	
	public List<WebElement> getArticleListingListItems() {
		return driver.findElements(webElementFactory.getByLocator("pagelistingv2.tag.item"));
	}

	public WebElement getArticleListingOrderedListLink(WebElement element) {
		return element.findElement(By.cssSelector("articleListingOrderedListLink"));
	}

	public List<WebElement> getArticleListingOrderedList() {
		return driver.findElements(webElementFactory.getByLocator("pagelistingv2.orderedlist"));
	}

	public WebElement getArticlelistingdescription() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("pagelistingv2.description");
	}

	public WebElement getFilterContainerBlock() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("pagelistingv2.filter.div");
	}

	public WebElement getPageListingV2UnorderedListFilter() throws ObjectNotFoundInORException {
		return webElementFactory.getElementWithInParent(getFilterContainerBlock(), By.cssSelector("ul.list"));
	}

	public List<WebElement> getPageListingV2ListItems() {
		return driver.findElements(webElementFactory.getByLocator("pagelistingv2.item"));
	}

	public WebElement getArticleTag() throws ObjectNotFoundInORException {
		return webElementFactory.getElement("pagelistingv2.tag.item.link");
	}
	public List<WebElement> getArticleListingV2ListItems() {
		return driver.findElements(webElementFactory.getByLocator("relatedarticle.item.heading"));
	}

	public WebElement getPageListingV2Component() {
		return webElementFactory.getElement(pageLisitngV2By);
	}
	
	public WebElement getanchorlink1() {
		return webElementFactory.getElement(anchorlink1By);
	}
	
	public WebElement getanchorlink2() {
		return webElementFactory.getElement(anchorlink2By);
	}
	
	public WebElement getanchorlink3() {
		return webElementFactory.getElement(anchorlink3By);
	}
	
	public WebElement getanchorlink4() {
		return webElementFactory.getElement(anchorlink4By);
	}
	
	public WebElement getanchorlink5() {
		return webElementFactory.getElement(anchorlink5By);
	}

	public Select getPageListingV2Filter() throws ObjectNotFoundInORException {
		return new Select(webElementFactory.getElement("pagelistingv2.filter.listbox"));
	}

	public void selectFilterByText(String filterText) throws ObjectNotFoundInORException {
		try {
			Select filter = getPageListingV2Filter();
			WebElement optionToBeSelected = null;
			optionToBeSelected = filter.getOptions().stream().filter(x -> x.getText().contains(filterText)).findFirst()
					.get();
			optionToBeSelected.click();
		} catch (NoSuchElementException e) {
			System.out.println("there is no filter option having text ");
			e.printStackTrace();
		}
	}

	public void selectFilterByIndex(int filterIndex) throws ObjectNotFoundInORException {
		try {
			Select filter = getPageListingV2Filter();
			filter.selectByIndex(filterIndex);
		} catch (NoSuchElementException e) {
			System.out.println("there is no filter option having index " + filterIndex);
			e.printStackTrace();
		}
	}

	public String getPageListingFilterCtaCount() {
		try {
			return webElementFactory.getElement("pagelistingv2.filter.count").getAttribute("innerHTML").trim();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public int getPageListingV2FilterOptionsCount() throws ObjectNotFoundInORException {
		return getPageListingV2Filter().getOptions().size();
	}

	public void selectFilterULByIndex(int index) throws ObjectNotFoundInORException {
		try {
			WebElement ul = getPageListingV2UnorderedListFilter();
			webElementFactory.getElementWithInParent(ul, By.xpath("./parent::div/span")).click();
			ul.findElements(By.xpath("./li")).get(index).click();
		} catch (NoSuchElementException e) {
			System.out.println("there is not filter option with index " + index);
			e.printStackTrace();
		}
	}

	public int getPageListingV2FilterUnorderedListOptionsCount() throws ObjectNotFoundInORException {
		return getPageListingV2UnorderedListFilter().findElements(By.xpath("./li")).size();
	}
	
}
