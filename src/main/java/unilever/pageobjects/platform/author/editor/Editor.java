package unilever.pageobjects.platform.author.editor;

public interface Editor {
	
	public void insertComponentInPage(String componentText, String pagePath);
	
}
