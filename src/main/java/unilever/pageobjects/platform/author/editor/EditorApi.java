package unilever.pageobjects.platform.author.editor;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.testng.Assert;

import com.google.common.base.Strings;

import framework.shared.FrameworkConstants;
import unilever.pageobjects.platform.usermanager.User;
import unilever.pageobjects.platform.usermanager.UserFactory;

//import shared.Constants;

public class EditorApi implements Editor {
	User user = UserFactory.getRandomUserFromList();

	@Override
	public void insertComponentInPage(String componentText, String pagePath) {
		// TODO Auto-generated method stub
		String componentValue = getComponentValue(componentText);
		given().
		auth().basic(user.getCrxUserId(),user.getCrxPassword()).
		param("./@CopyFrom","/apps/unilever-iea/components/"+componentValue+"/cq:template").
		param("_charset_","utf-8").
		param("./jcr:created","").		
		param("./jcr:createdBy","").
		param("./jcr:lastModified","").
		param("./jcr:lastModifiedBy","").		
		param("./sling:resourceType","unilever-iea/components/" + componentValue).
		param("parentResourceType","foundation/components/parsys").
		param(":order","last").
		param(":nameHint",componentValue).
	when().
		post(pagePath + "/jcr:content/flexi_hero_par/").
	then();	
	}
	
	private String getComponentValue(String componentText) {
		String value = null;
		try {
			componentText = componentText.replace(" ", "").trim();
			Properties componentProperties = new Properties();
			File componentMappingFile = new File(FrameworkConstants.SRC_MAIN_RESOURCES + "ComponentsMapping.properties");
			FileInputStream fs = new FileInputStream(componentMappingFile);
			componentProperties.load(fs);
			value = componentProperties.getProperty(componentText, null);
			if(Strings.isNullOrEmpty(value )) {
				Assert.fail(componentText + " has no mapping in ComponentsMapping.properties file");
			}			
		} catch (Exception e) {
			System.out.println("Exception occurred while fetching value from ComponentsMapping.properties file.");
		}		
		return value;
	}

}
