package unilever.pageobjects.platform.author.siteadmin;

public interface SiteAdmin {
	public void createPage(String baseUrl,String parentPath, String title, String label, String template);
	public void createLiveCopy(String baseUrl,String parentPath, String title, String label, String srcPath);
	public void deletePage(String baseUrl, String path);
	public void activatePage(String baseUrl, String path);
	public void deactivatePage(String baseUrl, String path);
	public void rollout(String baseUrl, String path, String targetPath);
}
