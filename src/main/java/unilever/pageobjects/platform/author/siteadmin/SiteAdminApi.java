package unilever.pageobjects.platform.author.siteadmin;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.testng.Assert;
import org.testng.util.Strings;

import unilever.pageobjects.platform.usermanager.User;
import unilever.pageobjects.platform.usermanager.UserFactory;

public class SiteAdminApi extends AbstractSiteAdmin {
	String strWcmCommandString = "/bin/wcmcommand";
	String strReplicateJson = "/bin/replicate.json";
	User user = UserFactory.getRandomUserFromList();
	@Override
	public void createPage(String baseUrl, String parentPathContentOnwards, String title, String label, String template) {
		if(Strings.isNullOrEmpty(parentPathContentOnwards)) {
			Assert.fail("parentPath can't be empty or null");
		}
		if(Strings.isNullOrEmpty(title) || Strings.isNullOrEmpty(label)) {
			Assert.fail("title/lable can't be empty or null");
		}
		if(Strings.isNullOrEmpty(template)) {
			Assert.fail("template can't be empty or null");
		}
		deletePage(baseUrl, parentPathContentOnwards + "/" + label, true);
		given().
		auth().basic(user.getCrxUserId(),user.getCrxPassword()).
		param("cmd","createPage").
		param("parentPath",parentPathContentOnwards).
		param("title",title).
		param("label",label).
		param("template",template).
		param("_charset_","utf-8").
	when().
		post(baseUrl + strWcmCommandString).
	then().statusCode(200);
	}

	@Override
	public void deletePage(String baseUrl, String path) {
		if(Strings.isNullOrEmpty(path)){
			Assert.fail("pagepath to be deleted can't be null or empty");
		}
		deactivatePage(baseUrl, path);
		given().
			auth().basic(user.getCrxUserId(),user.getCrxPassword()).
			param("cmd","deletePage").
			//param("path","/content/whitelabel/en_ca/home/manual/test").
			param("path",path).
			param("force","false").
			param("_charset_","utf-8").
		when().
			post(baseUrl + strWcmCommandString).
		then().statusCode(200);		
	}
	
	public void deletePage(String baseUrl, String path, boolean force) {
		if(Strings.isNullOrEmpty(path)){
			Assert.fail("pagepath to be deleted can't be null or empty");
		}
		deactivatePage(baseUrl, path);
		given().
			auth().basic(user.getCrxUserId(),user.getCrxPassword()).
			param("cmd","deletePage").
			//param("path","/content/whitelabel/en_ca/home/manual/test").
			param("path",path).
			param("force",force).
			param("_charset_","utf-8").
		when().
			post(baseUrl + strWcmCommandString).
		then().statusCode(200);		
	}

	@Override
	public void activatePage(String baseUrl, String path) {
		if(Strings.isNullOrEmpty(path)){
			Assert.fail("pagepath to be activated can't be null or empty");
		}
		given().
		auth().basic(user.getCrxUserId(),user.getCrxPassword()).
		param("cmd","Activate").
		param("path",path).
		param("_charset_","utf-8").
	when().
		post(baseUrl + strReplicateJson).
	then().statusCode(200);
	System.out.println("successfully published page through api  - " + path);	
	}

	@Override
	public void deactivatePage(String baseUrl, String path) {
		if(Strings.isNullOrEmpty(path)){
			Assert.fail("pagepath to be deactivated can't be null or empty");
		}
		given().
		auth().basic(user.getCrxUserId(),user.getCrxPassword()).
		param("cmd","Deactivate").
		//param("path","/content/whitelabel/en_ca/home/manual/testpage").
		param("path",path).
		param("_charset_","utf-8").
	when().
		post(baseUrl + strReplicateJson).
	then().statusCode(200);		
	}

	@Override
	public void createLiveCopy(String baseUrl, String parentPathContentOnwards, String title, String label, String srcPath) {
		// TODO Auto-generated method stub
		if(Strings.isNullOrEmpty(parentPathContentOnwards)) {
			Assert.fail("parentPathContentOnwards can't be empty or null");
		}
		if(Strings.isNullOrEmpty(title) || Strings.isNullOrEmpty(label)) {
			Assert.fail("title/lable can't be empty or null");
		}
		if(Strings.isNullOrEmpty(srcPath)) {
			Assert.fail("srcPath can't be empty or null");
		}
		deletePage(baseUrl, parentPathContentOnwards + "/" + label,true);
		given().
		auth().basic(user.getCrxUserId(),user.getCrxPassword()).
		param("cmd","createLiveCopy").
		param("_charset_","utf-8").
		param(":status","browser").
		param("missingPage@Delete","true").
		param("excludeSubPages@Delete","true").
		param("destPath",parentPathContentOnwards).
		param("title",title).
		param("label",label).
		param("srcPath",srcPath).
	when().
		post(baseUrl + strWcmCommandString).
	then().statusCode(200);	

	}

	/* (non-Javadoc)
	 * @see unilever.platform.author.siteadmin.SiteAdmin#rollout(java.lang.String, java.lang.String, java.lang.String)
	 * Description: This method is used to rollout a page in siteadmin of AEM. 
	 * 
	 */
	@Override
	public void rollout(String baseUrl, String path, String targetPath) {
		// TODO Auto-generated method stub
		if(Strings.isNullOrEmpty(path)) {
			Assert.fail("path can't be empty or null");
		}
		if(Strings.isNullOrEmpty(targetPath)) {
			Assert.fail("targetPath can't be empty or null");
		}
		given().
		auth().basic(user.getCrxUserId(),user.getCrxPassword()).
		param("cmd","rollout").
		param("_charset_","utf-8").
		param(":status","browser").
		param("paras","").
		param("path",path).
		param("msm:targetPath",targetPath).		
	when().
		post(baseUrl + strWcmCommandString).
	then().statusCode(200);	
		
	}
	
/*	private String getComponentValue(String baseUrl, String componentText) {
		String value = null;
		try {
			componentText = componentText.replace(" ", "").trim();
			Properties componentProperties = new Properties();
			File componentMappingFile = new File(Constants.SRC_MAIN_RESOURCES + "ComponentsMapping.properties");
			FileInputStream fs = new FileInputStream(componentMappingFile);
			componentProperties.load(fs);
			value = componentProperties.getProperty(componentText, null);
			if(Strings.isNullOrEmpty(value )) {
				Assert.fail(componentText + " has no mapping in ComponentsMapping.properties file");
			}			
		} catch (Exception e) {
			System.out.println("Exception occurred while fetching value from ComponentsMapping.properties file.");
		}		
		return value;
	}*/
}
