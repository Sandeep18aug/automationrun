package unilever.pageobjects.platform.usermanager;

public class User {
	private String crxUserId = null;
	private String crxPassword = null;
	private String aemUserId = null;
	private String aemPassword = null;

	public User(String crxuserid, String crxpassword, String aemuserid, String aempassword) {
		this.crxUserId = crxuserid;
		this.crxPassword = crxpassword;
		this.aemUserId = aemuserid;
		this.aemPassword = aempassword;
	}

	public String getCrxUserId() {
		return crxUserId;
	}

	public void setCrxUserId(String crxUserId) {
		this.crxUserId = crxUserId;
	}

	public String getCrxPassword() {
		return crxPassword;
	}

	public void setCrxPassword(String crxPassword) {
		this.crxPassword = crxPassword;
	}

	public String getAemUserId() {
		return aemUserId;
	}

	public void setAemUserId(String aemUserId) {
		this.aemUserId = aemUserId;
	}

	public String getAemPassword() {
		return aemPassword;
	}

	public void setAemPassword(String aemPassword) {
		this.aemPassword = aemPassword;
	}

	@Override
	public String toString() {
		return "Users [crxuserid=" + crxUserId + ", crxpassword=" + crxPassword + ", aemuserid=" + aemUserId
				+ ", aempassword=" + aemPassword + "]";
	}
}
