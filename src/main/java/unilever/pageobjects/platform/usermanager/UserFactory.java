package unilever.pageobjects.platform.usermanager;

import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import framework.exceptions.UsersNotSetupException;
import framework.shared.FrameworkConstants;

public class UserFactory implements JsonDeserializer<User> {
	private static String JSON = null;
	private static List<User> usersList = null;
	private static User defaultUser = new User(FrameworkConstants.CRX_SUPERADMIN_USER_ID, FrameworkConstants.CRX_SUPERADMIN_PASSWORD_ID, FrameworkConstants.AEM_SUPERADMIN_USER_ID, FrameworkConstants.AEM_SUPERADMIN_PASSWORD_ID );
	
	static {
		try {
			loadUsersFromJson();
		} catch (UsersNotSetupException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(usersList.isEmpty()){
				usersList.add(defaultUser);
			}
		}
	}

	public User deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		return null;
	}

	private static void loadUsersFromJson() throws UsersNotSetupException, Exception {
		JSON = new String(Files.readAllBytes(Paths.get(FrameworkConstants.USERS_JSON_FILE_PATH)));
		Type targetClassType = new TypeToken<ArrayList<User>>() {
		}.getType();
		usersList = new Gson().fromJson(JSON, targetClassType);		
	}

	public static synchronized User getRandomUserFromList(){
		try {
			int randomNumber = new Random().nextInt(usersList.size());
			return usersList.get(randomNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return defaultUser;
	}
}
