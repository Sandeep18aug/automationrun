package steps.business.published.components;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import cucumber.api.java.en.Then;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.BodyCopyHeadlineDefinition;

public class StepsBodyCopyHeadline {

	UserActions actions;
	World world;
	BodyCopyHeadlineDefinition bodyCopyHeadlineDef;
	SoftAssert softAssert;
	
	public StepsBodyCopyHeadline(World world, UserActions actions) {
		this.actions = actions;
		this.world = world;
		bodyCopyHeadlineDef = new BodyCopyHeadlineDefinition(world.getWebDriver(),world.getBrandName());
		softAssert = new SoftAssert();
	}
	
	@Then("^(?:i|I) verify if body copy headline has social sharing links if body copy headline component is available$")
	public void verifyBodyCopyHeadlineSocialSharingLinksPresent() {
		try {
			if (bodyCopyHeadlineDef.isPresent()) {
				List<WebElement> bodyCopyHeadlineComponents = world.getWebElementFactory().getElements(bodyCopyHeadlineDef.bodyCopyHeadlineComponentBy);
				int bodyCopyHeadlineCount  = bodyCopyHeadlineComponents.size();
				world.reportStepInfo("The page has " + bodyCopyHeadlineCount + " body copy headline component.");
				for (int bodyCopyHeadlineIndex = 0; bodyCopyHeadlineIndex < bodyCopyHeadlineCount; bodyCopyHeadlineIndex ++) {
					List<WebElement> bodyCopyHeadlineSocialShareLinks = bodyCopyHeadlineDef.getBodyCopySocialSharingHeadline();
					int bodyCopyHeadlineSocialShareLinksCount = bodyCopyHeadlineSocialShareLinks.size();
					if(bodyCopyHeadlineSocialShareLinksCount > 0) {
						world.reportStepInfo("Body copy headline " + (bodyCopyHeadlineIndex + 1) + " has " + 
								bodyCopyHeadlineSocialShareLinksCount + " social share links.");
					} else
						world.reportStepInfo("Body copy headline " + (bodyCopyHeadlineIndex + 1) + " has no social share links.");
				}
			} else
				world.reportStepInfo("body copy headline component isn't available in page.");
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Then("^(?:i|I) expect body copy headline title has some text if body copy headline component is available$")
	public void verifyBodyCopyHeadlineTitleTextPresent() {
		try {
			if (bodyCopyHeadlineDef.isPresent()) {
				List<WebElement> bodyCopyHeadlineComponents = world.getWebElementFactory().getElements(bodyCopyHeadlineDef.bodyCopyHeadlineComponentBy);
				int bodyCopyHeadlineCount  = bodyCopyHeadlineComponents.size();
				world.reportStepInfo("The page has " + bodyCopyHeadlineCount + " body copy headline component.");
				for (int bodyCopyHeadlineIndex = 0; bodyCopyHeadlineIndex < bodyCopyHeadlineCount; bodyCopyHeadlineIndex ++) {
					List<WebElement> bodyCopyHeadlineTitles =  bodyCopyHeadlineDef.getBodyCopyHeadlineElements();
					int bodyCopyHeadlineTitleListSize = bodyCopyHeadlineTitles.size();
					softAssert.assertTrue((bodyCopyHeadlineTitleListSize != 0), "Body copy headline " + (bodyCopyHeadlineIndex + 1) +
							" has no title.");
					softAssert.assertTrue((bodyCopyHeadlineTitleListSize == 1), "Body copy headline " + (bodyCopyHeadlineIndex + 1) +
							" has more than one titles instead of only one.");
					softAssert.assertTrue(!(bodyCopyHeadlineTitles.get(0).getText().trim().isEmpty()), "Body copy headline " + 
							(bodyCopyHeadlineIndex + 1) + " has title but title doesn't contain text.");
				}
				softAssert.assertAll();
			} else
				world.reportStepInfo("body copy headline component isn't available in page.");			
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Then("^(?:i|I) expect body copy headline article has some text if body copy headline component is available$")
	public void verifyBodyCopyHeadlineArticleTextPresent() {
		try {
			if (bodyCopyHeadlineDef.isPresent()) {
				List<WebElement> bodyCopyHeadlineComponents = world.getWebElementFactory().getElements(bodyCopyHeadlineDef.bodyCopyHeadlineComponentBy);
				int bodyCopyHeadlineCount  = bodyCopyHeadlineComponents.size();
				world.reportStepInfo("The page has " + bodyCopyHeadlineCount + " body copy headline component.");
				for (int bodyCopyHeadlineIndex = 0; bodyCopyHeadlineIndex < bodyCopyHeadlineCount; bodyCopyHeadlineIndex ++) {
					List<WebElement> bodyCopyHeadlineArticleList = bodyCopyHeadlineDef.getBodyCopyArticleHeadline();
					int bodyCopyHeadlineArticleListSize = bodyCopyHeadlineArticleList.size();
					softAssert.assertTrue((bodyCopyHeadlineArticleListSize != 0), "Body copy headline " + (bodyCopyHeadlineIndex + 1) +
							" has no articles.");
					softAssert.assertTrue((bodyCopyHeadlineArticleListSize == 1), "Body copy headline " + (bodyCopyHeadlineIndex + 1) +
							" has more than one articles instead of only one.");
					softAssert.assertTrue(!(bodyCopyHeadlineArticleList.get(0).getText().trim().isEmpty()), "Body copy headline " + 
							(bodyCopyHeadlineIndex + 1) + " has article but article doesn't contain text.");
				}
				softAssert.assertAll();
			} else
				world.reportStepInfo("body copy headline component isn't available in page.");			
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
