package steps.business.published.components;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import cucumber.api.java.en.Then;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.SocialGalleryDefinition;

public class StepsSocialGallery {

	UserActions actions;
	World world;
	SoftAssert softAssert;
	SocialGalleryDefinition socialGalleryDef;
	PublishedPageCommon publishedPageCommon;

	public StepsSocialGallery(World world, UserActions actions) {
		this.actions = actions;
		this.world = world;
		softAssert = new SoftAssert();
		socialGalleryDef = new SocialGalleryDefinition(world.getWebDriver(),world.getBrandName());
		publishedPageCommon = new PublishedPageCommon(world.getWebDriver(),world.getBrandName());
		
	}
	
//	@Then("^(?:i|I) click on all the items one by one and close the opened item panel if social gallery is available$")
//	public void clickItemClosePanel() throws Throwable {
//		try {
//			if (socialGalleryDef.ispresent()) {
//				List<WebElement> socialGalleryComponents = world.getWebElementFactory().getElements(socialGalleryDef.socialGalleryComponentBy);
//				int socialGalleryComponentCount = socialGalleryComponents.size();
//				world.reportStepInfo("Page has " + socialGalleryComponentCount + " social gallery component.");
//				for (int socialGalleryIndex = 0; socialGalleryIndex < socialGalleryComponentCount; socialGalleryIndex ++) {
//					actions.Scroll.scrollUntilInViewport(socialGalleryComponents.get(socialGalleryIndex));
//					actions.Scroll.scrollByYAxis(400);
//					actions.Wait.wait("3");
//					List<WebElement> socialGalleryItems = world.getWebElementFactory().getNestedElements(socialGalleryComponents.get
//							(socialGalleryIndex), socialGalleryDef.socialGalleryItemsBy);
//					int socialGalleryItemTotalCount = socialGalleryItems.size();
//					world.reportStepInfo("Verifying if social gallery " + (socialGalleryIndex + 1) + " has at least one item.");
//					if (socialGalleryItemTotalCount == 0)
//						Assert.fail("Social gallery " + (socialGalleryIndex + 1) + " has no items.");
//					else {
//						WebElement seeMoreBtn = world.getWebElementFactory().getNestedElement(socialGalleryComponents.get(socialGalleryIndex), 
//								socialGalleryDef.socialGallerySeeMoreBtnBy);
//						while (seeMoreBtn != null) {
//							actions.Scroll.scrollUntilInViewport(seeMoreBtn);
//							actions.Click.click(seeMoreBtn);
//							actions.Wait.wait("3");
//							seeMoreBtn = world.getWebElementFactory().getNestedElement(socialGalleryComponents.get(socialGalleryIndex), 
//									socialGalleryDef.socialGallerySeeMoreBtnBy);
//						}
//						socialGalleryItems = world.getWebElementFactory().getNestedElements(socialGalleryComponents.get
//								(socialGalleryIndex), socialGalleryDef.socialGalleryItemsBy);
//						socialGalleryItemTotalCount = socialGalleryItems.size();
//						for (int galleryItemIndex = 0; galleryItemIndex < socialGalleryItemTotalCount; galleryItemIndex ++) {
//							actions.Click.click(socialGalleryItems.get(galleryItemIndex));
//							actions.Wait.wait("1");
//							WebElement activeItemPanel = world.getWebElementFactory().getNestedElement(socialGalleryComponents.get
//									(socialGalleryIndex), socialGalleryDef.socialGalleryItemActivePanelBy);
//							WebElement activePanelCloseBtn = world.getWebElementFactory().getNestedElement(activeItemPanel, 
//									socialGalleryDef.socialGalleryItemActivePanelCloseBy);
//							WebElement activePanelNextBtn = world.getWebElementFactory().getNestedElement(activeItemPanel, 
//									socialGalleryDef.socialGalleryItemActivePanelNextBy);
//							WebElement activePanelText = world.getWebElementFactory().getNestedElement(activeItemPanel, 
//									socialGalleryDef.socialGalleryItemActivePanelTextBy);
//							WebElement activePanelPreviousBtn = world.getWebElementFactory().getNestedElement(activeItemPanel, 
//									socialGalleryDef.socialGalleryItemActivePanelPreviousBy);
//							String itemImageSrc = world.getWebElementFactory().getNestedElement(socialGalleryItems.get(galleryItemIndex), 
//									socialGalleryDef.socialGalleryItemsImagesBy).getAttribute("src").trim();
//							String itemImageURL = itemImageSrc.substring(itemImageSrc.indexOf("://") + 3, 
//									itemImageSrc.indexOf("ulenscale") - 1);
//							String activePanelImageSrc = world.getWebElementFactory().getNestedElement(activeItemPanel, 
//									socialGalleryDef.socialGalleryItemActivePanelImageBy).getAttribute("src").trim();
//							String activePanelImageURL = activePanelImageSrc.substring(activePanelImageSrc.indexOf("://") + 3, 
//									activePanelImageSrc.indexOf("ulenscale") - 1);
//							world.reportStepInfo("Verifying if the selected item image URL and the image"
//									+ " URL are same in social gallery " + (socialGalleryIndex + 1));
//							softAssert.assertTrue(itemImageURL.equals(activePanelImageURL), "In social gallery " + 
//									(socialGalleryIndex + 1) +" the images in active panel and selected item aren't same.\nThe selected item"
//											+ " image URL: " + itemImageURL + "\nThe active panel image URL: " + activePanelImageURL);
//							world.reportStepInfo("Verifying previous button except for first item in social gallery " 
//									+ (socialGalleryIndex + 1));
//							if (galleryItemIndex == 0)
//								softAssert.assertTrue(activePanelPreviousBtn == null, "The active panel has previous button in "
//									+ "social gallery " + (socialGalleryIndex + 1) + " even though the displayed item is the first one.");
//							else
//								softAssert.assertTrue(activePanelPreviousBtn != null, "The active panel doen't have previous "
//										+ "button for item number " + (galleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + 
//											" items in social gallery " + (socialGalleryIndex + 1));
//							world.reportStepInfo("Verifying next button except for last item in social gallery " 
//									+ (socialGalleryIndex + 1));
//							if (galleryItemIndex == (socialGalleryItemTotalCount - 1))
//								softAssert.assertTrue(activePanelNextBtn == null, "The last active panel has next button in "
//									+ "social gallery " + (socialGalleryIndex + 1) + " even though the displayed item is the last one.");
//							else
//								softAssert.assertTrue(activePanelNextBtn != null, "The active panel doen't have next button for item "
//										+ "number " + (galleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + " items in "
//											+ "social gallery " + (socialGalleryIndex + 1));
//							softAssert.assertTrue(activePanelCloseBtn != null, "The active panel doen't have next button for item "
//									+ "number " + (galleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + " items in "
//										+ "social gallery " + (socialGalleryIndex + 1));
//							softAssert.assertTrue(!activePanelText.getText().trim().isEmpty(), "In forward traverse:The active panel doen't "
//									+ "have content text for item number " + (galleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + 
//										" items in social gallery " + (socialGalleryIndex + 1));
//							softAssert.assertTrue(!activePanelText.getText().trim().isEmpty(), "In forward traverse:The active panel doen't "
//									+ "have content text for item number " + (galleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + 
//										" items in social gallery " + (socialGalleryIndex + 1));
//							actions.Click.click(activePanelCloseBtn);
//							activeItemPanel = world.getWebElementFactory().getNestedElement(socialGalleryComponents.get
//									(socialGalleryIndex), socialGalleryDef.socialGalleryItemActivePanelBy);
//							softAssert.assertTrue(activeItemPanel == null, "Close button doesn't work, active panel doesn't close"
//									+ " even after clicking close button.");
//						}
//					}
//				}
//				softAssert.assertAll();
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			Assert.fail(e.getMessage());
//		}
//	}
//	
//	@Then("^(?:i|I) traverse the social galerry items by clicking available Next and previous buttons if social gallery is available$")
//	public void traverseItemsWithNextPreviousButtons() throws Throwable {
//		try {
//			if (socialGalleryDef.ispresent()) {
//				List<WebElement> socialGalleryComponents = world.getWebElementFactory().getElements(socialGalleryDef.socialGalleryComponentBy);
//				int socialGalleryComponentCount = socialGalleryComponents.size();
//				world.reportStepInfo("Page has " + socialGalleryComponentCount + " social gallery component.");
//				for (int socialGalleryIndex = 0; socialGalleryIndex < socialGalleryComponentCount; socialGalleryIndex ++) {
//					actions.Scroll.scrollUntilInViewport(socialGalleryComponents.get(socialGalleryIndex));
//					actions.Scroll.scrollByYAxis(400);
//					actions.Wait.wait("3");
//					List<WebElement> socialGalleryItems = world.getWebElementFactory().getNestedElements(socialGalleryComponents.get
//							(socialGalleryIndex), socialGalleryDef.socialGalleryItemsBy);
//					int socialGalleryItemTotalCount = socialGalleryItems.size();
//					world.reportStepInfo("Verifying if social gallery " + (socialGalleryIndex + 1) + " has at least one item.");
//					if (socialGalleryItemTotalCount == 0)
//						Assert.fail("Social gallery " + (socialGalleryIndex + 1) + " has no items.");
//					else if (socialGalleryItemTotalCount == 1)
//						world.reportStepInfo("The social gallery " + (socialGalleryIndex + 1) + " has only one item.");
//					else if (socialGalleryItemTotalCount > 1) {
//						int nextClickCount = 0;
//						int socialGalleryItemIndex = 0;
//						WebElement seeMoreBtn = world.getWebElementFactory().getNestedElement(socialGalleryComponents.get(socialGalleryIndex), 
//								socialGalleryDef.socialGallerySeeMoreBtnBy);;
//						for (			;			;			) {
//							if (socialGalleryItemIndex < socialGalleryItemTotalCount) {
//								WebElement activeItemPanel = world.getWebElementFactory().getNestedElement(socialGalleryComponents.get
//										(socialGalleryIndex), socialGalleryDef.socialGalleryItemActivePanelBy);
//								WebElement activePanelCloseBtn = world.getWebElementFactory().getNestedElement(activeItemPanel, 
//										socialGalleryDef.socialGalleryItemActivePanelCloseBy);
//								WebElement activePanelNextBtn = world.getWebElementFactory().getNestedElement(activeItemPanel, 
//										socialGalleryDef.socialGalleryItemActivePanelNextBy);
//								WebElement activePanelText = world.getWebElementFactory().getNestedElement(activeItemPanel, 
//										socialGalleryDef.socialGalleryItemActivePanelTextBy);
//								WebElement activePanelPreviousBtn = world.getWebElementFactory().getNestedElement(activeItemPanel, 
//										socialGalleryDef.socialGalleryItemActivePanelPreviousBy);
//								world.reportStepInfo("In forward traverse:Verifying previous button except for first item in social gallery " 
//										+ (socialGalleryIndex + 1));
//								if (socialGalleryItemIndex == 0)
//									softAssert.assertTrue(activePanelPreviousBtn == null, "In forward traverse:The active panel has previous button in "
//										+ "social gallery " + (socialGalleryIndex + 1) + " even though the displayed item is the first one.");
//								else
//									softAssert.assertTrue(activePanelPreviousBtn != null, "In forward traverse:The active panel doen't have previous "
//											+ "button for item number " + (socialGalleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + 
//												" items in "
//												+ "social gallery " + (socialGalleryIndex + 1));
//								world.reportStepInfo("In forward traverse:Verifying next button except for last item in social gallery " 
//										+ (socialGalleryIndex + 1));
//								if (seeMoreBtn == null && socialGalleryItemIndex == (socialGalleryItemTotalCount - 1))
//									softAssert.assertTrue(activePanelNextBtn == null, "In forward traverse:The last active panel has next button in "
//										+ "social gallery " + (socialGalleryIndex + 1) + " even though the displayed item is the last one.");
//								else
//									softAssert.assertTrue(activePanelNextBtn != null, "In forward traverse:The active panel doen't have next button for item "
//											+ "number " + (socialGalleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + " items in "
//												+ "social gallery " + (socialGalleryIndex + 1));
//								softAssert.assertTrue(activePanelCloseBtn != null, "In forward traverse:The active panel doen't have next button for item "
//										+ "number " + (socialGalleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + " items in "
//											+ "social gallery " + (socialGalleryIndex + 1));
//								softAssert.assertTrue(!activePanelText.getText().trim().isEmpty(), "In forward traverse:The active panel doen't "
//										+ "have content text for item number " + (socialGalleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + 
//											" items in social gallery " + (socialGalleryIndex + 1));
//								int[] countOfRowColumns = publishedPageCommon.getCountOfRowColumn(socialGalleryItems);
//								int actuLRowCount = countOfRowColumns[0];
//								int actualColumnCount = countOfRowColumns[1];
//								int expectedRowCount = publishedPageCommon.getExpectedRowCount(socialGalleryItemTotalCount);
//								world.reportStepInfo("In forward traverse:Verifying if social gallery " + (socialGalleryIndex + 1) + 
//										" has items in 3X3 format.");
//								softAssert.assertTrue(actualColumnCount <= 3, "In forward traverse:There are more than 3 columns of items instead of "
//										+ "maximum 3 column.");
//								softAssert.assertTrue(expectedRowCount == actuLRowCount, "In forward traverse:Actual number of rows " + actuLRowCount + 
//										" isn't same as expected number of rows " + expectedRowCount + " for social gallery " +(socialGalleryIndex + 1));
//								String itemImageSrc = world.getWebElementFactory().getNestedElement(socialGalleryItems.get(socialGalleryItemIndex), 
//										socialGalleryDef.socialGalleryItemsImagesBy).getAttribute("src").trim();
//								String itemImageURL = itemImageSrc.substring(itemImageSrc.indexOf("://") + 3, 
//										itemImageSrc.indexOf("ulenscale") - 1);
//								String activePanelImageSrc = world.getWebElementFactory().getNestedElement(activeItemPanel, 
//										socialGalleryDef.socialGalleryItemActivePanelImageBy).getAttribute("src").trim();
//								String activePanelImageURL = activePanelImageSrc.substring(activePanelImageSrc.indexOf("://") + 3, 
//										activePanelImageSrc.indexOf("ulenscale") - 1);
//								world.reportStepInfo("In forward traverse:Verifying if URL of item " + (socialGalleryItemIndex + 1) + 
//										" image and the panel image URL are same in social gallery " + (socialGalleryIndex + 1));
//								softAssert.assertTrue(itemImageURL.equals(activePanelImageURL), "In forward traverse:In social gallery " + 
//										(socialGalleryIndex + 1) +" the active panel image and image of selected item number " + 
//											(socialGalleryItemIndex + 1) + " aren't same.\nThe selected item"
//												+ " image URL: " + itemImageURL + "\nThe active panel image URL: " + activePanelImageURL);
//								if ((seeMoreBtn != null) || ((socialGalleryItemIndex + 1) < socialGalleryItemTotalCount)) {
//									actions.Scroll.scrollUntilInViewport(activePanelNextBtn);
//									actions.Click.click(activePanelNextBtn);
//									actions.Wait.wait("1");
//									nextClickCount ++;
//									socialGalleryItemIndex ++;
//									if (nextClickCount == 9) {
//										actions.Wait.wait("4");
//										socialGalleryItems = world.getWebElementFactory().getNestedElements(socialGalleryComponents.get
//												(socialGalleryIndex), socialGalleryDef.socialGalleryItemsBy);
//										socialGalleryItemTotalCount = socialGalleryItems.size();
//										seeMoreBtn = world.getWebElementFactory().getNestedElement(socialGalleryComponents.get(socialGalleryIndex), 
//												socialGalleryDef.socialGallerySeeMoreBtnBy);										
//										if (seeMoreBtn != null)											
//											nextClickCount = 0;	
//									}
//								} else
//									socialGalleryItemIndex ++;
//							} else {
//								socialGalleryItemIndex --;
//								for (			;			;			) {
//									if (socialGalleryItemIndex >= 0) {
//										WebElement activeItemItemPanel = world.getWebElementFactory().getNestedElement(socialGalleryComponents.get
//												(socialGalleryIndex), socialGalleryDef.socialGalleryItemActivePanelBy);
//										WebElement activePanelCloseBtn = world.getWebElementFactory().getNestedElement(activeItemItemPanel, 
//												socialGalleryDef.socialGalleryItemActivePanelCloseBy);
//										WebElement activePanelNextBtn = world.getWebElementFactory().getNestedElement(activeItemItemPanel, 
//												socialGalleryDef.socialGalleryItemActivePanelNextBy);
//										WebElement activePanelText = world.getWebElementFactory().getNestedElement(activeItemItemPanel, 
//												socialGalleryDef.socialGalleryItemActivePanelTextBy);
//										WebElement activePanelPreviousBtn = world.getWebElementFactory().getNestedElement(activeItemItemPanel, 
//												socialGalleryDef.socialGalleryItemActivePanelPreviousBy);
//										world.reportStepInfo("In backward traverse:Verifying previous button except for first item in social gallery " 
//												+ (socialGalleryIndex + 1));
//										if (socialGalleryItemIndex == 0)
//											softAssert.assertTrue(activePanelPreviousBtn == null, "In backward traverse:The active panel has previous button in "
//												+ "social gallery " + (socialGalleryIndex + 1) + " even though the displayed item is the first one.");
//										else
//											softAssert.assertTrue(activePanelPreviousBtn != null, "In backward traverse:The active panel doen't "
//													+ "have previous button for item number " + (socialGalleryItemIndex + 1) + " out of " + 
//														socialGalleryItemTotalCount + " items in social gallery " + (socialGalleryIndex + 1));
//										world.reportStepInfo("In backward traverse:Verifying next button except for last item in social gallery " 
//												+ (socialGalleryIndex + 1));
//										if (socialGalleryItemIndex == (socialGalleryItemTotalCount - 1))
//											softAssert.assertTrue(activePanelNextBtn == null, "In backward traverse:The last active panel has next button in "
//												+ "social gallery " + (socialGalleryIndex + 1) + " even though the displayed item is the last one.");
//										else
//											softAssert.assertTrue(activePanelNextBtn != null, "In backward traverse:The active panel doen't have next button "
//													+ "for item number " + (socialGalleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + " items in "
//														+ "social gallery " + (socialGalleryIndex + 1));
//										softAssert.assertTrue(activePanelCloseBtn != null, "In backward traverse:The active panel doen't have next button for item "
//												+ "number " + (socialGalleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + " items in "
//													+ "social gallery " + (socialGalleryIndex + 1));
//										softAssert.assertTrue(!activePanelText.getText().trim().isEmpty(), "In backward traverse:The active panel doen't "
//												+ "have content text for item number " + (socialGalleryItemIndex + 1) + " out of " + socialGalleryItemTotalCount + 
//													" items in social gallery " + (socialGalleryIndex + 1));
//										int[] countOfRowColumns = publishedPageCommon.getCountOfRowColumn(socialGalleryItems);
//										int actuLRowCount = countOfRowColumns[0];
//										int actualColumnCount = countOfRowColumns[1];
//										int expectedRowCount = publishedPageCommon.getExpectedRowCount(socialGalleryItemTotalCount);
//										world.reportStepInfo("In backward traverse:Verifying if social gallery " + (socialGalleryIndex + 1) + 
//												" has items in 3X3 format.");
//										softAssert.assertTrue(actualColumnCount <= 3, "In backward traverse:There are more than 3 columns of items instead "
//												+ "of maximum 3 column.");
//										softAssert.assertTrue(expectedRowCount == actuLRowCount, "In backward traverse:Actual number of rows " + actuLRowCount + " isn't same "
//												+ "as expected number of rows " + expectedRowCount + " for social gallery " +(socialGalleryIndex + 1));
//										String itemImageSrc = world.getWebElementFactory().getNestedElement(socialGalleryItems.get(socialGalleryItemIndex), 
//												socialGalleryDef.socialGalleryItemsImagesBy).getAttribute("src").trim();
//										String itemImageURL = itemImageSrc.substring(itemImageSrc.indexOf("://") + 3, 
//												itemImageSrc.indexOf("ulenscale") - 1);
//										String activePanelImageSrc = world.getWebElementFactory().getNestedElement(activeItemItemPanel, 
//												socialGalleryDef.socialGalleryItemActivePanelImageBy).getAttribute("src").trim();
//										String activePanelImageURL = activePanelImageSrc.substring(activePanelImageSrc.indexOf("://") + 3, 
//												activePanelImageSrc.indexOf("ulenscale") - 1);
//										world.reportStepInfo("In backward traverse:Verifying if URL of item " + (socialGalleryItemIndex + 1) + 
//												" image and the panel image URL are same in social gallery " + (socialGalleryIndex + 1));
//										softAssert.assertTrue(itemImageURL.equals(activePanelImageURL), "In backward traverse:In social gallery " + 
//												(socialGalleryIndex + 1) +" the active panel image and image of selected item number " + 
//													(socialGalleryItemIndex + 1) + " aren't same.\nThe selected item"
//														+ " image URL: " + itemImageURL + "\nThe active panel image URL: " + activePanelImageURL);
//										if (socialGalleryItemIndex != 0) {
//											actions.Click.click(activePanelPreviousBtn);
//											actions.Wait.wait("1");											
//										}
//										socialGalleryItemIndex --;
//									} else {
//										world.reportStepInfo("Verified traversing the items in social gallery " + (socialGalleryIndex + 1) + " "
//												+ "by clicking next and previous buttons.");
//										break;
//									}
//								}
//							}
//							if (socialGalleryItemIndex < 0)
//								break;
//						}
//					}
//				}
//				softAssert.assertAll();	
//			}	
//		} catch(Exception e) {
//			e.printStackTrace();
//			Assert.fail(e.getMessage());
//		}
//	}
//	
//	@Then("^(?:i|I) verify clicking available Show more button until all the items are displayed if social gallery is available$")
//	public void verifyClickingShowMoreButton() throws Throwable {
//		try {
//			if (socialGalleryDef.ispresent()) {
//				List<WebElement> socialGalleryComponents = world.getWebElementFactory().getElements(socialGalleryDef.socialGalleryComponentBy);
//				int socialGalleryComponentCount = socialGalleryComponents.size();
//				world.reportStepInfo("Page has " + socialGalleryComponentCount + " social gallery component.");
//				for (int socialGalleryIndex = 0; socialGalleryIndex < socialGalleryComponentCount; socialGalleryIndex ++) {
//					actions.Scroll.scrollUntilInViewport(socialGalleryComponents.get(socialGalleryIndex));
//					actions.Scroll.scrollByYAxis(400);
//					actions.Wait.wait("5");
//					List<WebElement> socialGalleryItems = world.getWebElementFactory().getNestedElements(socialGalleryComponents.get
//							(socialGalleryIndex), socialGalleryDef.socialGalleryItemsBy);
//					int socialGalleryItemTotalCount = socialGalleryItems.size();
//					if (socialGalleryItemTotalCount == 0)
//						Assert.fail("Social gallery " + (socialGalleryIndex + 1) + " has no items.");
//					else {
//						WebElement seeMoreBtn = world.getWebElementFactory().getNestedElement(socialGalleryComponents.get(socialGalleryIndex), 
//								socialGalleryDef.socialGallerySeeMoreBtnBy);
//						if (seeMoreBtn == null) {
//							world.reportStepInfo("Verifying if social gallery " + (socialGalleryIndex + 1) + " displays"
//									+ " maximum 9 items when Show more button isn't displayed.");
//							softAssert.assertTrue((socialGalleryItemTotalCount <= 9), "Social gallery " + (socialGalleryIndex + 1) + 
//									" initially displays more than 9 items even without having and clicking Show more button.");
//						}
//						else {
//							int showMoreClickCount = 0;
//							do {
//								showMoreClickCount ++;
//								int minimumItemsAfterShowMoreClick = 9 * showMoreClickCount;
//								int maximumItemsAfterShowMoreClick = (showMoreClickCount * 9) + 9;
//								actions.Scroll.scrollUntilInViewport(seeMoreBtn);
//								actions.Click.click(seeMoreBtn);
//								actions.Wait.wait("3");
//								verifyGalleryItemsImages();
//								List<WebElement> moreSocialGalleryItems = world.getWebElementFactory().getNestedElements(socialGalleryComponents.get
//										(socialGalleryIndex), socialGalleryDef.socialGalleryItemsBy);
//								int moreSocialGalleryItemTotalCount = moreSocialGalleryItems.size();
//								List<WebElement> activeItemPanel = world.getWebElementFactory().getNestedElements(socialGalleryComponents.get
//										(socialGalleryIndex), socialGalleryDef.socialGalleryItemActivePanelBy);
//								world.reportStepInfo("Verifying that by default there is only one active item panel after page load.");
//								softAssert.assertTrue(!(activeItemPanel.size() < 1), "There is no active item panel for social gallery " + 
//										(socialGalleryIndex + 1));
//								softAssert.assertTrue(!(activeItemPanel.size() > 1), "There are more than one active item panel for social "
//										+ "gallery " + (socialGalleryIndex + 1));
//								String firstItemImageSrc = world.getWebElementFactory().getNestedElement(socialGalleryItems.get(0), 
//										socialGalleryDef.socialGalleryItemsImagesBy).getAttribute("src").trim();
//								String firstItemImageURL = firstItemImageSrc.substring(firstItemImageSrc.indexOf("://") + 3, 
//										firstItemImageSrc.indexOf("ulenscale") - 1);
//								String activePanelImageSrc = world.getWebElementFactory().getNestedElement(activeItemPanel.get(0), 
//										socialGalleryDef.socialGalleryItemActivePanelImageBy).getAttribute("src").trim();
//								String activePanelImageURL = activePanelImageSrc.substring(activePanelImageSrc.indexOf("://") + 3, 
//										activePanelImageSrc.indexOf("ulenscale") - 1);
//								world.reportStepInfo("Verifying if the first item image URL and the defaullt panel image"
//										+ " URL are same in social gallery " + (socialGalleryIndex + 1));
//								softAssert.assertTrue(firstItemImageURL.equals(activePanelImageURL), "In social gallery " + 
//										(socialGalleryIndex + 1) +" the images in default active panel and first item aren't same.\nThe first item"
//												+ " image URL: " + firstItemImageURL + "\nThe default panel image URL: " + activePanelImageURL);
//								WebElement activePanelCloseBtn = world.getWebElementFactory().getNestedElement(activeItemPanel.get(0), 
//										socialGalleryDef.socialGalleryItemActivePanelCloseBy);
//								WebElement activePanelNextBtn = world.getWebElementFactory().getNestedElement(activeItemPanel.get(0), 
//										socialGalleryDef.socialGalleryItemActivePanelNextBy);
//								WebElement activePanelText = world.getWebElementFactory().getNestedElement(activeItemPanel.get(0), 
//										socialGalleryDef.socialGalleryItemActivePanelTextBy);
//								WebElement activePanelPreviousBtn = world.getWebElementFactory().getNestedElement(activeItemPanel.get(0), 
//										socialGalleryDef.socialGalleryItemActivePanelPreviousBy);
//								world.reportStepInfo("Verifying presence of close, next buttons, active panet text and absence of "
//										+ "previous button in " + "social gallery " + (socialGalleryIndex + 1));
//								softAssert.assertTrue(activePanelCloseBtn != null, "The default active panel doesn't have close button in "
//										+ "social gallery " + (socialGalleryIndex + 1));
//								softAssert.assertTrue(activePanelNextBtn != null, "The default active panel doesn't have next button in "
//											+ "social gallery " + (socialGalleryIndex + 1));
//								softAssert.assertTrue(!activePanelText.getText().trim().isEmpty(), "The default active panel doesn't have "
//										+ "content text in social gallery " + (socialGalleryIndex + 1));
//								softAssert.assertTrue(activePanelPreviousBtn == null, "The active panel has previous button in "
//										+ "social gallery " + (socialGalleryIndex + 1) + " even though the displayed item is the first one.");
//								world.reportStepInfo("Verifying if social gallery " + (socialGalleryIndex + 1) + " displays"
//										+ " more than " + minimumItemsAfterShowMoreClick + " items after Show more button click"
//												+ " number " + showMoreClickCount);
//								softAssert.assertTrue((moreSocialGalleryItemTotalCount > minimumItemsAfterShowMoreClick), "Total number"
//										+ " of items is not more than " + minimumItemsAfterShowMoreClick + " but still Show more button"
//												+ " was displayed in social gallery " + (socialGalleryIndex + 1));
//								world.reportStepInfo("Verifying if social gallery " + (socialGalleryIndex + 1) + " displays"
//										+ " maximum " + maximumItemsAfterShowMoreClick + " items after Show more button click "
//										+ "number " + showMoreClickCount);
//								softAssert.assertTrue((moreSocialGalleryItemTotalCount <= maximumItemsAfterShowMoreClick), 
//										"After Show more button click number " + showMoreClickCount + " the social gallery " + 
//												(socialGalleryIndex + 1) + " displays more than " + maximumItemsAfterShowMoreClick
//												+ " items.");
//								int[] seeMoreCountOfRowColumns = publishedPageCommon.getCountOfRowColumn(socialGalleryItems);
//								int seeMoreActuLRowCount = seeMoreCountOfRowColumns[0];
//								int seeMoreActualColumnCount = seeMoreCountOfRowColumns[1];
//								int seeMoreExpectedRowCount = publishedPageCommon.getExpectedRowCount(socialGalleryItemTotalCount);
//								world.reportStepInfo("Verifying if social gallery " + (socialGalleryIndex + 1) + " has items in "
//										+ "3X3 format after Show more button click number " + showMoreClickCount);
//								softAssert.assertTrue(seeMoreActualColumnCount == 3, "Number of columns is not equal to 3 after Show "
//										+ "more button click number " + showMoreClickCount);
//								softAssert.assertTrue(seeMoreExpectedRowCount == seeMoreActuLRowCount, "Actual number of rows isn't "
//										+ "equal to expected number of rows for social gallery " +(socialGalleryIndex + 1) + "."
//												+ " Actual number=" + seeMoreActuLRowCount + " and Expected "
//												+ "number=" + seeMoreExpectedRowCount );
//								actions.Wait.wait("1");
//								seeMoreBtn = world.getWebElementFactory().getNestedElement(socialGalleryComponents.get(socialGalleryIndex), 
//										socialGalleryDef.socialGallerySeeMoreBtnBy);
//							} while (seeMoreBtn != null);	
//							verifyGalleryItemsImages();
//						}
//					}
//					world.reportStepInfo("Show more button has been clicked until all the items of social gallery " + 
//							(socialGalleryIndex + 1) + " are displayed.");
//				}
//				softAssert.assertAll();
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			Assert.fail(e.getMessage());
//		}
//	}
//	
//	public void verifyGalleryItemsImages() {
//		try {
//			world.reportStepInfo("Verifying if the socialgallery items have images.");
//			List<WebElement> socialGalleryComponents = world.getWebElementFactory().getElements(socialGalleryDef.socialGalleryComponentBy);
//			int socialGalleryComponentCount = socialGalleryComponents.size();
//			for (int socialGalleryIndex = 0; socialGalleryIndex < socialGalleryComponentCount; socialGalleryIndex ++) {
//				List<WebElement> socialGalleryItems = world.getWebElementFactory().getNestedElements(socialGalleryComponents.get
//						(socialGalleryIndex), socialGalleryDef.socialGalleryItemsBy);
//				int socialGalleryItemTotalCount = socialGalleryItems.size();
//				for (int socialGalleryItemIndex = 0; socialGalleryItemIndex < socialGalleryItemTotalCount; socialGalleryItemIndex ++) {
//					WebElement galleryItemImage = world.getWebElementFactory().getNestedElement(socialGalleryItems.get(socialGalleryItemIndex), 
//							socialGalleryDef.socialGalleryItemsImagesBy);
//					String galleryItemImageSrc = galleryItemImage.getAttribute("src").trim();
//					softAssert.assertTrue(!galleryItemImageSrc.isEmpty(), "In social gallery " + (socialGalleryIndex + 1) + " the item"
//							+ (socialGalleryItemIndex + 1) +" has no image.");
//				}
//			}
//			softAssert.assertAll();
//		} catch (Exception e) {
//			e.printStackTrace();
//			Assert.fail(e.getMessage());
//		}
//	}
//	
//	@Then("^(?:i|I) expect to see items, See more button if necessary and default item in gallery is the first one if social gallery is available$")
//	public void verifySocialGalleryItemsPresence() throws Throwable {
//		try {
//			if (socialGalleryDef.ispresent()) {
//				List<WebElement> socialGalleryComponents = world.getWebElementFactory().getElements(socialGalleryDef.socialGalleryComponentBy);
//				int socialGalleryComponentCount = socialGalleryComponents.size();
//				world.reportStepInfo("Page has " + socialGalleryComponentCount + " social gallery component.");
//				for (int socialGalleryIndex = 0; socialGalleryIndex < socialGalleryComponentCount; socialGalleryIndex ++) {
//					actions.Scroll.scrollUntilInViewport(socialGalleryComponents.get(socialGalleryIndex));
//					actions.Scroll.scrollByYAxis(400);
//					actions.Wait.wait("5");
//					List<WebElement> socialGalleryItems = world.getWebElementFactory().getNestedElements(socialGalleryComponents.get
//							(socialGalleryIndex), socialGalleryDef.socialGalleryItemsBy);
//					int socialGalleryItemTotalCount = socialGalleryItems.size();
//					world.reportStepInfo("Verifying if social gallery " + (socialGalleryIndex + 1) + " has at least one item.");
//					if (socialGalleryItemTotalCount == 0)
//						Assert.fail("Social gallery " + (socialGalleryIndex + 1) + " has no items.");
//					else {
//						verifyGalleryItemsImages();
//						int[] countOfRowColumns = publishedPageCommon.getCountOfRowColumn(socialGalleryItems);
//						int actuLRowCount = countOfRowColumns[0];
//						int actualColumnCount = countOfRowColumns[1];
//						int expectedRowCount = publishedPageCommon.getExpectedRowCount(socialGalleryItemTotalCount);
//						world.reportStepInfo("Verifying if social gallery " + (socialGalleryIndex + 1) + " has items in 3X3 "
//								+ "format.");
//						softAssert.assertTrue(actualColumnCount <= 3, "There are more than 3 columns of items instead of maximum 3 column.");
//						softAssert.assertTrue(expectedRowCount == actuLRowCount, "Actual number of rows " + actuLRowCount + " isn't same "
//								+ "as expected number of rows " + expectedRowCount + " for social gallery " +(socialGalleryIndex + 1));
//						
//						List<WebElement> activeItemPanel = world.getWebElementFactory().getNestedElements(socialGalleryComponents.get
//								(socialGalleryIndex), socialGalleryDef.socialGalleryItemActivePanelBy);
//						world.reportStepInfo("Verifying that by default there is only one active item panel after page load.");
//						softAssert.assertTrue(!(activeItemPanel.size() < 1), "There is no active item panel for social gallery " + 
//								(socialGalleryIndex + 1));
//						softAssert.assertTrue(!(activeItemPanel.size() > 1), "There are more than one active item panel for social "
//								+ "gallery " + (socialGalleryIndex + 1));
//						String firstItemImageSrc = world.getWebElementFactory().getNestedElement(socialGalleryItems.get(0), 
//								socialGalleryDef.socialGalleryItemsImagesBy).getAttribute("src").trim();
//						String firstItemImageURL = firstItemImageSrc.substring(firstItemImageSrc.indexOf("://") + 3, 
//								firstItemImageSrc.indexOf("ulenscale") - 1);
//						String activePanelImageSrc = world.getWebElementFactory().getNestedElement(activeItemPanel.get(0), 
//								socialGalleryDef.socialGalleryItemActivePanelImageBy).getAttribute("src").trim();
//						String activePanelImageURL = activePanelImageSrc.substring(activePanelImageSrc.indexOf("://") + 3, 
//								activePanelImageSrc.indexOf("ulenscale") - 1);
//						world.reportStepInfo("Verifying if the first item image URL and the defaullt panel image"
//								+ " URL are same in social gallery " + (socialGalleryIndex + 1));
//						softAssert.assertTrue(firstItemImageURL.equals(activePanelImageURL), "In social gallery " + 
//								(socialGalleryIndex + 1) +" the images in default active panel and first item aren't same.\nThe first item"
//										+ " image URL: " + firstItemImageURL + "\nThe default panel image URL: " + activePanelImageURL);
//						WebElement activePanelCloseBtn = world.getWebElementFactory().getNestedElement(activeItemPanel.get(0), 
//								socialGalleryDef.socialGalleryItemActivePanelCloseBy);
//						WebElement activePanelNextBtn = world.getWebElementFactory().getNestedElement(activeItemPanel.get(0), 
//								socialGalleryDef.socialGalleryItemActivePanelNextBy);
//						WebElement activePanelText = world.getWebElementFactory().getNestedElement(activeItemPanel.get(0), 
//								socialGalleryDef.socialGalleryItemActivePanelTextBy);
//						WebElement activePanelPreviousBtn = world.getWebElementFactory().getNestedElement(activeItemPanel.get(0), 
//								socialGalleryDef.socialGalleryItemActivePanelPreviousBy);
//						world.reportStepInfo("Verifying presence of close, next buttons, active panet text and absence of "
//								+ "previous button in " + "social gallery " + (socialGalleryIndex + 1));
//						softAssert.assertTrue(activePanelCloseBtn != null, "The default active panel doesn't have close button in "
//								+ "social gallery " + (socialGalleryIndex + 1));
//						if (socialGalleryItemTotalCount > 1)
//							softAssert.assertTrue(activePanelNextBtn != null, "The default active panel doesn't have next button in "
//									+ "social gallery " + (socialGalleryIndex + 1));
//						if (socialGalleryItemTotalCount == 1)
//							softAssert.assertTrue(activePanelNextBtn == null, "The default active panel has next button in "
//									+ "social gallery " + (socialGalleryIndex + 1) + " even though the galerry has only one item.");
//						softAssert.assertTrue(!activePanelText.getText().trim().isEmpty(), "The default active panel doesn't have "
//								+ "content text in social gallery " + (socialGalleryIndex + 1));
//						softAssert.assertTrue(activePanelPreviousBtn == null, "The default active panel has previous button in "
//								+ "social gallery " + (socialGalleryIndex + 1) + " even though the displayed item is the first one.");
//						if (socialGalleryItemTotalCount < 9) {
//							world.reportStepInfo("Verifying if social gallery " + (socialGalleryIndex + 1) + " displays Show"
//									+ " more button when gallery displays less than 9 items.");
//							softAssert.assertTrue((world.getWebElementFactory().getNestedElement(socialGalleryComponents.get(socialGalleryIndex), 
//									socialGalleryDef.socialGallerySeeMoreBtnBy) == null), " See more button is unnecessary "
//											+ "displayed even though social gallery " + (socialGalleryIndex + 1) + 
//												" has less than 9 items.");
//						}
//					}
//				}
//				softAssert.assertAll();
//			}			
//		} catch(Exception e) {
//			e.printStackTrace();
//			Assert.fail(e.getMessage());
//		}
//	}
}
