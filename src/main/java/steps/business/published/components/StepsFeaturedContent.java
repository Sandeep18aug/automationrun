package steps.business.published.components;

import java.util.List;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.FeaturedContent;


	public class StepsFeaturedContent {
		UserActions actions;
		World world;
		FeaturedContent featuredcontentPage;
		SoftAssert softAssert;
		static String parentWindow = "";
		static String parentWindowURL = "";
		static String parentWindowTitle = "";
		static int parentPageScrollPositionBefore = 0;
	
	public StepsFeaturedContent(World world, UserActions actions) {
		this.actions = actions;
		this.world = world;
		this.featuredcontentPage = new FeaturedContent(world.getWebDriver(),world.getBrandName());
		softAssert = new SoftAssert();
	}
	
	//     @And("^(?:I|i) click on the cta of the featured-content component$")
	//     public void click_CTA_featured_Content() {
	//    	 
	//    	List<WebElement> cta = this.featuredcontentPage.getfeaturedcontentCTA();    	 
	// 		Assert.assertNotNull(cta,
	// 				"featured-content cta is not appearing for url " + world.getWebDriver().getCurrentUrl());
	// 		world.reportStepInfo("featured-content cta is present on page");
	// 		world.getScenarioContext().addScenarioData("featuredcontentctahref", ((WebElement) cta).getAttribute("href"));
	// 		((WebElement) cta).click();
	//	
	//}     

		@And("^(?:I|i) click on the cta of the featured-content component$")
		public void click_CTA_featured_Content() {
			 
			WebElement cta = this.featuredcontentPage.getfeaturedcontentCTA1();    	 
			Assert.assertNotNull(cta,
					"featured-content cta is not appearing for url " + world.getWebDriver().getCurrentUrl());
			world.reportStepInfo("featured-content cta is present on page");
			world.getScenarioContext().addScenarioData("featuredcontentctahref", cta.getAttribute("href"));
			cta.click();
		
		}  
     @Then("^it should take me to the corresponding page of featured content cta$")
 	public void verify_CTA_URL_featured_Content_Page() {
 		this.actions.Wait.waitForPageReady();
 		String expectedUrl = (String) world.getScenarioContext().getScenarioData("featuredcontentctahref");
 		String actualUrl = world.getWebDriver().getCurrentUrl();
 		Assert.assertTrue(expectedUrl.equals(actualUrl), "the page is not navigated to expected url. featured content cta href was "
 				+ expectedUrl + System.lineSeparator() + " actual url is " + actualUrl);
 	}


    @Then("^(?:I|i) verify that featured content component heading is appearing$")
    public void i_verify_that_featuredcontent_component_heading_is_rendering() {
    // Write code here that turns the phrase above into concrete actions
	WebElement featuredcontentHeading = this.featuredcontentPage.featuredcontentheading();

    Assert.assertNotNull(featuredcontentHeading, "featured content heading is not rendering for url " + world.getWebDriver().getCurrentUrl());

    world.reportStepInfo("featured content heading is present on page");


}

    @Then("^(?:I|i) verify that featured content component image is appearing$")
    public void i_verify_that_featuredcontent_component_image_is_rendering() {
    // Write code here that turns the phrase above into concrete actions
	WebElement featuredcontentImage = this.featuredcontentPage.featuredcontentimage();
    Assert.assertNotNull(featuredcontentImage, "featured content image is not rendering for url " + world.getWebDriver().getCurrentUrl());
    world.reportStepInfo("featured content image is present on page");
}

    @Then("^(?:I|i) verify that featured content component description is appearing$")
    public void i_verify_that_featuredcontent_component_description_is_rendering() {
    // Write code here that turns the phrase above into concrete actions
	WebElement featuredcontentDescription = this.featuredcontentPage.featuredcontentdescription();
    Assert.assertNotNull(featuredcontentDescription, "featured content description is not rendering for url " + world.getWebDriver().getCurrentUrl());
    world.reportStepInfo("featured content description is present on page");
}

    @And("^(?:I|i) verify that there is 10 featured-content present on the homepage$")
    public void featuredContent_visible() {
	 Assert.assertTrue(this.featuredcontentPage.getFeacturedContentList().size() == 10,"Featured content is not present on page");
	world.reportStepInfo("Featured content is present on page");
}

    @And("^(?:I|i) verify that each featured-content has cta link/button present on the page$")
    public void featuredContent_CTA_visible() {
	Assert.assertTrue(this.featuredcontentPage.getfeaturedcontentCTA().size() > 0,
			"CTA is not present on page");
	world.reportStepInfo("CTA is present on page");
}
    @Then("^(?:i|I) expect to see (\\d+) featured content components in the page$")
	public void verifyFeaturedContentCount(int featuredContentExpectedCount) {
		try {
			if (featuredcontentPage.isPresent()) {
				List<WebElement> featuredContentComponents = featuredcontentPage.getFeacturedContents();
				int featuredContentActualCount = featuredContentComponents.size();
				world.reportStepInfo("Verifying that the page has eaxctly " + 
						featuredContentExpectedCount + " featured content components.");
				Assert.assertTrue(featuredContentExpectedCount == featuredContentActualCount, 
						"Expected and actual featured content component counts don't "
						+ "match, expected = " + featuredContentExpectedCount 
						+ " and actual = " + featuredContentActualCount);
			} else
				Assert.fail("The page doesn't have Featured Content component.");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
    
    @Then("^(?:i|I) expect to see (\\d+) image in featured content component$")
	public void verifyFeaturedContentImageCount(int featuredContentImageExpectedCount) {
		try {
			WebElement featuredContentComponent = featuredcontentPage.getFeacturedContents().get(0);
			actions.Scroll.scrollTillTopOfElement(featuredContentComponent);
			actions.Wait.wait("2");
			List<WebElement> featuredContentImages = featuredcontentPage.getFeaturedContentImages(featuredContentComponent);
			int featuredContentImageActualCount = featuredContentImages.size();
			world.reportStepInfo("Verifying that the page has eaxctly " + 
					featuredContentImageExpectedCount + " featured content images.");
			Assert.assertTrue(featuredContentImageExpectedCount == featuredContentImageActualCount, 
					"Expected and actual featured content image counts don't "
					+ "match, expected = " + featuredContentImageExpectedCount 
					+ " and actual = " + featuredContentImageActualCount);			
		} catch(Exception e) {	
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
    
    @Then("^(?:i|I) expect to see featured content heading$")
	public void verifyFeaturedContentHeading() {
		try {
			WebElement featuredContentComponent = featuredcontentPage.getFeacturedContents().get(0);
			WebElement featuredContentHeading = featuredcontentPage.getFeaturedContentHeading(featuredContentComponent);
			world.reportStepInfo("Verifying the heading of featured content component.");
			if (featuredContentHeading != null)
				Assert.assertTrue(!featuredContentHeading.getText().trim().isEmpty(), 
						"Featured content heading has no visible text.");
			else
				Assert.fail("The featured content component has no heading.");			
		} catch(Exception e) {	
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
    
    @Then("^(?:i|I) expect to see featured content description$")
	public void verifyFeaturedContentDescription() {
		try {
			WebElement featuredContentComponent = featuredcontentPage.getFeacturedContents().get(0);
			WebElement featuredContentDescription = featuredcontentPage.getFeaturedContentDescription(featuredContentComponent);
			world.reportStepInfo("Verifying the description of featured content component.");
			if (featuredContentDescription != null) {
				Assert.assertTrue(!featuredContentDescription.getText().trim().isEmpty(), 
						"Featured content description has no visible text.");
			}else
				Assert.fail("The page doesn't have Featured Content component.");	
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
    
    @Then("^(?:i|I) expect not to see video in featured content component$")
    public void featuredContentVideo() throws Throwable {
    	try {
    		WebElement featuredContentComponent = featuredcontentPage.getFeacturedContents().get(0);
    		WebElement featuredContentVideo = featuredcontentPage.getFeaturedContentVideo(featuredContentComponent);
    		world.reportStepInfo("Verifying that video isn't prsent in featured content "
    				+ "component");
    		Assert.assertTrue(featuredContentVideo == null, "The featured content component "
    				+ "unexpectedly contains video.");
    	} catch (Exception e) {
    		e.printStackTrace();
			Assert.fail(e.getMessage());
    	}
    }
	
    @Then("^(?:i|I) expect to see (\\d+) CTA in featured content component$")
	public void verifyFeaturedContentCTACount(int featuredContentCTAExpectedCount) {
		try {
			WebElement featuredContentComponent = featuredcontentPage.getFeacturedContents().get(0);
			List<WebElement> featuredContentCTAs = featuredcontentPage.getFeaturedContentCTAs(featuredContentComponent);
			int featuredContentCTAActualCount = featuredContentCTAs.size();
			world.reportStepInfo("Verifying that the page has eaxctly " + 
					featuredContentCTAExpectedCount + " featured content CTAs.");
			Assert.assertTrue(featuredContentCTAExpectedCount == featuredContentCTAActualCount, 
					"Expected and actual featured content CTA counts don't "
					+ "match, expected = " + featuredContentCTAExpectedCount 
					+ " and actual = " + featuredContentCTAActualCount);			
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
    
    @Then("^(?:i|I) click on featured content CTA$")
	public void clickFeaturedContentCTA() throws Throwable {
		try {
			WebElement featuredContentComponent = featuredcontentPage.getFeacturedContents().get(0);
			world.reportStepInfo("Verifying by clicking featured content CTA");
			WebElement featuredContentCTA = featuredcontentPage.getFeaturedContentCTAs(featuredContentComponent).get(0);
			parentWindow = world.getWebDriver().getWindowHandle();
			parentWindowURL = world.getWebDriver().getCurrentUrl();
			parentWindowTitle = world.getWebDriver().getTitle();
			parentPageScrollPositionBefore = actions.Scroll.getYOffsetOfPage();
			actions.Click.click(featuredContentCTA);
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
    
    @Then("^(?:i|I) expect clicking featured content CTA opens URL in new browser window/tab$")
	public void verifyNewBrowserWindowForCTAClick() {
		try {
			Set<String> allWindows = world.getWebDriver().getWindowHandles();
			world.reportStepInfo("Verifying if clicking featured content CTA opens URL "
					+ "in new browser window/tab");
			if (allWindows.size() < 1)
				Assert.fail("Clicking featured content CTA closes parent window, instead of "
						+ "opening the URL in new browser window/tab.");
			else if (allWindows.size() == 1)
				Assert.fail("Clicking featured content CTA opens URL in the same window "
						+ "instead of in a new browser window/tab.");
			else if (allWindows.size() > 2)
				Assert.fail("Clicking featured content CTA opens more than one browser "
						+ "window/tab.");
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
    
    @Then("^(?: i|I) switch to the featured content page$")
	public void switchToFeaturedContentPage() throws Throwable {
		try {
			world.reportStepInfo("Switching to featured content page.");
			actions.SwitchWindow.switchTabByIndex(2);
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
    
    @Then("^(?:i|I) expect to see different URL and page title after clicking featured content CTA$")
	public void verifyFeaturedContentCTAClickURL() throws Throwable {
		try {
			String featuredContentPageURL = world.getWebDriver().getCurrentUrl();
			String featuredContentPageTitle = world.getWebDriver().getTitle();
			world.reportStepInfo("Verifying that featured content page URL is different than parent URL");
			Assert.assertTrue(parentWindowURL != featuredContentPageURL, "The URLs of featured content page"
					+ " and and featured content compenent containing page are same.");
			Assert.assertTrue(featuredContentPageTitle != parentWindowTitle, "The titles of featured content page"
					+ " and and featured content compenent containing page are same.");
			world.getWebDriver().close();
			actions.SwitchWindow.switchTabByIndex(1);
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
    
    @Then("^(?: i|I) expect same scroll position after coming back to featured content component container page$")
	public void verifyFeaturedContentScrollPosition() {
		try {
			int parentPageScrollPositionAfter = actions.Scroll.getYOffsetOfPage();
			Assert.assertTrue(parentPageScrollPositionBefore == parentPageScrollPositionAfter, "Scroll position "
					+ "hasn't been maintained after coming back to featured content component container page.");
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
