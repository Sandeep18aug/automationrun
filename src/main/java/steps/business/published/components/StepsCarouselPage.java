package steps.business.published.components;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.CarouselV2;

public class StepsCarouselPage {

	UserActions actions;
	World capabilities;
	CarouselV2 countrySelectorPage;

	public StepsCarouselPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.countrySelectorPage = new CarouselV2(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	

	@Then("^(?:I|i) verify that Next and Previous buttons are appearing for carousel component$")
	public void clickNextButton() throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement nextbutton = this.countrySelectorPage.getCarouselV2NextButton();
		Assert.assertNotNull(nextbutton, "Next button is not appearing for Carousel");
		WebElement previousButton = this.countrySelectorPage.getCarouselV2PreviousButton();
		Assert.assertNotNull(previousButton, "Previous Button is not appearing for Carousel");
		
	}
	
	@When("^(?:i|I) verify Pause button appears for carousel component$")
	public void clickPauseButton() throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement pauseButton = this.countrySelectorPage.getCarouselV2PauseButton();
		Assert.assertNotNull(pauseButton, "pause Button Button is not appearing for Carousel");
	}
	
	@Then("^(?:i|I) verify that carousel item is getting changed by clicking next button$")
	public void carouselItem() throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement pauseButton = this.countrySelectorPage.getCarouselV2PauseButton();
		pauseButton.click();
		WebElement firstItem = this.countrySelectorPage.getCarouselV2FirstItem();
		firstItem.click();
		Assert.assertNotNull(firstItem, "Not able to locate first item of carousel");
		WebElement secondItem = this.countrySelectorPage.getCarouselV2SecondItem();
		secondItem.click();
		WebElement currentItem = this.countrySelectorPage.getCarouselV2CurrentItem();
		Assert.assertNotNull(currentItem, "Not able to locate second item of carousel");
	}
}
