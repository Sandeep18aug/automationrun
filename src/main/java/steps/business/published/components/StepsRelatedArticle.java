package steps.business.published.components;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.BackToTopPage;
import unilever.pageobjects.platform.publish.components.definitions.RelatedArticle;

public class StepsRelatedArticle {
	UserActions actions;
	World capabilities;
	PublishedPageCommon publishedPage;
	BackToTopPage backtotopPage;
	RelatedArticle relatedarticlePage;

	public StepsRelatedArticle(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.backtotopPage = new BackToTopPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.relatedarticlePage = new RelatedArticle(capabilities.getWebDriver(),capabilities.getBrandName());

	}

	@And("^(?:I|i) verify that 3 articles are present on the homepage$")
	public void verifyarticlespresent() {
		actions.Wait.waitForPageReady();
		this.relatedarticlePage.RelatedArticleItemvisible();
		Assert.assertTrue(this.relatedarticlePage.RelatedArticleItemvisible().size() > 0,
				"3 articles are not present on page");
		this.capabilities.reportStepInfo("3 articles are present on page");	
	}
	

	@And("^(?:I|i) click on the cta of the related-articles component$")
	public void click_relatedarticles_CTA() {
		actions.Wait.waitForPageReady();
		WebElement cta = this.relatedarticlePage.getRelatedArticleCTA();
		Assert.assertNotNull(cta,
				"related article cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("relatedarticlectahref", cta.getAttribute("href"));
		cta.click();
	}

	@Then("^it should take me to the corresponding page of related articles cta$")
	public void verifyrelatedarticleCtaUrlPage() throws Throwable {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("relatedarticlectahref");
		this.actions.Wait.wait("3");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();

		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related article cta href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);

	}

	@And("^(?:I|i) click on any of the items of article page$")
	public void clickOnAnyItemOfArticlePage() {
		actions.Wait.waitForPageReady();
		List<WebElement> relatedarticleItems = this.relatedarticlePage.getRelatedArticleItems();
		Assert.assertNotNull(relatedarticleItems, "articles are not appearing on page");
		int randomNumber = RandomUtil.getRandomNumber(1, relatedarticleItems.size() - 1);
		WebElement listItem = relatedarticleItems.get(randomNumber);
		String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("selectedrelatedarticleitemurl", href);
		listItem.click();
		this.capabilities.reportStepInfo("clicked on article item with href " + href);
	}

	@And("^(?:I|i) click on the article image$")
	public void click_related_article_image() {
		WebElement articleImage = this.relatedarticlePage.getRelatedArticleImages();
		Assert.assertNotNull(articleImage,
				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
				articleImage.getAttribute("href"));
		articleImage.findElement(By.xpath(".//img")).click();
		//articleImage.click();
		this.capabilities.reportStepInfo("related article image has been clicked");
	}
	
	@And("^(?:I|i) click on the article img$")
	public void click_related_article_img() {
		WebElement articleImg = this.relatedarticlePage.getRelatedArticleImages();
		Assert.assertNotNull(articleImg,
				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
				articleImg.getAttribute("href"));
		articleImg.click();
		this.capabilities.reportStepInfo("related article image has been clicked");
	}
	
	
	@And("^(?:I|i) click on the article image of axe$")
	public void click_related_article_image_axe() {
		WebElement articleImage = this.relatedarticlePage.getRelatedArticleImagesAxe();
		Assert.assertNotNull(articleImage,
				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
				articleImage.getAttribute("href"));
		System.out.println("###########");
		System.out.println(articleImage.getAttribute("href"));
		System.out.println("###########");
		this.actions.Scroll.scrollPublishedPageDownByPixels(800); 
		articleImage.click();
		this.capabilities.reportStepInfo("related article image has been clicked");
	}
	
	@And("^(?:I|i) click on the article image of clear$")
	public void click_related_article_image_clear() {
		WebElement articleImage = this.relatedarticlePage.getRelatedArticleImagesClear();
		Assert.assertNotNull(articleImage,
				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
				articleImage.getAttribute("href"));
		this.actions.Scroll.scrollPublishedPageDownByPixels(500); 
		articleImage.click();
		this.capabilities.reportStepInfo("related article image has been clicked");
	}

	@Then("^it should take me to the corresponding page of article image$")
	public void verify_related_article_image() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext()
				.getScenarioData("relatedarticleimagehref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		System.out.println("###########");
		
		System.out.println("expectedUrl"+expectedUrl);
		if(actualUrl!=null) {
			System.out.println("actualUrl"+actualUrl);
		}
		System.out.println("###########");
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related article cta href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
		this.capabilities.reportStepInfo("You have been taken to the corresponding page");
	}

	@And("^(?:I|i) verify that cta link/button of related-articles component is present on the page$")
	public void verifyCTA() {
		actions.Wait.waitForPageReady();
		this.relatedarticlePage.getRelatedArticleCTA();
		this.capabilities.reportStepInfo("CTA is present on the page");
	}

	@Then("^(?:I|i) verify that related article component image is rendering$")
	public void i_verify_that_relatedarticle_component_image_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleImage = this.relatedarticlePage.getRelatedArticleImages();
		Assert.assertNotNull(relatedarticleImage,
				"related article image is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article Image is present on page");
	}

	@Then("^(?:I|i) verify that related article component heading is appearing$")
	public void i_verify_that_relatedarticle_component_heading_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleHeading = this.relatedarticlePage.getRelatedArticleHeading();
		Assert.assertNotNull(relatedarticleHeading,
				"related article heading is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article heading is present on page");
	}

	@Then("^(?:I|i) verify that related article component description is appearing$")
	public void i_verify_that_relatedarticle_component_description_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleDescription = this.relatedarticlePage.getRelatedArticleDescription();
		Assert.assertNotNull(relatedarticleDescription, "related article description is not rendering for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article description is present on page");
	}

}
