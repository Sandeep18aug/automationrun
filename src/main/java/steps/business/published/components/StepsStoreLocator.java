package steps.business.published.components;

import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.StoreLocatorPage;

public class StepsStoreLocator {

	UserActions actions;
	World capabilities;
	StoreLocatorPage storeLocatorPage;

	public StepsStoreLocator(World capabilities, UserActions actions) throws Throwable {
		this.actions = actions;
		this.capabilities = capabilities;
		this.storeLocatorPage = new StoreLocatorPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@Then("^(?:I|i) select \"([^\"]*)\" category for store locator$")
	public void i_select_category_for_store_locator(String category) throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(category,
				this.storeLocatorPage.getCategoryListbox());
	}
	
	@Then("^(?:I|i) select random category for store locator$")
	public void i_select_random_category_for_store_locator() throws Throwable {
		this.actions.Wait.waitForPageReady();
		Select category = this.storeLocatorPage.getCategoryListbox();
		this.actions.SelectText.selectRandomOption(category);
	}

	@Then("^(?:I|i) select store browse search tab by text \"([^\"]*)\"$")
	public void i_select_store_browse_tab(String tab) throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.storeLocatorPage.getStoreBrowseSearchTabByText(tab).click();
	}
	
	
	@Then("^(?:I|i) select \"([^\"]*)\" category for store locator of brand \"([^\"]*)\"$")
	public void i_select_category_for_store_locator(String category, String brandName) throws Throwable {
		this.actions.Wait.waitForPageReady();
		if(brandName.trim().equalsIgnoreCase("caress")) {
			this.storeLocatorPage.getCategoryListOfButtons().stream().filter(
					x->x.getAttribute("title").equalsIgnoreCase(category)).findFirst().get().click();
			
			
		}
	}

	@Then("^(?:I|i) select \"([^\"]*)\" sub categry for store locator$")
	public void i_select_sub_categry_for_store_locator(String subCategory) throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(subCategory,
				this.storeLocatorPage.getSubCategoryListbox());
		this.actions.Wait.wait("3");

	}

	@Then("^(?:I|i) should be able to see products for (?:select|selected) category(?: and sub category|)$")
	public void i_should_be_able_to_see_products_for_select_category_and_sub_category() throws Throwable {
		this.actions.Wait.waitForPageReady();
		List<WebElement> products = this.storeLocatorPage.getProductsList();
		int productCount = products.size();
		if (productCount == 0)
			Assert.fail("products are not present for selected category and subcategory");
		this.capabilities.reportStepInfo(productCount + " products for selected category and subcategory are present");
	}

	@When("^(?:I|i) select any of the random product for (?:seleted|selected) category(?: and subcategory|)$")
	public void i_select_any_of_the_random_product_for_seleted_category_and_subcategory() throws Throwable {
		this.actions.Wait.waitForPageReady();
		List<WebElement> products = this.storeLocatorPage.getProductsList();
		int productCount = products.size();
		if (productCount == 0)
			Assert.fail("products are not present for selected category and subcategory");
		//int randomNumber = new Random().nextInt(productCount);
		int randomNumber = RandomUtil.getRandomNumber(1, productCount);
		WebElement product = products.get(randomNumber-1);
		String productText = product.getText();
		//product.findElement(By.xpath(".//label|.//input[contains(@class,'choose-variant')]|.//a")).click();
		//WebElement element = 
		this.storeLocatorPage.selectProductFromList(product);
		this.capabilities.reportStepInfo("successfully selected product  - " +productText );
	}
	
	@When("^(?:I|i) select product number (\\d+) for (?:seleted|selected) category(?: and subcategory|)$")
	public void selectStoreLocaterProductByIndex(int index) throws Throwable {
		this.actions.Wait.waitForPageReady();
		List<WebElement> products = this.storeLocatorPage.getProductsList();
		int productCount = products.size();
		if (productCount == 0)
			Assert.fail("products are not present for selected category and subcategory");
		//int randomNumber = new Random().nextInt(productCount);
		WebElement product = products.get(index-1);
		String productText = product.getText();
		//product.findElement(By.xpath(".//label|.//input[contains(@class,'choose-variant')]|.//a")).click();
		//WebElement element = 
		this.storeLocatorPage.selectProductFromList(product);
		this.capabilities.reportStepInfo("successfully selected product  - " +productText );
	}
	
	@When("^(?:I|i) select any of the random product button for seleted category and subcategory$")
	public void selectButton() throws Throwable {
		this.actions.Wait.waitForPageReady();
		List<WebElement> products = this.storeLocatorPage.getProductsList();
		int productCount = products.size();
		if (productCount == 0)
			Assert.fail("products are not present for selected category and subcategory");
		int randomNumber = new Random().nextInt(productCount);
		WebElement product = products.get(randomNumber);
		String productText = product.getText();
		product.findElement(By.xpath(".//input[contains(@class,'choose-variant')]")).click();
		this.capabilities.reportStepInfo("successfully selected product  - " +productText );
	}

	@When("^(?:I|i) enter \"([^\"]*)\" zip code$")
	public void i_enter_zip_code(String zipCode) throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.storeLocatorPage.getPostalCodeEditbox().sendKeys(zipCode);
		this.capabilities.reportStepInfo("successfully entered text " +zipCode +"in postal code textbox");
	}

	@When("^(?:I|i) select \"([^\"]*)\" as find with store range$")
	public void i_select_as_find_with_store_range(String range) throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(range,
				this.storeLocatorPage.getFindStoreWithInListbox());
		this.capabilities.reportStepInfo("successfully selected " + range + " as option  ");
	}

	@When("^(?:I|i) click on find in store button$")
	public void i_click_on_find_in_store_button() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.storeLocatorPage.getFindStoreButton().click();
		this.capabilities.reportStepInfo("successfully clicked on find store button");
		this.actions.Wait.wait("3");
	}
	@When("^(?:I|i) click on refresh store button$")
	public void i_click_on_refresh_store_button() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.storeLocatorPage.getRefreshStoreButton().click();
		this.capabilities.reportStepInfo("successfully clicked on refresh button");
		this.actions.Wait.wait("3");
	}
	
	@When("^(?:I|i) click on refresh button on store search result page$")
	public void refreshButtonStoreSearchResultPage() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.storeLocatorPage.getStoreResultRefreshButton().click();;
		this.capabilities.reportStepInfo("successfully clicked on refresh button");
		this.actions.Wait.wait("3");
	}
	
	@Then("^(?:I|i) should be able to see map for searched stores$")
	public void i_should_be_able_to_map() throws Throwable {
		this.actions.Wait.waitForPageReady();
		Assert.assertNotNull(this.storeLocatorPage.getMap(),"map is not present");
		this.capabilities.reportStepInfo("map is present on the page");
	}

//	@Then("^(?:I|i) should be able to see the selected product$")
//	public void i_should_be_able_to_see_the_selected_product() throws Throwable {
//		// Write code here that turns the phrase above into concrete actions
//		throw new PendingException();
//	}

	@Then("^(?:I|i) should be able to see stores list$")
	public void i_should_be_able_to_see_stores_list() throws Throwable {
		this.actions.Wait.waitForPageReady();
		Assert.assertTrue(this.storeLocatorPage.getStoreSearchResultList().size() > 0, "store results are not populated");	
	}

}
