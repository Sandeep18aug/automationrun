package steps.business.published.components;

import org.testng.Assert;
import cucumber.api.java.en.Then;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.*;

public class StepsSocialSharing {

	UserActions actions;
	World capabilities;
	SocialSharing SocialSharing;

	public StepsSocialSharing(World capabilities, UserActions actions) throws Throwable {
		this.actions = actions;
		this.capabilities = capabilities;
		this.SocialSharing = new SocialSharing(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@Then("^(?:I|i) verify that social sharing icons are (?:rendering|appearing)$")
	public void isocialsharingIcons() {
		this.actions.Wait.waitForPageReady();
		Assert.assertTrue(this.SocialSharing.socialsharingChannels() != null,
				"socialsharing icons are not present on page");
	}

}
