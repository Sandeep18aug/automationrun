package steps.business.published.components;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.Then;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.HeroV2;
import unilever.pageobjects.platform.publish.components.definitions.QuickPoll;

public class StepsPublishedQuickPoll {
	UserActions actions;
	World capabilities;
	QuickPoll quickPoll;
	
	public StepsPublishedQuickPoll(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.quickPoll = new QuickPoll(capabilities.getWebDriver(),capabilities.getBrandName());
	}
	
	
	@Then("^(?:i|I) select the answer number (\\d+) to the quick poll question$")
	public void selectQuickPollAnswerByIndex(int index) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		WebElement ans = this.quickPoll.getAnswerObjectByIndex(index);
		this.actions.Scroll.scrollToTopOfElement(ans);
		this.actions.Click.clickByJavaScript(ans);
		this.capabilities.reportStepInfo("quich poll answer with index " + index + " is selected");
	}

	@Then("^(?:i|I) verify that quickpoll is successfully submitted with message \"([^\"]*)\"$")
	public void verifyQuickPollIsSubmittedSuccessfully(String message) {
	    String resMessage = this.quickPoll.getQuickPollSubmitMessage();
	    Assert.assertTrue(resMessage.trim().contains(message.trim()), "actual message :: " + resMessage + " and expected message :: " + message );	    
	}

	
	
	
}
