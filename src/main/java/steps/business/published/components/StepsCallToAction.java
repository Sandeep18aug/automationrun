package steps.business.published.components;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.BackToTopPage;
import unilever.pageobjects.platform.publish.components.definitions.CallToAction;

public class StepsCallToAction {
	UserActions actions;
	World capabilities;
	PublishedPageCommon publishedPage;
	BackToTopPage backtotopPage;
	CallToAction calltoactionPage;

	public StepsCallToAction(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.backtotopPage = new BackToTopPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.calltoactionPage = new CallToAction(capabilities.getWebDriver(),capabilities.getBrandName());

	}

	@And("^(?:I|i) click on the cta of the call-to-action component$")
	public void click_relatedarticles_CTA() {
		actions.Wait.waitForPageReady();
		WebElement cta = this.calltoactionPage.getRelatedArticleCTA();
		Assert.assertNotNull(cta,
				"related article cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("relatedarticlectahref", cta.getAttribute("href"));
		cta.click();
	}
	
	@And("^(?:I|i) click on the cta1 of the call-to-action component$")
	public void click_relatedarticles_CTA1() {
		actions.Wait.waitForPageReady();
		WebElement cta = this.calltoactionPage.getRelatedArticleCTA1();
		Assert.assertNotNull(cta,
				"related article cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("relatedarticlectahref", cta.getAttribute("href"));
		cta.click();
	}

}
