package steps.business.published.components;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.*;

public class StepsTags {

	UserActions actions;
	World capabilities;
	Tags tags;

	public StepsTags(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.tags = new Tags(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@When("^(?:i|I) click on any of the tags rendering on page$")
	public void tags() {
		this.actions.Wait.waitForPageReady();
		List<WebElement> tagsCount = this.tags.getTagsList();
		int randomNumber = new Random().nextInt(tagsCount.size());
		WebElement listItem = tagsCount.get(randomNumber);
		listItem.click();
	}

	@And("^(?:i|I) verify that tags are rendering on page$")
	public void tagsRendering() {
		this.actions.Wait.waitForPageReady();
		List<WebElement> tagsCount = this.tags.getTagsList();
		Assert.assertNotNull(tagsCount, "No tags are appearing on page");
	}

}
