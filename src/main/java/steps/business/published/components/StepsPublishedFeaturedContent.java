package steps.business.published.components;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.Then;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.FeaturedContent;
import unilever.pageobjects.platform.publish.components.definitions.HeroV2;

public class StepsPublishedFeaturedContent {
	UserActions actions;
	World capabilities;
	FeaturedContent featuredcontent;

	public StepsPublishedFeaturedContent(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.featuredcontent = new FeaturedContent(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@Then("^(?:i|I) verify that featured content cta is (?:appearing|rendering|present)$")
	public void i_verify_that_featured_content_cta_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		List<WebElement> featurecontentCTA = this.featuredcontent.getfeaturedcontentCTA();
		Assert.assertNotNull(featurecontentCTA,
				"featured content cta is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Featured Content CTA is present on page");
	}
	
	@Then("^(?:I|i) verify that Featured Content Text is present on page$")
	public void i_verify_featuredcontent_text_SK() {
		WebElement featuredContentTextSK = this.featuredcontent.getFeaturedContentText();
		Assert.assertNotNull(featuredContentTextSK,
				"Featured Content Text SK is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Featured Content Text is present on page");
	}
	
	
	
	@Then("^(?:I|i) verify that Featured Content Image is present on page$")
	public void i_verify_featuredcontent_image_SK() {
		
		WebElement featuredContentImageSK = this.featuredcontent.getFeaturedContentImage();
		Assert.assertNotNull(featuredContentImageSK,
				"Featured Content Image SK is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Featured Content Image is present on page");
	}
	
	@Then("^(?:I|i) verify that Featured Content Heading is present on page$")
	public void i_verify_featuredcontent_heading_SK() {
		WebElement featuredContentHeadingSK = this.featuredcontent.getFeaturedContentHeading();
		Assert.assertNotNull(featuredContentHeadingSK,
				"Featured Content Heading SK is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Featured Content Heading  is present on page");
	}
	
	@Then("^(?:i|I) click on Featured Content CTA present on page$")
	public void click_CTA_fc() throws Throwable {
		WebElement featuredcontentCTA = this.featuredcontent.getFeaturedContentCTA();
		Assert.assertNotNull(featuredcontentCTA,
				"Featured Content CTA button is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Featured Content CTA is present on page");
		this.capabilities.getScenarioContext().addScenarioData("featuredcontentCtahref", featuredcontentCTA.getAttribute("href"));
		featuredcontentCTA.click();
	}

	/*@Then("^(?:i|I) verify that (?:herov2 component|hero) heading is (?:appearing|rendering|present)$")
	public void i_verify_that_hero_component_heading_is_appearing() {
		String heading = this.heroV2.getHeroV2Heading();
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(heading.trim()),
				"herov2 heading is empty for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("hero v2 heading is present on page");
	}

	@Then("^(?:i|I) verify that (?:herov2 component|hero) content is (?:appearing|rendering|present)$")
	public void i_verify_that_hero_component_content_is_appearing() {
		// Write code here that turns the phrase above into concrete actions
		String content = this.heroV2.getHeroV2BodyContent();
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(content),
				"herov2 content is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("hero v2 body content is present on page");
	}

	@Then("^(?:i|I) verify that (?:herov2 component|hero) cta is (?:appearing|rendering|present)$")
	public void i_verify_that_hero_component_cta_is_appearing() {
		Assert.assertNotNull(this.heroV2.getHeroV2CtaButton(),
				"herov2 cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("hero v2 body cta is present on page");
	}

	@Then("^(?:i|I) click on cta of (?:herov2 component|hero component)$")
	public void clickHeroCta() {
		WebElement cta = this.heroV2.getHeroV2CtaButton();
		Assert.assertNotNull(cta,
				"herov2 cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("hero v2 body cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("heroctahref", cta.getAttribute("href"));
		cta.click();
	}

	@Then("^it should take me to the corresponding page of hero component cta$")
	public void verifyHeroCtaUrlPage() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("heroctahref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl), "the page is not navigated to expected url. hero cta href was "
				+ expectedUrl + System.lineSeparator() + " actual url is " + actualUrl);
	}*/
}
