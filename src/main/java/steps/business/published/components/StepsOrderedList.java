package steps.business.published.components;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import cucumber.api.java.en.Then;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.OrderedListDefinition;

public class StepsOrderedList {
	UserActions actions;
	World world;
	SoftAssert softAssert;
	OrderedListDefinition orderedListDef;
	
	public StepsOrderedList(World world, UserActions actions) {
		this.actions = actions;
		this.world = world;
		softAssert = new SoftAssert();
		orderedListDef = new OrderedListDefinition(world.getWebDriver(),world.getBrandName());
	}
	
	@Then("^(?:i|I) expect to see (\\d+) Ordered List components in the page$")
	public void verifyOrderedListCount(int orderedListExpectedCount) {
		try {
			if (orderedListDef.isPresent()) {
				List<WebElement> orderedListComponents = orderedListDef.getOrderListComponents();
				int orderedListActualCount = orderedListComponents.size();
				world.reportStepInfo("Verifying that the page has eaxctly " + 
						orderedListExpectedCount + " ordered list components.");
				Assert.assertTrue(orderedListExpectedCount == orderedListActualCount, 
						"Expected and actual ordered list component counts don't "
						+ "match, expected = " + orderedListExpectedCount 
						+ " and actual = " + orderedListActualCount);
			} else
				Assert.fail("The page doesn't have Ordered List component.");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Then("^(?:i|I) expect not to see heading of all Ordered List components in the page$")
	public void verifyAbsenceOfOrderedListHeading() {
		try {
			List<WebElement> orderedListComponents = orderedListDef.getOrderListComponents();
			int orderedListComponentsCount = orderedListComponents.size();
			for (int orderedListIndex = 0; orderedListIndex < orderedListComponentsCount; 
					orderedListIndex ++) {
				WebElement currentOrderedList = orderedListComponents.get(orderedListIndex);
				actions.Scroll.scrollTillTopOfElement(currentOrderedList);
				world.reportStepInfo("Verifying that the Ordered List component "
						+ (orderedListIndex + 1) +" in page doesn't have heading.");
				WebElement orderedListHeading = orderedListDef.getOrederedListHeading(currentOrderedList);
				softAssert.assertTrue(orderedListHeading == null, "The ordered list component"
						+ (orderedListIndex + 1) + " in page contains heading but heading "
								+ "shouldn't exist.");
			}
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Then("^(?:i|I) expect not to see description of all Ordered List components in the page$")
	public void verifyAbsenceOfOrderedListDescription() {
		try {
			List<WebElement> orderedListComponents = orderedListDef.getOrderListComponents();
			int orderedListComponentsCount = orderedListComponents.size();
			for (int orderedListIndex = 0; orderedListIndex < orderedListComponentsCount; 
					orderedListIndex ++) {
				WebElement currentOrderedList = orderedListComponents.get(0);
				actions.Scroll.scrollTillTopOfElement(currentOrderedList);
				world.reportStepInfo("Verifying that the Ordered List component "
						+ (orderedListIndex + 1) + " in page doesn't have heading.");
				WebElement orderedListDescription = orderedListDef.getOrederedListDescription(currentOrderedList);
				softAssert.assertTrue(orderedListDescription == null, "The ordered list component"
						+ (orderedListIndex + 1) + " in page contains description but "
								+ "description shouldn't exist.");
			}
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Then("^(?:i|I) expect to see (\\d+) list items in all Ordered List components in page$")
	public void verifyListInOrderList(int orderedListItemExpectedCount) {
		try {
			List<WebElement> orderedListComponents = orderedListDef.getOrderListComponents();
			int orderedListComponentsCount = orderedListComponents.size();
			for (int orderedListIndex = 0; orderedListIndex < orderedListComponentsCount; 
					orderedListIndex ++) {
				WebElement currentOrderedList = orderedListComponents.get(0);
				actions.Scroll.scrollTillTopOfElement(currentOrderedList);
				List<WebElement> listItemsInOrderedList = orderedListDef.getOrederedListItems(currentOrderedList);
				world.reportStepInfo("Verifying the order list items and their visible text in " 
						+ "ordered list number " + (orderedListIndex + 1));
				int orderedListItemActualCount = listItemsInOrderedList.size();
				if (listItemsInOrderedList.size() != 0) {
					softAssert.assertTrue(orderedListItemExpectedCount == orderedListItemActualCount, 
							"Expected and actual ordered list item counts don't match in ordered list "
							+ "number " + (orderedListIndex + 1) + ". Expected = " + 
							orderedListItemExpectedCount + " and actual = " + 
							orderedListItemActualCount);
					for (int listItemIndex = 0; listItemIndex < listItemsInOrderedList.size(); listItemIndex ++)
						softAssert.assertTrue(!listItemsInOrderedList.get(listItemIndex).getText().trim().isEmpty(), 
								"The ordered list item " + (listItemIndex + 1) + " has no text in ordered list "
								+ "number " + (orderedListIndex + 1));
				} else
					softAssert.fail("The ordered list component number " + (orderedListIndex + 1) + " has"
							+ " no list items.");
			}
			softAssert.assertAll();
		} catch(Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
