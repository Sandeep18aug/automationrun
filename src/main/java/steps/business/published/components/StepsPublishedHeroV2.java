package steps.business.published.components;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.Then;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.HeroV2;

public class StepsPublishedHeroV2 {
	UserActions actions;
	World capabilities;
	HeroV2 heroV2;

	public StepsPublishedHeroV2(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.heroV2 = new HeroV2(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@Then("^(?:i|I) verify that (?:herov2 component|hero) image is (?:appearing|rendering|present)$")
	public void i_verify_that_herov_component_image_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement heroV2Image = this.heroV2.getHeroV2Image();
		Assert.assertNotNull(heroV2Image,
				"herov2 image is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("hero v2 image is present on page");
	}

	@Then("^(?:i|I) verify that (?:herov2 component|hero) heading is (?:appearing|rendering|present)$")
	public void i_verify_that_hero_component_heading_is_appearing() {
		String heading = this.heroV2.getHeroV2Heading();
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(heading.trim()),
				"herov2 heading is empty for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("hero v2 heading is present on page");
	}

	@Then("^(?:i|I) verify that (?:herov2 component|hero) content is (?:appearing|rendering|present)$")
	public void i_verify_that_hero_component_content_is_appearing() {
		// Write code here that turns the phrase above into concrete actions
		String content = this.heroV2.getHeroV2BodyContent();
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(content),
				"herov2 content is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("hero v2 body content is present on page");
	}

	@Then("^(?:i|I) verify that (?:herov2 component|hero) cta is (?:appearing|rendering|present)$")
	public void i_verify_that_hero_component_cta_is_appearing() {
		Assert.assertNotNull(this.heroV2.getHeroV2CtaButton(),
				"herov2 cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("hero v2 body cta is present on page");
	}

	@Then("^(?:i|I) click on cta of (?:herov2 component|hero component)$")
	public void clickHeroCta() {
		WebElement cta = this.heroV2.getHeroV2CtaButton();
		Assert.assertNotNull(cta,
				"herov2 cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("hero v2 body cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("heroctahref", cta.getAttribute("href"));
		cta.click();
	}

	@Then("^it should take me to the corresponding page of hero component cta$")
	public void verifyHeroCtaUrlPage() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("heroctahref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl), "the page is not navigated to expected url. hero cta href was "
				+ expectedUrl + System.lineSeparator() + " actual url is " + actualUrl);
	}
	
	@Then("^(?:i|I) verify that (?:herov2 component|hero) readMore is (?:appearing|rendering|present)$")
	public void heroComponent_readMore_is_appearing() {
		Assert.assertNotNull(this.heroV2.getHeroV2ReadMoreLink(),
				"herov2 readMore is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("hero v2 readMore is present on page");
	}
}
