package steps.business.published.components;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import cucumber.api.java.en.Then;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.FeaturedContent;
import unilever.pageobjects.platform.publish.pdp.ProductCategoryPage;

public class StepsProductCategory {
	
	UserActions actions;
	World world;
	ProductCategoryPage productCategoryPage;
	SoftAssert softAssert;

public StepsProductCategory(World world, UserActions actions) {
	this.actions = actions;
	this.world = world;
	this.productCategoryPage = new ProductCategoryPage(world.getWebDriver(),world.getBrandName());
	softAssert = new SoftAssert();
}

@Then("^(?:I|i) verify that Product Category Section Heading is present on page$")
public void i_verify_productcategory_heading() {
	WebElement pcHeading = this.productCategoryPage.getProductCategoryHeading();
	Assert.assertNotNull(pcHeading,
			"Product Category Section Heading is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
	this.world.reportStepInfo("Product Category Section Heading is present on page");
}

@Then("^(?:I|i) verify that Product Category Section Subheading is present on page$")
public void i_verify_productcategory_subheading() {
	WebElement pcsubHeading = this.productCategoryPage.getProductCategorySubHeading();
	Assert.assertNotNull(pcsubHeading,
			"Product Category Section SubHeading is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
	this.world.reportStepInfo("Product Category Section SubHeading is present on page");
}

@Then("^(?:I|i) verify that Product Category Section Image is present on page$")
public void i_verify_productcategory_image() {
	WebElement pcsubHeading = this.productCategoryPage.getProductCategory();
	Assert.assertNotNull(pcsubHeading,
			"Product Category Section Image is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
	this.world.reportStepInfo("Product Category Section Image is present on page");
}

@Then("^(?:i|I) click on Product Category Section present on page$")
public void click_Product_Category_Section() throws Throwable {
	WebElement productcategory = this.productCategoryPage.getProductCategoryExploreButton();
	Assert.assertNotNull(productcategory,
			"Product Category Section is not appearing for url " + this.world.getWebDriver().getCurrentUrl());
	this.world.reportStepInfo("Product Category Section is present on page");
	this.world.getScenarioContext().addScenarioData("pchref", productcategory.getAttribute("href"));
	productcategory.click();
	actions.Wait.wait(5);
}

@Then("^it should take me to the corresponding page of Product Category Section page$")
public void verify_Product_Category_Section() throws Throwable {
	this.actions.Wait.waitForPageReady();
	actions.Wait.wait(5);
	String expectedUrl = (String) this.world.getScenarioContext().getScenarioData("pchref");
	String actualUrl = this.world.getWebDriver().getCurrentUrl();
	Assert.assertTrue(expectedUrl.equals(actualUrl), "the page is not navigated to expected url. Product Category Section href was "
			+ expectedUrl + System.lineSeparator() + " actual url is " + actualUrl);
}

}
