package steps.business.published.components;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.FeaturedContent;
import unilever.pageobjects.platform.publish.components.definitions.PageListingV2;


public class StepsPageListingv2 {
	UserActions actions;
	World capabilities;
	PageListingV2 pagelistingv2page;

public StepsPageListingv2(World capabilities, UserActions actions) {
	this.actions = actions;
	this.capabilities = capabilities;
	this.pagelistingv2page = new PageListingV2(capabilities.getWebDriver(),capabilities.getBrandName());
}


@Then("^(?:I|i) verify that Page Listing V2 component heading is appearing$")
public void i_verify_that_pagelistingv2_component_heading_is_rendering() {
    // Write code here that turns the phrase above into concrete actions
	WebElement pagelistingv2Heading = this.pagelistingv2page.pagelistingv2heading();
   Assert.assertNotNull(pagelistingv2Heading, "page listing v2 heading is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
   this.capabilities.reportStepInfo("page listing v2 heading is present on page");
}

@Then("^(?:I|i) verify that Page Listing V2 component image is appearing$")
public void i_verify_that_pagelistingv2_component_image_is_rendering() {
    // Write code here that turns the phrase above into concrete actions
	WebElement pagelistingv2Image = this.pagelistingv2page.pagelistingv2image();
   Assert.assertNotNull(pagelistingv2Image, "page listing v2 image is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
   this.capabilities.reportStepInfo("page listing v2 image is present on page");
}

@Then("^(?:I|i) verify that Page Listing V2 component description is appearing$")
public void i_verify_that_pagelistingv2_component_description_is_rendering() {
    // Write code here that turns the phrase above into concrete actions
	WebElement pagelistingv2Description = this.pagelistingv2page.pagelistingv2description();
   Assert.assertNotNull(pagelistingv2Description, "page listing v2 description is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
   this.capabilities.reportStepInfo("page listing v2 description is present on page");
}

@Then("^(?:I|i) verify that Page Listing V2 component Sub-heading is appearing$")
public void i_verify_that_pagelistingv2_component_subheading_is_rendering() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
	WebElement featuredcontentHeading = this.pagelistingv2page.pagelistingv2suheading();
   Assert.assertNotNull(featuredcontentHeading, "page listing v2 sub heading is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
   this.capabilities.reportStepInfo("page listing v2 sub heading is present on page");
}

@And("^(?:I|i) verify that page listing v2 component has cta link/button present on the page$")
public void pagelistingv2_CTA_visible() throws Throwable {
	
	Assert.assertNotNull(this.pagelistingv2page.pagelistingv2CTA(),
			"page listing v2 cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
	this.capabilities.reportStepInfo("page listing v2 CTA is present on page");
}

@And("^(?:I|i) click on the cta of the page listing v2 component$")
public void click_CTA_URL_pagelistingv2() throws Throwable {
		WebElement cta = this.pagelistingv2page.pagelistingv2CTA();
		Assert.assertNotNull(cta,
				"page listing v2 cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("page listing v2 cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("pagelistingv2ctahref", cta.getAttribute("data-href"));
		cta.click();
	
}

@Then("^it should take me to the corresponding page of page listing cta$")
public void verify_CTA_URL_pagelistingv2_Page() {
	this.actions.Wait.waitForPageReady();
	String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("pagelistingv2ctahref");
	String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
	Assert.assertTrue(actualUrl.equalsIgnoreCase(expectedUrl),
			"page hasn't navigated to expected url => " + expectedUrl + ". instead it was navigated to => " + actualUrl);
	this.capabilities.reportStepInfo("page is successfully navigated to url - " + expectedUrl);
}

@And("^(?:I|i) verify pagination present on page listing page$")
public void pagelistingv2_pagination_visible() throws Throwable {
	
	Assert.assertNotNull(this.pagelistingv2page.pagelistingv2pagination(),
			"page listing v2 cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
	this.capabilities.reportStepInfo("page listing v2 CTA is present on page");
}

@And("^(?:I|i) click on the pagination cta of the page listing v2 component$")
public void click_CTA_Pagination_pagelistingv2() throws Throwable {
		WebElement cta = this.pagelistingv2page.pagelistingv2pagination();
		Assert.assertNotNull(cta,
				"page listing v2 pagination cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("page listing v2 pagination cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("pagelistingv2paginationhref", cta.getAttribute("href"));
		cta.click();
	
}

@Then("^it should take me to the corresponding page of page listing cta pagination$")
public void verify_CTA_URL_pagelistingv2_Pagination() {
	this.actions.Wait.waitForPageReady();
	String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("pagelistingv2paginationhref");
	String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
	Assert.assertTrue(actualUrl.equalsIgnoreCase(expectedUrl),
			"page hasn't navigated to expected url => " + expectedUrl + ". instead it was navigated to => " + actualUrl);
	this.capabilities.reportStepInfo("page is successfully navigated to url - " + expectedUrl);
}

@Then("^I verify show more functionality is working on Page Listing Page$")
public void verify_pagination_pagelisting() throws Throwable {
			actions.Wait.waitForPageReady();
			Assert.assertNotNull(this.pagelistingv2page.getShowMoreButtonPageListing(),
					"pagination cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
			this.capabilities.reportStepInfo("Load more button is present on page");
			
			int initialListCount = this.pagelistingv2page.getPageListingItems().size();
			this.capabilities.reportStepInfo("Items count before clicking load more is " + initialListCount);
			this.pagelistingv2page.getShowMoreButtonPageListing().click();
			this.actions.Wait.wait("2");
			int LaterListCount = this.pagelistingv2page.getPageListingItems().size();
			this.capabilities.reportStepInfo("Items count after clicking load more is " + LaterListCount);
			this.actions.Wait.wait("2");	
			Assert.assertTrue(initialListCount <= LaterListCount, "Load more functionality is not working properly");
			this.actions.Scroll.scrollPublishedPageToBottom();
			this.actions.Wait.wait("2");
			int finalListCount = this.pagelistingv2page.getPageListingItems().size();
			//List<WebElement> finalList = this.slpPage.getSearchListingPaginationItem() ;
			this.capabilities.reportStepInfo("Items count after scrolling down is " + finalListCount);
			Assert.assertTrue(LaterListCount <= finalListCount, "Load more functionality is not working properly");
		}


@And("^(?:I|i) click on any of the items on Page Listing V2$")
public void click_random_article_on_pagelistingv2( ) {
	actions.Wait.waitForPageReady();
	List<WebElement> pageListingV2Items = this.pagelistingv2page.getPageListingItems();
	Assert.assertNotNull(pageListingV2Items,"page listing v2 item are not appearing on page");
	int randomNumber = RandomUtil.getRandomNumber(1, pageListingV2Items.size()-1);
	WebElement listItem = pageListingV2Items.get(randomNumber);
	String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
	capabilities.getScenarioContext().addScenarioData("selectedpagelistingv2itemurl", href);
	listItem.click();
	this.capabilities.reportStepInfo("clicked on page listing v2 item with href " + href);
}

@Then("^it should take me to the corresponding page of Page Listing V2 Image$")
public void verify_related_article_image() {
	this.actions.Wait.waitForPageReady();
	String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("selectedpagelistingv2itemurl");
	String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
	Assert.assertTrue(expectedUrl.equals(actualUrl), "the page is not navigated to expected url. Page Listing V2 Image href was "
			+ expectedUrl + System.lineSeparator() + " actual url is " + actualUrl);
}

}
