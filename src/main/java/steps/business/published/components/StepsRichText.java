package steps.business.published.components;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.testng.util.Strings;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.*;
import unilever.pageobjects.platform.publish.components.definitions.*;

public class StepsRichText {

	UserActions actions;
	World capabilities;
	LiveChat LiveChatPage;
	RichTextV2 RichTextV2;
	SoftAssert softAssert;

	public StepsRichText(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.LiveChatPage = new LiveChat(capabilities.getWebDriver(),capabilities.getBrandName());
		this.RichTextV2 = new RichTextV2(capabilities.getWebDriver(),capabilities.getBrandName());
		softAssert = new SoftAssert();
	}

	@And("^(?:i|I) verify rich text heading is rendering on page$")
	public void richV2Heading() {
		this.actions.Wait.waitForPageReady();
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(this.RichTextV2.getRichTextV2heading().trim()),
				"rich text heading is not present on page");

	}

	@And("^(?:i|I) verify rich text description is rendering on page$")
	public void richV2Description() {
		this.actions.Wait.waitForPageReady();
		Assert.assertTrue(
				Strings.isNotNullAndNotEmpty(
						this.RichTextV2.getRichTextV2Description().getAttribute("innerHTML").trim()),
				"rich text description is not present on page");

	}
	
	@Then("^(?:i|I) expect to see (\\d+) Rich Text components in the page$")
	public void verifyRichTextCount(int richTextExpectedCount) {
		try {
			if (RichTextV2.isPresent()) {
				List<WebElement> richTextComponents = RichTextV2.getRichTextV2Components();
				int richTextActualCount = richTextComponents.size();
				capabilities.reportStepInfo("Verifying that the page has eaxctly " + 
						richTextExpectedCount + " rich text components.");
				Assert.assertTrue(richTextExpectedCount == richTextActualCount, "Expected and actual "
						+ "rich text component counts don't match, expected = " + richTextExpectedCount 
						+ " and actual = " + richTextActualCount);
			} else
				Assert.fail("The page doesn't have Rich Text component.");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Then("^(?:i|I) expect to see small heading for all Rich Text components in the page$")
	public void verifyRichTextSmallHeading() {
		try {
			List<WebElement> richTextComponents = RichTextV2.getRichTextV2Components();
			int richTextCount = richTextComponents.size();
			capabilities.reportStepInfo("The page has " + richTextCount + " Rich Text component(s).");
			for (int richTextIndex = 0; richTextIndex < richTextCount; richTextIndex ++) {	
				WebElement currentRichText = richTextComponents.get(richTextIndex);
				actions.Scroll.scrollTillTopOfElement(currentRichText);
				capabilities.reportStepInfo("Verifying that Rich Text " + (richTextIndex + 1) + 
						" has small title.");
				WebElement smallTitle = RichTextV2.getSmallHeadingInRichText(currentRichText);
				if (smallTitle == null)
					softAssert.fail("Rich Text " + (richTextIndex + 1) + " doesn't have small title.");
				else {
					capabilities.reportStepInfo("Verifying that existing small title of Rich Text " + 
							(richTextIndex + 1) + " has some text.");
					softAssert.assertTrue(!smallTitle.getText().trim().isEmpty(), "Small title of Rich Text "
							+ (richTextIndex + 1) + " doesn't have text.");
				}
			}
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@Then("^(?:i|I) expect to see description for all Rich Text components in the page$")
	public void verifyRichTextDescription() {
		try {
			List<WebElement> richTextComponents = RichTextV2.getRichTextV2Components();
			int richTextCount = richTextComponents.size();
			capabilities.reportStepInfo("The page has " + richTextCount + " Rich Text component.");
			for (int richTextIndex = 0; richTextIndex < richTextCount; richTextIndex ++) {	
				WebElement currentRichText = richTextComponents.get(richTextIndex);
				actions.Scroll.scrollTillTopOfElement(currentRichText);
				capabilities.reportStepInfo("Verifying that Rich Text " + (richTextIndex + 1) + 
						" has a description.");
				WebElement richTextDescription = RichTextV2.getDescriptionInRichText(currentRichText);
				if (richTextDescription == null)
					softAssert.fail("Rich Text " + (richTextIndex + 1) + " doesn't have description.");
				else {
					capabilities.reportStepInfo("Verifying that existing description of Rich Text " + 
							(richTextIndex + 1) + " has some text.");
					softAssert.assertTrue(!richTextDescription.getText().trim().isEmpty(), "Description "
							+ "of Rich Text " + (richTextIndex + 1) + " doesn't have text.");
				}
			}
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
