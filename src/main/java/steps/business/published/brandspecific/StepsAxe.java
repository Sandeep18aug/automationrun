package steps.business.published.brandspecific;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.brandspecific.Cleanipedia;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.searchlistingpage.SearchListingPage;

public class StepsAxe {
	UserActions actions;
	World capabilities;
	SearchListingPage slp;
	Cleanipedia cleanipedia;
	
	public StepsAxe(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.slp = new SearchListingPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.cleanipedia = new Cleanipedia(capabilities.getWebDriver(),capabilities.getBrandName());
	}
	

}

