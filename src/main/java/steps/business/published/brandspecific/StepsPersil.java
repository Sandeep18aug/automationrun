package steps.business.published.brandspecific;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;

public class StepsPersil {
	UserActions actions;
	World capabilities;
	PublishedPageCommon publishedPage;
	
	public StepsPersil(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());

	}
	
	@Then("^(?:i|I) click on video for Persil brand$")
	public void clickOnVideoForPersil() throws Throwable {
		this.capabilities.getWebDriver().switchTo().defaultContent();
		this.publishedPage.switchToVideoIframe();
		WebElement video = this.publishedPage.getVideoObject();
		this.actions.Click.clickByActionsClass(video);
		this.capabilities.reportStepInfo("successfully clicked video");
		this.actions.Wait.wait("3");
	}
}

