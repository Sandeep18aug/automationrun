package steps.business.published.brandspecific;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.brandspecific.CWLoginAndRegistrationPage;
import unilever.pageobjects.platform.publish.brandspecific.CWSite;

public class StepsCWSite {

	UserActions actions;
	World world;
	CWLoginAndRegistrationPage loginAndRegistrationPage;
	CWSite cwSite;
	
	public StepsCWSite(World world, UserActions getBasicKeywords) {
		this.actions = getBasicKeywords;
		this.world = world;
		this.loginAndRegistrationPage = new CWLoginAndRegistrationPage(world.getWebDriver(),world.getBrandName());
		this.cwSite = new CWSite(world.getWebDriver(),world.getBrandName());
	}

	@Given("^(?:I|i) search \"([^\"]*)\" keyword in cw site$")
	public void searchText(String text) throws ObjectNotFoundInORException {
		this.cwSite.searchText(text);
	}
	
	@When("^(?:i|I) login into cw site with userid \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void i_login_into_cw_site_with_userid_and_password(String userid, String password) throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement emailTextBox = loginAndRegistrationPage.getEmailTextBox();
		actions.Wait.waitForObjectToVisible(emailTextBox);
		Assert.assertTrue(emailTextBox.isDisplayed(), "email text box is not displayed on login page");
		emailTextBox.sendKeys(userid);
		WebElement passwordTextBox = loginAndRegistrationPage.getPasswordTextBox();
		actions.Wait.waitForObjectToVisible(passwordTextBox);
		Assert.assertTrue(passwordTextBox.isDisplayed(), "password text box is not displayed on login page");
		passwordTextBox.sendKeys(password);
		WebElement loginButton = loginAndRegistrationPage.getLoginButton();
		actions.Wait.waitForObjectToVisible(loginButton);
		Assert.assertTrue(loginButton.isDisplayed(), "login button text box is not displayed on login page");
		loginButton.click();
		this.world.reportStepInfo("successfully entered userid and passoword and clicked on login button");
	}

	@Then("^(?:i|I) should be able to login successfully in cw site$")
	public void i_should_be_able_to_login_successfully_i_cw_site() {
		actions.Wait.waitForPageReady();
		System.out.println("logged in");
		Assert.assertTrue(world.getWebElementFactory().isElementVisibleOnPage(By.cssSelector("div.display-name-wrapper span.display-name")),"user is not logged in");
	}
}


