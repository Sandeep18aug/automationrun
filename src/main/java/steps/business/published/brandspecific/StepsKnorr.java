package steps.business.published.brandspecific;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.brandspecific.Cleanipedia;

import unilever.pageobjects.platform.publish.brandspecific.Knorr;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.RelatedProduct;
import unilever.pageobjects.platform.publish.searchlistingpage.SearchListingPage;

public class StepsKnorr {
	UserActions actions;
	World capabilities;
	PublishedPageCommon publishedPage;
	RelatedProduct relatedproductPage;
	
	public StepsKnorr(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.relatedproductPage = new RelatedProduct(capabilities.getWebDriver(),capabilities.getBrandName());
	}
	
	@And("^(?:I|i) close email signup popup(?: for knorr us|)$")
	public void closeEmailSignupPopup() throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement signUpPopUpCloseButton = this.publishedPage.getCloseButtonForSignUpPopup();
		if (signUpPopUpCloseButton != null && signUpPopUpCloseButton.isDisplayed()) {
			signUpPopUpCloseButton.click();
			this.actions.Wait.wait("2");
			this.actions.Hover.hover(this.publishedPage.getGlobalNavLogo());
			this.actions.Wait.wait("1");
		}
	}
	
	@When("^(?:i|I) close knorr poll container$")
	public void closeKnorrPollContainer() {
		new Knorr(this.capabilities.getWebDriver(), this.capabilities.getBrandName()).closeKnorrPollContainer();
	}
	
	@And("^(?:I|i) click on the related-product component of knorr$")
    public void click_relatedproductknorr() {
		actions.Wait.waitForPageReady();
		WebElement cta = this.relatedproductPage.getRelatedProductsVisibleItem();
		Assert.assertNotNull(cta,
				"related product is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related product present on page");
		this.capabilities.getScenarioContext().addScenarioData("relatedproducthref", cta.getAttribute("href"));
		//cta.click();
		this.relatedproductPage.clickByJavaScript(cta);
	}
}

