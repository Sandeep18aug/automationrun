package steps.business.published.brandspecific;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import com.sun.tools.xjc.Driver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.brandspecific.CWLoginAndRegistrationPage;
import unilever.pageobjects.platform.publish.brandspecific.CWSite;
import unilever.pageobjects.platform.publish.brandspecific.DQMLoginAndRegistrationPage;
import unilever.pageobjects.platform.publish.brandspecific.DQMSite;
import unilever.pageobjects.platform.publish.homepage.HomePage;

public class StepsDQMSite {

	UserActions actions;
	World world;
	HomePage homePage;
	DQMLoginAndRegistrationPage loginAndRegistrationPage1;
	DQMSite dqmSite;
	
	
	public StepsDQMSite(World world, UserActions getBasicKeywords) {
		this.actions = getBasicKeywords;
		this.world = world;
		this.loginAndRegistrationPage1 = new DQMLoginAndRegistrationPage(world.getWebDriver(),world.getBrandName());
		this.dqmSite = new DQMSite(world.getWebDriver(),world.getBrandName());
	}

	@Given("^(?:I|i) search \"([^\"]*)\" keyword in dqm site$")
	public void searchText(String text) throws ObjectNotFoundInORException {
		this.dqmSite.searchText(text);
	}
	
	@When("^(?:i|I) login into dqm site with userid \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void i_login_into_dqm_site_with_userid_and_password(String userid, String password) throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement emailTextBox = loginAndRegistrationPage1.getEmailTextBox();
		actions.Wait.waitForObjectToVisible(emailTextBox);
		Assert.assertTrue(emailTextBox.isDisplayed(), "email text box is not displayed on login page");
		emailTextBox.sendKeys(userid);
		WebElement passwordTextBox = loginAndRegistrationPage1.getPasswordTextBox();
		actions.Wait.waitForObjectToVisible(passwordTextBox);
		Assert.assertTrue(passwordTextBox.isDisplayed(), "password text box is not displayed on login page");
		passwordTextBox.sendKeys(password);
		WebElement loginButton = loginAndRegistrationPage1.getLoginButton();
		actions.Wait.waitForObjectToVisible(loginButton);
		Assert.assertTrue(loginButton.isDisplayed(), "login button text box is not displayed on login page");
		loginButton.click();
		this.world.reportStepInfo("successfully entered userid and passoword and clicked on login button");
	}
	
	@Then("^(?:i|I) should be able to login successfully in dqm site$")
	public void i_should_be_able_to_login_successfully_i_dqm_site() {
		actions.Wait.waitForPageReady();
		System.out.println("User is successfully logged in");
		
	}
	
	@And("^(?:I|i) enter website name as \"([^\"]*)\"$")
	public void i_select_website_as(String country) throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement countryTextBox = loginAndRegistrationPage1.getCountryTextBox();
		actions.Wait.waitForObjectToVisible(countryTextBox);
		Assert.assertTrue(countryTextBox.isDisplayed(), "email text box is not displayed on login page");
		actions.Enter.enterTextInObjectFromClipboard(country, countryTextBox);
	}
	
	@Then("^(?:i|I) click on all the links$")
	public void brokenlinks() throws Throwable {
		actions.Wait.waitForPageReady();
		
		
		
}
}



