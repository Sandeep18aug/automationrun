package steps.business.published.brandspecific;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.brandspecific.Cleanipedia;

import unilever.pageobjects.platform.publish.brandspecific.Tresemme;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.RelatedProduct;
import unilever.pageobjects.platform.publish.searchlistingpage.SearchListingPage;

public class StepsTresemme {
                UserActions actions;
                World capabilities;
                Tresemme tresemme;
                RelatedProduct relatedproductPage;
                public StepsTresemme(World capabilities, UserActions actions) {
                                this.actions = actions;
                                this.capabilities = capabilities;
                                this.tresemme = new Tresemme(capabilities.getWebDriver(), capabilities.getBrandName());
                                this.relatedproductPage = new RelatedProduct(capabilities.getWebDriver(),capabilities.getBrandName());
                }


                @Then("^(?:i|I) click on Show More Article Button on Tresemme Search Listing Page$")
                public void clickshowmorearticletbuttonsearchlisting() {
                                WebElement showmorebutton = this.tresemme.getSearchListingTresemmeCTA();
                                Assert.assertNotNull(showmorebutton, "searchlisting Show More Article Button is not appearing");
                                this.capabilities.reportStepInfo("search listing Show More Article Button is present on page");
                                showmorebutton.click();
                }
       
                
                @And("^(?:I|i) click on the related-product component of tresemme$")
                public void click_relatedproducttresemme() {
            		actions.Wait.waitForPageReady();
            		WebElement cta = this.relatedproductPage.getRelatedProductsVisibleItem();
            		Assert.assertNotNull(cta,
            				"related product is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
            		this.capabilities.reportStepInfo("related product present on page");
            		this.capabilities.getScenarioContext().addScenarioData("relatedproducthref", cta.getAttribute("href"));
            		//cta.click();
            		this.relatedproductPage.clickByJavaScript(cta);
            	}


}
