package steps.business.published.brandspecific;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.brandspecific.Cleanipedia;

import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.RelatedArticle;
import unilever.pageobjects.platform.publish.searchlistingpage.SearchListingPage;

public class StepsCleanipedia {
	UserActions actions;
	World capabilities;
	SearchListingPage slp;
	Cleanipedia cleanipedia;
	
	public StepsCleanipedia(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.slp = new SearchListingPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}
	
	
//	@And("^(?:I|i) click on the article image of axe$")
//	public void click_related_article_image_axe() {
//		RelatedArticle relatedArticle = new RelatedArticle(this.capabilities.getWebDriver(), this.capabilities.getBrandName());
//		WebElement articleImage = relatedArticle.getRelatedArticleImagesAxe();
//		Assert.assertNotNull(articleImage,
//				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
//		this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
//				articleImage.getAttribute("href"));
//		System.out.println("###########");
//		System.out.println(articleImage.getAttribute("href"));
//		System.out.println("###########");
//		this.actions.Scroll.scrollPublishedPageDownByPixels(1000); 
//		articleImage.click();
//		this.capabilities.reportStepInfo("related article image has been clicked");
//	}
}

