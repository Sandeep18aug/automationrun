package steps.business.published.brandspecific;

import java.io.IOException;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.When;

import steps.business.generic.UserActions;
import steps.generic.keywords.World;

import unilever.pageobjects.platform.publish.brandspecific.TheGoodStuffHair;

import unilever.pageobjects.platform.publish.pdp.ProductDetailPage;

public class StepsTheGoodStuffHair {
	UserActions actions;
	World capabilities;
	ProductDetailPage pdp;
	TheGoodStuffHair theGoodStuffHair;

	public StepsTheGoodStuffHair(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.pdp = new ProductDetailPage(capabilities.getWebDriver(), capabilities.getBrandName());
		this.theGoodStuffHair = new TheGoodStuffHair(capabilities.getWebDriver(), capabilities.getBrandName());
	}

	@When("i choose \"([^\"]*)\" retailer in add to bag dialog for The Good Stuff brand")
	public void i_choose_retailer_in_add_to_bag(String retailer) throws IOException {
		WebElement retailerListBox = this.pdp.getAddToBag().getRetailerWebElement();
		retailerListBox.click();
		this.theGoodStuffHair.clickRetailerOptionByText(retailer);
	}

	@When("i choose \"([^\"]*)\" quantity in add to bag dialog for The Good Stuff brand")
	public void i_choose_quantity_in_add_to_bag(String quantity) throws Throwable {
		WebElement qtyListBox = this.pdp.getAddToBag().getQuantityWebElement();
		qtyListBox.click();
		this.theGoodStuffHair.clickQtyOptionByText(quantity);
		this.actions.Wait.wait("1");
	}

}
