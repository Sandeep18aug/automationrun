package steps.business.author.siteadmin;

import cucumber.api.java.en.And;
import framework.shared.FrameworkConstants;
import framework.shared.UrlHandler;
import steps.business.generic.UserActions;
import steps.generic.keywords.*;
import unilever.pageobjects.platform.author.siteadmin.SiteAdminApi;

import java.io.File;
import java.io.FileInputStream;
import java.time.Duration;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Strings;

public class CreatePage {
	List<WebElement> elements;
	String sPageIdentifier;
	World world;
	UserActions actions;
	WebElement lnkCreate, lnkCreatePage, UnileverFlexiTemplate, btnNext, txtPageName, txtTitle, txtPageTitle,
			txtNavigationTitle, btnCreatePage, btnCreatePageDone;
	By BTN_Search_ByCss = By
			.cssSelector("nav.foundation-layout-mode2-item-active button[data-target='#granite-rail-search']");
	By BTN_Create_ByCss = By.cssSelector("nav.foundation-layout-mode2-item-active a[title='Create'] span");
	By EDT_Search_ByCss = By.cssSelector("#granite-rail-search .search input");
	By DIV_OverlayMask_ByCss = By.cssSelector(".overlay-mask");
	By Result_SearchedArticles = By.cssSelector("article.card-page.foundation-collection-item");
	By CHK_SelectAll_SearchedArticles = By.cssSelector("header.card-page.selectable>i.select");
	By BTN_More_ByCss = By
			.cssSelector("nav[class*='foundation-layout-mode2-item-active'] i.coral-Icon.coral-Icon--more");
	By LNK_Unpublish_Primary = By.cssSelector("a[title='Unpublish']");
	By LNK_Unpublish_Secondary = By.xpath("//a[text()='Unpublish']");
	By LNK_Delete_More = By.cssSelector("button[title='Delete']");
	By BTN_Delete_CoralModal = By
			.xpath("//div[contains(@class,'coral-Modal') and contains(@style,'block')]//button[text()='Delete']");
	By BTN_CrossSearch = By.xpath("//div[@class='granite-endor-Title']/button");
	By LNK_ClearAll = By.xpath("//span[text()='Clear All']");
	By Loader = By.cssSelector("div.foundation-ui-mask>coral-wait");
	By emptyDiv = By.cssSelector("#granite-omnisearch-result-empty");
	By resultContentDiv = By.id("granite-omnisearch-result-content");
	By resultItems = By.cssSelector("#granite-omnisearch-result-content coral-masonry-item");
	// By firstCellOfToBeDeletedRow =
	// By.xpath("//table[@id='granite-omnisearch-result']//tr[child::td[@value='BrandSocialChannels_01']]/td[1]");
	By closeSearch = By.cssSelector("button.granite-omnisearch-typeahead-close>coral-icon");
	// Object Declaration for Login Page

	// *************************************************************************************************************************************

	private enum ViewType {
		ListView("List View"), ColumnView("Column View"), CardView("Card View");
		private String viewType;

		private ViewType(String viewType) {
			this.viewType = viewType;
		}
	}

	public CreatePage(World runner, UserActions actions) {
		this.world = runner;
		this.actions = actions;
	}

	private void changeViewType(ViewType viewType) throws Throwable {
		System.out.println("changing viewtype to " + viewType.viewType);
		WebDriverWait waitDriver = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		WebElement BTN_ChangeLayout = waitDriver.until(
				ExpectedConditions.elementToBeClickable(By.cssSelector("#granite-collection-switcher-toggle>button")));
		if (!BTN_ChangeLayout.getAttribute("title").trim().equalsIgnoreCase(viewType.viewType)) {
			BTN_ChangeLayout.click();
			Thread.sleep(1000);
			BTN_ChangeLayout = waitDriver.until(ExpectedConditions
					.elementToBeClickable(By.cssSelector("#granite-collection-switcher-toggle>button")));
			if (!BTN_ChangeLayout.getAttribute("title").trim().equalsIgnoreCase(viewType.viewType)) {
				waitDriver.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//coral-selectlist-item[text()='" + viewType.viewType + "']")))
						.click();
				Thread.sleep(3000);
			}
		}
	}

	// @And("^I delete \"([^\"]*)\" page$")
	public void deleteExistingPage(String sPageName) throws Throwable {
		Thread.sleep(3000);
		actions.Wait.waitForPageReady();
		changeViewType(ViewType.ListView);
		System.out.println("Deleting Page:: " + sPageName);
		WebDriverWait waitDriver = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		Actions action = new Actions(world.getWebDriver());
		List<WebElement> tableItems = world.getWebDriver()
				.findElements(By.cssSelector("table[role='grid']>tbody>tr"));
		if (tableItems.size() > 0) {
			waitDriver
					.until(ExpectedConditions
							.elementToBeClickable(By.xpath("//*[@id='shell-collectionpage-rail-toggle']/button")))
					.click();
			Thread.sleep(1000);
			waitDriver
					.until(ExpectedConditions.elementToBeClickable(By.xpath(
							"//*[@id='shell-collectionpage-rail-toggle']//coral-selectlist-item[text()='Filter']")))
					.click();
			Thread.sleep(3000);
			actions.Wait.waitForObjectToPerish(Loader);
			Thread.sleep(2000);
			WebElement searchTextbox = waitDriver
					.until(ExpectedConditions.visibilityOfElementLocated(By.name("fulltext")));
			action.click(searchTextbox).pause(1000).sendKeys(searchTextbox, sPageName).pause(2000).sendKeys(Keys.ESCAPE)
					.pause(1000).sendKeys(Keys.ENTER).perform();
			Thread.sleep(3000);
			actions.Wait.waitForObjectToPerish(Loader);
			Thread.sleep(2000);
			if (world.getWebElementFactory().isElementVisibleOnPage(emptyDiv, 10)) {
				actions.Click.click(world.getWebElementFactory().getElement(closeSearch));
				return;
			} else {
				changeViewType(ViewType.ListView);
				Thread.sleep(2000);
				WebElement firstCellOfToBeDeletedRow = world.getWebElementFactory().getElement(By.xpath(
						"//table[@id='granite-omnisearch-result']//tr[child::td[@value='" + sPageName + "']]/td[1]"));
				firstCellOfToBeDeletedRow.click();
				WebElement btnDeleteOfSelectionBar = waitDriver.until(ExpectedConditions.elementToBeClickable(By.xpath(
						"//*[@id='granite-shell-search-result-selectionbar']//button[@*='delete']/coral-button-label")));
				btnDeleteOfSelectionBar.click();
				WebElement btnDeleteOfDialog = waitDriver.until(ExpectedConditions.elementToBeClickable(
						By.xpath("//coral-dialog[contains(@class,'is-open')]//button[.='Delete']/coral-button-label")));
				btnDeleteOfDialog.click();
				Thread.sleep(1000);
				actions.Wait.waitForObjectToPerish(Loader);
				actions.Click.click(world.getWebElementFactory().getElement(closeSearch));
			}
		}

		/*
		 * //WebElement searchTextbox =
		 * waitDriver.until(ExpectedConditions.elementToBeClickable(EDT_Search_ByCss));
		 * waitDriver.until(ExpectedConditions.attributeContains(DIV_OverlayMask_ByCss,
		 * "style", "display: none;")); Thread.sleep(2000);
		 * //waitDriver.until(ExpectedConditions.elementToBeClickable(By.
		 * cssSelector("button[title='Card View']"))).click(); Thread.sleep(5000); if
		 * (world.getBrowser().findElements(Result_SearchedArticles).size() > 0)
		 * { WebElement selectArticle = null; try { selectArticle =
		 * waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector(
		 * "article[data-foundation-collection-item-id*='" + sPageName +
		 * "']>i[class='select']"))); } catch (Exception e) {} if (selectArticle !=
		 * null) { selectArticle.click(); Thread.sleep(1000);
		 * waitDriver.until(ExpectedConditions.elementToBeClickable(BTN_More_ByCss)).
		 * click(); // waitDriver.until(ExpectedConditions.elementToBeClickable(
		 * LNK_Unpublish_Primary)).click(); //
		 * waitDriver.until(ExpectedConditions.elementToBeClickable(
		 * LNK_Unpublish_Secondary)).click();
		 * waitDriver.until(ExpectedConditions.elementToBeClickable(LNK_Delete_More)).
		 * click(); waitDriver.until(ExpectedConditions.elementToBeClickable(
		 * BTN_Delete_CoralModal)).click(); Thread.sleep(10000); } else {
		 * waitDriver.until(ExpectedConditions.elementToBeClickable(LNK_ClearAll)).click
		 * (); waitDriver.until(ExpectedConditions.visibilityOfElementLocated(
		 * BTN_Create_ByCss)); } } else {
		 * waitDriver.until(ExpectedConditions.elementToBeClickable(LNK_ClearAll)).click
		 * (); waitDriver.until(ExpectedConditions.visibilityOfElementLocated(
		 * BTN_Create_ByCss)); }
		 * waitDriver.until(ExpectedConditions.elementToBeClickable(BTN_CrossSearch)).
		 * click();
		 */ }

	private void changeLayoutTo(String layOut) {

	}

	@And("^I create a page (.*) using \"([^\"]*)\"$")
	public void createPage(String sPageName, String sTemplateName) throws Throwable {
		System.out.println("Creating Page:: " + sPageName);
		// deleteExistingPage(sPageName);
		Thread.sleep(2000);
		WebDriverWait waitDriver = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		// lnkCreate =
		// waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector("i[class='coral-Icon
		// coral-Icon--add']")));
		lnkCreate = waitDriver.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@id='granite-shell-actionbar']//coral-button-label[text()='Create']")));
		actions.Click.click(lnkCreate);
		Thread.sleep(1000);
		lnkCreatePage = waitDriver.until(ExpectedConditions.elementToBeClickable(By.linkText("Create Page")));
		actions.Click.click(lnkCreatePage);
		Thread.sleep(1000);
		UnileverFlexiTemplate = waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector(
				"article[data-foundation-collection-item-id='/apps/unilever-iea/templates/unileverBaseTemplate'] * h4")));
		actions.Click.click(UnileverFlexiTemplate);

		btnNext = waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[data-action='next']")));
		actions.Click.click(btnNext);

		txtPageName = waitDriver
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[name='pageName']")));
		actions.Enter.enter(sPageName, txtPageName);

		txtTitle = waitDriver
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[name='./jcr:title']")));
		actions.Enter.enter(sPageName, txtTitle);

		txtPageTitle = waitDriver
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[name='./pageTitle']")));
		actions.Enter.enter(sPageName, txtPageTitle);

		txtNavigationTitle = waitDriver
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[name='./navTitle']")));
		actions.Enter.enter(sPageName, txtNavigationTitle);

		btnCreatePage = waitDriver
				.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button[data-action='next']")));
		actions.Click.click(btnCreatePage);

		btnCreatePageDone = waitDriver.until(ExpectedConditions
				.elementToBeClickable(By.cssSelector("div[class='coral-Modal-footer']>button:nth-of-type(1)")));
		actions.Click.click(btnCreatePageDone);

		String sCurrentURL = world.getWebDriver().getCurrentUrl();
		sCurrentURL = sCurrentURL.replaceAll("sites", "editor");
		String sAuthorPageURL = sCurrentURL + "/" + sPageName + ".html";
		world.getWebDriver().get(sAuthorPageURL);
		actions.Wait.waitForPageReady();
	}

	/**
	 * @param title
	 *            : Title of the Live Copy page
	 * @param name
	 *            : Name of the Live Copy page
	 * @param liveCopyFromPath
	 *            : Source path from where rollout has to be made
	 * @param liveCopyTargetPath
	 *            : Target folder path under which which Target copy will be create
	 *            by the name attribute
	 * @param prefillFormWithProfileData
	 *            :
	 * @param createNewPageIfAlreadyExists
	 *            :
	 * @throws Throwable
	 */
	@And("^I create New Live Copy (.*) in classic view$")
	public void createNewLiveCopyPageInClassicMode(String title, String name, String liveCopyFromPath,
			String liveCopyTargetPath, String prefillFormWithProfileData, String createNewPageIfAlreadyExists)
			throws Throwable {
		if (FrameworkConstants.SITE_ADMIN_APIEXECUTION_FLAG) {
//			SiteAdminApi siteAdminAPI = new SiteAdminApi();
//			String authorUrl = world.getAuthorUrlForScenarioContext();
//			authorUrl = authorUrl.replace(FrameworkConstants.Environment.getAuthorbaseUrl(), "");
//			authorUrl = authorUrl.replace("/siteadmin#", "");
//			authorUrl = authorUrl.replace("/siteadmin.html#", "");
//			authorUrl = authorUrl.replace("/siteadmin.html", "");
//			authorUrl = authorUrl.replace("siteadmin#", "");
//			authorUrl = authorUrl.replace("siteadmin.html#", "");
//			authorUrl = authorUrl.replace("siteadmin.html", "");
//			siteAdminAPI.createLiveCopy(FrameworkConstants.Environment.getAuthorbaseUrl(), liveCopyTargetPath, title, name,
//					liveCopyFromPath);
		} else {
			actions.Wait.waitForPageReady();
			Thread.sleep(4000);
			actions.Wait.waitForObjectToPerish("DIV_Loading");
			WebElement selectedNode = getSelectedNodeFromTree();
			// boolean pageExists = isPageExist(title);
			// if(!(pageExists &&
			// createNewPageIfAlreadyExists.trim().equalsIgnoreCase("No")))
			if (!(createNewPageIfAlreadyExists.trim().equalsIgnoreCase("No"))) {
				// if(pageExists)
				deletePage(title);
				Thread.sleep(2000);
				selectedNode.click();
				actions.Wait.waitForObjectToPerish("DIV_Loading");
				Thread.sleep(4000);
				WebElement elem = world.getWebElementFactory().getElement("BTNNew");
				WebElement btnNew = new WebDriverWait(world.getWebDriver(), FrameworkConstants.LARGE_WAIT)
						.until(ExpectedConditions.elementToBeClickable(elem));
				Actions action = new Actions(world.getWebDriver());
				action.moveToElement(btnNew, btnNew.getSize().getWidth() - 5, btnNew.getSize().getHeight() / 2)
						.pause(Duration.ofSeconds(5)).click().build().perform();
				actions.Click.click("BTN_NewLiveCopy");
				actions.VerifyElementVisible.verifyElementVisible("DialogCreateLiveCopy");
				elem = null;
				elem = world.getWebElementFactory().getElement("FieldSetSelectMissingPages");
				if (!elem.getAttribute("class").contains("collapsed")) {
					actions.Click.click("BTN_Arrow_FieldSetSelectMissingPages");
				}
				Thread.sleep(4000);
				elem = null;
				elem = world.getWebElementFactory().getElement("FieldSetChooseLiveCopySource");
				if (elem.getAttribute("class").contains("collapsed")) {
					actions.Click.click("BTN_Arrow_FieldSetChooseLiveCopySource");
				}
				Thread.sleep(4000);
				actions.Enter.enter(title, "EDT_Title_CreateLiveCopy");
				actions.Enter.enter(name, "ED	T_Name_CreateLiveCopy");
				actions.Enter.enter(liveCopyFromPath, "EDT_LiveCopyFrom_CreateLiveCopy");
				Thread.sleep(3000);
				WebElement option = world.getWebElementFactory()
						.getElement(By.xpath("(//div[@*='" + liveCopyFromPath + "'])[last()]"));
				option.click();
				// click.click("EDT_Name_CreateLiveCopy");
				Thread.sleep(3000);
				actions.Click.click("BTN_Create_CreateLiveCopy");
			}
			actions.Wait.waitForPageReady();
			Thread.sleep(4000);
		}
		world.setPageNameToBeCreated(name);
	}

	public WebElement getSelectedNodeFromTree() {
		return world.getWebElementFactory()
				.getElement(By.cssSelector("div[id='cq-siteadmin-tree'] div[class*='selected']"));
		// return driver.findElement(By.cssSelector("div[id='cq-siteadmin-tree']
		// div[class*='selected']"));
	}

	public boolean isPageExist(String pageName) {
		try {
			WebElement selectedNode = getSelectedNodeFromTree();
			WebElement expectedPage = null;
			expectedPage = selectedNode.findElement(By.xpath("./../ul/li/div/a/span[text()='" + pageName + "']"));
			if (expectedPage == null)
				return false;
			else
				expectedPage.click();
			return true;
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	/**
	 * (ClassicView)This method first de-activate the published page and then delete
	 * that page
	 * 
	 * @param sPageName
	 *            : Pagename to be deleted from tree hierarchy
	 * @throws Throwable
	 */
	@And("^I delete \"([^\"]*)\" page in classic view$")
	public void deletePage(String sPageName) throws Throwable {
		WebElement selectedNode = getSelectedNodeFromTree();
		if (selectedNode == null) {
			Assert.fail("unable to get selected node in tree");
		}
		Thread.sleep(1000);
		if (isPageExist(sPageName)) {
			actions.Wait.waitForObjectToPerish("DIV_Loading");
			actions.Click.click("BTNDeactivate");
			actions.Wait.waitForObjectToPerish("DIV_Loading");
			Thread.sleep(1000);
			actions.Click.click("BTNYesPopUpDialog");
			actions.Wait.waitForObjectToPerish("DIV_Loading");
			Thread.sleep(1000);
			actions.Click.click("BTNDelete");
			actions.Wait.waitForObjectToPerish("DIV_Loading");
			Thread.sleep(1000);
			actions.Click.click("BTNYesPopUpDialog");
			actions.Wait.waitForObjectToPerish("DIV_Loading");
			Thread.sleep(2000);
			selectedNode.findElement(By.xpath(".//a")).click();
			Thread.sleep(3000);
			actions.Wait.waitForObjectToPerish("DIV_Loading");
		}
	}

	@And("^I delete \"([^\"]*)\" page$")
	public void deletePageNew(String sPageName) throws Throwable {
		if (FrameworkConstants.SITE_ADMIN_APIEXECUTION_FLAG) {
//			SiteAdminApi siteAdminAPI = new SiteAdminApi();
//			String authorUrl = world.getAuthorUrlForScenarioContext();
//			authorUrl = authorUrl.replace(FrameworkConstants.Environment.getAuthorbaseUrl(), "");
//			authorUrl = authorUrl.replace("/siteadmin#", "");
//			authorUrl = authorUrl.replace("/siteadmin.html#", "");
//			authorUrl = authorUrl.replace("/siteadmin.html", "");
//			authorUrl = authorUrl.replace("siteadmin#", "");
//			authorUrl = authorUrl.replace("siteadmin.html#", "");
//			authorUrl = authorUrl.replace("siteadmin.html", "");
//			siteAdminAPI.deletePage(FrameworkConstants.Environment.getAuthorbaseUrl(), authorUrl + "/" + sPageName);
//		} else {
//			deletePage(sPageName);
		}
	}

	/**
	 * @param sPageName
	 * @param sTemplateName
	 * @throws Throwable
	 */
	@And("^I create a page \"([^\"]*)\" using \"([^\"]*)\" template$")
	public void createPageInClassicMode(String sPageName, String sTemplateName) throws Throwable {
//		if (FrameworkConstants.SITE_ADMIN_APIEXECUTION_FLAG) {
//			SiteAdminApi siteAdminAPI = new SiteAdminApi();
//			String authorUrl = world.getAuthorUrlForScenarioContext();
//			authorUrl = authorUrl.replace(FrameworkConstants.Environment.getAuthorbaseUrl(), "");
//			authorUrl = authorUrl.replace("/siteadmin#", "");
//			authorUrl = authorUrl.replace("/siteadmin.html#", "");
//			authorUrl = authorUrl.replace("/siteadmin.html", "");
//			authorUrl = authorUrl.replace("siteadmin#", "");
//			authorUrl = authorUrl.replace("siteadmin.html#", "");
//			authorUrl = authorUrl.replace("siteadmin.html", "");
//			String templateValue = getTemplateValue(sTemplateName);
//			siteAdminAPI.createPage(FrameworkConstants.Environment.getAuthorbaseUrl(), authorUrl, sPageName, sPageName,
//					templateValue);
//			// String editorurl = FrameworkConstants.Environment.getAuthorUrl() + ""
//			UrlHandler urlHandler = new UrlHandler();
//			String editorurl = urlHandler.append(FrameworkConstants.Environment.getAuthorbaseUrl())
//					.append(FrameworkConstants.EDITOR_DOT_HTML_APPEND).append(authorUrl).append(sPageName)
//					.appendDotHtml().getUrl();
//			world.getWebDriver().navigate().to(editorurl);
//			actions.Wait.waitForPageReady();
//		} else {
//			createNewPageThroughUIOnly(sPageName, sTemplateName);
//		}
//		world.setPageNameToBeCreated(sPageName);
	}

	@And("^I create a page \"([^\"]*)\" using \"([^\"]*)\" in classic view$")
	public void createNewPageThroughUIOnly(String sPageName, String sTemplateName) throws Throwable {
		actions.Wait.waitForPageReady();
		Thread.sleep(4000);
		actions.Wait.waitForObjectToPerish("DIV_Loading");
		deletePage(sPageName);
		Thread.sleep(4000);
		actions.Click.click("BTNNew");
		actions.VerifyElementVisible.verifyElementVisible("DLG_CreatePage");
		actions.Enter.enter(sPageName, "EDT_Title_DialogCreatePage");
		actions.Enter.enter(sPageName, "EDT_Name_DialogCreatePage");
		actions.Click.click(sTemplateName);
		actions.Click.click("BTN_Create");
		actions.Wait.waitForPageReady();
		Thread.sleep(4000);
		String editorURL = world.getWebDriver().getCurrentUrl();
		editorURL = editorURL.replace("siteadmin#", "editor.html").replace("siteadmin.html#", "editor.html");
		editorURL = editorURL + "/" + sPageName + ".html";
		// String url = driver.getCurrentUrl().replace("siteadmin#", "editor.html") +
		// "/" + sPageName + ".html";
		world.getWebDriver().navigate().to(editorURL);
		actions.Wait.waitForPageReady();
		Thread.sleep(2000);
		world.setPageNameToBeCreated(sPageName);

	}

	private String getTemplateValue(String templateText) {
		String value = null;
		try {
			templateText = templateText.replace(" ", "").trim();
			Properties templateProperties = new Properties();
			File templateMappingFile = new File(FrameworkConstants.SRC_MAIN_RESOURCES + "TemplateMapping.properties");
			FileInputStream fs = new FileInputStream(templateMappingFile);
			templateProperties.load(fs);
			value = templateProperties.getProperty(templateText, null);
			if (Strings.isNullOrEmpty(value)) {
				Assert.fail(templateText + " has no mapping in TemplateMapping.properties file");
			}
		} catch (Exception e) {
			System.out.println("Exception occurred while fetching value from TemplateMapping.properties file.");
		}
		return value;
	}

}
