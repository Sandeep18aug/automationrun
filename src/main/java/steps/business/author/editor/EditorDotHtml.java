package steps.business.author.editor;

import cucumber.api.java.en.And;
import framework.shared.FrameworkConstants;
import framework.shared.UrlHandler;
import steps.business.generic.UserActions;
import steps.generic.keywords.*;
import steps.business.generic.StepsCQDialogOperations;
import unilever.pageobjects.platform.author.editor.EditorApi;
import unilever.pageobjects.platform.author.siteadmin.SiteAdminApi;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class EditorDotHtml {
	World world;
	UserActions actions;
	StepsCQDialogOperations cqDialog;
	By by_Btn_PageInfo = By.cssSelector("coral-actionbar-item[class='coral-ActionBar-item']>a[id='pageinfo-trigger']");
	By by_Btn_PublishPage = By.cssSelector("div#pageinfo-data>button[title='Publish Page' i]");
	By by_Btn_OpenProperties = By.cssSelector("div#pageinfo-data>button[title='Open Properties' i]");
	By DIV_CQDialog_Loader = By.xpath("//div[contains(@class,'coral-Wait coral-Wait--center coral-Wait')]");
	By by_DragComponentHere = By.cssSelector("div[data-path*='flexi_hero_par/*']");
	By by_EditableToolbar = By.id("EditableToolbar");
	By by_Btn_InsertNewComponent_EditableToolbar = By
			.cssSelector("#EditableToolbar>button[title='Insert component']>coral-icon");
	By by_Dialog_InsertNewComponent = By
			.cssSelector("coral-dialog.InsertComponentDialog[style*='display: block']>div.coral-Dialog-wrapper");
	By by_Input_EnterKeyword_Dialog_InsertNewComponent = By.cssSelector(
			"coral-dialog.InsertComponentDialog>div.coral-Dialog-wrapper input[placeholder='Enter Keyword']");
	By CQDialog = By.cssSelector("div[class*='cq-dialog']");
	By EDT_Tags_TabBasic_Page = By.cssSelector("*[data-cq-msm-lockable='cq:tags'] input[type='text']");
	By BTN_Preview = By.cssSelector("coral-actionbar-secondary button[data-layer='Preview']");

	public EditorDotHtml(World runner, UserActions getBasicKeywords, StepsCQDialogOperations cqDialog) {
		this.world = runner;
		this.actions = getBasicKeywords;
		this.cqDialog = cqDialog;
	}

	@And("^I publish the authored page$")
	public void publishPage() throws Throwable {
		System.out.println("publishing page");
		if (FrameworkConstants.SITE_ADMIN_APIEXECUTION_FLAG) {
//			SiteAdminApi siteAdminAPI = new SiteAdminApi();
//			String authorUrl = world.getAuthorUrlForScenarioContext();
//			authorUrl = authorUrl.replace(FrameworkConstants.Environment.getAuthorbaseUrl(), "");
//			authorUrl = authorUrl.replace("/siteadmin#", "");
//			authorUrl = authorUrl.replace("/siteadmin.html#", "");
//			authorUrl = authorUrl.replace("/siteadmin.html", "");
//			authorUrl = authorUrl.replace("siteadmin#", "");
//			authorUrl = authorUrl.replace("siteadmin.html#", "");
//			authorUrl = authorUrl.replace("siteadmin.html", "");
//			authorUrl = authorUrl.replace("/editor.html#", "");
//			authorUrl = authorUrl.replace("/editor.html", "");
//			siteAdminAPI.activatePage(FrameworkConstants.Environment.getAuthorbaseUrl(), authorUrl + "/" + world.getPageNameToBeCreated());
		} else {
			actions.Wait.waitForPageReady();
			actions.Click.click(by_Btn_PageInfo);
			Thread.sleep(2000);
			actions.Click.click(by_Btn_PublishPage);
			actions.Wait.waitForPageReady();
			Thread.sleep(5000);
			if (world.getWebDriver().getTitle().toLowerCase().contains("publish")) {
				world.getWebDriver().findElement(By.xpath("//button[child::coral-button-label[text()='Publish']]"))
						.click();
				actions.Wait.waitForPageReady();
				Thread.sleep(5000);
			}
		}
	}

	@And("^I add (.*) tags into current page$")
	public void applyTagsOnCurrentPage(String tags) throws Throwable {
		actions.Wait.waitForPageReady();
		actions.Click.click(by_Btn_PageInfo);
		actions.Wait.waitForPageReady();
		actions.Click.click(by_Btn_OpenProperties);
		actions.Wait.waitForObjectToPerish(DIV_CQDialog_Loader);
		actions.VerifyElementVisible.verifyElementVisible(CQDialog);
		cqDialog.applyTagsToTextBox(EDT_Tags_TabBasic_Page, tags);
		cqDialog.saveAndCloseProperties();
	}

	// @And("^I add (.*) tags into current page$")
	public void openCQDialogForCurrentPage() throws Throwable {
		actions.Wait.waitForPageReady();
		actions.Click.click(by_Btn_PageInfo);
		actions.Wait.waitForPageReady();
		actions.Click.click(by_Btn_OpenProperties);
		actions.Wait.waitForObjectToPerish(DIV_CQDialog_Loader);
		actions.VerifyElementVisible.verifyElementVisible(CQDialog);
		actions.Click.click("BTN_Expand_CQDialog");
	}

	private void openInsertNewComponentDialog(String dragComponentHere) throws Throwable {
		actions.Wait.waitForPageReady();
		world.getWebElementFactory().getElement(dragComponentHere).click();
		actions.VerifyElementVisible.verifyElementVisible(by_EditableToolbar);
		actions.Click.click(world.getWebElementFactory().getElement(by_Btn_InsertNewComponent_EditableToolbar));
		actions.VerifyElementVisible.verifyElementVisible(by_Dialog_InsertNewComponent);
	}

	@And("^I add \"([^\"]*)\" component in \"([^\"]*)\"$")
	public void addComponent(String componentName, String DragComponentHere) throws Throwable {
		addComponent(componentName, "Unilever Foundation", DragComponentHere);
	}

	
	@And("^I add \"([^\"]*)\" component of category \"([^\"]*)\" in \"([^\"]*)\"$")
	public void addComponent(String componentName, String category, String DragComponentHere) throws Throwable {
		actions.Wait.waitForPageReady();
		openInsertNewComponentDialog(DragComponentHere);
		Thread.sleep(2000);
		WebElement componentToSelect = null;
		if (componentName.toLowerCase().contains(";value=")) {
			String componentText = componentName.split(";")[0];
			String componentValue = componentName.split(";")[1].replace("value=", "");
			actions.Enter.enter(componentText, world.getWebElementFactory().getElement(by_Input_EnterKeyword_Dialog_InsertNewComponent));
			actions.Wait.wait("1");
			componentToSelect = world.getWebElementFactory().getElement(By.xpath(
					".//coral-selectlist-group[@label='"+category+"']/coral-selectlist-item[contains(@value,'"
							+ componentValue + "')]"));
		} else {
			actions.Enter.enter(componentName, world.getWebElementFactory().getElement(by_Input_EnterKeyword_Dialog_InsertNewComponent));
			actions.Wait.wait("1");
			componentToSelect = world.getWebElementFactory().getElement(
					By.xpath(".//coral-selectlist-group[@label='"+category+"']/coral-selectlist-item[text()=\""
							+ componentName + "\"]"));
		}
		if (componentToSelect == null) {
			Assert.fail(componentName + " is not present in the list.");
			return;
		}
		componentToSelect.click();
	}

	
	public void addComponentThroughUI(String componentName, String DragComponentHere) throws Throwable {
		actions.Wait.waitForPageReady();
		openInsertNewComponentDialog(DragComponentHere);
		actions.Enter.enter(componentName, world.getWebElementFactory().getElement(by_Input_EnterKeyword_Dialog_InsertNewComponent));
		Thread.sleep(2000);
		WebElement componentToSelect = null;
		if (componentName.toLowerCase().startsWith("value=")) {
			String value = componentName.replace("value=", "");
			componentToSelect = world.getWebElementFactory().getElement(By.xpath(
					".//coral-selectlist-group[@label='Unilever Foundation']/coral-selectlist-item[contains(@value,'"
							+ value + "')]"));
		} else {
			componentToSelect = world.getWebElementFactory().getElement(
					By.xpath(".//coral-selectlist-group[@label='Unilever Foundation']/coral-selectlist-item[text()=\""
							+ componentName + "\"]"));
		}
		if (componentToSelect == null) {
			Assert.fail(componentName + " is not present in the list.");
			return;
		}
		componentToSelect.click();
	}

	
	@And("^I add \"([^\"]*)\" Unilever Email Campaign component in \"([^\"]*)\"$")
	public void addEmailComponent(String componentName, String DragComponentHere) throws Throwable {
		actions.Wait.waitForPageReady();
		openInsertNewComponentDialog(DragComponentHere);
		actions.Enter.enter(componentName, world.getWebElementFactory().getElement(by_Input_EnterKeyword_Dialog_InsertNewComponent));
		Thread.sleep(2000);
		WebElement componentToSelect = world.getWebElementFactory().getElement(
				By.xpath(".//coral-selectlist-group[@label='Unilever Email Campaign']/coral-selectlist-item[text()='"
						+ componentName + "']"));
		if (componentToSelect == null) {
			Assert.fail(componentName + " is not present in the list.");
			return;
		}
		componentToSelect.click();
	}

	@And("^I add \"([^\"]*)\" Unilever Foundation Form component in \"([^\"]*)\"$")
	public void addUnileverfoundationForm(String componentName, String DragComponentHere) throws Throwable {
		actions.Wait.waitForPageReady();
		openInsertNewComponentDialog(DragComponentHere);
		actions.Enter.enter(componentName, world.getWebElementFactory().getElement(by_Input_EnterKeyword_Dialog_InsertNewComponent));
		Thread.sleep(2000);
		WebElement componentToSelect = world.getWebElementFactory().getElement(By
				.xpath(".//coral-selectlist-group[@label='Unilever Foundation - Forms']/coral-selectlist-item[text()='"
						+ componentName + "']"));
		if (componentToSelect == null) {
			Assert.fail(componentName + " is not present in the list.");
			return;
		}
		componentToSelect.click();
	}

	@And("^I add Number of Steps as (.*) in page properties for Samples")
	public void addStepsinSamples(String step) throws Throwable {
		actions.Wait.waitForPageReady();
		actions.Wait.wait("5");
		actions.Click.click(by_Btn_PageInfo);
		actions.Wait.waitForPageReady();
		actions.Click.click(by_Btn_OpenProperties);
		actions.Wait.waitForObjectToPerish(DIV_CQDialog_Loader);
		actions.VerifyElementVisible.verifyElementVisible(CQDialog);
		actions.Click.click(By.xpath("//a[text()='Cof Template Properties']"));
		actions.Enter.enter(step, By.name("./noofSteps"));
		cqDialog.saveAndCloseProperties();
		Thread.sleep(4000);
	}
	
	
	@And("^I (Hide|Unhide) Site Map in page properties")
	public void hideSiteMapincampaigns(String hide) throws Throwable {
		actions.Wait.waitForPageReady();
		actions.Click.click(by_Btn_PageInfo);
		actions.Wait.waitForPageReady();
		actions.Click.click(by_Btn_OpenProperties);
		actions.Wait.waitForObjectToPerish(DIV_CQDialog_Loader);
		actions.VerifyElementVisible.verifyElementVisible(CQDialog);
		// actions.Click.click("BTN_Expand_CQDialog");
		actions.Click.click(By.xpath("//a[text()='Advance SEO']"));
		By cancelInheritanceForHideInSiteMap = By.xpath(
				"(//section[@class='coral-TabPanel-pane is-active']//div[@class='coral-Form-fieldwrapper coral-Form-fieldwrapper--singleline'])[3]//i[@class='coral-Icon coral-Icon--sizeS coral-Icon--link']");
		if (world.getWebElementFactory().isElementVisibleOnPage(cancelInheritanceForHideInSiteMap)) {
			actions.Click.click(cancelInheritanceForHideInSiteMap);
			actions.Click.click(By.xpath("//div[@class='coral-Dialog-wrapper']//coral-button-label[text()='Yes']"));
		}
		if (hide.toLowerCase().equalsIgnoreCase("hide"))
			actions.Check.check(By.name("./hideInSiteMap"));
		else
			actions.Check.unCheck(By.name("./hideInSiteMap"));
		cqDialog.saveAndCloseProperties();
		actions.Click.click(by_Btn_PageInfo);
		Thread.sleep(2000);
		actions.Click.click(by_Btn_PublishPage);
	}

	public void changeToPreviewMode() throws Throwable {
		actions.Wait.waitForPageReady();
		actions.Click.click(BTN_Preview);
	}

	public void waitUntillEditable() throws Throwable {
		try {
			actions.Wait.waitForPageReady();
			WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.LARGE_WAIT);
			By byProperty = By.cssSelector("#FullScreenMask[style='display: none;']");
			wait.until(ExpectedConditions.visibilityOfElementLocated(byProperty));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@And("^I insert \"([^\"]*)\" component$")
	public void addComponentInPageUsingApi(String componentName) throws Throwable {
		EditorApi editorApi = new EditorApi();
		String pagePath = world.getAuthorUrlForScenarioContext() + "/" + world.getPageNameToBeCreated();
		pagePath = pagePath.replace("/siteadmin#", "").replace("/siteadmin.html#", "").replace("/siteadmin.html", "");
		pagePath = pagePath.replace("siteadmin#", "").replace("siteadmin.html#", "").replace("siteadmin.html", "");
		editorApi.insertComponentInPage(componentName, pagePath);
		UrlHandler urlHandler = new UrlHandler();
		String editorurl = urlHandler.append(world.getAuthorUrlForScenarioContext())
				.append(world.getPageNameToBeCreated()).appendDotHtml().getUrl();
		editorurl = urlHandler.convertToEditorMode(editorurl);
		world.getWebDriver().navigate().to(editorurl);
		actions.Wait.waitForPageReady();
	}
}
