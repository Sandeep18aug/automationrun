package steps.business.generic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.business.author.editor.EditorDotHtml;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.brandspecific.CWLoginAndRegistrationPage;
import unilever.pageobjects.platform.publish.common.LoginPage;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;

public class StepsLogin {
	

	UserActions actions;
	World capabilities;
	LoginPage loginPage;
	

	public StepsLogin(World capabilities, UserActions getBasicKeywords) {
		this.actions = getBasicKeywords;
		this.capabilities = capabilities;
		this.loginPage = new LoginPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@Then("^I expect Login Page to Open$")
	public void iExpectLoginPageToOpen() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		loginPage.acceptCookiesIfPresent();
		actions.VerifyElementVisible.verifyElementVisible("loginpage.email");

	}

	@When("^I login with (.*) and (.*)$")
	public void loginWithUserIdAndPassword(String userID, String password) throws Throwable {
		loginPage.loginAs(userID, password);
	}

	@When("^I login through header with (.*) and (.*)$")
	public void loginThroughHeaderWithUserIdAndPassword(String userID, String password) throws Throwable {
		loginPage.loginAs(userID, password);
	}
	
	@Then("^(?:i|I) submit valid email \"([^\"]*)\" for forget password$")
	public void enterPasswordForForgetPassword(String email) throws Throwable {
		loginPage.waitForPageReady();
		loginPage.getEmailTextBox().sendKeys(email);
		loginPage.getGetNewPasswordButton().click();
	}

	@Then("^(?:i|I) should be able to see thankyou message \"([^\"]*)\"$")
	public void verifyThankyouPage(String thankYouMessageText) throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement thankyou = loginPage.getThankyouMessage();
		actions.Wait.waitForObjectToVisible(thankyou);
		Assert.assertTrue(thankyou.getText().contains(thankYouMessageText),thankYouMessageText + " thank you message is missing");
	}
	


	
}
