package steps.business.generic.listingpages;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.alp.ArticleListingPage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;

public class StepsArticleListingPage {

	UserActions actions;
	World capabilities;
	ArticleListingPage alpPage;

	public StepsArticleListingPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.alpPage = new ArticleListingPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}
	
	@Then("^(?:I|i) verify that Article Listing component description is appearing$")
	public void i_verify_that_pagelistingv2_component_description_is_rendering() throws ObjectNotFoundInORException {
	    // Write code here that turns the phrase above into concrete actions
		WebElement articlelistingDescription = this.alpPage.getArticlelistingdescription();
	   Assert.assertNotNull(articlelistingDescription, "Article listing description is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
	   this.capabilities.reportStepInfo("Article listing description is present on page");
	}

	@And("^(?:I|i)t should take me to (?:an|the) article listing page$")
	public void verifyPageListingPageLoads() {
		actions.Wait.waitForPageReady();
		// String submenu = (String)
		// capabilities.getScenarioContext().getScenarioData("submenutext");
		// this.capabilities.reportStepInfo("in previous step, following submenu was
		// clicked - " + submenu);
		// String href = (String)
		// capabilities.getScenarioContext().getScenarioData("submenuhref");
		String currentUrl = capabilities.getWebDriver().getCurrentUrl();
		// Assert.assertTrue(currentUrl.equalsIgnoreCase(href),
		// "after clicking on " + submenu + " submenu, page hasn't navigated to correct
		// url. " + href + System.lineSeparator() + " Actual url is " + currentUrl);
		this.capabilities.reportStepInfo("page is successfully navigated to url - " + currentUrl);
		Assert.assertTrue(this.alpPage.isComponentPresent(ComponentsDataRole.PAGE_LISTING_V2),
				"pageListingV2 component is not present on the page. hence it is not article listing page.");
	}
	
	
	@And("^(?:I|i) click on article tag of article listing page$")
	public void clickarticle_tagging() throws ObjectNotFoundInORException {
		actions.Wait.waitForPageReady();
		WebElement articleTag = this.alpPage.getArticleTag();
 		Assert.assertNotNull(articleTag,"articleTag is not appearing on ALP");
 		this.capabilities.reportStepInfo("articleTag is present on page");
 		//this.capabilities.getScenarioContext().addScenarioData("articleTaghref",href);
 		articleTag.click();
		
	}
	

	@And("^(?:I|i) should see article list items on article listing page$")
	public void verifyArticleItemsPresent() {
		actions.Wait.waitForPageReady();
		int productListV2ItemCount = this.alpPage.getPageListingV2ListItems().size();
		Assert.assertTrue(productListV2ItemCount > 0, "there are no article listing v2 items on page with url "
				+ capabilities.getWebDriver().getCurrentUrl());
		this.capabilities
				.reportStepInfo("by default, total number of product list items found are " + productListV2ItemCount);
	}
	

	@And("^(?:I|i) apply any random filter on article listing page$")
	public void applyRandomFilter() throws NumberFormatException, InterruptedException, ObjectNotFoundInORException {
		actions.Wait.waitForPageReady();
		String productsCount = this.alpPage.getPageListingFilterCtaCount();
		this.capabilities.reportStepInfo("total products count is " + productsCount);
		this.capabilities.getScenarioContext().addScenarioData("filterPageCount", productsCount);
		int totalFilterOptions = this.alpPage.getPageListingV2FilterOptionsCount();
		int random = RandomUtil.getRandomNumber(1, totalFilterOptions - 1);
		this.alpPage.selectFilterByIndex(random);
		this.capabilities.reportStepInfo("successfully selected filter with index " + random);
		this.actions.Wait.wait("5");
	}

	@And("^(?:I|i) should be able to see that article page filter is successfully applied$")
	public void verifyFilterApplied() {
		actions.Wait.waitForPageReady();
		String productsCount = this.alpPage.getPageListingFilterCtaCount();
		String previousCount = (String) this.capabilities.getScenarioContext().getScenarioData("filterPageCount");
		Assert.assertNotEquals(productsCount, previousCount, "filter is not applied");
		this.capabilities.reportStepInfo("filter is applied. before filter there were " + previousCount
				+ " products and after filter there are " + productsCount + " products");
	}

	@And("^(?:I|i) apply any random filter on article listing page for brand \"([^\"]*)\"$")
	public void applyRandomFilter(String brandName) throws Throwable {
		actions.Wait.waitForPageReady();
		String productsCount = this.alpPage.getPageListingFilterCtaCount();
		this.capabilities.reportStepInfo("total products count is " + productsCount);
		this.capabilities.getScenarioContext().addScenarioData("filterPageCount", productsCount);
		if (brandName.trim().equalsIgnoreCase("sunsilk") || brandName.trim().equalsIgnoreCase("pepsodent")) {
			int filterCount = this.alpPage.getPageListingV2FilterUnorderedListOptionsCount();
			int random = RandomUtil.getRandomNumber(1, filterCount - 1);
			this.alpPage.selectFilterULByIndex(random);
			this.alpPage.waitForLoaderToDisappear();
			this.capabilities.reportStepInfo("successfully selected filter with index " + random);
			this.actions.Wait.wait("3");
		} else {
			Assert.fail(brandName + " is not handelled yet");
		}
	}
	
//	@When("^(?:i|I) click on any of the items of article listing page$")
//	public void i_click_on_any_of_the_items_of_article_listing_page() {
//		actions.Wait.waitForPageReady();
//		String href = "";
//		List<WebElement> articleTagList = this.alpPage.getArticleListingListItems();
//		if(articleTagList.size() > 0) {
//			int randomItem = new Random().nextInt(articleTagList.size());
//			WebElement element = this.alpPage.getArticleListItemLink(articleTagList.get(randomItem));
//		    href = element.getAttribute("href");
//		    this.capabilities.getScenarioContext().addScenarioData("selectedTagHref", href);
//		    element.click();
//		}else {
//			Assert.fail("search list items are not present");
//		}
//		this.capabilities.reportStepInfo("clicking on search listing item with url - " + href);
//	}
	
	@And("^(?:I|i) click on any of the items of article listing page$")
	public void clickOnRandomPageListingV2Item() {
		actions.Wait.waitForPageReady();
		List<WebElement> artcleItems = this.alpPage.getPageListingV2ListItems();
		int randomNumber = new Random().nextInt(artcleItems.size());
		WebElement listItem = artcleItems.get(randomNumber);
		String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("selectedpagelistingv2itemurl", href);
		listItem.click();
	}

	
	@When("^(?:i|I) verify that Ordered Lists are present on article listing page$")
	public void i_verify_orderedlist_on_article_listing_page() {
		actions.Wait.waitForPageReady();		
		List<WebElement> orderedList = this.alpPage.getArticleListingOrderedList();
		if(orderedList.size() > 0) {
			int randomItem = new Random().nextInt(orderedList.size());
			WebElement element = this.alpPage.getArticleListingOrderedListLink(orderedList.get(randomItem));
		}else {
			Assert.fail("ordered list items are not present");
		}
		this.capabilities.reportStepInfo("ordered list items are present");
	}	
}
