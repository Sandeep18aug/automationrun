package steps.business.generic.listingpages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.searchlistingpage.SearchListingPage;

public class StepsSearchListingPage {

	UserActions actions;
	World capabilities;
	SearchListingPage slpPage;
	PublishedPageCommon publishedPage;

	public StepsSearchListingPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.slpPage = new SearchListingPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@And("^(?:I|i)t should take me to (?:a|the) search listing page$")
	public void verifySearchListingPageLoads() {
		actions.Wait.waitForPageReady();
		Assert.assertTrue(
				capabilities.getWebDriver()
						.findElements(By.cssSelector("div[data-role='" + ComponentsDataRole.SEARCH_LISTING_V2
								+ "'],div[data-role='" + ComponentsDataRole.SEARCH_LISTING + "']"))
						.size() > 0,
				"searchListingV2 component is not present on the page. hence it is not search listing page.");
		this.capabilities.reportStepInfo("successfully navigated to search listing page");
	}

	@Then("^(?:i|I) should be able to see search listing (tabs|filters)$")
	public void i_should_be_able_to_see_search_listing_tabs(String tabOrFilter) {
		actions.Wait.waitForPageReady();
		if(tabOrFilter.trim().equalsIgnoreCase("tabs")) {
			Assert.assertTrue(this.slpPage.getSearchListingTabItems().size() > 0,
					"search listing tabs are not present on page");
			this.capabilities.reportStepInfo("search listing tabs are present on search listing page");
		} else if(tabOrFilter.trim().equalsIgnoreCase("filters")) {
			Assert.assertTrue(this.slpPage.getSearchListingFilters() != null,
					"search listing filters are not present on page");
			this.capabilities.reportStepInfo("search listing filters are present on search listing page");
		}
	}
	
	@Then("^(?:i|I) should be able to see (\\d+) search listing (tabs|filters)$")
	public void verifySearchListingFiltersTabsCount(int filtersTabsExpectedCount, String tabOrFilter) {
		actions.Wait.waitForPageReady();
		if(tabOrFilter.trim().equalsIgnoreCase("tabs")) {
			int searchListingTabsActualCount = slpPage.getSearchListingTabItems().size();
			Assert.assertTrue(searchListingTabsActualCount == filtersTabsExpectedCount, "Search listing tabs "
					+ "expected count is as per the actual. Expected count = " + filtersTabsExpectedCount + 
					" and actual count = " + searchListingTabsActualCount);
			this.capabilities.reportStepInfo("Search listing tabs' expected count is as per the actual on "
					+ "search listing page.");
		} else if(tabOrFilter.trim().equalsIgnoreCase("filters")) {
			int searchListingFiltersActualCount = slpPage.getSearchListingFilters().size();
			Assert.assertTrue(searchListingFiltersActualCount == filtersTabsExpectedCount, "Search listing "
					+ " filters expected count is as per the actual. Expected count = " + 
					filtersTabsExpectedCount + " and actual count = " + searchListingFiltersActualCount);
			this.capabilities.reportStepInfo("Search listing filters' expected count is as per the actual on "
					+ "search listing page.");
		}
	}

	@Then("^(?:i|I) should be able to see search list items$")
	public void i_should_be_able_to_see_search_list_items() {
		actions.Wait.waitForPageReady();
		Assert.assertTrue(this.slpPage.getSearchListingItems().size() > 0,
				"search listing items are not present on page");
		this.capabilities.reportStepInfo("search listing items are present on search listing page");
	}

	@Then("^(?:i|I) expect search list item name is present$")
	public void i_expect_search_list_item_name_is_present() throws NumberFormatException, InterruptedException {
		actions.Wait.waitForPageReady();
		this.actions.Wait.wait("4");
		List<WebElement> searchListItems = this.slpPage.getSearchListingItems();
		String searchListItemName = "";
		if (searchListItems.size() > 0) {
			WebElement searchListItem = searchListItems.get(0);
			searchListItemName = this.slpPage.getSearchListItemName(searchListItem);
			Assert.assertTrue(Strings.isNotNullAndNotEmpty(searchListItemName),"Search list item name/title is "
					+ "not present for first item");
		} else {
			Assert.fail("search list items are not present");
		}
		this.capabilities.reportStepInfo("search listing item name is present - '" + searchListItemName + "'");
	}

	@Then("^(?:i|I) expect search list item description is present$")
	public void i_expect_search_list_item_description_is_present() {
		actions.Wait.waitForPageReady();
		List<WebElement> searchListItems = this.slpPage.getSearchListingItems();
		String searchLisItemDesc = "";
		if (searchListItems.size() > 0) {
			WebElement searchListItem = searchListItems.get(0);
			searchLisItemDesc = this.slpPage.getSearchListItemDescription(searchListItem);
			Assert.assertTrue(Strings.isNotNullAndNotEmpty(searchLisItemDesc),"Search list item description is "
					+ "not present for first item");
		} else {
			Assert.fail("search list items are not present");
		}
		this.capabilities.reportStepInfo("search listing item description is present - '" + searchLisItemDesc + "'");
	}

	@Then("^(?:i|I) expect search list item image is present$")
	public void i_expect_search_list_item_image_is_present() {
		actions.Wait.waitForPageReady();
		List<WebElement> searchListItems = this.slpPage.getSearchListingItems();
		if (searchListItems.size() > 0) {
			WebElement searchListItem = searchListItems.get(0);
			Assert.assertTrue(this.slpPage.getSearchListItemImage(searchListItem) != null,"Search list item image "
					+ "is not present for first item");
		} else {
			Assert.fail("search list items are not present");
		}
		this.capabilities.reportStepInfo("search listing item image is present");
	}
	

	@When("^(?:i|I) click on any of the items of search listing page$")
	public void i_click_on_any_of_the_items_of_search_listing_page() {
		actions.Wait.waitForPageReady();
		String href = "";
		List<WebElement> searchListItems = this.slpPage.getSearchListingItems();
		if(searchListItems.size() > 0) {
			int randomItem = new Random().nextInt(searchListItems.size());
			WebElement element = this.slpPage.getSearchListItemLink(searchListItems.get(randomItem));
		    href = element.getAttribute("href");
		    this.capabilities.getScenarioContext().addScenarioData("selectedItemHref", href);
		    element.click();
		}else {
			Assert.fail("search list items are not present");
		}
		this.capabilities.reportStepInfo("clicking on search listing item with url - " + href);
	}
	
	@Then("^i verify that search listing component has cta link/button present on the page$")
	public void i_verify_that_searchlisting_component_cta_is_appearing() {
		Assert.assertNotNull(this.slpPage.getSearchListingCta(),
				"search listing cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("search listing cta is present on page");
	}
	
	@Then("^i click on the cta of the search listing component$")
	public void clicksearchlistingCta() {
		actions.Wait.waitForPageReady();
		WebElement cta = this.slpPage.getSearchListingCta();
		Assert.assertNotNull(cta,
				"searchlisting cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("search listing cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("searchlistingctahref", cta.getAttribute("href"));
		cta.click();
	}
	
	@When("^(?:i|I) click on first item of search listing page$")
	public void i_click_on_first_item_of_search_listing_page() {
		actions.Wait.waitForPageReady();
		String href = "";
		List<WebElement> searchListItems = this.slpPage.getSearchListingItems();
		if(searchListItems.size() > 0) {
			WebElement element = this.slpPage.getSearchListItemLink(searchListItems.get(0));
		    href = element.getAttribute("href");
		    this.capabilities.getScenarioContext().addScenarioData("selectedItemHref", href);
		    element.click();
		}else {
			Assert.fail("search list items are not present");
		}
		this.capabilities.reportStepInfo("clicking on search listing item with url - " + href);
	}

	@Then("^(?:i|I)t should take me to corresponding page$")
	public void it_should_take_me_to_corresponding_page() {
		actions.Wait.waitForPageReady();
		String selectedItemHref = (String) capabilities.getScenarioContext().getScenarioData("selectedItemHref");
		selectedItemHref = selectedItemHref.replace("http:", "https:");
		selectedItemHref = selectedItemHref.replace("-&-", "-");
		String currentUrl = capabilities.getWebDriver().getCurrentUrl().replace("-&-", "-");
		currentUrl = currentUrl.replace("http:", "https:");
		Assert.assertTrue(currentUrl.equalsIgnoreCase(selectedItemHref),
				"page hasn't navigated to expected url => " + selectedItemHref + ". instead it was navigated to "
						+ "=> " + currentUrl);
		this.capabilities.reportStepInfo("page is successfully navigated to url - " + selectedItemHref);
	}		
		
	@When("^(?:i|I) select \"([^\"]*)\" search listing (tab|filter)$")
	public void i_select_search_listing_tab(String tabText, String filterOrTab) throws Throwable {
		actions.Wait.waitForPageReady();
		if(filterOrTab.trim().equalsIgnoreCase("tab"))
			this.slpPage.selectSearchListingTabItem(tabText);
		else if(filterOrTab.trim().equalsIgnoreCase("filter"))
			this.slpPage.selectSearchListingFilter(tabText);
		this.slpPage.waitForLoaderToDisappear();
	}

	
	@When("^(?:i|I) select \"([^\"]*)\" search listing filter for brand \"([^\"]*)\"")
	public void i_select_search_listing_filter_for_brand(String filterText, String brand) {
	    if(brand.trim().equalsIgnoreCase("ponds")) {
	    	actions.Wait.waitForPageReady();
	    	this.slpPage.selectSearchListingFilterUlItem(filterText);
	    	this.capabilities.reportStepInfo("successfully selected filter with text " + filterText);
	  }
	}
	    
	    
   @Then("^I verify show more functionality is working on Search Listing Page$")
   public void verify_pagination_searchlistinglisting() throws Throwable {
			actions.Wait.waitForPageReady();
			Assert.assertNotNull(this.slpPage.getShowMoreButton(),
					"pagination cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
			this.capabilities.reportStepInfo("Load more button is present on page");
			
			int initialListCount = this.slpPage.getSearchListingItems().size();
			this.capabilities.reportStepInfo("Items count before clicking load more is " + initialListCount);
			this.slpPage.getShowMoreButton().click();
			this.actions.Wait.wait("2");
			int LaterListCount = this.slpPage.getSearchListingItems().size();
			this.capabilities.reportStepInfo("Items count after clicking load more is " + LaterListCount);
			this.actions.Wait.wait("2");	
			Assert.assertTrue(initialListCount <= LaterListCount, "Load more functionality is not working properly");
			this.actions.Scroll.scrollPublishedPageToBottom();
			this.actions.Wait.wait("2");
			int finalListCount = this.slpPage.getSearchListingItems().size();
			//List<WebElement> finalList = this.slpPage.getSearchListingPaginationItem() ;
			this.capabilities.reportStepInfo("Items count after scrolling down is " + finalListCount);
			Assert.assertTrue(LaterListCount <= finalListCount, "Load more functionality is not working properly");
		}
   
   @Then("^i click on reset button on search listing page$")
	public void clickrestbuttonsearchlisting() {
	    this.actions.Wait.waitForPageReady();
		WebElement resetbutton = this.slpPage.getSearchListingResetButton();
		System.out.println("resetbutton found: " + resetbutton.toString());
		Assert.assertNotNull(resetbutton,"searchlisting rest button is not appearing" );
		this.capabilities.reportStepInfo("search listing rest button is present on page");
		resetbutton.click();
	}
  
  @Then("^i should not be able to see search result$")
	public void noresult_searchlisting() {
		WebElement noresult = this.slpPage.getSearchListingPlaceholder();
		Assert.assertNotNull(noresult,"searchlisting result is not appearing" );
		this.capabilities.reportStepInfo("search listing result is present on page");
		
	}
  
  @Then("^(?:i|I) should see close button overlay on search listing page$")
	public void i_should_see_close_button_overlay() {
	    // Write code here that turns the phrase above into concrete actions
	    Assert.assertNotNull(this.slpPage.getSearchListingCloseButton(),"close button overlay is not present on page");
	    this.capabilities.reportStepInfo("close button overlay is present in the page");
	    
	}
  
  @And("^(?:i|I) should see sorry message is diaplyed as \"([^\"]*)\"$")
	public void verifysorrymessage(String message) throws Throwable {
		this.actions.Wait.waitForPageReady();
		String actualMessage = this.slpPage.getSearchListingIncorrectSearchMsg();
		Assert.assertEquals(message, actualMessage, "Actual - " + actualMessage + " and Expected - " + message);
  }
	@Then("^(?:I|i) verify sorry message is displayed as search result$")
	public void searchSorryTextFromGlobalSearch() throws Throwable {
		actions.Wait.waitForPageReady();
		Assert.assertTrue(this.publishedPage.getGlobalNavigation().getSearchInvalidInputMessage() != null,
				"Sorry message is not getting displayed for invalid text searched");

	}
   
   @And("^(?:I|i) verify sorry message displayed is \"([^\"]*)\"$")
	public void searchSorryText(String sorryMessage) throws Throwable {
		actions.Wait.waitForPageReady();
		String sorryMessagePage = this.publishedPage.getGlobalNavigation().getSearchInvalidInputMessage().getText().trim();
		Assert.assertTrue(sorryMessagePage.equalsIgnoreCase(sorryMessage),
				"Sorry message doesn't matches");

	}
	
 
	
//	@And("^(?:I|i) should see search listing items on article listing page$")
//	public void verifySearchListV2ItemsPresent() {
//		actions.Wait.waitForPageReady();
//		int productListV2ItemCount = this.alpPage.getPageListingV2ListItems().size();
//		Assert.assertTrue(productListV2ItemCount > 0,
//				"there are no article listing v2 items on page with url "
//						+ capabilities.getWebDriver().getCurrentUrl());
//		this.capabilities.reportStepInfo("by default, total number of product list items found are " + productListV2ItemCount);
//	}
//	
//	@And("^i click on any of the items of article listing page$")
//	public void clickOnRandomPageListingV2Item() {
//		actions.Wait.waitForPageReady();
//		List<WebElement> productListingV2Items = this.alpPage.getPageListingV2ListItems();
//		int randomNumber = new Random().nextInt(productListingV2Items.size());
//		WebElement listItem = productListingV2Items.get(randomNumber);
//		String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
//		capabilities.getScenarioContext().addScenarioData("selectedpagelistingv2itemurl", href);
//		listItem.click();
//	}

}
