package steps.business.generic.listingpages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.alp.ArticleListingPage;
import unilever.pageobjects.platform.publish.common.ComponentsDataRole;
import unilever.pageobjects.platform.publish.components.definitions.ProductListingV2;
import unilever.pageobjects.platform.publish.components.definitions.RelatedArticle;
import unilever.pageobjects.platform.publish.plp.ProductListingPage;;

public class StepsProductListingPage {

	UserActions actions;
	World capabilities;
	ProductListingPage plpPage;
	ProductListingV2 plV2Page;
	ArticleListingPage alpPage;
	
	
	public StepsProductListingPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.plpPage = new ProductListingPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.plV2Page = new ProductListingV2(capabilities.getWebDriver(),capabilities.getBrandName());
		this.alpPage = new ArticleListingPage(capabilities.getWebDriver(),capabilities.getBrandName());
		
	}
		
	@And("^(?:I|i)t should take me to (?:a|the) product listing page$")
	 public void verifyProductListingPageLoads() {
        actions.Wait.waitForPageReady();
//        String currentUrl = capabilities.getWebDriver().getCurrentUrl();
//        String submenu = (String) capabilities.getScenarioContext().getScenarioData("submenutext");
//        if(Strings.isNotNullAndNotEmpty(submenu)) {
//               System.out.println("in previous step, following submenu was clicked - " + submenu);
//               String href = (String) capabilities.getScenarioContext().getScenarioData("submenuhref");
//               Assert.assertTrue(currentUrl.equalsIgnoreCase(href),
//                            "after clicking on " + submenu + " submenu, page hasn't navigated to correct url. expected - " + href + System.lineSeparator() + " actual - " + currentUrl);        
//        }
//        this.capabilities.reportStepInfo("page is successfully navigated to url - " + currentUrl);
        Assert.assertTrue(
                     capabilities.getWebDriver().findElements(By.cssSelector("div[data-role='"+ComponentsDataRole.PRODUCT_LISTING_V2+"'],div[data-role='"+ComponentsDataRole.PRODUCT_LISTING+"'],div[class='"+ComponentsDataRole.PRODUCT_LISTING_STARTERKIT+"']"))
                                   .size() > 0,
                     "pageListingV2 component is not present on the page. hence it is not product listing page.");
        this.capabilities.reportStepInfo("it is a product listing page");
 }





	@And("^(?:I|i) should see product list items on product listing page$")
	public void verifyProductListV2ItemsPresent() {
		actions.Wait.waitForPageReady();
		List<WebElement> productListingV2Items = this.plpPage.getProductListingV2ListItems();
		Assert.assertNotNull(productListingV2Items,"products are not appearing on page");
		int productListV2ItemCount = productListingV2Items.size();
		Assert.assertTrue(productListV2ItemCount > 0,
				"there are no items present on product listing page with url "
						+ capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("by default, total number of product list items found are " + productListV2ItemCount);
	}
	
	
	@And("^(?:I|i) should see product list has (\\d+) items on product listing page$")
	public void verifyProductListV2ItemsCount(int count) {
		actions.Wait.waitForPageReady();
		List<WebElement> productListingV2Items = this.plpPage.getProductListingV2ListItems();
		Assert.assertNotNull(productListingV2Items,"products are not appearing on page");
		int productListV2ItemCount = productListingV2Items.size();
		Assert.assertTrue(productListV2ItemCount == count,
				"Product count is not matching up with provided count " +count
						+ capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("by default, total number of product list items found are " + productListV2ItemCount);
	}
	
	@And("^(?:I|i) click on any of the items of product listing page$")
	public void clickOnRandomProductListingV2Item() throws Throwable {
		actions.Wait.waitForPageReady();
		List<WebElement> productListingV2Items = this.plpPage.getProductListingV2ListItems();
		Assert.assertNotNull(productListingV2Items,"products are not appearing on page");
		//int randomNumber = new Random().nextInt(productListingV2Items.size());
		int randomNumber = RandomUtil.getRandomNumber(1, productListingV2Items.size()-1);
		WebElement listItem = productListingV2Items.get(randomNumber-1);
		String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("selectedproductlistingv2itemurl", href);
		actions.Scroll.scrollToTopOfElement(listItem.findElement(By.cssSelector("*[href]")));
		actions.Click.clickByActionsClass(listItem.findElement(By.cssSelector("*[href]")));
		this.capabilities.reportStepInfo("clicked on product listing item with href " + href);
	}
	
	@And("^(?:I|i) click on product tag of article listing page$")
	public void click_product_tagging() {
		actions.Wait.waitForPageReady();
		WebElement productTag = this.plpPage.getProductTag();
 		Assert.assertNotNull(productTag,"productTag is not appearing for ALP");
 		this.capabilities.reportStepInfo("productTag is present on page");
 		productTag.click();
		
	}

	@And("^(?:I|i) apply any random filter on product listing page$")
	public void applyRandomFilter() throws Throwable {
		actions.Wait.waitForPageReady();
		String productsCount = this.plpPage.getProductListingFilterProductsCount();
		this.capabilities.getScenarioContext().addScenarioData("filterProductCount", productsCount);
		int totalFilterOptions = this.plpPage.getProductListingV2FilterOptionsCount();
		int random = RandomUtil.getRandomNumber(1, totalFilterOptions -1);
		this.plpPage.selectFilterByIndex(random);
		this.plpPage.waitForLoaderToDisappear();
		this.capabilities.reportStepInfo("successfully selected filter with index " + random);	
		this.actions.Wait.wait("5");
	}
	
	@And("^(?:I|i) apply \"([^\"]*)\" and \"([^\"]*)\" filter on product listing page$")
	public void applyMultipleRandomFilter(String firstFilter, String secondFilter) throws Throwable {
		actions.Wait.waitForPageReady();
		String productsCount = this.plpPage.getProductListingFilterProductsCount();
		this.capabilities.getScenarioContext().addScenarioData("filterProductCount", productsCount);
		WebElement firstFilterWebElement = this.plpPage.getFirstFilterListBox();
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(firstFilter, firstFilterWebElement);
		WebElement secondFilterWebElement = this.plpPage.getSecondFilterListBox();
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(secondFilter, secondFilterWebElement);
		this.plpPage.waitForLoaderToDisappear();
		this.capabilities.reportStepInfo("successfully selected filters with text " + firstFilter + " and " + secondFilter);	
		this.actions.Wait.wait("5");
	}
	
	@And("^(?:I|i) apply filter by index (\\d+) on product listing page$")
	public void applyFilterByIndex(int index) throws Throwable {
		actions.Wait.waitForPageReady();
		String productsCount = this.plpPage.getProductListingFilterProductsCount();
		this.capabilities.getScenarioContext().addScenarioData("filterProductCount", productsCount);
//		int totalFilterOptions = this.plpPage.getProductListingV2FilterOptionsCount();
//		int random = RandomUtil.getRandomNumber(1, totalFilterOptions -1);
		this.plpPage.selectFilterByIndex(index);
		this.plpPage.waitForLoaderToDisappear();
		this.capabilities.reportStepInfo("successfully selected filter with index " + index);	
		this.actions.Wait.wait("3");
	}

	
	@And("^(?:I|i) apply any random filter on (?:pepsodent|signal|ponds) product listing page$")
	public void applyRandomFilterWithUnorderedList() throws Throwable {
		this.actions.Wait.waitForPageReady();
		String productsCount = this.plpPage.getProductListingFilterProductsCount();
		this.capabilities.getScenarioContext().addScenarioData("filterProductCount", productsCount);
		int totalFilterOptions = this.plpPage.getProductListingV2FilterUnorderedListOptionsCount();
		int random = RandomUtil.getRandomNumber(1, totalFilterOptions -1);
		this.plpPage.selectFilterULByIndex(random);
		this.actions.Wait.wait("15");
		this.plpPage.waitForLoaderToDisappear();
		this.capabilities.reportStepInfo("successfully selected filter with index " + random);
		
	}
	
	@And("^(?:I|i) apply filter by index (\\d+) on (?:pepsodent|signal|ponds) product listing page$")
	public void applyRandomFilterWithUnorderedList(int index) throws Throwable {
		this.actions.Wait.waitForPageReady();
		String productsCount = this.plpPage.getProductListingFilterProductsCount();
		this.capabilities.getScenarioContext().addScenarioData("filterProductCount", productsCount);
		this.plpPage.selectFilterULByIndex(index);
		this.actions.Wait.wait("15");
		this.plpPage.waitForLoaderToDisappear();
		this.capabilities.reportStepInfo("successfully selected filter with index " + index);
		
	}

	
	@And("^(?:I|i) should be able to see that filter is successfully applied$")
	public void verifyFilterApplied() throws NumberFormatException, InterruptedException {
		actions.Wait.waitForPageReady();
		String productsCount = this.plpPage.getProductListingFilterProductsCount();
		String previousCount = (String)this.capabilities.getScenarioContext().getScenarioData("filterProductCount");
		Assert.assertNotEquals(productsCount, previousCount, "Filter is not applied. Before filter, there were " +previousCount+ " products and after filter, there are " + productsCount + " products");
		this.capabilities.reportStepInfo("filter is applied. before filter there were " +previousCount+ " products and after filter there are " + productsCount + " products");
	}
	
	@And("^i verify filter is rendering on Product Page$")
	public void verifyFilterAppearing() {
		   Assert.assertNotNull(this.plpPage.getProductListingV2Filter(), "Product Listing V2 Filter is not rendering for Product Page");
		   this.capabilities.reportStepInfo("Product Listing V2 Filter is present on page");
		   }
	@And("^(?:I|i) verify that product listing v2 component has cta link/button present on the page$")
	public void productlistingv2_CTA_visible() {
		this.actions.Wait.waitForPageReady();
		Assert.assertNotNull(this.plV2Page.getProductListingV2CtaButton(),
				"product listing v2 cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("product listing v2 CTA is present on page");
	}
	
	@And("^(?:I|i) verify that product name is displayed for product listing v2 component present on the page$")
	public void productlistingv2_Name_visible() {
		this.actions.Wait.waitForPageReady();
		//List <WebElement> productNameElement = actions.Element.getElements();
		Assert.assertTrue(this.plpPage.getProductListingV2Names().size() > 0,
				"Product Name for product listing v2 is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Product Name for product listing v2 is present on page");
	}
	
	@And("^(?:I|i) should see article list items on product listing page$")
	public void articlelisting_Name_visible() {
		this.actions.Wait.waitForPageReady();
		//List <WebElement> productNameElement = actions.Element.getElements();
		Assert.assertTrue(this.plpPage.getArticleListingV2Names().size() > 0,
				"Article for article listing is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Article for article listing v2 is present on page");
	}
	
	@And("^(?:I|i) click on any of the article link for related articles on product listing page$")
	public void clickOnRandomArticleListingItem() {
		actions.Wait.waitForPageReady();
		List<WebElement> articleListingItems = this.alpPage.getArticleListingV2ListItems();
		int randomNumber = new Random().nextInt(articleListingItems.size());
		WebElement listItem = articleListingItems.get(randomNumber);
		String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("selectedpagelistingv2itemurl", href);
		listItem.click();
	}
	
	@And("^(?:I|i) click on any of the article of related articles$")
	public void clickAnyArticleListingItem() {
		actions.Wait.waitForPageReady();
		List<WebElement> articleListingItems = this.alpPage.getArticleListingV2ListItems();
		int randomNumber = new Random().nextInt(articleListingItems.size());
		WebElement listItem = articleListingItems.get(randomNumber);
		String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("selectedarticlelistingitemurl", href);
		listItem.click();
	}
	
	@Then("^(?:I|i)t should take me to the corresponding page of article page$")
	public void verifyRelatedarticlePage() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext()
				.getScenarioData("selectedarticlelistingitemurl");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related article link was " + expectedUrl
						+ System.lineSeparator() + " actual link is " + actualUrl);
	}
	
	@And("^(?:I|i) click on article image for related articles on product listing page$") //added on 17jan by Ritanshu
	public void clickrelatedarticleplp() {
		actions.Wait.waitForPageReady();
	    WebElement relatedarticleplp = this.plpPage.getRelatedArticleImagePLP();
	    Assert.assertNotNull(relatedarticleplp,
				"article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("article image clicked on plp page");
		this.capabilities.getScenarioContext().addScenarioData("relatedarticleplplinkhref", relatedarticleplp.getAttribute("href"));
		relatedarticleplp.click();
	}

	
	@Then("(?:I|i) click on the \"(.+)\" button$")
	public void buttonPresent(String button) {
		//actions.Wait.waitForPageReady();
		actions.Scroll.scrollPublishedPageDownByPixels(1000);
		WebElement buttonitem = this.plpPage.getButton();
		Assert.assertTrue(buttonitem.isDisplayed(), "button with text " + button + " is not visible");
		buttonitem.click();
		this.capabilities.reportStepInfo("clicking " + button + " button");
	}
	
	@Then("^quick-view panel should expand$")
	public void buttonClick() {
		actions.Wait.waitForPageReady();
		//actions.Scroll.scrollPublishedPageDownByPixels(1100);
		WebElement panel = this.plpPage.getQuickViewPanel();
		Assert.assertTrue(panel.isDisplayed(), "quick view panel is not visible");
		if (panel.isDisplayed()) 
			this.capabilities.reportStepInfo("quick view panel is shown");
	}
	
	//When I click on cross button quick view panel should close
	@When("^(?:I|i) click on cross button quick view panel should close$")
	public void panelClose() {
		actions.Wait.waitForPageReady();
		//actions.Scroll.scrollPublishedPageDownByPixels(1100);
		WebElement closepanel = this.plpPage.getClosePanel();
		Assert.assertTrue(closepanel.isDisplayed(), "cross button to close quick view panel not visible");
		closepanel.click();
		this.capabilities.reportStepInfo("quick view panel closed");
	}
	
	@Then("^(?:I|i) verify that PLP Filter Label is present on  PLP page$")
	public void i_verify_PLP_Filterlabel() {
		WebElement plpfilterlabel = this.plpPage.getPLPFilterLabel();
		Assert.assertNotNull(plpfilterlabel,
				"PLP Filter Label is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("PLP Filter Label is present on page");
	}
	
	@And("^(?:I|i) click on any of the items of Collection Filter for All Product$")
	public void clickOnRandomCollectionFilter() {
		actions.Wait.waitForPageReady();
		List<WebElement> collectionFilter = this.plpPage.getPLPCollectionFilterItemAllProduct();
		int randomNumber = new Random().nextInt(collectionFilter.size());
		WebElement listCollectionItem = collectionFilter.get(randomNumber);
		//String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		//capabilities.getScenarioContext().addScenarioData("selectedpagelistingv2itemurl", href);
		listCollectionItem.click();
	}
	
	
	@And("^(?:I|i) click on any of the items of Flavor Filter for All Product$")
	public void clickOnRandomFlavorFilter() {
		actions.Wait.waitForPageReady();
		List<WebElement> flavorFilter = this.plpPage.getPLPFlavorFilterItemAllProduct();
		int randomNumber = new Random().nextInt(flavorFilter.size());
		WebElement listCollectionItem = flavorFilter.get(randomNumber);
		//String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		//capabilities.getScenarioContext().addScenarioData("selectedpagelistingv2itemurl", href);
		listCollectionItem.click();
	}
	
	@And("^(?:I|i) click on any of the items of Flavor Filter$")
	public void clickOnRandomTeaBagFlavorFilter() {
		actions.Wait.waitForPageReady();
		List<WebElement> teaBagFlavorFilter = this.plpPage.getPLPFlavor();
		int randomNumber = new Random().nextInt(teaBagFlavorFilter.size());
		WebElement listTeaBagFlavorItem = teaBagFlavorFilter.get(randomNumber);
		//String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		//capabilities.getScenarioContext().addScenarioData("selectedpagelistingv2itemurl", href);
		listTeaBagFlavorItem.click();
	}
	
	@And("^(?:I|i) click on any of the items of Segment Filter$")
	public void clickOnRandomTeaBagSegmentFilter() {
		actions.Wait.waitForPageReady();
		List<WebElement> teaBagSegmentFilter = this.plpPage.getPLPSegment();
		int randomNumber = new Random().nextInt(teaBagSegmentFilter.size());
		WebElement listTeaBagSegmentItem = teaBagSegmentFilter.get(randomNumber);
		//String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		//capabilities.getScenarioContext().addScenarioData("selectedpagelistingv2itemurl", href);
		listTeaBagSegmentItem.click();
	}
	
	@And("^(?:I|i) click on any of the items of Segment Filter for All Product$")
	public void clickOnRandomSegmentFilter() {
		actions.Wait.waitForPageReady();
		List<WebElement> segmentFilter = this.plpPage.getPLPSegmentFilterItemAllProduct();
		int randomNumber = new Random().nextInt(segmentFilter.size());
		WebElement listCollectionItem = segmentFilter.get(randomNumber);
		//String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		//capabilities.getScenarioContext().addScenarioData("selectedpagelistingv2itemurl", href);
		listCollectionItem.click();
	}
}
