package steps.business.generic;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import framework.shared.RandomUtil;
import steps.generic.keywords.World;
//import steps.generic.SetCapabilities;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.BackToTopPage;
import unilever.pageobjects.platform.publish.components.definitions.HomeStory;

public class StepsHomeStory {
	UserActions actions;
	World capabilities;
	PublishedPageCommon publishedPage;
	BackToTopPage backtotopPage;
	HomeStory homestoryPage;

	public StepsHomeStory(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.backtotopPage = new BackToTopPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.homestoryPage = new HomeStory(capabilities.getWebDriver(),capabilities.getBrandName());

	}
	

	@And("^(?:I|i) click on the cta of the home-story component$")
	public void click_homestory_CTA() {
		actions.Wait.waitForPageReady();
		WebElement cta = this.homestoryPage.getHomeStoryCTA();
		Assert.assertNotNull(cta,
				"home story cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("home story cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("homestoryctahref", cta.getAttribute("href"));
		cta.click();
	}

	@Then("^it should take me to the corresponding page of home story cta$")
	public void verifyhomestoryCtaUrlPage() throws Throwable {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("homestoryctahref");
		this.actions.Wait.wait("3");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();

		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related article cta href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
	}

	@And("^(?:I|i) click on any of the items of home story page$")
	public void clickOnAnyItemOfArticlePage() {
		actions.Wait.waitForPageReady();
		List<WebElement> relatedarticleItems = this.homestoryPage.getHomeStoryItems();
		Assert.assertNotNull(relatedarticleItems, "articles are not appearing on page");
		int randomNumber = RandomUtil.getRandomNumber(1, relatedarticleItems.size() - 1);
		WebElement listItem = relatedarticleItems.get(randomNumber);
		String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("selectedrelatedarticleitemurl", href);
		listItem.click();
		this.capabilities.reportStepInfo("clicked on article item with href " + href);
	}

	@And("^(?:I|i) click on the some story image$")
	public void click_home_story_image() {
		WebElement homeStoryImage = this.homestoryPage.getHomeStoryImages();
		Assert.assertNotNull(homeStoryImage,
				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
				homeStoryImage.getAttribute("href"));
		homeStoryImage.click();
		this.capabilities.reportStepInfo("home story image has been clicked");

	}
	

	@Then("^it should take me to the corresponding page of home story image$")
	public void verify_homestory_image() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext()
				.getScenarioData("relatedarticleimagehref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related article cta href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
		this.capabilities.reportStepInfo("You have been taken to the corresponding page");
	}

	@And("^(?:I|i) verify that cta link/button of home story component is present on the page$")
	public void verifyCTA() {
		actions.Wait.waitForPageReady();
		this.homestoryPage.getHomeStoryCTA();
		this.capabilities.reportStepInfo("CTA is present on the page");
	}

	@Then("^(?:I|i) verify that home story component image is rendering$")
	public void i_verify_that_homestory_component_image_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement homestoryImage = this.homestoryPage.getHomeStoryImages();
		Assert.assertNotNull(homestoryImage,
				"home story image is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("home story Image is present on page");
	}

	@Then("^(?:I|i) verify that home story component heading is appearing$")
	public void i_verify_that_homestory_component_heading_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement homestoryHeading = this.homestoryPage.getHomeStoryHeading();
		Assert.assertNotNull(homestoryHeading,
				"home story heading is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("home story heading is present on page");
	}

	@Then("^(?:I|i) verify that home story component description is appearing$")
	public void i_verify_that_homestory_component_description_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement homestoryDescription = this.homestoryPage.getHomeStoryDescription();
		Assert.assertNotNull(homestoryDescription, "home story description is not rendering for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("home story description is present on page");
	}

}
