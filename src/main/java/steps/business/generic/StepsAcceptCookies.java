package steps.business.generic;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import cucumber.api.java.en.And;
import framework.shared.FrameworkConstants;
import steps.generic.keywords.*;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;

public class StepsAcceptCookies {
	World capabilities;
	Click Click;
	String cookiesFrame = "iframe[onload='___cq.iframeloaded();'],#cqcqPanel iframe,iframe[src*='cookiepolicy']";
	By byButtonAccept = By.cssSelector("button.closeButton.acceptButton,a.o-btn--accept");
	By byButtonDecline = By.cssSelector("button.closeButton.declineButton,,a.o-btn--decline");
	By unileverCookiePopup = By.cssSelector("div[data-role='html-injector'] span[id='unileverCookiePopup__close']");
	PublishedPageCommon publishedPageCommon;

	public StepsAcceptCookies(World capabilities, Click Click) {
		this.capabilities = capabilities;
		this.Click = Click;
		this.publishedPageCommon = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@And("^I accept cookies$")
	public void acceptCookies() throws Throwable {
		System.out.println("Accepting Cookies");
		/*
		 * WebDriverWait wait = new WebDriverWait(runner.getBrowser(),
		 * FrameworkConstants.MEDIUM_WAIT); WebElement element = null; try { element =
		 * wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(
		 * cookiesFrame))); } catch (Exception e) { element = null; } if (element !=
		 * null) { runner.getBrowser().switchTo().frame(element); Thread.sleep(1000);
		 * WebElement btnAccept = null; try {
		 * 
		 * btnAccept = wait.until(ExpectedConditions.elementToBeClickable(
		 * By.cssSelector("div[id='Panel'] * div[class='rightcol']>a:nth-of-type(1)")));
		 * // btnAccept = //
		 * wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
		 * "//*[@id='Panel']//a[contains(@class,'decline')]")));
		 * 
		 * } catch (Exception e) { btnAccept = null; } if (btnAccept != null) {
		 * btnAccept.click(); } Thread.sleep(2000);
		 * runner.getBrowser().switchTo().defaultContent(); Thread.sleep(1000); }
		 */
	}
	

	@And("^I decline cookies$")
	public void declineCookies() throws Throwable {
		System.out.println("Declining Cookies");
		/*
		 * WebDriverWait wait = new WebDriverWait(runner.getBrowser(),
		 * FrameworkConstants.MEDIUM_WAIT); WebElement element = null; try { element =
		 * wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(
		 * cookiesFrame))); } catch (Exception e) { element = null; } if (element !=
		 * null) { runner.getBrowser().switchTo().frame(element); Thread.sleep(1000);
		 * runner.getBrowser().findElement(By.xpath(
		 * "//*[@id='Panel']//a[contains(@class,'decline')]")).click();
		 * Thread.sleep(1000); runner.getBrowser().switchTo().defaultContent();
		 * Thread.sleep(1000); }
		 */
	}

	@And("^I expect cookies popup is appearing$")
	public void verifyCookiePopupAppears() throws Throwable {
		System.out.println("Verifying Cookies PopUp");
		WebDriverWait wait = new WebDriverWait(capabilities.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		WebElement element = null;
		try {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cookiesFrame)));
		} catch (Exception e) {
			element = null;
		}
		if (element == null) {
			Assert.fail("Cookie Popup is not appearing on the page");
		} else {
			System.out.println("Cookie popup is appearing on the page");
		}
	}

	@And("^I expect cookies popup is appearing and I accept it$")
	public void verifyCookiePopupAppearsAndAcceptIt() throws Throwable {
		WebDriverWait wait = new WebDriverWait(capabilities.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		WebElement element = null;
		try {
			element = wait
					.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cookiesFrame)));
		} catch (Exception e) {
			element = null;
		}
		if (element != null) {
			capabilities.getWebDriver().switchTo().frame(element);
			Thread.sleep(1000);
			WebElement btnAccept = null;
			try {
				btnAccept = wait.until(ExpectedConditions.elementToBeClickable(byButtonAccept));
			} catch (Exception e) {
				btnAccept = null;
			}
			if (btnAccept != null) {
				btnAccept.click();
				System.out.println("successfully accepted cookie popup");
			}
			Thread.sleep(2000);
			capabilities.getWebDriver().switchTo().defaultContent();
			Thread.sleep(1000);
		}
	}

	@And("^I expect cookies popup is appearing and I decline it$")
	public void verifyCookiePopupAppearsAndDeclineIt() throws Throwable {
		WebDriverWait wait = new WebDriverWait(capabilities.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		WebElement element = null;
		try {
			Thread.sleep(3000);
			element = wait
					.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cookiesFrame)));
		} catch (Exception e) {
			element = null;
		}
		if (element != null) {
			capabilities.getWebDriver().switchTo().frame(element);
			Thread.sleep(1000);
			WebElement btnAccept = null;
			try {
				btnAccept = wait.until(ExpectedConditions.elementToBeClickable(byButtonDecline));
			} catch (Exception e) {
				btnAccept = null;
			}
			if (btnAccept != null) {
				btnAccept.click();
				System.out.println("successfully declined cookie popup");
			}
			Thread.sleep(2000);
			capabilities.getWebDriver().switchTo().defaultContent();
			Thread.sleep(1000);
		}
	}
	@And("^I expect cookies popup is not appearing$")
	public void verifyCookiePopupNotAppearing() throws Throwable {
		System.out.println("Verifying Cookies PopUp is not appearning");
		WebDriverWait wait = new WebDriverWait(capabilities.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		WebElement element = null;
		try {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cookiesFrame)));
		} catch (Exception e) {
			element = null;
		}
		if (element != null) {
			Assert.fail("Cookie Popup is appearing on the page");
		} else {
			System.out.println("Cookie popup is not appearing on the page");
		}
	}

	@And("^I accept cookies for (.*)$")
	public void acceptCookiesFor(String locale) throws Throwable {
		System.out.println("Accepting Cookies");
		if (locale.trim().toLowerCase().equals("en_ca")) {
			return;
		}
		WebDriverWait wait = new WebDriverWait(capabilities.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		WebElement element = null;
		try {
			element = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cookiesFrame)));
		} catch (Exception e) {
			element = null;
		}
		if (element != null) {
			capabilities.getWebDriver().switchTo().frame(element);
			Thread.sleep(1000);
			WebElement btnAccept = null;
			try {

				btnAccept = wait.until(ExpectedConditions.elementToBeClickable(
						By.cssSelector("div[id='Panel'] * div[class='rightcol']>a:nth-of-type(1)")));
				// btnAccept =
				// wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='Panel']//a[contains(@class,'decline')]")));

			} catch (Exception e) {
				btnAccept = null;
			}
			if (btnAccept != null) {
				btnAccept.click();
			}
			Thread.sleep(2000);
			capabilities.getWebDriver().switchTo().defaultContent();
			Thread.sleep(1000);
		}

	}

}
