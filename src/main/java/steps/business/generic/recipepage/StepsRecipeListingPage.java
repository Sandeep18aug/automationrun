package steps.business.generic.recipepage;


import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;
import cucumber.api.java.en.And;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.recipe.RecipePage;

public class StepsRecipeListingPage {

	UserActions actions;
	World capabilities;
	RecipePage recipePage;
	// String pr

	public StepsRecipeListingPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.recipePage = new RecipePage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@And("^(?:I|i) should be able to see recipe title as \"([^\"]*)\" on recipe page$")
	public void verifyRecipeTitleOnRecipePageAs(String title) {
		Assert.assertEquals(this.recipePage.getRecipeTitleText(),title,"recipe title is empty on recipe page " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("recipe title is found to be ---> " + title);
	}
	
	
	@And("^(?:I|i) should be able to see recipe description as \"([^\"]*)\" on recipe page$")
	public void verifyRecipeDescriptionOnRecipePageAs(String description) {
		Assert.assertEquals(this.recipePage.getRecipeDescriptionText(),description,"recipe description is empty on recipe page " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("recipe description is found to be ---> " + description);
	}
	
	@And("^(?:I|i) should be able to see recipe title on recipe page$")
	public void verifyRecipeTitleOnRecipePage() {
		String recipeTitle = this.recipePage.getRecipeTitleText();
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(recipeTitle),"recipe title is empty on recipe page " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("recipe title is found to be ---> " + recipeTitle);
	}
	
	
	@And("^(?:I|i) should be able to see recipe description on recipe page$")
	public void verifyRecipeDescriptionOnRecipePage() {
		String recipeDescription = this.recipePage.getRecipeDescriptionText();
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(recipeDescription),"recipe description is empty on recipe page " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("recipe description is found to be ---> " + recipeDescription);
	}

	@And("^(?:I|i) should be able to see rating review on recipe page$")
	public void verifyRecipeRatingReviewOnRecipePage() {
		boolean RecipeRatingReview = this.recipePage.getRecipeRatingReview().isDisplayed();
		Assert.assertTrue(RecipeRatingReview ,"recipe rating review is not present on recipe page " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Rating Review for Recipe is present");
	}
	
	@And("^(?:I|i) should see Show Nutritional Facts button works fine for hellmanns brand$")
	public void verifyShowNutritionalFactsWorksFine(){
		this.recipePage.getShowNutritionalFacts().click();
		WebElement heading = this.recipePage.getRecipePageNutrientsTableHeading();
		Assert.assertNotNull(heading, "Recipe page nutrient table heading is not present on page");
		this.capabilities.reportStepInfo("Recipe page nutrient table heading is present as ---> " + heading.getText());
	}
	
	
	
}
