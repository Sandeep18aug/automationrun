package steps.business.generic.recipepage;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.pdp.ProductDetailPage;
import unilever.pageobjects.platform.publish.recipe.RecipeListingPage;
import unilever.pageobjects.platform.publish.recipe.RecipePage;

public class StepsRecipePage {

	UserActions actions;
	World capabilities;
	RecipeListingPage recipeListingPage;
	// String pr

	public StepsRecipePage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.recipeListingPage = new RecipeListingPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	
	@Then("^(?:i|I) should be able to see recipe listing heading$")
	public void i_should_be_able_to_see_recipe_listing_heading() {
	    WebElement recipeListingHeading = this.recipeListingPage.getRecipeListingHeadingObject();
	    Assert.assertNotNull(recipeListingHeading, "recipe listing heading is not present on the page" );
	    this.capabilities.getScenarioContext().addScenarioData("itemscountinheading", this.recipeListingPage.getRecipeListingItemsCountInHeading());
	    this.capabilities.reportStepInfo("recipe listing heading is present on the page");
	}

	@Then("^(?:i|I) should be able to see recipe listing items$")
	public void i_should_be_able_to_see_recipe_listing_items() {
		  List<WebElement> recipeListingHeading = this.recipeListingPage.getRecipeListingItems();
		  Assert.assertTrue(recipeListingHeading.size() > 0, "recipe listing items are not present on page");;
	}

	@When("^(?:i|I) click on recipe listing filter button$")
	public void i_click_on_recipe_listing_filter_button() throws Throwable {
		this.actions.Scroll.scrollPublishedPageDownByPixels(200);
		WebElement element = this.recipeListingPage.getRecipeListingFilterButtonObject();
		this.actions.Click.click(element);
		this.actions.Wait.wait("2");
	}

	@Then("^(?:i|I) should be able to see recipe listing filter options$")
	public void i_should_be_able_to_see_recipe_listing_filter_options() {
	    Assert.assertTrue(this.recipeListingPage.getRecipeFilterOptions().size() > 0,"recipe listing filter options are not present on the screen");
	    this.capabilities.reportStepInfo("recipe listing filters are present on the page");
	}

	@When("^(?:i|I) apply the first recipe listing filter$")
	public void i_apply_the_first_recipe_listing_filter() throws NumberFormatException, InterruptedException {
		this.recipeListingPage.getRecipeFilterOptions().get(0).click();
		this.recipeListingPage.getRecipeFilterApplyButton().click();
		this.actions.Wait.wait("3");
	}

	@Then("^(?:i|I) should be able to see that recipe listing filter is successfully applied$")
	public void i_should_be_able_to_see_that_recipe_listing_filter_is_successfully_applied() throws NumberFormatException, InterruptedException {
	   this.actions.Wait.waitForPageReady();
	   this.actions.Wait.wait("5");
	   int storedItemsCount = (int) this.capabilities.getScenarioContext().getScenarioData("itemscountinheading");
	   int currentItemsCount = this.recipeListingPage.getRecipeListingItemsCountInHeading();
	   Assert.assertNotEquals(storedItemsCount, currentItemsCount,"filters are not applied.");
	   this.capabilities.reportStepInfo("recipe filters are applied successfully, before filters, total items were " + storedItemsCount);
	   this.capabilities.reportStepInfo("after recipe filters, new items are " + currentItemsCount);
	}
	
	
}
