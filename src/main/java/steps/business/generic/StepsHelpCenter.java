package steps.business.generic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.*;
import unilever.pageobjects.platform.publish.components.definitions.*;
import unilever.pageobjects.platform.publish.helpcenter.HelpCenter;

public class StepsHelpCenter {

	UserActions actions;
	World capabilities;
	LiveChat LiveChatPage;
	RichTextV2 RichTextV2;
	HelpCenter helpCenter;

	public StepsHelpCenter(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.LiveChatPage = new LiveChat(capabilities.getWebDriver(),capabilities.getBrandName());
		this.RichTextV2 = new RichTextV2(capabilities.getWebDriver(),capabilities.getBrandName());
		this.helpCenter = new HelpCenter(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@Then("^(?:i|I) verify live chat exists on page$")
	public void liveChatV2() {
		this.actions.Wait.waitForPageReady();
		Assert.assertTrue(
				Strings.isNotNullAndNotEmpty(this.LiveChatPage.getliveChatV2ComponentHeading().getText().trim()),
				"Live chat is not present on Help center page");
		this.LiveChatPage.getliveChatV2ComponentHeading().click();
	}

	@And("^(?:i|I) verify live chat window opens on click when chat agent is online$")
	public void liveChatV2FunctionalityOnline() throws Throwable {
		this.actions.Wait.waitForPageReady();
		Assert.assertTrue(this.LiveChatPage.isPresent(), "agent is offline");
	}

	@And("^(?:i|I) verify live chat offline message is displayed when chat agent is offline$")
	public void liveChatV2FunctionalityOffline() {
		this.actions.Wait.waitForPageReady();
		this.LiveChatPage.getliveChatV2ComponentHeading().click();
	}

	@And("^(?:i|I) verify \"([^\"]*)\" heading exists on Help Center page$")
	public void richTextV2Elements(String h2Headings) {
		this.actions.Wait.waitForPageReady();
		Assert.assertNotNull(this.RichTextV2.getRichTextV2ComponentWithHeading(h2Headings), "Heading doesn't matches");

	}

	@And("^(?:i|I) verify clicking on \"([^\"]*)\" navigates user to \"([^\"]*)\" from Help Center page$")
	public void richTextV2Elements(String Headings, String url) {

		this.actions.Wait.waitForPageReady();
		this.RichTextV2.getRichTextV2ComponentWithHeading(Headings).click();
		this.actions.Wait.waitForPageReady();
		String currentUrl = this.capabilities.getWebDriver().getCurrentUrl();
		this.capabilities.getWebDriver().navigate().back();
		this.actions.Wait.waitForPageReady();
		
		Assert.assertTrue(currentUrl.contains(url));

	}
	
	@When("^(?:I|i) click on the email us link$")
	public void clickemailus() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement emailus = this.helpCenter.getemailus();
		actions.Hover.hover(emailus);
		this.capabilities.getScenarioContext().addScenarioData("contactuslinkhref", emailus.getAttribute("href"));
		actions.Click.click(emailus);
		this.capabilities.reportStepInfo("Successfully clicked on email us link");
	}
	
	@When("^(?:I|i) click on the email us link then it takes us to contact-us page$")
	public void clickemailus1() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement emailus = this.helpCenter.getemailus();
		//actions.Hover.hover(emailus);
		this.capabilities.getScenarioContext().addScenarioData("contactuslinkhref", emailus.getAttribute("href"));
		actions.Click.click(emailus);
		System.out.println("Successfully clicked or hover on email us link");
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("contactuslinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl), "the page is not navigated to expected url. contact us link href was "
				+ expectedUrl + System.lineSeparator() + " actual url is " + actualUrl);		
	}
}
