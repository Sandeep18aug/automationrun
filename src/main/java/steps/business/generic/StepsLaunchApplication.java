package steps.business.generic;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.configuration.BaseUrl;
import framework.shared.FrameworkConstants;
import framework.shared.GetUrl;
import framework.shared.UrlHandler;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.usermanager.User;
import unilever.pageobjects.platform.usermanager.UserFactory;

public class StepsLaunchApplication {
	private World capabilities;
	private UserActions getBasicKeywords;
	WebElement txtUserName, txtPassword, btnLogin;
	GetUrl getConfig = new GetUrl();

	public StepsLaunchApplication(World runner, UserActions getBasicKeywords) {
		this.capabilities = runner;
		this.getBasicKeywords = getBasicKeywords;
	}

//	@And("^I open the authoring url using valid credential$")
//	public void authorLogin() throws Throwable {
//		try {
//			String url = getConfig.getValue("AuthoringSite");
//			if (url == null)
//				Assert.fail("No URL is present in 'globalConfig.properties' file for variable 'AuthoringSite'");			
//			url = processAuthoringUrl(url);
//			getBasicKeywords.Launch.launch(url, false);
//			loginIntoSiteAdmin();			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			Assert.fail(e.getMessage());
//		}
//	}
//	
//	@And("^I open the home authoring url using valid credential$")
//	public void authorHomeLogin() throws Throwable {
//		try {
//			String url = getConfig.getUrl("HomeAuthoringSite");
//			if (url == null)
//				Assert.fail("No URL is present in 'globalConfig.properties' file for variable 'AuthoringSite'");			
//			url = processAuthoringUrl(url);
//			getBasicKeywords.Launch.launch(url, false);
//			loginIntoSiteAdmin();			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			Assert.fail(e.getMessage());
//		}
//		openAuthorUrl("HomeAuthoringSite");		
//	}
//
//	@And("^I open and login into the author url (.*)$")
//	public void openAuthorUrl(String urlToBeLaunched) throws Throwable {
//		try {
//			String url = getConfig.getValue(urlToBeLaunched);
//			if (url == null)
//				url = urlToBeLaunched;
//			url = processAuthoringUrl(url);
//		getBasicKeywords.Launch.launch(url,false);
//			loginIntoSiteAdmin();
//		} catch (Exception e) {
//			e.printStackTrace();
//		Assert.fail(e.getMessage());
//			}
//	}
//	
//	private String processAuthoringUrl(String url) throws Throwable {
//		if (url.contains("/") && !(url.startsWith("http"))) {
//			url = new UrlHandler(FrameworkConstants.Environment.getAuthorbaseUrl())
//					.append(FrameworkConstants.SITE_ADMIN_APPEND)
//					.append("content")
//					.append(FrameworkConstants.Environment.getBrand())
//					.append(FrameworkConstants.Environment.getAuthorlocale())
//					.append(url).getUrl();
//			this.DriverHandle.setBrandNameForScenarioContext(FrameworkConstants.Environment.getBrand());
//			this.DriverHandle.setLocaleNameForScenarioContext(FrameworkConstants.Environment.getAuthorlocale());
//		}else if(url.startsWith("http")) {
//			UrlHandler urlHandler = new UrlHandler();
//			String baseUrl = urlHandler.append(url).getBaseUrl();
//			url = url.replace(baseUrl, FrameworkConstants.Environment.getAuthorbaseUrl());
//		}
//		this.DriverHandle.setAuthorUrlForScenarioContext(url);
//		return url;
//	}

//	@And("^I open and login into the editor url (.*)$")
//	public void openEditorUrl(String urlToBeLaunched) throws Throwable {
//		try {
//			String url = getConfig.getValue(urlToBeLaunched);
//			if (url == null)
//				url = urlToBeLaunched;
//			if (url.contains("/") && !(url.startsWith("http"))) {
//				url = new UrlHandler(FrameworkConstants.Environment.getAuthorbaseUrl())
//						.append(FrameworkConstants.EDITOR_DOT_HTML_APPEND)
//						.append("content")
//						.append(FrameworkConstants.Environment.getBrand())
//						.append(FrameworkConstants.Environment.getAuthorlocale())
//						.append(url)
//						.getUrl();
//				this.DriverHandle.setBrandNameForScenarioContext(FrameworkConstants.Environment.getBrand());
//				this.DriverHandle.setLocaleNameForScenarioContext(FrameworkConstants.Environment.getAuthorlocale());
//			}else if(url.startsWith("http")) {
//				UrlHandler urlHandler = new UrlHandler();
//				String baseUrl = urlHandler.append(url).getBaseUrl();
//				url = url.replace(baseUrl, FrameworkConstants.Environment.getAuthorbaseUrl());
//			}
//			String[] urlPart = url.split("/");
//			String pageName = urlPart[urlPart.length -1];
//			String pagePathUrl =url.replace("/" + pageName, "");
//			pageName = pageName.replace(".html", "");		
//			this.DriverHandle.setAuthorUrlForScenarioContext(pagePathUrl);
//			this.DriverHandle.setPageNameToBeCreated(pageName);
//			getBasicKeywords.Launch.launch(url,false);
//			loginIntoSiteAdmin();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			Assert.fail(e.getMessage());
//		}
//	}

//	@And("^I open publish url (.*)$")
//	public void openPublishedUrl(String sUrl) throws Throwable {
//		try {
//			String url = getConfig.getValue(sUrl);
//			if (url == null)
//				url = sUrl;
//			UrlHandler urlHandler = new UrlHandler();
//			if (url.contains("/") && !(url.startsWith("http"))) {
//				url = urlHandler.append(FrameworkConstants.Environment.getPublishbaseUrl())
//						.append(getPublisherLocale(FrameworkConstants.Environment.getAuthorlocale()))
//						.append(url).getUrl();
//			}else if(url.startsWith("http")) {
//				url = urlHandler.removeAuthInfoFromPublishedUrl(url);
//				String baseUrl = urlHandler.append(url).getBaseUrl();
//				url = url.replace(baseUrl, FrameworkConstants.Environment.getPublishbaseUrl());
//			}
//			url = urlHandler.convertToAuthEnabledPublishedUrl(url);					
//			getBasicKeywords.Launch.launch(url, FrameworkConstants.PROXY_ENABLED);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	@And("^I open author page url (.*)$")
	public void openUrl(String sUrl) throws Throwable {
		try {
			String url = getConfig.getUrl(sUrl);
			if (url == null)
				url = sUrl;
			getBasicKeywords.Launch.launch(url, FrameworkConstants.PROXY_ENABLED);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	@And("^the brand \"([^\"]*)\" has been built$")
//	public void openBrandUrl( String pageName) throws Throwable {
//		
//		try {
//			String url = getConfig.getValue(FrameworkConstants.Environment.getBrand() + "." +FrameworkConstants.Environment.getPublishedlocale()+ "." + pageName);
//			if (url == null)
//				url = pageName;
//			UrlHandler urlHandler = new UrlHandler();
//			if ( !(url.startsWith("http"))) {
//				url = urlHandler.append(FrameworkConstants.Environment.getPublishbaseUrl())
//						.append(FrameworkConstants.Environment.getPublishedlocale())
//						.append(url).getUrl();
//			}else if(url.startsWith("http")) {
//				url = urlHandler.removeAuthInfoFromPublishedUrl(url);
//				String baseUrl = urlHandler.append(url).getBaseUrl();
//				url = url.replace(baseUrl, FrameworkConstants.Environment.getPublishbaseUrl());
//			}
//			if(!Strings.isNullOrEmpty(FrameworkConstants.Environment.getPublisherAuthUserID()))
//				url = urlHandler.convertToAuthEnabledPublishedUrl(url);					
//			getBasicKeywords.Launch.launch(url, FrameworkConstants.PROXY_ENABLED);
//			getBasicKeywords.Wait.waitForPageReady();
//			PublishedPageCommon publishedPage = new PublishedPageCommon(DriverHandle.getWebDriver());
//			publishedPage.acceptCookiesIfPresent();
//			publishedPage.acceptEvidonCookiesIfPresent();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	

	
	@When("^(?:I|i) (?:launch this|open url) \"([^\"]*)\"$")
	//@And("^the \"([^\"]*)\" brand \"([^\"]*)\" of \"([^\"]*)\" locale has been built$")
	public void openBrandWiseUrl( String url) throws Throwable {
		
		try {
			this.capabilities.reportStepInfo("launching url - "+url);
			getBasicKeywords.Launch.launch(url, FrameworkConstants.PROXY_ENABLED);
			this.capabilities.reportStepInfo("this url is launched successfully " + url);
			getBasicKeywords.Wait.waitForPageReady();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Then("^(?:I|i) should get 200 OK status for the \"([^\"]*)\"$")
	public void httpresponse( String url) throws Throwable {
		
		try {
			this.capabilities.reportStepInfo("checking http response status of following url - " + url);
			getBasicKeywords.Launch.httpresponsestatus(url);
			getBasicKeywords.Wait.waitForPageReady();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@And("^the \"([^\"]*)\" brand \"([^\"]*)\" of \"([^\"]*)\" locale has been built$")
	public void openBrandUrl( String brand, String pageName, String locale) throws Throwable {
		capabilities.getScenarioContext().addScenarioData("brand", brand);
		capabilities.getScenarioContext().addScenarioData("locale", locale);
		try {
			String url = getConfig.getUrl(brand + "." +locale+ "." + pageName);
			if (url == null)
				Assert.fail("no url is present in urls.properties for key :: " + brand + "." +locale+ "." + pageName );
			UrlHandler urlHandler = new UrlHandler();
			if ( !(url.trim().startsWith("http"))) {
				url = urlHandler.append(BaseUrl.getBaseUrl(brand+"."+locale + "." + FrameworkConstants.Environment.getEnvironment()).trim())
						.append(url).getUrl();
			}
			this.capabilities.reportStepInfo("launching url - "+url);
			getBasicKeywords.Launch.launch(url, FrameworkConstants.PROXY_ENABLED);
			this.capabilities.setBrandName(brand);
			this.capabilities.reportStepInfo("this url is launched successfully "+url);
			getBasicKeywords.Wait.waitForPageReady();
			PublishedPageCommon publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
			publishedPage.closeCampaignPopup();
			publishedPage.acceptCookiesIfPresent();
			publishedPage.acceptEvidonCookiesIfPresent();
			if(this.capabilities.getBrandName().equals("clear")) {
				publishedPage.closeUnileverCookiePopupIfPresent();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@And("^I open URL for BIN (.*)$")
	public void openURLforScrambled(String sUrl) throws Throwable {
		try {
			String url = getConfig.getUrl(sUrl);
			if (url == null)
				url = sUrl;
			getBasicKeywords.Launch.launch(url, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String getPublisherLocale(String locale) {		
		switch (locale.toLowerCase()) {
		case "en_us":
			return "us/en";
		case "en_ca":
			return "ca/en";
		default:
			return null;
		}
	}
	
//	public void loginIntoSiteAdmin() throws Throwable {
//		User user = UserFactory.getRandomUserFromList();
//		String username = null;
//		String password = null;
//		username = FrameworkConstants.Environment.getAuthorUserID().equalsIgnoreCase("user.json") ? user.getAemUserId() :FrameworkConstants.Environment.getAuthorUserID();
//		password = FrameworkConstants.Environment.getAuthorPassword().equalsIgnoreCase("user.json") ? user.getAemPassword() :FrameworkConstants.Environment.getAuthorPassword();
//		txtUserName = DriverHandle.getWebDriver().findElement(By.cssSelector("input[name='j_username']"));
//		getBasicKeywords.Enter.enter(username, txtUserName);
//		txtPassword = DriverHandle.getWebDriver().findElement(By.cssSelector("input[name='j_password']"));
//		getBasicKeywords.Enter.enter(password, txtPassword);
//		btnLogin = DriverHandle.getWebDriver().findElement(By.cssSelector("button[type='submit']"));
//		getBasicKeywords.Click.click(btnLogin);
//		getBasicKeywords.Wait.waitForPageReady();
//	}

	//for popup
//	@And("^I log into component using valid credential$")
//	public void configureLogin() throws Throwable {
//		// getBasicKeywords.Launch.launch(URL);
//		txtUserName = DriverHandle.getWebDriver().findElement(By.xpath("//input[@id='Credentials-Username']"));
//		getBasicKeywords.Enter.enter(getConfig.getValue("Username_01"), txtUserName);
//		txtPassword = DriverHandle.getWebDriver().findElement(By.xpath("//input[@id='Credentials-Password']"));
//		getBasicKeywords.Enter.enter(getConfig.getValue("Password_01"), txtPassword);
//		getBasicKeywords.Wait.wait("5");
//		btnLogin = DriverHandle.getWebDriver().findElement(By.cssSelector("#input-submit"));
//		getBasicKeywords.Click.click(btnLogin);
//	}
//
//	//for popup
//		@And("^I log into published site with userid (.*) and password (.*)$")
//		public void loginIntoPublishedSiteUsingUseridAndPassword(String userid, String password) throws Throwable {
//			// getBasicKeywords.Launch.launch(URL);
//			if(userid.startsWith("^")) {
//				userid = getConfig.getValue(userid.replace("^", ""));
//			}
//			if(password.startsWith("^")) {
//				password = getConfig.getValue(password.replace("^", ""));
//			}			
//			txtUserName = DriverHandle.getWebDriver().findElement(By.xpath("//input[@id='Credentials-Username']"));
//			getBasicKeywords.Enter.enter(userid, txtUserName);
//			txtPassword = DriverHandle.getWebDriver().findElement(By.xpath("//input[@id='Credentials-Password']"));
//			getBasicKeywords.Enter.enter(password, txtPassword);
//			btnLogin = DriverHandle.getWebDriver().findElement(By.cssSelector("#input-submit"));
//			getBasicKeywords.Click.click(btnLogin);
//			getBasicKeywords.Wait.wait("10");
//		}
//	
//	
//	//for login page
//	@And("^I log into component using valid credential for LoginPage$")
//	public void configure_LoginPage() throws Throwable {
//		// getBasicKeywords.Launch.launch(URL);
//		getBasicKeywords.Wait.wait("5");
//		txtUserName = DriverHandle.getWebDriver().findElement(By.xpath("//div[contains(@class,'flexi_hero_par parsys')]//input[@id='Credentials-Username']"));
//		getBasicKeywords.Enter.enter(getConfig.getValue("Username_01"), txtUserName);
//		txtPassword = DriverHandle.getWebDriver().findElement(By.xpath("//div[contains(@class,'flexi_hero_par parsys')]//input[@id='Credentials-Password']"));
//		getBasicKeywords.Enter.enter(getConfig.getValue("Password_01"), txtPassword);
//		//getBasicKeywords.Enter.enter(getConfig.getPassword1(), txtPassword + "\n");
//		getBasicKeywords.Wait.wait("3");
//		btnLogin = DriverHandle.getWebDriver().findElement(By.xpath("//div[contains(@class,'c-login-wrapper')]//button[@id='input-submit']"));
//		getBasicKeywords.Click.click(btnLogin);
//	}
//	
//	//for pop-up
//	@And("^I log into component using invalid username$")
//	public void configureinvalidusername() throws Throwable {
//		// getBasicKeywords.Launch.launch(URL);
//		txtUserName = DriverHandle.getWebDriver().findElement(By.xpath("//input[@id='Credentials-Username']"));
//		getBasicKeywords.Enter.enter(getConfig.getValue("Username_02"), txtUserName);
//		txtPassword = DriverHandle.getWebDriver().findElement(By.xpath("//input[@id='Credentials-Password']"));
//		getBasicKeywords.Enter.enter(getConfig.getValue("Password_01"), txtPassword);
//		btnLogin = DriverHandle.getWebDriver().findElement(By.xpath("//button[@id='input-submit']"));
//		getBasicKeywords.Click.click(btnLogin);
//
//	}
//	
//	//for login page
//	@And("^I log into component using invalid username for LoginPage$")
//	public void configureinvalidusername_LoginPage() throws Throwable {
//		
//		// getBasicKeywords.Launch.launch(URL);
//		txtUserName = DriverHandle.getWebDriver().findElement(By.xpath("//div[contains(@class,'flexi_hero_par parsys')]//input[@id='Credentials-Username']"));
//		getBasicKeywords.Enter.enter(getConfig.getValue("Username_02"), txtUserName);
//		txtPassword = DriverHandle.getWebDriver().findElement(By.xpath("//div[contains(@class,'flexi_hero_par parsys')]//input[@id='Credentials-Password']"));
//		getBasicKeywords.Enter.enter(getConfig.getValue("Password_01"), txtPassword);
//		btnLogin = DriverHandle.getWebDriver().findElement(By.xpath("//div[contains(@class,'c-login-wrapper')]//button[@id='input-submit']"));
//		getBasicKeywords.Click.click(btnLogin);
//
//	}
//	
//    //for pop-up
//	@And("^I log into component using invalid password$")
//	public void configureinvalidpassword() throws Throwable {
//		// getBasicKeywords.Launch.launch(URL);
//		txtUserName = DriverHandle.getWebDriver().findElement(By.xpath("//input[@id='Credentials-Username']"));
//		getBasicKeywords.Enter.enter(getConfig.getValue("Username_01"), txtUserName);
//		txtPassword = DriverHandle.getWebDriver().findElement(By.xpath("//input[@id='Credentials-Password']"));
//		getBasicKeywords.Enter.enter(getConfig.getValue("Password_02"), txtPassword);
//		btnLogin = DriverHandle.getWebDriver().findElement(By.xpath("//button[@id='input-submit']"));
//		getBasicKeywords.Click.click(btnLogin);
//
//	}
//	
//	//for login page
//	//for pop-up
//		@And("^I log into component using invalid password for LoginPage$")
//		public void configureinvalidpassword_LoginPage() throws Throwable {
//			// getBasicKeywords.Launch.launch(URL);
//			txtUserName = DriverHandle.getWebDriver().findElement(By.xpath("//div[contains(@class,'flexi_hero_par parsys')]//input[@id='Credentials-Username']"));
//			getBasicKeywords.Enter.enter(getConfig.getValue("Username_01"), txtUserName);
//			txtPassword = DriverHandle.getWebDriver().findElement(By.xpath("//div[contains(@class,'flexi_hero_par parsys')]//input[@id='Credentials-Password']"));
//			getBasicKeywords.Enter.enter(getConfig.getValue("Password_02"), txtPassword);
//			btnLogin = DriverHandle.getWebDriver().findElement(By.xpath("//div[contains(@class,'c-login-wrapper')]//button[@id='input-submit']"));
//			getBasicKeywords.Click.click(btnLogin);
//
//		}
//
//		
////		@And("^I navigate to (.*) page$")
////		public void navigateToUrl(String sUrl) throws Throwable {
////			try {
////				String url = getConfig.getValue(sUrl);
////				if (url == null)
////					Assert.fail("no url is present in globalconfig.properties for key :: " sUrl );
////				UrlHandler urlHandler = new UrlHandler();
////				if ( !(url.startsWith("http"))) {
////					url = urlHandler.append(BaseUrl.getBaseUrl(brand+"."+locale + "." + FrameworkConstants.Environment.getEnvironment()))
////							.append(url).getUrl();
////				}
//////					else if(url.startsWith("http")) {
//////					url = urlHandler.removeAuthInfoFromPublishedUrl(url);
//////					String baseUrl = urlHandler.append(url).getBaseUrl();
//////					url = url.replace(baseUrl, FrameworkConstants.Environment.getPublishbaseUrl());
//////				}
//////				if(!Strings.isNullOrEmpty(FrameworkConstants.Environment.getPublisherAuthUserID()))
//////					url = urlHandler.convertToAuthEnabledPublishedUrl(url);					
////				getBasicKeywords.Launch.navigateTo(url);
////				getBasicKeywords.Wait.waitForPageReady();
////				PublishedPageCommon publishedPage = new PublishedPageCommon(DriverHandle.getWebDriver());
////				publishedPage.acceptCookiesIfPresent();
////				publishedPage.acceptEvidonCookiesIfPresent();
////			} catch (Exception e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////			}
		
}
