package steps.business.generic;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.Forms;

public class StepsForms {
	UserActions actions;
	World capabilities;
	Forms formsPage;

	public StepsForms(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.formsPage = new Forms(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@And("^(?:i|I) enter random email id$")
	public void enterRandomEmailAddress() {
		String randomText = RandomStringUtils.random(10, true, true);
		this.formsPage.enterEmailAddress(randomText + "@gmail.com");
	}

	@And("^(?:I|i) enter email address as \"([^\"]*)\"$")
	public void enter_emailaddress(String name) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterEmailAddress(name);

	}

	@And("^(?:I|i) enter first name as \"([^\"]*)\"$")
	public void enter_first_name(String name) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterFirstName(name);

	}

	@And("^(?:I|i) select title as \"([^\"]*)\"$")
	public void select_title(String title) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectPrefix(title);

	}
	
	@And("^(?:I|i) enter prefix as \"([^\"]*)\"$")
	public void select_Prefix(String Prefix) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectPrefix(Prefix);

	}

	@And("^(?:I|i) select gender as \"([^\"]*)\"$")
	public void select_gender(String Gender) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectGenderTextBox(Gender);

	}

	@And("^(?:I|i) select enquiry type as \"([^\"]*)\"$")
	public void select_enquirytype(String enquiryType) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectEnquiryType(enquiryType);

	}
	
	@And("^(?:I|i) select UPC Details as \"([^\"]*)\"$")
	public void select_UPCCode(String UPCCode) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectUPCDeatils(UPCCode);

	}
	
	@And("^(?:I|i) select Manufacturing Code as \"([^\"]*)\"$")
	public void select_ManufacturingCode(String ManufacturingCode) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectManufacturingCodeAvailability(ManufacturingCode);

	}

	@And("^(?:I|i) select County as \"([^\"]*)\"$")
	public void select_region(String Country) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectRegionTextBox(Country);

	}

	@And("^(?:I|i) enter last name as \"([^\"]*)\"$")
	public void enter_last_name(String name) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterLastName(name);

	}

	@And("^(?:I|i) enter address line 1 as \"([^\"]*)\"$")
	public void enter_address(String name) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterAddress1(name);

	}

	@And("^(?:I|i) enter address line 2 as \"([^\"]*)\"$")
	public void enter_address_line2(String name) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterAddress2(name);

	}

	@And("^(?:I|i) enter City as \"([^\"]*)\"$")
	public void enter_city(String name) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterCity(name);

	}

	@And("^(?:I|i) enter Postal Code as \"([^\"]*)\"$")
	public void enter_postal_code(String num) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterPostalCode(num);

	}

	@And("^(?:I|i) enter Zip Code as \"([^\"]*)\"$")
	public void enter_zip_code(String num) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterZipCode(num);

	}
	
	@And("^(?:I|i) enter Prefix as \"([^\"]*)\"$")
	public void enter_Prefix(String prefix) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterZipCode(prefix);

	}

	@And("^(?:I|i) select Country as \"([^\"]*)\"$")
	public void select_country(String country) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectCountry(country);

	}
	
	@And("^(?:I|i) select State as \"([^\"]*)\"$")
	public void select_State(String state) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectState(state);

	}

	@And("^(?:I|i) enter Contact Number as \"([^\"]*)\"$")
	public void enter_contact_number(String num) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterContactNumber(num);

	}

	@And("^(?:I|i) check age Consent checkbox$")
	public void check_age_consent() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkAgeConsentCheckBox();

	}

	@And("^(?:I|i) check Brand Opt-in checkbox$")
	public void check_brand_optin() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkBrandOptInCheckBox();

	}

	@And("^(?:I|i) check Corporate Opt-in checkbox$")
	public void check_corporate_optin() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkCorporateOptInCheckBox();

	}

	@And("^(?:I|i) check Mail Opt-in checkbox$")
	public void check_mail_optin() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkMailCheckBox();

	}

	@And("^(?:I|i) verify Contact Us Heading Text$")
	public void verify_contactus_heading() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkMailCheckBox();

	}

	// PRODUCT DETAILS

	@And("^(?:I|i) select bar code on product type as \"([^\"]*)\"$")
	public void select_barcode_product_type(String barCode) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectBarCodeAvailabilityBox(barCode);

	}

	@And("^(?:I|i) enter (?:bar|UPC) code as \"([^\"]*)\"$")
	public void enter_bar_code(String num) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterBarCodeBox(num);

	}

	@And("^(?:I|i) enter Product as \"([^\"]*)\"$")
	public void enter_product(String num) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterProductBox(num);

	}

	@And("^(?:I|i) enter Size as \"([^\"]*)\"$")
	public void enter_size(String num) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterSizeBox(num);

	}

	@And("^(?:I|i) enter Manufacturing Code as \"([^\"]*)\"$")
	public void enter_manufacturing_code(String num) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterManufacturingCode(num);

	}

	@And("^(?:I|i) enter Purchased Location as \"([^\"]*)\"$")
	public void enter_purchased_location(String num) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterLocationPurchased(num);

	}

	@And("^(?:I|i) select Manufacturing Code Availability as \"([^\"]*)\"$")
	public void enterManufacturingCodeAvailability(String selectManufacturingCodeAvailability) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.selectManufacturingCodeAvailability(selectManufacturingCodeAvailability);

	}

	@And("^(?:I|i) enter Store Name as \"([^\"]*)\"$")
	public void enterStoreName(String name) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterStoreName(name);

	}

	@And("^(?:I|i) enter Query as \"([^\"]*)\"$")
	public void enterComment(String name) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterQuery(name);

	}

	@And("^(?:I|i) check By Email Brand checkbox$")
	public void check_bymailbrand_optin_() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkByEmailBrandOptin();

	}

	@And("^(?:I|i) check By Email All checkbox$")
	public void check_bymailall_optin() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkByEmailAllOptin();

	}

	@And("^(?:I|i) check By Sms Brand checkbox$")
	public void check_smsbrand_optin() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkbySmsBrandOptin();

	}

	@And("^(?:I|i) check By Sms All checkbox$")
	public void check_smsall_optin() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkbySmsAllOptin();

	}

	@And("^(?:I|i) check By Post Brand checkbox$")
	public void check_postbrand_optin() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkByMailBrandOptin();

	}

	@And("^(?:I|i) check By Post All checkbox$")
	public void check_postall_optin() {
		this.actions.Wait.waitForPageReady();
		this.formsPage.checkByMailAllOptin();

	}

	@And("^(?:I|i) enter date of birth as \"([^\"]*)\"$")
	public void enter_dateBirth(String Date) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterDateOfBirth(Date);

	}
	
	@And("^(?:I|i) enter expiry date as \"([^\"]*)\"$")
	public void expirydate(String ExpiryDate) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterExpiryDate(ExpiryDate);

	}
	
	@And("^(?:I|i) enter date of purchase as \"([^\"]*)\"$")
	public void dateofpurchase(String DateOfPurchase) {
		this.actions.Wait.waitForPageReady();
		this.formsPage.enterDateOfPurchase(DateOfPurchase);

	}

	@And("^(?:I|i) click on the signup Close button$")
	public void click_Signup_Close_button() throws Throwable {
		WebElement SignUpClosebutton = this.formsPage.getCloseButtonForms();
		Assert.assertNotNull(SignUpClosebutton,
				"SignUpClosebutton is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("SignUpClosebutton is present on page");
		actions.Click.clickAt(SignUpClosebutton, 5, 5);
		//SignUpClosebutton.click();
	}
	

}

