package steps.business.generic;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import framework.shared.RandomUtil;
import steps.generic.keywords.World;
//import steps.generic.SetCapabilities;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.BackToTopPage;
import unilever.pageobjects.platform.publish.components.definitions.ListingArticle;

public class StepsListingArticle {
	UserActions actions;
	//SetCapabilities capabilities;
	World capabilities;
	PublishedPageCommon publishedPage;
	BackToTopPage backtotopPage;
	ListingArticle listingarticlePage;

	public StepsListingArticle(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.backtotopPage = new BackToTopPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.listingarticlePage = new ListingArticle(capabilities.getWebDriver(),capabilities.getBrandName());

	}
	
	@And("^(?:I|i) click on the listing article image$")
	public void click_related_article_image() {
		WebElement articleImage = this.listingarticlePage.getListingArticleImages();
		Assert.assertNotNull(articleImage,
				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
				articleImage.getAttribute("href"));
		articleImage.click();
		this.capabilities.reportStepInfo("related article image has been clicked");

	}

	@Then("^it should take me to the corresponding page of listing article image$")
	public void verify_listing_article_image() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext()
				.getScenarioData("listingarticleimagehref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related article cta href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
		this.capabilities.reportStepInfo("You have been taken to the corresponding page");
	}

	@Then("^(?:I|i) verify that listing article component image is rendering$")
	public void i_verify_that_listingarticle_component_image_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement listingarticleImage = this.listingarticlePage.getListingArticleImages();
		Assert.assertNotNull(listingarticleImage,
				"listing article image is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("listing article Image is present on page");
	}

	@Then("^(?:I|i) verify that listing article component heading is appearing$")
	public void i_verify_that_listingarticle_component_heading_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement listingarticleHeading = this.listingarticlePage.getListingArticleHeading();
		Assert.assertNotNull(listingarticleHeading,
				"listing article heading is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("listing article heading is present on page");
	}

	@Then("^(?:I|i) verify that listing article component description is appearing$")
	public void i_verify_that_listingarticle_component_description_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement listingarticleDescription = this.listingarticlePage.getListingArticleDescription();
		Assert.assertNotNull(listingarticleDescription, "listing article description is not rendering for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("listing article description is present on page");
	}

}
