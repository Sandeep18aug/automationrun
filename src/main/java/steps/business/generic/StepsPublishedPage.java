package steps.business.generic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.BackToTopPage;

public class StepsPublishedPage {

	UserActions actions;
	World capabilities;
	PublishedPageCommon publishedPage;
	BackToTopPage backtotopPage;

	public StepsPublishedPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.backtotopPage = new BackToTopPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@And("^(?:I|i) see the structure of the \"([^\"]*)\"$")
	public void verifyPage(String page) {

	}

	@And("^(?:I|i) verify that (?:one|either) of the following components \"([^\"]*)\" exists on the \"([^\"]*)\"$")
	public void verifyEitherComponent(String components, String pageName) {
		actions.Wait.waitForPageReady();
		String[] componentNames = components.split(";");
		for (String componentName : componentNames) {
			if (this.publishedPage.isComponentPresent(componentName)) {
				System.out.println(componentName + " component is present on the page");
				return;
			}
		}
		Assert.fail("none of the following components " + components + "  is present on page : " + pageName);
	}

	@And("^(?:I|i) verify that one of the following integrations \"([^\"]*)\" exists on the \"([^\"]*)\"$")
	public void verifyintegrations(String integrations, String pageName) {
		actions.Wait.waitForPageReady();
		String[] integrationNames = integrations.split(";");
		for (String integrationName : integrationNames) {
			if (this.publishedPage.isIntegrationPresent(integrationName)) {
				System.out.println(integrationName + "integration is present");
				return;
			}
		}
		Assert.fail("none of the following integration" + integrations + "is present on the page : " + pageName);
	}

	@And("^(?:I|i) verify that only one of the following components \"([^\"]*)\" exists on the \"([^\"]*)\"$")
	public void verifyOnlyOneFromComponentListExists(String components, String pageName) {
		actions.Wait.waitForPageReady();
		List<String> componentExistsOnPage = new ArrayList<String>();
		String[] componentNames = components.split(";");
		for (String componentName : componentNames) {
			if (publishedPage.isComponentPresent(componentName)) {
				componentExistsOnPage.add(componentName);
			}
		}
		if (componentExistsOnPage.size() == 0) {
			Assert.fail("none of the following components " + components + "  is present on page : " + pageName);
		} else if (componentExistsOnPage.size() > 1) {
			Assert.fail("multiple components " + Arrays.toString(componentExistsOnPage.toArray())
					+ "  are present on page : " + pageName);
		} else {
			this.capabilities.reportStepInfo("Only following component is present on page - "
					+ Arrays.toString(componentExistsOnPage.toArray()));
		}
	}

	@And("^(?:I|i) verify that following component(?:s|) \"([^\"]*)\" exist(?:s|) on the \"([^\"]*)\"$")
	public void verifyAllComponents(String components, String pageName) {
		actions.Wait.waitForPageReady();
		String[] componentNames = components.split(";");
		boolean componentFlag = true;
		Set<String> strComponentsNotPresent = new HashSet<String>();
		for (String componentName : componentNames) {
			if (!publishedPage.isComponentPresent(componentName)) {
				componentFlag = false;
				strComponentsNotPresent.add(componentName);
			}
		}
		if (!componentFlag)
			Assert.fail("following components " + Arrays.toString(strComponentsNotPresent.toArray())
					+ " are not present on page : " + pageName);
	}

	@And("^(?:I|i) verify that either all or few of the following components exist on page:$")
	public void verifyAllOrFewOfTheComponents(String components) {
		actions.Wait.waitForPageReady();
		components = components.trim().replaceAll("^\"|\"$", "");
		List<String> componentNames = Arrays.asList(components.split(";"));
		boolean componentFlag = true;
		Set<String> strComponentsNotPresent = new HashSet<String>();
		List<WebElement> actualComponents = publishedPage.getComponentList();

		for (WebElement actualComponent : actualComponents) {
			String componentName = publishedPage.getComponentName(actualComponent);
			if (!componentNames.contains(componentName)) {
				componentFlag = false;
				strComponentsNotPresent.add(componentName);
			}
		}
		if (!componentFlag) {
			Assert.fail("following components " + Arrays.toString(strComponentsNotPresent.toArray())
					+ " present on page but not expected." + System.lineSeparator() + "Expected components are "
					+ components);
		}
	}

	@And("^(?:I|i) verify that global search is present on the page$")
	public void verifyGlobalSearchIsPresent() throws Throwable {
		actions.Wait.waitForPageReady();
		// WebElement element = actions.Element.getElement(lnkGlobalSearchBy);
		WebElement element = this.publishedPage.getGlobalNavigation().getGlobalSearchIcon();
		Assert.assertTrue(element != null, "global search icon is not present in globaln navigation");
		this.capabilities.reportStepInfo("Global Search is present on the page");
		actions.Click.click(element);
		Assert.assertTrue(this.publishedPage.getGlobalNavigation().getGlobalSearchTextBox() != null,
				"global search text box is not appearing on page");
		this.publishedPage.getGlobalNavigation().getGlobalSearchClose().click();
	}

	@And("^I accept Evidon cookies popup if present$")
	public void acceptEvidonCookies() throws Throwable {
		this.capabilities.reportStepInfo("Accepting Evidon Cookies Popup if present");
		this.publishedPage.acceptEvidonCookiesIfPresent();
		actions.Wait.wait("2");
		actions.Wait.waitForPageReady();
	}

	@And("^I expect that Evidon cookies popup is present and I accept it$")
	public void verifyAndAcceptEvidonCookies() throws Throwable {
		this.capabilities.reportStepInfo("Verifying and Accepting Evidon Cookies");
		this.publishedPage.getEvidonCookiesOkButton().click();
	}

	@And("^(?:I|i) search \"([^\"]*)\" text in global search")
	public void searchTextFromGlobalSearch(String searchText) throws Throwable {
		actions.Wait.waitForPageReady();
		// WebElement element = actions.Element.getElement(lnkGlobalSearchBy);
		WebElement element = this.publishedPage.getGlobalNavigation().getGlobalSearchIcon();
		Assert.assertTrue(element != null, "global search icon is not present in global navigation");
		this.capabilities.reportStepInfo("Global Search is present on the page");
		actions.Click.click(element);
		WebElement searchTextBox = this.publishedPage.getGlobalNavigation().getGlobalSearchTextBox();
		Assert.assertTrue(searchTextBox != null,
				"global search text box is not appearing on page");
		actions.Wait.wait("4");
		searchTextBox.sendKeys(searchText);
		this.publishedPage.getGlobalNavigation().getGlobalSearchButton().click();
		this.capabilities.reportStepInfo(
				searchText + " text was entered on global search textbox and successfully clicked in search button");
	}
	


	// this step is to be used if you need to handle special cases
	@And("^(?:I|i) search \"([^\"]*)\" text in global search for \"([^\"]*)\" brand")
	public void searchTextFromGlobalSearch(String searchText, String brandName) throws Throwable {
		this.actions.Wait.waitForPageReady();
		// WebElement element = actions.Element.getElement(lnkGlobalSearchBy);
		if (brandName.toLowerCase().contains("ponds")) {
			WebElement element = this.publishedPage.getGlobalNavigation().getGlobalSearchIcon(brandName);
			Assert.assertTrue(element != null, "global search icon is not present in globaln navigation");
			this.capabilities.reportStepInfo("Global Search is present on the page");
			this.actions.Click.click(element);
			Assert.assertTrue(this.publishedPage.getGlobalNavigation().getGlobalSearchTextBox() != null,
					"global search text box is not appearing on page");
			this.publishedPage.getGlobalNavigation().getGlobalSearchTextBox().sendKeys(searchText);
			this.publishedPage.getGlobalNavigation().getGlobalSearchButton().click();
		}
	}

	@And("^(?:I|i) verify suggestions are displayed for \"([^\"]*)\" text in global search")
	public void searchSuggestionsFromGlobalSearch(String searchText) throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement element = this.publishedPage.getGlobalNavigation().getGlobalSearchIcon();
		Assert.assertTrue(element != null, "global search icon is not present in globaln navigation");
		this.capabilities.reportStepInfo("Global Search is present on the page");
		this.actions.Click.click(element);
		Assert.assertTrue(this.publishedPage.getGlobalNavigation().getGlobalSearchTextBox() != null,
				"global search text box is not appearing on page");
		this.publishedPage.getGlobalNavigation().getGlobalSearchTextBox().sendKeys(searchText);
		Assert.assertTrue(this.publishedPage.getGlobalNavigation().getGlobalSearchSuggestions() != null,
				"suggestion for search text is not appearing on page");
		this.actions.Wait.wait("3");
	}

	@Then("^(?:i|I) should see video (?:link|icon|button) is present on the page$")
	public void i_should_see_video_link_is_present_on_the_page() throws Throwable{
		// Write code here that turns the phrase above into concrete actions
		Assert.assertNotNull(this.publishedPage.getVideoIconButton(), "video icon is not present on page");
		this.capabilities.reportStepInfo("video icon is present in the page");
	}

	@When("^(?:i|I) click on video (?:link|icon|button)$")
	public void i_click_on_video_link() throws Throwable {
		this.publishedPage.clickVideoIconButton();
		this.capabilities.reportStepInfo("successfully clicked on video icon");
		this.actions.Wait.wait("5");
	}

	@Then("^(?:i|I) should see video is playing$")
	public void i_should_see_video_should_be_playing() throws ObjectNotFoundInORException {
		this.actions.Wait.waitForPageReady();
		// Write code here that turns the phrase above into concrete actions
		this.capabilities.getWebDriver().switchTo().defaultContent();
		this.publishedPage.switchToVideoIframe();
		WebElement video = this.publishedPage.getVideoObjectDiv();
		Assert.assertTrue(video.getAttribute("class").contains("playing-mode"), "video is not playing");
		this.capabilities.reportStepInfo("video is playing");
	}

	@Then("^(?:i|I) should see video on the page$")
	public void i_should_see_video_on_playing() throws ObjectNotFoundInORException {
		// Write code here that turns the phrase above into concrete actions
		this.capabilities.getWebDriver().switchTo().defaultContent();
		this.publishedPage.switchToVideoIframe();
		WebElement video = this.publishedPage.getVideoObject();
		Assert.assertTrue(video != null, "video is not present");
		this.capabilities.reportStepInfo("video is playing");
	}

	@Then("^(?:i|I) should see video is not playing$")
	public void i_should_see_video_is_not_playing() throws ObjectNotFoundInORException {
		this.capabilities.getWebDriver().switchTo().defaultContent();
		this.publishedPage.switchToVideoIframe();
		WebElement video = this.publishedPage.getVideoObjectDiv();
		Assert.assertTrue(video.getAttribute("class").contains("paused-mode"), "video is playing");
		this.capabilities.reportStepInfo("video is not playing");
	}

	@Then("^(?:i|I) click on video$")
	public void clickOnVideo() throws Throwable {
		this.capabilities.getWebDriver().switchTo().defaultContent();
		this.publishedPage.switchToVideoIframe();
		WebElement video = this.publishedPage.getVideoObject();
		this.actions.Click.clickByActionsClass(video);
		this.capabilities.reportStepInfo("successfully clicked video");
		this.actions.Wait.wait("3");
	}
	
	@And("^i see feedback diolog pops up and i close it$")
	public void closeFeedBackPopup() throws ObjectNotFoundInORException {
		this.publishedPage.getCloseButtonForFeedbackPopup().click();
	}


	@And("^(?:I|i) should see the sign up link in the global navigation$")
	public void verifyVisibilityOfSignUpLink() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		Assert.assertTrue(this.publishedPage.isSignUpLinkVisible() == true, 
				"Sign up link isn't visible in page.");
	}
	
	@And("^(?:I|i) should see the sign up link in header$")
	public void verifyVisibilityOfSignUpLinkinheader() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToTop();
		Assert.assertTrue(this.publishedPage.isSignUpLinkVisible() == true, 
				"Sign up link isn't visible in page.");
	}
	
	@And("^(?:I|i) should see the contact US link in the global navigation$")
	public void verifyContactUsLinkispresent() {
		this.actions.Wait.waitForPageReady();
//		this.publishedPage.contactUslinkvisible();
		// Assert.assertTrue(this.publishedPage.getErrorObject() != null, "this is not
		// the higene page " + this.publishedPage.driver.getCurrentUrl());
	}
	
	@And("^(?:I|i) should see the smartLabel link in the global navigation$")
	public void verifysmartLabelLinkispresent() {
		this.actions.Wait.waitForPageReady();
//		this.publishedPage.smartLabellinkvisible();
		// Assert.assertTrue(this.publishedPage.getErrorObject() != null, "this is not
		// the higene page " + this.publishedPage.driver.getCurrentUrl());
	}
	
	@And("^(?:I|i) click on the smartLabel link in the global navigation$")
	public void clickSmartLabel()
	{
		this.actions.Wait.waitForPageReady();
		WebElement SmartLabelButtonTazo = this.publishedPage.smartLabellinkvisible();
		this.capabilities.getScenarioContext().addScenarioData("smartlabelhref",
				SmartLabelButtonTazo.getAttribute("href"));
		this.publishedPage.smartLabellinkvisible().click();
		this.capabilities.reportStepInfo("Successfully clicked on Smart Label");
	}
	

	@And("^(?:I|i)t should take me to smart label page$")
	public void verifySmartLabel()
	{
		this.actions.Wait.waitForPageReady();
		Assert.assertTrue(capabilities.getWebDriver().getCurrentUrl().contains("smartlabel"),"the current page is not smart label page");
		this.capabilities.reportStepInfo("successfully Verified the SmartLabel link");
	}


//	@And("^(?:I|i) click (?:on a|on) the back to top CTA$")
//	public void clickBackToTopCTA() throws NumberFormatException, InterruptedException {
//		this.actions.Wait.wait("2");
//		WebElement cta = this.backtotopPage.CTABackToTopvisible();
//		Assert.assertNotNull(cta,
//				"back to top cta is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
//		this.capabilities.reportStepInfo("back to top cta is present on page");
//		this.capabilities.getScenarioContext().addScenarioData("backtotopctahref", cta.getAttribute("href"));
//		cta.click();
//		this.capabilities.reportStepInfo("sucessfully clicked on Back To Top CTA");
//		this.actions.Wait.wait("2");
//	}

	@And("^(?:I|i) click on the sign up link in the global navigation$")
	public void clickSignUpLink() throws ObjectNotFoundInORException {
		this.actions.Wait.waitForPageReady();
		WebElement signUpLink = this.publishedPage.getSignUpLink();
		if (signUpLink == null)
			Assert.fail("Sign up link isn't available in page.");
		else {
			signUpLink.click();
			this.capabilities.reportStepInfo("successfully clicked on signup link");
		}
	}
	
	@And("^(?:I|i) click on the contact us link in the global navigation$")
	public void clickContactUSLink() throws NumberFormatException, InterruptedException {
	this.actions.Wait.waitForPageReady();
		WebElement contactusButtonTazo = this.publishedPage.contactUslinkvisible();
  	this.capabilities.getScenarioContext().addScenarioData("contactushref", contactusButtonTazo.getAttribute("href"));
		this.publishedPage.contactUslinkvisible().click();
		this.capabilities.reportStepInfo("successfully clicked on contact US link");
		 this.actions.Wait.wait("3");
	}

	@And("^(?:I|i)t should take me to sign up page$")
	public void verifysignuptitle() throws ObjectNotFoundInORException {
		this.actions.Wait.waitForPageReady();
		this.publishedPage.titleSignUplinkvisible();
		this.capabilities.reportStepInfo("successfully Verified the signup link");
		// this.actions.Wait.wait("3");
	}
	
	@And("^(?:I|i)t should take me to contact us page$")
	public void verifycontactusLink() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("contactushref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl), "the page is not navigated to expected url. contact us href was "
				+ expectedUrl + System.lineSeparator() + " actual url is " + actualUrl);
	}
	
	@And("^(?:I|i) verify that I am able to interact with the integration$")
	public void verifyintegrateditems() throws Throwable{
		this.actions.Wait.waitForPageReady();
		this.publishedPage.OlpaicSlidervisible();
		this.backtotopPage.CTABackToTopvisible();
		this.capabilities.reportStepInfo("integarted items are visible");
		// this.actions.Wait.wait("3");
	}

	@And("^(?:I|i) verify that count of component \"([^\"]*)\" on this page is (\\d+)$")
	public void verifyintegrateditems(String ComponantName, int count) {
		this.actions.Wait.waitForPageReady();
		int Compcount = this.publishedPage.getComponentCountOnPage(ComponantName);
		Assert.assertEquals(count, Compcount, "Component Count doesn't matches");
	}

	@And("^(?:I|i) click on the submit button on Forms Page$")
	public void click_submit_button() throws Throwable {
		int currentTotalOpenedWindow = this.capabilities.getWebDriver().getWindowHandles().size();
		this.actions.SwitchWindow.switchTabByIndex(currentTotalOpenedWindow);
		WebElement submitbutton = this.publishedPage.getSubmitButton();
		Assert.assertNotNull(submitbutton,
				"submitbutton is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("submit button is present on page");
		this.capabilities.getScenarioContext().addScenarioData("submitbuttonhref", submitbutton.getAttribute("href"));
		this.actions.Wait.wait("4");
		//submitbutton.click();
		this.publishedPage.clickByJavaScript(submitbutton);
	}

	@And("^(?:I|i) verify Enquiry Error Message is visible on Forms page$")
	public void verifyenquiryMsg() throws Throwable {
		this.publishedPage.enquiryMsgvisible();
		this.capabilities.reportStepInfo("Enquiry field is visible on page");
	}
	
	
	@And("^(?:I|i) verify Email Address Error Message is visible on Forms page$")
	public void verifyemailadderrMsg() throws Throwable {
		this.publishedPage.emailAddErrMsgvisible();
		this.capabilities.reportStepInfo("Email Address Error Message is visible on page");
	}

	@And("^(?:I|i) verify First Name Error Message is visible on Forms page$")
	public void verifyfirstnameerrMsg() throws Throwable {
		WebElement firstNameErrorMsg = this.publishedPage.firstnameErrMsgvisible();
		Assert.assertNotNull(firstNameErrorMsg, "First Name Error Message is not visible on page");
		this.capabilities.reportStepInfo("First Name Error Message is visible on page");
	}

	@And("^(?:I|i) verify Last Name Error Message is visible on Forms page$")
	public void verifylastnameerrMsg() throws Throwable{
		WebElement lastNameErrorMsg = this.publishedPage.lastnameErrMsgvisible();
		Assert.assertNotNull(lastNameErrorMsg, "Last Name Error Message is not visible on page");
		this.capabilities.reportStepInfo("Last Name Error Message is visible on page");
	}

	@And("^(?:I|i) verify Address Error Message is visible on Forms page$")
	public void verifyaddresserrMsg() throws Throwable{
		WebElement addressErrorMsg = this.publishedPage.addressErrMsgvisible();
		Assert.assertNotNull(addressErrorMsg, "Address Error Message is not visible on page");
		this.capabilities.reportStepInfo("Address Error Message is visible on page");
	}

	@And("^(?:I|i) verify City Error Message is visible on Forms page$")
	public void verifycityerrMsg() throws Throwable{
		WebElement cityErrorMsg = this.publishedPage.cityErrMsgvisible();
		Assert.assertNotNull(cityErrorMsg, "City Error Message is not visible on page");
		this.capabilities.reportStepInfo("City Error Message is visible on page");
	}

	@And("^(?:I|i) verify Contact No Error Message is visible on Forms page$")
	public void verifycontactnumbererrMsg() throws Throwable{
		WebElement cityErrorMsg = this.publishedPage.contactNumberErrMsgvisible();
		Assert.assertNotNull(cityErrorMsg, "Contact No Error Message is not visible on page");
		this.capabilities.reportStepInfo("Contact No Error Message is visible on page");
	}

	@And("^(?:I|i) verify Age Consent Error Message is visible on Forms page$")
	public void verifyageconsenterrMsg() throws Throwable{
		WebElement ageConsentErrorMsg = this.publishedPage.ageConsentErrMsgvisible();
		Assert.assertNotNull(ageConsentErrorMsg, "Age Consent Error Message is not visible on page");
		this.capabilities.reportStepInfo("Age Consent Error Message is visible on page");
	}

	@And("^(?:I|i) verify Postal Code Error Message is visible on Forms page$")
	public void verifypostalcodeerrMsg() throws Throwable{
		WebElement postalCodeErrorMsg = this.publishedPage.postalCodeErrMsgvisible();
		Assert.assertNotNull(postalCodeErrorMsg, "Postal Code Error Message is not visible on page");
		this.capabilities.reportStepInfo("Postal Code Error Message is visible on page");
	}

	@And("^(?:I|i) verify Zip Code Error Message is visible on Forms page$")
	public void verifyZipcodeErrMsg() throws Throwable{
		WebElement zipCodeErrorMsg = this.publishedPage.zipCodeErrMsgvisible();
		Assert.assertNotNull(zipCodeErrorMsg, "Zip Code Error Message is not visible on page");
		this.capabilities.reportStepInfo("Zip Code Error Message is visible on page");
	}

	@And("^(?:I|i) verify DOB Error Message is visible on Forms page$")
	public void verifyDOB_ErrorMsg() throws Throwable{
		WebElement DOBErrorMsg = this.publishedPage.DOBErrMsgvisible();
		Assert.assertNotNull(DOBErrorMsg, "DOB Error Message is not visible on page");
		this.capabilities.reportStepInfo("DOB Error Message is visible on page");
	}

	@And("^(?:I|i) verify Birth Date Error Message is visible on Forms page$")
	public void verifybirthDateMsg() throws Throwable{
		WebElement birthDateErrorMsg = this.publishedPage.birthDateErrMsgvisible();
		Assert.assertNotNull(birthDateErrorMsg, "Birth Date Error Message is not visible on page");
		this.capabilities.reportStepInfo("Birth Date Error Message is visible on page");
	}

	@And("^(?:I|i) verify Bar Code Error Message is visible on Forms page$")
	public void verifybarcodeerrMsg() throws Throwable{
		WebElement barCodeErrorMsg = this.publishedPage.barCodeErrMsgvisible();
		Assert.assertNotNull(barCodeErrorMsg, "Bar Code Error Message is not visible on page");
		this.capabilities.reportStepInfo("Bar Code Error Message is visible on page");
	}

	@And("^(?:I|i) verify Product Error Message is visible on Forms page$")
	public void verifyproductErrorrMsg() throws Throwable{
		WebElement productErrorMsg = this.publishedPage.productErrMsgvisible();
		Assert.assertNotNull(productErrorMsg, "Product Error Message is not visible on page");
		this.capabilities.reportStepInfo("Product Error Message is visible on page");
	}

	@And("^(?:I|i) verify Manufacturing Code Error Message is visible on Forms page$")
	public void verifymanufacturingcodeerrMsg() throws Throwable{
		WebElement manCodeErrorMsg = this.publishedPage.manufacturingCodeErrMsgvisible();
		Assert.assertNotNull(manCodeErrorMsg, "Manufacturing Code Error Message is not visible on page");
		this.capabilities.reportStepInfo("Manufacturing Code Error Message is visible on page");
	}

	@And("^(?:I|i) verify Query Error Message is visible on Forms page$")
	public void verifyqueryErrorMsg() throws Throwable{
		WebElement queryErrorMsg = this.publishedPage.queryErrMsgvisible();
		Assert.assertNotNull(queryErrorMsg, "Query Error Message is not visible on page");
		this.capabilities.reportStepInfo("Query Error Message is visible on page");
	}
	
	//@And("^(?:I|i) verify all the contact us form links are accessible$")
	//public void verifyurls()
	//{
		//List<WebElemnt> Links = this.publishedPage.activeformlinks();
	   // Assert.assertTrue(Links.size()>0,"Links are present on the page");
	
		
	//}
	
	

	@And("^(?:I|i) verify Question Error Message is visible on Forms page$")
	public void verifyQuestionErrMsgvisible() throws Throwable{
		WebElement questionErrorMsg = this.publishedPage.questionErrMsgvisible();
		Assert.assertNotNull(questionErrorMsg, "Question Error Message is not visible on page");
		this.capabilities.reportStepInfo("Question Error Message is visible on page");
	}

	@Then("^(?:i|I) should see breadcrumb is present on the page$")
	public void i_should_see_breadcrumb_is_present() throws Throwable{
		Assert.assertNotNull(this.publishedPage.getbreadCrumbLinkVisible(), "breadcrumb is not present on page");
		this.capabilities.reportStepInfo("breadcrumb is present in the page");
	}
	
	@And("^(?:I|i) verify that count of component \"([^\"]*)\" on article page is (\\d+)$")
	public void verifyintegrateditemsById(String ComponantName, int count) {
		this.actions.Wait.waitForPageReady();
		int Compcount = this.publishedPage.getComponentCountOnPageById(ComponantName);
		Assert.assertEquals(count, Compcount, "Component Count doesn't matches");
	}
	
	@And("^(?:I|i) verify that count of component \"([^\"]*)\" on bwsLayout page is (\\d+)$")
	public void verifyintegrateditemsByClass(String ComponantName, int count) {
		this.actions.Wait.waitForPageReady();
		int Compcount = this.publishedPage.getComponentCountOnPageByClass(ComponantName);
		Assert.assertEquals(count, Compcount, "Component Count doesn't matches");
	}
	
	@And("^(?:I|i) verify that count of component \"([^\"]*)\" on vaseline page is (\\d+)$")
	public void verifyintegrateditemsForVaseline(String ComponantName, int count) {
		this.actions.Wait.waitForPageReady();
		int Compcount = this.publishedPage.getComponentCountOnPageForVaseline(ComponantName);
		Assert.assertEquals(count, Compcount, "Component Count doesn't matches");
	}
	
	
}
