package steps.business.generic;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.util.Strings;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.FeaturedContent;
import unilever.pageobjects.platform.publish.homepage.HomePage;

public class StepsHomePage {

	UserActions actions;
	World capabilities;
	HomePage homePage;
	FeaturedContent featuredcontent;

	public StepsHomePage(World capabilities, UserActions getBasicKeywords) {
		this.actions = getBasicKeywords;
		this.capabilities = capabilities;
		this.homePage = new HomePage(capabilities.getWebDriver(), capabilities.getBrandName());
		this.featuredcontent = new FeaturedContent(capabilities.getWebDriver(), capabilities.getBrandName());

	}

	@Then("^I expect global navigation has tabs$")
	public void verifyTabsCount() throws Throwable {
		actions.Wait.waitForPageReady();
		int tabCount = this.homePage.getGlobalNavigation().getNavigationTabsList().size();
		System.out.println(tabCount);
	}

//	@Then("^(?:I|i) (?:hover over|select|click on) the \"([^\"]*)\" tab of global footer$")
//	public void Click_GlobalFooter_TabByText(String tabText) throws Throwable {
//		this.actions.Wait.waitForPageReady();
//		this.actions.Scroll.scrollPublishedPageToBottom();
//		WebElement tabElement = homePage.getGlobalFooter().getFooterTabByText(tabText);
//		// actions.Hover.hover(tabElement);
//		actions.Click.click(tabElement);
//		System.out.println("successfully clicked on globalfootern tab " + tabText);
//	}

	@And("^(?:I|i) verify that the brand logo is present in the global navigation$")
	public void verify_brand_logo_globalnavigation() throws Throwable {
		actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		WebElement brandlogo = this.homePage.getGlobalNavigation().getGlobalNavLogo();
		Assert.assertNotNull(brandlogo,
				"Brand Logo is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Brand Logo is present in the global navigation");

	}

	@And("^(?:I|i) verify that the menu icon is present in the header$")
	public void i_verify_menu_icon_header() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement menuicon = this.homePage.getGlobalNavigation().getMenuIcon();
		Assert.assertNotNull(menuicon,
				"Menu icon is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Menu Icon is present in the global header");

	}

	@Then("^(?:I|i) click on menu icon in global header$")
	public void i_click_menu_icon() throws Throwable {
		WebElement menuiconglobalheader = this.homePage.getGlobalNavigation().getMenuIcon();
		Assert.assertNotNull(menuiconglobalheader, "menu icon is not appearing in global header");
		this.capabilities.reportStepInfo("menu icon is appearing in global header");
		// this.actions.Scroll.scrollPublishedPageToBottom();
		menuiconglobalheader.click();

	}

	@Then("^(?:I|i) expect the global navigation to have the following \"([^\"]*)\" tabs$")
	public void verifyGlobalNavigationTabs(String tabsString) throws Throwable {
		actions.Wait.waitForPageReady();
		// asserting that expected tab list string which will come from feature file as
		// parameter shouldn't be empty string
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(tabsString),
				"expected tabs list string should not be empty or null");
		// fetching tabs as web-element list
		List<WebElement> actualtabListWebElements = this.homePage.getGlobalNavigation().getNavigationTabsList();
		List<String> expectedTabList = Arrays.asList(tabsString.split(";"));
		String actualTabsString = "";
		List<String> actualTabList = new ArrayList<String>();
		for (WebElement actualTabWebElement : actualtabListWebElements) {
			String str = actualTabWebElement.getText().trim();
			if (!str.equals("")) {
				actualTabsString += actualTabWebElement.getText().trim() + ";";

				actualTabList.add(actualTabWebElement.getText().trim());
			}
		}
		System.out.println("Actual tabs are " + actualTabsString);
		String tabMismatch = "Tabs mismatch." + System.lineSeparator() + "Actual tabs are : " + actualTabsString
				+ System.lineSeparator() + "Expected tabs are : " + tabsString;
		// Assert.assertTrue(actualtabListWebElements.size() == expectedTabList.size(),
		// tabMismatch);
		Collections.sort(actualTabList);
		Collections.sort(expectedTabList);
		Assert.assertTrue(actualTabList.equals(expectedTabList), tabMismatch);
	}

	@Then("^(?:I|i) (?:hover over|select|click on) the \"([^\"]*)\" tab of global navigation$")
	public void hoverOverGlobalNavTabByText(String tabText) throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement = homePage.getGlobalNavigation().getNavigationTabByText(tabText);
		actions.Hover.hover(tabElement);
		actions.Click.click(tabElement);
		System.out.println("successfully clicked or hover on globalnavigation tab " + tabText);
	}

	@When("^(?:I|i) (?:hover over|select|click on) the tab1 of tabbed content$")
	public void clicktabbedcontent1() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement1 = featuredcontent.gettabbedcontent1();
		actions.Hover.hover(tabElement1);
		actions.Click.click(tabElement1);
		System.out.println("successfully clicked or hover on tabbed content1 ");
		actions.Wait.waitForPageReady();
	}

	@When("^(?:I|i) (?:hover over|select|click on) the tab2 of tabbed content$")
	public void clicktabbedcontent2() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement2 = featuredcontent.gettabbedcontent2();
		actions.Hover.hover(tabElement2);
		actions.Click.click(tabElement2);
		System.out.println("successfully clicked or hover on all tabbed content2 ");
		actions.Wait.waitForPageReady();
	}

	@When("^(?:I|i) (?:hover over|select|click on) the tab3 of tabbed content$")
	public void clicktabbedcontent3() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement3 = featuredcontent.gettabbedcontent3();
		actions.Hover.hover(tabElement3);
		actions.Click.click(tabElement3);
		System.out.println("successfully clicked or hover on all tabbed content3 ");
		actions.Wait.waitForPageReady();
	}

	@When("^(?:I|i) (?:hover over|select|click on) the tab4 of tabbed content$")
	public void clicktabbedcontent4() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement4 = featuredcontent.gettabbedcontent4();
		actions.Hover.hover(tabElement4);
		actions.Click.click(tabElement4);
		System.out.println("successfully clicked or hover on all tabbed content4 ");
		actions.Wait.waitForPageReady();
	}

	@When("^(?:I|i) (?:hover over|select|click on) the tab5 of tabbed content$")
	public void clicktabbedcontent5() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement5 = featuredcontent.gettabbedcontent5();
		actions.Hover.hover(tabElement5);
		actions.Click.click(tabElement5);
		System.out.println("successfully clicked or hover on tabbed content5 ");
		actions.Wait.waitForPageReady();
	}

	@When("^(?:I|i) (?:hover over|select|click on) the tab6 of tabbed content$")
	public void clicktabbedcontent6() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement6 = featuredcontent.gettabbedcontent6();
		actions.Hover.hover(tabElement6);
		actions.Click.click(tabElement6);
		System.out.println("successfully clicked or hover on tabbed content6 ");
		actions.Wait.waitForPageReady();
	}

	@Then("^(?:I|i) expect to see following submenus \"([^\"]*)\" under \"([^\"]*)\" tab$")
	public void verifyGlobalNavigationSubTabs(String subMenuString, String tabText) throws Throwable {
		actions.Wait.waitForPageReady();
		// asserting that expected tab list string which will come from feature file as
		// parameter shouldn't be empty string
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(subMenuString),
				"expected submenu list string should not be empty or null");
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(tabText), "tab text string should not be empty or null");
		// fetching tabs as web-element list
		List<WebElement> actualSubMenuList = this.homePage.getGlobalNavigation().getSubMenuListOfNavigationTab(tabText);
		List<String> expectedSubMenuList = Arrays.asList(subMenuString.split(";"));
		String actualSubMenuString = "";
		List<String> actualTabList = new ArrayList<String>();
		for (WebElement actualSubMenu : actualSubMenuList) {
			actualSubMenuString += actualSubMenu.getText().trim() + ";";
			if (!actualSubMenu.getText().trim().equals("")) {
				actualTabList.add(actualSubMenu.getText().trim());
			}
			// actualTabList.removeAll((Arrays.asList("", null)));
		}
		System.out.println("Actual submenus are " + actualSubMenuString);
		String submenuMismatchString = "Sub menu mismatch." + System.lineSeparator() + "Actual sub menus are : "
				+ actualSubMenuString + System.lineSeparator() + "Expected sub menus are : " + subMenuString;
		Assert.assertTrue(actualTabList.size() == expectedSubMenuList.size(), submenuMismatchString);
		Collections.sort(actualTabList);
		Collections.sort(expectedSubMenuList);
		Assert.assertTrue(actualTabList.equals(expectedSubMenuList), submenuMismatchString);
	}

	@Then("^(?:I|i) click on any of the submenu links of tab \"([^\"]*)\"$")
	public void clickAnySubMenuOfTab(String tabText) throws Throwable {
		actions.Wait.waitForPageReady();
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(tabText), "Tab text is not mentioned in the step");
		List<WebElement> actualSubMenuList = this.homePage.getGlobalNavigation().getSubMenuListOfNavigationTab(tabText);
		Assert.assertTrue(!actualSubMenuList.isEmpty(), "No submenu present in tab " + tabText);
		int randomNumber = new Random().nextInt(actualSubMenuList.size());
		WebElement submenu = actualSubMenuList.get(randomNumber);
		String submenutext = submenu.getText();
		System.out.println("clicking on submenu " + submenutext);
		String href = submenu.findElement(By.xpath("./a[@href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("submenutext", submenu.getText());
		capabilities.getScenarioContext().addScenarioData("submenuhref", href);
		actions.Hover.hover(submenu);
		// actions.Click.clickAt(submenu, 5, 5);
		actions.Click.clickByActionsClass(submenu);
		// submenu.click();
		System.out.println("clicked on submenu " + submenutext);
	}

	@Then("^(?:I|i) click on \"([^\"]*)\" submenu link of tab \"([^\"]*)\"$")
	public void clickOnSubMenuOfTab(String subMenuText, String tabText) throws Throwable {
		actions.Wait.waitForPageReady();
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(tabText), "Tab text is not mentioned in the step");
		WebElement submenu = this.homePage.getGlobalNavigation().getSubMenuOfNavigationTab(tabText, subMenuText);
		Assert.assertTrue(submenu != null, "No submenu with text " + subMenuText + " present in tab " + tabText);
		System.out.println("clicking on submenu " + submenu.getText());
		WebElement a = submenu.findElement(By.xpath(".//a[@href]"));
		String href = a.getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("submenutext", subMenuText);
		capabilities.getScenarioContext().addScenarioData("submenuhref", href);
		if (this.capabilities.getBrandName().trim().contains("lbp")
				|| this.capabilities.getBrandName().trim().contains("lhp")
				|| this.capabilities.getBrandName().trim().contains("apothe")) {
			// WebElement span = a.findElement(By.tagName("span"));
			Actions actions = new Actions(capabilities.getWebDriver());
			actions.click(a).build().perform();
			// this.actions.Click.clickByActionsClass(span);
		} else {
			a.click();
			// this.homePage.getGlobalNavigation().clickOnSubMenuOfNavigationTab(tabText,
			// subMenuText);
		}
		this.actions.Wait.wait("3");
	}

	@Then("^(?:I|i) click on \"([^\"]*)\" (?:option|link) (?:of|under) \"([^\"]*)\" submenu link of tab \"([^\"]*)\"$")
	public void clickOnSubMenuOfTab(String tier3String, String subMenuText, String tabText) throws Throwable {
		actions.Wait.waitForPageReady();
		Assert.assertTrue(Strings.isNotNullAndNotEmpty(tabText), "Tab text is not mentioned in the step");
		List<WebElement> actualSubMenuList = this.homePage.getGlobalNavigation().getSubMenuListOfNavigationTab(tabText);
		Assert.assertTrue(!actualSubMenuList.isEmpty(), "No submenu present in tab " + tabText);
		WebElement submenu = this.homePage.getGlobalNavigation().getSubMenuOfNavigationTab(tabText, subMenuText);
		System.out.println("clicking on submenu " + submenu.getText());
		submenu.click();
		WebElement tier3Element = this.homePage.getGlobalNavigation().getTier3SubMenu(submenu, tier3String);

		String href = tier3Element.findElement(By.xpath("./a[@href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("submenutext", subMenuText);
		capabilities.getScenarioContext().addScenarioData("submenuhref", href);
		// this.homePage.getGlobalNavigation().clickOnSubMenuOfNavigationTab(tabText,
		// subMenuText);
		tier3Element.click();
	}

	@And("^(?:I|i) verify that the brand logo is present in the footer$")
	public void verify_brand_logo_footer() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement brandlogo = this.homePage.getGlobalFooter().getbrandlogo();
		Assert.assertNotNull(brandlogo,
				"Brand Logo is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Brand Logo is present in the global footer");

	}

	@And("^(?:I|i) verify that social share links are present$")
	public void verify_socialsharelinkpresent_footer() throws Throwable {
		actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		this.actions.Wait.wait("2");
		String href = "";
		List<WebElement> sociallinkItems = this.homePage.getGlobalFooter().getSocialIconList();
		Assert.assertNotNull(sociallinkItems,
				"social share link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		sociallinkItems.forEach((temp) -> {
			System.out.println("Social share link " + temp.getText() + " is present");
		});
//		WebElement socialsharingfacebook = this.homePage.getGlobalFooter().getFBLink();
//		Assert.assertNotNull(socialsharingfacebook,
//
//
//				"social share link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
//		/*
//		WebElement socialsharingtwitter = this.homePage.getGlobalFooter().getTwitterLink();
//		Assert.assertNotNull(socialsharingtwitter,
//				"social share link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
//				*/
//		WebElement socialsharinginstagram = this.homePage.getGlobalFooter().getInstagramLink();
//		Assert.assertNotNull(socialsharinginstagram,
//
//				"social share insta link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
//
//		// following code block commented by Abhishek on 19th Dec since not applicable
//		// for lhp brand
//		/*
//		 * WebElement socialsharingtwitter =
//		 * this.homePage.getGlobalFooter().getTwitterLink();
//		 * Assert.assertNotNull(socialsharingtwitter,
//		 * "social share twitter link is not appearing for url " +
//		 * this.capabilities.getWebDriver().getCurrentUrl()); WebElement
//		 * socialsharingYoutube = this.homePage.getGlobalFooter().getYoutubeLink();
//		 * Assert.assertNotNull(socialsharingYoutube,
//		 * "social share youtube link is not appearing for url " +
//		 * this.capabilities.getWebDriver().getCurrentUrl());
//		 */
//
//		// following step info modified by Abhishek for lhp brand
//		// this.capabilities.reportStepInfo("Facebook , Twitter , Instagram and Mail are
//		// present in the global footer");
//		this.capabilities.reportStepInfo("Facebook , Instagram are present in the global footer");

	}

	@And("^(?:I|i) clicking on any social share link$")
	public void clickRandomSocialShareLinkInFooter() throws Throwable {

		actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		this.actions.Wait.wait("2");
		String href = "";
		List<WebElement> sociallinkItems = this.homePage.getGlobalFooter().getSocialIconList();
		if (sociallinkItems.size() > 0) {
			int randomItem = new Random().nextInt(sociallinkItems.size());
			WebElement element = sociallinkItems.get(randomItem);
			href = element.getAttribute("href");
			this.capabilities.getScenarioContext().addScenarioData("sociallinkhref", href);
			element.click();
			this.actions.Wait.wait("2");
		} else {
			Assert.fail("social share link has not been clicked");
		}
		this.capabilities.reportStepInfo("clicking on social share link with url - " + href);
	}

	@And("^(?:I|i) click on social share link with title \"([^\"]*)\"$")
	public void clickSocialShareLinkInFooter(String socialShareText) throws Throwable {

		actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		this.actions.Wait.wait("2");
		List<WebElement> sociallinkItems = this.homePage.getGlobalFooter().getSocialIconList();
		sociallinkItems.stream().filter(x -> x.getAttribute("title").equalsIgnoreCase(socialShareText)).findFirst()
				.get().click();
		this.capabilities.reportStepInfo("clicked on social share link with title - " + socialShareText);
	}

	@Then("^(?:I|i)t takes me to the corresponding social site page$")
	public void verify_socialsharelink_Page() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.SwitchWindow.switchTabByIndex(2);
		String expectedBaseUrl = homePage.getBaseURLAndSubPath(
				(String) this.capabilities.getScenarioContext().getScenarioData("sociallinkhref"));
		String actualBaseUrl = homePage.getBaseURLAndSubPath(capabilities.getWebDriver().getCurrentUrl());
		expectedBaseUrl = expectedBaseUrl.endsWith("/") ? expectedBaseUrl.substring(0, expectedBaseUrl.length() - 1)
				: expectedBaseUrl;
		actualBaseUrl = actualBaseUrl.endsWith("/") ? actualBaseUrl.substring(0, actualBaseUrl.length() - 1)
				: actualBaseUrl;

		Assert.assertTrue(
				expectedBaseUrl.equalsIgnoreCase(actualBaseUrl)
						&& homePage.getSocialSharing().isSocialMediaPageDisplayed(actualBaseUrl),
				"The page is not navigated to expected base url or the social media page isn't as per the opened URL. Social link href was "
						+ expectedBaseUrl + System.lineSeparator() + " actual url is " + actualBaseUrl);
	}

	@Then("^(?:I|i)t takes me to the social site with url having \"([^\"]*)\"$")
	public void verifySocialSharelinkPage(String url) throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.SwitchWindow.switchTabByIndex(2);
		Assert.assertTrue(capabilities.getWebDriver().getCurrentUrl().contains(url),
				"The purrent page is not having '" + "url" + "' text in its url.");
	}

	@And("^(?:I|i) click on Pinterest social share Link$")
	public void click_Pinterest() throws Throwable {
		WebElement PinterestSocialShare = this.homePage.getGlobalFooter().getPinterestLink();
		Assert.assertNotNull(PinterestSocialShare, "Pinterest Social Share button  is not appearing for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Pinterest Social Share button  is present on page");
		this.capabilities.getScenarioContext().addScenarioData("Pinteresthref",
				PinterestSocialShare.getAttribute("href"));
		PinterestSocialShare.click();
	}

	@Then("^(?:I|i)t should take me to Pinterest social share page$")
	public void verify_Pinterest() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.SwitchWindow.switchTabByIndex(2);
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("Pinteresthref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. Pinterest href was " + expectedUrl + System.lineSeparator()
						+ " actual url is " + actualUrl);
	}

	@And("^(?:I|i) click on Facebook social share Link$")
	public void click_facebook() throws Throwable {
		WebElement fbSocialShare = this.homePage.getGlobalFooter().getFBLink();
		Assert.assertNotNull(fbSocialShare,
				"fb Social Share button  is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("fb Social Share button is present on page");
		this.capabilities.getScenarioContext().addScenarioData("fbhref", fbSocialShare.getAttribute("href"));
		fbSocialShare.click();
	}

	@Then("^(?:I|i)t should take me to Facebook social share page$")
	public void verify_facebook() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.SwitchWindow.switchTabByIndex(2);
//		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("fbhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(actualUrl.contains("facebook"),
				"the page is not navigated to facebook url. but navigated to " + actualUrl);
	}

	@And("^(?:I|i) click on Instagram social share Link$")
	public void click_Instagram() throws Throwable {
		WebElement InstagramSocialShare = this.homePage.getGlobalFooter().getInstagramLink();
		Assert.assertNotNull(InstagramSocialShare, "Instagram Social Share button  is not appearing for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Instagram Social Share button  is present on page");
		this.capabilities.getScenarioContext().addScenarioData("Instagramhref",
				InstagramSocialShare.getAttribute("href"));
		InstagramSocialShare.click();
	}

	@Then("^(?:I|i)t should take me to Instagram social share page$")
	public void verify_Instagram() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.SwitchWindow.switchTabByIndex(2);
		// String expectedUrl = (String)
		// this.capabilities.getScenarioContext().getScenarioData("Instagramhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(actualUrl.contains("instagram"),
				"the page is not navigated to Instagram url. but navigated to" + actualUrl);
	}

	@And("^(?:I|i) click on Twitter social share Link$")
	public void click_Twitter() throws Throwable {
		WebElement TwitterSocialShare = this.homePage.getGlobalFooter().getTwitterLink();
		Assert.assertNotNull(TwitterSocialShare, "Instagram Social Share button  is not appearing for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Twitter Social Share button  is present on page");
		this.capabilities.getScenarioContext().addScenarioData("Twitterhref", TwitterSocialShare.getAttribute("href"));
		TwitterSocialShare.click();
	}

	@Then("^(?:I|i)t should take me to Twitter social share page$")
	public void verify_Twitter() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.SwitchWindow.switchTabByIndex(2);
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("Twitterhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(actualUrl.contains("twitter"),
				"the page is not navigated to twitter url. but navigated to" + actualUrl);
	}

	@And("^(?:I|i) verify that country selector image is present in the footer$")
	public void verify_countryselector_image_footer() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement countryselectorImage = this.homePage.getGlobalFooter().getcountryselectorimage();
		Assert.assertNotNull(countryselectorImage,
				"country selector Image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("country selector image is present in the global footer");

	}

	@Then("^(?:I|i) verify that unilever logo is appearing in the footer$")
	public void i_verify_unilever_logo_is_rendering() throws Throwable {

		this.actions.Scroll.scrollPublishedPageToBottom();
		WebElement unileverlogo = this.homePage.getGlobalFooter().getunilverlogo();
		Assert.assertNotNull(unileverlogo,
				"unilever logo is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("unilever logo is present on page");
		this.actions.Wait.wait("4");
	}

	@Then("^(?:I|i) verify that disclaimer is appearing in the footer$")
	public void i_verify_disclaimer_is_rendering() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		this.actions.Scroll.scrollPublishedPageToBottom();
		WebElement disclaimer = this.homePage.getGlobalFooter().getdisclaimer();
		Assert.assertNotNull(disclaimer,
				"disclaimer is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("disclaimer is present on page");
	}

	@Then("^(?:I|i) verify that copyright icon is appearing in the footer$")
	public void i_verify_copyright_icon_is_rendering() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		this.actions.Scroll.scrollPublishedPageToBottom();
		WebElement copyright = this.homePage.getGlobalFooter().getcopyrighticon();
		Assert.assertNotNull(copyright,
				"copyright icon is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("copyright icon is present on page");
	}
	
	@Then("^(?:I|i) click on the contact us link of header$")
	public void i_click_contactus_link_of_header() {
		this.actions.Wait.waitForPageReady();
		WebElement contactusheader = this.homePage.getGlobalFooter().getContactusHeaderLink();
		Assert.assertNotNull(contactusheader,
				"contact us link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("contact us link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("contactuslinkhref", contactusheader.getAttribute("href"));
		this.homePage.clickByJavaScript(contactusheader);
	}
	
	
	
	@Then("^(?:I|i) click on the contact us link of global footer$")
	public void i_click_contactus_link() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		WebElement contactus = this.homePage.getGlobalFooter().getContactusLink();
		Assert.assertNotNull(contactus,
				"contact us link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("contact us link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("contactuslinkhref", contactus.getAttribute("href"));
		this.actions.Wait.wait("5");
		this.actions.Scroll.scrollPublishedPageToBottom();
		//contactus.click();
		this.homePage.clickByJavaScript(contactus);
	}

	@Then("^(?:I|i) click on contact us form menu link on helpcenter$")
	public void i_click_Contactus_form_menu_link_from_helpcenter() {
		this.actions.Wait.waitForPageReady();
		WebElement contactmenu = this.homePage.getGlobalFooter().getContactmenulink();
		Assert.assertNotNull(contactmenu, "Contact us form is not appearing under the menu for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Contact Us form link is present on page under the menu tab");
		this.homePage.clickByJavaScript(contactmenu);
	}
	
	@Then("^(?:I|i) click on contact us form link on helpcenter$")
	public void i_click_contactus_link_form_helpcenter() throws Throwable {
		this.actions.Wait.waitForPageReady();
		int currentTotalOpenedWindow = this.capabilities.getWebDriver().getWindowHandles().size();
		this.actions.SwitchWindow.switchTabByIndex(currentTotalOpenedWindow);
		WebElement contact = this.homePage.getGlobalFooter().getcontactlink();
		Assert.assertNotNull(contact, "Contact us form is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Contact Us form link is present on page");
		//contact.click();
		this.homePage.clickByJavaScript(contact);
	}

	@Then("^(?:I|i) click on the Help Center link$")
	public void i_click_helpcenter_link() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		WebElement helpcenter = this.homePage.getGlobalFooter().getHelpCenter();
		Assert.assertNotNull(helpcenter,
				"help-center link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Help Center link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("helpcenterlinkhref", helpcenter.getAttribute("href"));
		this.actions.Wait.wait("2");
		helpcenter.click();
		this.actions.Wait.wait("2");
	}

	@Then("^(?:I|i)t should take me to contact us form page$")
	public void verify_contactus_link_Page() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("contactuslinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. contact us link href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
	}

	@Then("^(?:I|i)t should take me to help center form page$")
	public void verify_helpcenter_link_Page() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("helpcenterlinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. help center link href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
	}

	@Then("^(?:I|i)t should take me to email us form page$")
	public void verify_emailus_link_Page() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("emailuslinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. help center link href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
	}

	@Then("^(?:I|i) click on the site map link of global footer$")
	public void i_click_site_map_link() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		WebElement siteMap = this.homePage.getGlobalFooter().getSiteMapLink();
		Assert.assertNotNull(siteMap,
				"site map link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("site map link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("sitemaplinkhref", siteMap.getAttribute("href"));
		this.actions.Wait.wait("4");
		siteMap.click();
	}

	@Then("^(?:I|i)t should take me to site map page$")
	public void verify_sitemap_link_Page() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("sitemaplinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. site map link href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
	}

	@And("^(?:I|i) click on SignUp Link is present in footer$")
	public void click_Signup_link_button_footer() throws Throwable {
		WebElement signupButtonFooter = this.homePage.getGlobalFooter().getSignUpLink();
		Assert.assertNotNull(signupButtonFooter,
				"SignUp button  is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("SignUp  is present on footer page");
		this.capabilities.getScenarioContext().addScenarioData("SignUpfooterhref",
				signupButtonFooter.getAttribute("href"));
		signupButtonFooter.click();
	}

	@Then("^(?:I|i) click on the terms of use link of global footer$")
	public void i_click_termsofuse_link() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		int totalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		WebElement termsofuse = this.homePage.getGlobalFooter().gettermsofuseLink();
		Assert.assertNotNull(termsofuse,
				"terms of use link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("terms of use link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("termsofuselinkhref", termsofuse.getAttribute("href"));
		this.actions.Wait.wait("2");
		termsofuse.click();
		this.actions.Wait.wait("2");
		int currentTotalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		if (totalOpenedWindows < currentTotalOpenedWindows) {
			this.actions.SwitchWindow.switchTabByIndex(currentTotalOpenedWindows);
		}
	}

	@Then("^(?:I|i)t should take me to terms of use page$")
	public void verify_termsofuse_link_Page() throws Throwable {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("termsofuselinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. terms of use link href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
	}

	@Then("^(?:I|i) click on the Cookie Policy link of global footer$")
	public void i_click_CookiePolicy_link() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		WebElement cookiepolicy = this.homePage.getGlobalFooter().getcookiePolicyLink();
		Assert.assertNotNull(cookiepolicy,
				"cookie Policy link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("cookie Policy link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("cookiePolicylinkhref",
				cookiepolicy.getAttribute("href"));
		this.actions.Wait.wait("4");
		cookiepolicy.click();
	}

	@Then("^(?:I|i)t should take me to Cookie Policy page$")
	public void verify_CookiePolicy_link_Page() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("cookiePolicylinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. cookie Policy link href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);

	}

	@And("^(?:I|i) click on Assesibility Link is present in footer$")
	public void click_Assesibility_link_button_footer() throws Throwable {
		WebElement AssessButtonFooter = this.homePage.getGlobalFooter().getAssesibilityLinkFooter();
		Assert.assertNotNull(AssessButtonFooter,
				"Assesibility button  is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Assesibility  is present on footer page");
		this.capabilities.getScenarioContext().addScenarioData("Assesibilityfooterhref",
				AssessButtonFooter.getAttribute("href"));
		AssessButtonFooter.click();
	}
	
	@Then("^(?:I|i) click on the FAQs link of global footer$")
	public void i_click_faq_link() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		//this.actions.Scroll.scrollTillTopOfElement(this.homePage.getGlobalFooter().getfaqSLink());
		WebElement faq = this.homePage.getGlobalFooter().getfaqSLink();
		Assert.assertNotNull(faq,
				"FAQ link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("FAQ link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("FAQlinkhref", faq.getAttribute("href"));
		this.actions.Wait.wait("4");
		faq.click();
	}

	@Then("^(?:I|i) click on the FAQs link present in header$")
	public void i_click_faq_link_citra() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToTop();
		//this.actions.Scroll.scrollTillTopOfElement(this.homePage.getGlobalFooter().getfaqSLink());
		WebElement faq = this.homePage.getGlobalFooter().getfaqSLink();
		Assert.assertNotNull(faq,
				"FAQ link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("FAQ link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("FAQlinkhref", faq.getAttribute("href"));
		this.actions.Wait.wait("4");
		faq.click();
	}

	@Then("^(?:I|i) click on the FAQs link for skip$")
	public void i_click_faq_link_skip() throws Throwable {
		this.actions.Wait.waitForPageReady();
		//this.actions.Scroll.scrollPublishedPageToTop();
		this.actions.Scroll.scrollTillTopOfElement(this.homePage.getGlobalFooter().getfaqSLink());
		WebElement faq = this.homePage.getGlobalFooter().getfaqSLink();
		Assert.assertNotNull(faq,
				"FAQ link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("FAQ link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("FAQlinkhref", faq.getAttribute("href"));
		this.actions.Wait.wait("4");
		faq.click();
	}
	
	
	@Then("^(?:I|i)t should take me to FAQs page$")
	public void verify_faq_link_Page() throws Throwable {
		this.actions.Wait.waitForPageReady();
		int currentTotalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		this.actions.SwitchWindow.switchTabByIndex(currentTotalOpenedWindows);

		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("FAQlinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl), "the page is not navigated to expected url. FAQ link href was "
				+ expectedUrl + System.lineSeparator() + " actual url is " + actualUrl);
	}

	@Then("^(?:I|i) click on the Privacy Policy link of global footer$")
	public void i_click_privacypolicy_link() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		int totalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		WebElement privacypolicy = this.homePage.getGlobalFooter().getprivacyPolicyLink();
		Assert.assertNotNull(privacypolicy,
				"Privacy Policy link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("privacy policy link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("privacypolicylinkhref",
				privacypolicy.getAttribute("href"));
		this.actions.Wait.wait("4");
		this.actions.Scroll.scrollPublishedPageToBottom();
		privacypolicy.click();
		int currentTotalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		if (totalOpenedWindows < currentTotalOpenedWindows) {
			this.actions.SwitchWindow.switchTabByIndex(currentTotalOpenedWindows);
		}
	}

	@Then("^(?:I|i)t should take me to Privacy Policy page$")
	public void verify_privacypolicy_link_Page() throws Throwable {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("privacypolicylinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. Privacy Policy link href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
	}

	@Then("^(?:I|i) click on the Ad Choice link$")
	public void i_click_AdChoice_link() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		int totalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		WebElement adchoice = this.homePage.getGlobalFooter().getAdChoiceLink();
		Assert.assertNotNull(adchoice,
				"adchoice link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("adchoice link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("adchoicelinkhref", adchoice.getAttribute("href"));
		this.actions.Wait.wait("4");
		adchoice.click();
		int currentTotalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		if (totalOpenedWindows < currentTotalOpenedWindows) {
			this.actions.SwitchWindow.switchTabByIndex(currentTotalOpenedWindows);
		}
	}

	@Then("^(?:I|i)t should take me to Ad Choice page$")
	public void verify_AdChoice_link_Page() throws Throwable {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("adchoicelinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. adchoice link href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
	}

	@Then("^(?:I|i)t should open an Ad Choice popup$")
	public void verify_AdChoice_popup() throws Throwable {
		this.actions.Wait.waitForPageReady();
		// this.actions.Frame.switchToFrameByWebelement("_ev_iframe");
		this.actions.Frame.switchToFrame("_ev_iframe");
		WebElement adChoicePopup = this.homePage.getGlobalFooter().getAdChoicePopup();
		Assert.assertNotNull(adChoicePopup, "Adchoice popup not appearing");
	}

	@Then("^(?:I|i) click on the Store Locator link$")
	public void i_click_StoreLocator_link() throws Throwable {
		WebElement storelocator = this.homePage.getGlobalFooter().getStoreLocatorLink();
		Assert.assertNotNull(storelocator,
				"store locator link is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("adchoice link is present on page");
		this.capabilities.getScenarioContext().addScenarioData("storelocatorlinkhref",
				storelocator.getAttribute("href"));
		this.actions.Wait.wait("4");
		storelocator.click();
	}

	@Then("^(?:I|i)t should take me to Store Locator page$")
	public void verify_StoreLocator_link_Page() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("storelocatorlinkhref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. Store Locator link href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
	}

	@And("^(?:I|i) verify that following links(?:s|) \"([^\"]*)\" are present in the global footer$")
	public void verifyAllComponents(String links) throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		this.actions.Wait.wait("2");
		String[] linkTextArray = links.split(";");
		boolean linkNotPresentFlag = true;
		Set<String> strLinksNotPresent = new HashSet<String>();
		for (String linkText : linkTextArray) {
			if (this.homePage.getGlobalFooter().getFooterNavigationLinkByText(linkText) == null) {
				linkNotPresentFlag = false;
				strLinksNotPresent.add(linkText);
			}
		}
		if (!linkNotPresentFlag)
			Assert.fail("following components " + Arrays.toString(strLinksNotPresent.toArray())
					+ " are not present in the global footer");
		else
			this.capabilities.reportStepInfo("expected links are present in global footer => " + links);
	}

	
	@And("^(?:I|i) click on the global footer link having text \"([^\"]*)\"$")
	public void clickGlobalFooterLinkByText(String linkText) throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		this.actions.Wait.wait("2");
		WebElement globalFooterLink = this.homePage.getGlobalFooter().getFooterNavigationLinkByText(linkText);
		Assert.assertNotNull(globalFooterLink, "There is not global footer link with text = " + linkText);
		globalFooterLink.click();
	}
	
	@Then("^(?:I|i) click on close button in global nav tab$")
	public void i_click_close_button() throws Throwable {
		WebElement closebuttonglobalnav = this.homePage.getCloseButtonGlobalNav();
		Assert.assertNotNull(closebuttonglobalnav, "close button is not appearing in global nav");
		this.capabilities.reportStepInfo("close button is appearing in global nav");
		closebuttonglobalnav.click();

	}

	@Then("^(?:I|i) click on brand logo in global footer$")
	public void i_click_brand_logo() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		WebElement brandlogoglobalfooter = this.homePage.getGlobalFooter().getbrandlogo();
		Assert.assertNotNull(brandlogoglobalfooter, "brand logo is not appearing in global footer");
		this.capabilities.reportStepInfo("brand logo is appearing in global footer");
		this.actions.Scroll.scrollPublishedPageToBottom();
		this.actions.Wait.wait("2");
		brandlogoglobalfooter.click();

	}

	@Then("^(?:I|i) click on brand logo in global navigation$")
	public void i_click_brand_logo_globalnavigation() throws Throwable {
		WebElement brandlogoglobalnavigation = this.homePage.getGlobalNavigation().getGlobalNavLogo();
		Assert.assertNotNull(brandlogoglobalnavigation, "brand logo is not appearing in global navigation");
		this.capabilities.reportStepInfo("brand logo is appearing in global navigation");
		brandlogoglobalnavigation.click();

	}

	@Then("^(?:I|i) verify FAQ heading$")
	public void i_verify_FAQ_link() throws Throwable {
		WebElement faqTitle = this.homePage.getGlobalFooter().getfaqTitle();
		Assert.assertNotNull(faqTitle, "Faq heading is not appearing for url ");
		this.capabilities.reportStepInfo("Faq heading is present in the FAQ");
	}

	@Then("^(?:I|i) click on the FAQ Question link$")
	public void i_click_FAQ_link() throws Throwable {
		WebElement faqExpandButton = this.homePage.getGlobalFooter().getfaqExpandButton();
		Assert.assertNotNull(faqExpandButton, "Faq Question link is not appearing for url ");
		this.capabilities.reportStepInfo("Faq Question link is present in the FAQ");
		faqExpandButton.click();
	}

	@Then("^(?:I|i)t should show me to the FAQ rich text$")
	public void verify_FAQ_richtext() throws Throwable {

		WebElement FaqRichText = this.homePage.getGlobalFooter().getFaqRichText();
		Assert.assertNotNull(FaqRichText, "Faq RichText is not appearing for url ");
		this.capabilities.reportStepInfo("Faq RichText is present in the FAQ");

	}

	@When("^(?:I|i) should be able to see Accordian on Collection Detail Page$")
	public void see_accordian_list() throws Throwable {
		this.actions.Wait.waitForPageReady();
		int accordianCDPlist = this.homePage.getAccordianList().size();
		this.capabilities.reportStepInfo("No of Accordian Present : " + accordianCDPlist);
		Assert.assertTrue(accordianCDPlist >= 0, "accordian is not present");

	}

	@When("^(?:I|i) verify expand and collapse functionality of Accordian$")
	public void verifyexpand_collapse_functionality() throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement accordion = this.homePage.getAccordianPanelByIndex(0);
		Assert.assertTrue(accordion.getAttribute("aria-expanded").contains("true"),
				"Accordion panel is not collapsed by default");
		this.capabilities.reportStepInfo("accordion is collapsed by default");
		accordion.click();
		Assert.assertTrue(accordion.getAttribute("aria-expanded").contains("true"),
				"Accordion panel is not expanded after clicking");
		this.capabilities.reportStepInfo("accordion is expanded after click");
		accordion.click();
		Assert.assertTrue(accordion.getAttribute("aria-expanded").contains("false"),
				"Accordion panel is not collapsed after clicking on expanded panel");
		this.capabilities.reportStepInfo("accordion is collapsed after click");

	}

	@Then("^(?:I|i) click on the global footer link with title \"([^\"]*)\"$")
	public void clickOnFooterLinkWithText(String linkText) {
		this.actions.Wait.waitForPageReady();
		this.homePage.getGlobalFooter().getSocialIconList();
	}

	@When("^(?:I|i) expand first accordion panel$")
	public void expandFirstAccordionPanel() throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement accordion = this.homePage.getAccordianPanelByIndex(0);
		if (accordion.getAttribute("class").contains("collapsed"))
			accordion.click();
		this.capabilities.reportStepInfo("first accordion panel is clicked");
	}

	@When("^(?:I|i) collapse first accordion panel$")
	public void collapseFirstAccordionPanel() throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement accordion = this.homePage.getAccordianPanelByIndex(0);
		if (!accordion.getAttribute("class").contains("collapsed"))
			accordion.click();
		this.capabilities.reportStepInfo("first accordion panel is clicked");
	}

	@When("^(?:I|i) expect that first accordion panel should be expanded$")
	public void verifyFirstAccordionPanelExpanded() throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement accordion = this.homePage.getAccordianPanelByIndex(0);
		Assert.assertTrue(accordion.getAttribute("aria-expanded").contains("true"),
				"First accordion panel is not expanded");
		this.capabilities.reportStepInfo("first accordion panel is expanded");
	}

	@When("^(?:I|i) expect that first accordion panel should be collapsed$")
	public void verifyFirstAccordionPanelCollapsed() throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement accordion = this.homePage.getAccordianPanelByIndex(0);
		Assert.assertTrue(accordion.getAttribute("aria-expanded").contains("false"),
				"First accordion panel is not collapsed");
		this.capabilities.reportStepInfo("first accordion panel is collapsed");

	}

	@When("^(?:I|i) click on any tabbed-content$")
	public void tabbedcontent() {
		this.actions.Wait.waitForPageReady();

	}

	@When("^(?:I|i) click on the back to top CTA$")
	public void clickBackToTopCta() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.homePage.clickBackToTopCTA();
		this.actions.Wait.wait("3");
		this.capabilities.reportStepInfo("successfully clicked on back to top cta");
	}

	@When("^(?:I|i) (expand|collapse) accordion panel (\\d+)$")
	public void expandOrCollapseAccordionPanel(String expandOrCollapse, int index) throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement accordion = this.homePage.getAccordianPanelByIndex(index - 1);
		if (expandOrCollapse.equalsIgnoreCase("expand")) {
			if (accordion.getAttribute("class").contains("collapsed") || !accordion.getAttribute("class").contains("is-active")) {
				WebElement accordionHeadingLink = this.homePage.webElementFactory.getElementWithInParent(accordion,
						By.xpath(".//a"));
				if (accordionHeadingLink != null) {
					accordionHeadingLink.click();
				} else {
					accordion.click();
				}
			}
			this.capabilities.reportStepInfo("accordion panel with index " + index + " is expanded");
		} else if (expandOrCollapse.equalsIgnoreCase("collapse")) {
			if (!accordion.getAttribute("class").contains("collapsed")) {
				WebElement accordionHeadingLink = this.homePage.webElementFactory.getElementWithInParent(accordion,
						By.xpath(".//a"));
				if (accordionHeadingLink != null) {
					accordionHeadingLink.click();
				} else {
					accordion.click();
				}
			}
			this.capabilities.reportStepInfo("accordion panel with index " + index + " is collapsed");
		}
	}

	@When("^(?:I|i) expect that accordion panel with index (\\d+) is (expanded|collapsed)$")
	public void verifyAccordionPanelExpandedOrCollapsed(int index, String expandOrCollapse) throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement accordion = this.homePage.getAccordianPanelByIndex(index - 1);
		if (expandOrCollapse.equalsIgnoreCase("expanded")) {
			Assert.assertTrue(!accordion.getAttribute("class").contains("collapsed"),
					"Accordion panel with index " + index + " is not expanded");
			this.capabilities.reportStepInfo("Accordion panel with index " + index + " is not expanded");
		} else if (expandOrCollapse.equalsIgnoreCase("collapsed")) {
			Assert.assertTrue(accordion.getAttribute("class").contains("collapsed"),
					"Accordion panel with index " + index + " is expanded");
			this.capabilities.reportStepInfo("Accordion panel with index " + index + " is expanded");

		}
	}

}
