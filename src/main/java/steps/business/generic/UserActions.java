package steps.business.generic;

import steps.generic.keywords.*;

public class UserActions {
	
	public World runner;
	public Launch Launch;
	public Click Click;
	public RefreshPage RefreshPage;
	public CloseBrowser CloseBrowser;
	public DeleteCookies DeleteCookies;
	public DragDrop DragDrop;
	public Enter Enter;
	public Hover Hover;
	public ResizeWindow ResizeWindow;
	public SelectText SelectText;
	public VerifyCSSAttribute VerifyCSSAttribute;
	public VerifyElementEnabled VerifyElementEnabled;
	public VerifyElementVisible VerifyElementVisible;
	public VerifyText VerifyText;
	public VerifyTitle VerifyTitle;
	public Check Check;
	//public WebElementFactory Element;
	public WindowHandling SwitchWindow;
	public Scroll Scroll;
	public RichTextEdit rteEdit;
	public Wait Wait;
	public FrameHandling Frame;
    public UserActions(RichTextEdit rteEdit,Scroll Scroll,WindowHandling SwitchWindow,World runner, Launch Launch, Click Click, RefreshPage RefreshPage, CloseBrowser CloseBrowser,
    		DeleteCookies DeleteCookies, DragDrop DragDrop, Enter Enter, Hover Hover, ResizeWindow ResizeWindow, SelectText SelectText,
    		VerifyCSSAttribute VerifyCSSAttribute, VerifyElementEnabled VerifyElementEnabled, VerifyElementVisible VerifyElementVisible,
    		VerifyText VerifyText, VerifyTitle VerifyTitle, Check Check, Wait Wait,FrameHandling Frame){
        this.runner = runner;
        this.Click = Click;
        this.Launch = Launch;
        this.RefreshPage = RefreshPage;
        this.CloseBrowser = CloseBrowser;
        this.DeleteCookies = DeleteCookies;
        this.DragDrop = DragDrop;
        this.Enter = Enter;
        this.Hover = Hover;
        this.ResizeWindow = ResizeWindow;
        this.SelectText = SelectText;
        this.VerifyCSSAttribute = VerifyCSSAttribute;
        this.VerifyElementEnabled = VerifyElementEnabled;
        this.VerifyElementVisible = VerifyElementVisible;
        this.VerifyText = VerifyText;
        this.VerifyTitle = VerifyTitle;
        this.Check = Check;
        //this.Element= new WebElementFactory(runner.getWebDriver(), runner.getBrandName());
        this.SwitchWindow = SwitchWindow;
        this.Scroll = Scroll;
        this.rteEdit = rteEdit;
        this.Wait = Wait;
        this.Frame=Frame;
    }
}
