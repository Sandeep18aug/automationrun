package steps.business.generic.pdp;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.pdp.ProductDetailPage;
import unilever.pageobjects.platform.publish.pdp.TurnTo;

public class StepsPdpTurnTo {

	UserActions actions;
	World capabilities;
	ProductDetailPage pdpPage;
	// String pr

	public StepsPdpTurnTo(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.pdpPage = new ProductDetailPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}
	@Then("^(?:I|i) should be able to (?:see|select) \"([^\"]*)\" Tab$")
	public void i_should_be_able_to_see_Tab(String tabString) throws Throwable {
	    this.actions.Wait.waitForPageReady();
	    this.actions.Scroll.scrollToTopOfElement(this.pdpPage.getTabSection());
	    this.actions.Wait.wait("2");
	    this.actions.Scroll.scrollPublishedPageDownByPixels(200);
	    //this.actions.Scroll.scrollPublishedPageToBottom();
	    this.actions.Wait.wait("5");
	    WebElement tab = this.pdpPage.getTab(tabString);
	    Assert.assertNotNull(tab, tabString + " tab is not present on the page");
	    tab.click();
	    System.out.println(tabString + " tab is present and found");
	}
	
	@Then("^(?:I|i) should be able to expand turn to section$")
	public void i_should_be_able_to_expand_section() throws NumberFormatException, InterruptedException {
	    this.actions.Wait.waitForPageReady();
	    //this.actions.Scroll.scrollToTopOfElement(this.pdpPage.getTabSection());
	    //this.actions.Wait.wait("2");
	    //this.actions.Scroll.scrollPublishedPageDownByPixels(200);
	    //this.actions.Wait.wait("2");
//	    WebElement tab = this.pdpPage.getTab(tabString);
//	    Assert.assertNotNull(tab, tabString + " tab is not present on the page");
//	    tab.click();
	    this.pdpPage.expandTurnToSection();
	    this.capabilities.reportStepInfo("turn to section is expanded");
	}
	
	@Then("^(?:I|i) should be able to expand \"([^\"]*)\" section of turn to for brand \"([^\"]*)\"$")
	public void i_should_be_able_to_expand_accordion(String accordionheading, String brand) throws NumberFormatException, InterruptedException {
	    this.actions.Wait.waitForPageReady();
	    this.pdpPage.expandTurnToSection();
	    this.capabilities.reportStepInfo("turn to section is expanded");
	}

	@When("^(?:I|i) submit question \"([^\"]*)\"$")
	public void i_submit_question(String questionText) {
		this.actions.Wait.waitForPageReady();
		TurnTo turnTo = this.pdpPage.submitTurnToQuestion(questionText);
		
		String firstName = RandomStringUtils.randomAlphanumeric(10);
		turnTo.enterFirstName(firstName);
		this.capabilities.reportStepInfo("first name is entered as " + firstName);
		
		String lastName = RandomStringUtils.randomAlphanumeric(10);
		turnTo.enterLastName(lastName);
		this.capabilities.reportStepInfo("last name is entered as " + lastName);
		
		String email = RandomStringUtils.randomAlphanumeric(10) + "@gmai.com";	
		turnTo.enterEmail(email);
		this.capabilities.reportStepInfo("email is entered as " + email);
		
		String nickName = RandomStringUtils.randomAlphanumeric(10);	
		
		turnTo.enterNickName(nickName);
		this.capabilities.reportStepInfo("nickname is entered as " + nickName);
		
		//turnTo.submit();
		//this.capabilities.reportStepInfo("question is successfully submitted");
	}

//	@When("^(?:I|i) enter firstname as \"([^\"]*)\" in turn to$")
//	public void i_enter_firstname_as_in_turn_to(String string) {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new PendingException();
//	}
//
//	@When("^(?:I|i) enter lastname as \"([^\"]*)\" in turn to$")
//	public void i_enter_lastname_as_in_turn_to(String string) {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new PendingException();
//	}
//
//	@When("^(?:I|i) enter email address as \"([^\"]*)\" in turn to$")
//	public void i_enter_email_address_as_in_turn_to(String string) {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new PendingException();
//	}
//
//	@When("^(?:I|i) enter nickname as \"([^\"]*)\" in turn to$")
//	public void i_enter_nickname_as_in_turn_to(String string) {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new PendingException();
//	}
//
//	@When("^(?:I|i) submit turn to dialog$")
//	public void i_submit_turn_to_dialog() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new PendingException();
//	}

	@Then("^(?:I|i) should be able to see that question is successfully submitted$")
	public void i_should_be_able_to_see_that_question_is_successfully_submitted() {
	    this.actions.Wait.waitForPageReady();
	    //String message = this.pdpPage.getTurnToDialog().getSubmitMessageText();
	    //Assert.assertNotNull(message, "question is not successfully submitted");
	    
	    /* Comment by Avikash
	     * To avoid, repeated question submission, we have stopped submitting the turn to question.
	     * There were some conncerns shown by turn to team members that there are repeated questions being submitted 
	     * */
	}

}
