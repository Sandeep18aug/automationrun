package steps.business.generic.pdp;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.RandomUtil;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.pdp.ProductDetailPage;

public class StepsPdpAddToBag {

	UserActions actions;
	World capabilities;
	ProductDetailPage pdpPage;
	// String pr

	public StepsPdpAddToBag(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.pdpPage = new ProductDetailPage(capabilities.getWebDriver(), capabilities.getBrandName());
	}

	@Then("i should be able to see add to bag dialog")
	public void i_should_be_able_to_see_add_to_bag_dialog() {
		Assert.assertTrue(this.pdpPage.getAddToBag().isPresent(), "Add to bag dialog is not present");
	}

	@When("i choose \"([^\"]*)\" retailer in add to bag dialog")
	public void i_choose_retailer_in_add_to_bag(String retailer) throws Throwable {
		WebElement retailerElement = this.pdpPage.getAddToBag().getRetailerWebElement();
		if (retailerElement.getTagName().equalsIgnoreCase("select")) {
			this.actions.SelectText.selectRandomOption(this.pdpPage.getAddToBag().getRetailerListBox());
			this.actions.Wait.wait("1");
		} else if (retailerElement.getTagName().equalsIgnoreCase("md-select")) {
			retailerElement.click();
			this.actions.Wait.wait("2");
			List<WebElement> listRetailers = this.capabilities.getWebElementFactory()
					.getElements("pdp.addtobag.retailer.list.item");
			boolean elementFound = false;
			for(WebElement retailerItem:listRetailers) {
				if(retailerItem.getText().trim().equalsIgnoreCase(retailer)) {
					elementFound = true;
					retailerItem.click();
					break;
				}
			}
			Assert.assertTrue(elementFound, "No retailer option with text " + retailer);
			
		}
	}

	@When("i choose random retailer in add to bag dialog")
	public void i_choose_retailer_in_add_to_bag() throws Throwable {

		WebElement retailerElement = this.pdpPage.getAddToBag().getRetailerWebElement();
		if (retailerElement.getTagName().equalsIgnoreCase("select")) {
			this.actions.SelectText.selectRandomOption(this.pdpPage.getAddToBag().getRetailerListBox());
			this.actions.Wait.wait("1");
		} else if (retailerElement.getTagName().equalsIgnoreCase("md-select")) {
			retailerElement.click();
			this.actions.Wait.wait("2");
			List<WebElement> listRetailers = this.capabilities.getWebElementFactory()
					.getElements("pdp.addtobag.retailer.list.item");
			int listSize = listRetailers.size();
			int randomValue = RandomUtil.getRandomNumber(1, listSize - 1);
			listRetailers.get(randomValue).click();
			this.actions.Wait.wait("1");
		}

	}

	@When("i choose \"([^\"]*)\" size in add to bag dialog")
	public void i_choose_size_in_add_to_bag(String size) throws Throwable {
		WebElement sizeElement = this.pdpPage.getAddToBag().getSizeWebElement();
		if (sizeElement.getTagName().equalsIgnoreCase("select")) {
			this.actions.SelectText.selectByTextContainsTextIgnoreCase(size,
					this.pdpPage.getAddToBag().getSizeListBox());
			this.actions.Wait.wait("1");
		} else if (sizeElement.getTagName().equalsIgnoreCase("md-select")) {
			sizeElement.click();
			this.actions.Wait.wait("2");
			List<WebElement> listSize = this.capabilities.getWebElementFactory()
					.getElements("pdp.addtobag.size.list.item");
//			listSize.stream().filter((option) -> option.getText().trim().equalsIgnoreCase(size)).findFirst().get()
//					.click();
			
			boolean elementFound = false;
			for(WebElement sizeItem:listSize) {
				if(sizeItem.getText().trim().equalsIgnoreCase(size)) {
					elementFound = true;
					sizeItem.click();
					break;
				}
			}
			Assert.assertTrue(elementFound, "No size option with text " + size);
			this.actions.Wait.wait("1");
		}

	}

	@When("i choose random size in add to bag dialog")
	public void i_choose_random_size_in_add_to_bag() throws Throwable {

		WebElement sizeElement = this.pdpPage.getAddToBag().getSizeWebElement();
		if (sizeElement.getTagName().equalsIgnoreCase("select")) {
			this.actions.SelectText.selectRandomOption(this.pdpPage.getAddToBag().getSizeListBox());
			this.actions.Wait.wait("1");
		} else if (sizeElement.getTagName().equalsIgnoreCase("md-select")) {
			sizeElement.click();
			this.actions.Wait.wait("2");
			List<WebElement> listSize = this.capabilities.getWebElementFactory()
					.getElements("pdp.addtobag.size.list.item");
			int size = listSize.size();
			int randomValue = RandomUtil.getRandomNumber(1, size - 1);
			listSize.get(randomValue).click();
			this.actions.Wait.wait("1");
		}
	}

	@When("i choose \"([^\"]*)\" quantity in add to bag dialog")
	public void i_choose_quantity_in_add_to_bag(String quantity) throws Throwable {

		WebElement quantityElement = this.pdpPage.getAddToBag().getQuantityWebElement();
		if (quantityElement.getTagName().equalsIgnoreCase("select")) {
			this.actions.SelectText.selectByTextContainsTextIgnoreCase(quantity,
					this.pdpPage.getAddToBag().getQuantityListBox());
			this.actions.Wait.wait("1");
		} else if (quantityElement.getTagName().equalsIgnoreCase("md-select")) {
			quantityElement.click();
			this.actions.Wait.wait("2");
			List<WebElement> listQty = this.capabilities.getWebElementFactory()
					.getElements("pdp.addtobag.qty.list.item");
//			listQty.stream().filter((option) -> option.getText().trim().equalsIgnoreCase(quantity)).findFirst().get()
//					.click();
			
			boolean elementFound = false;
			for(WebElement qtyItem:listQty) {
				if(qtyItem.getText().trim().equalsIgnoreCase(quantity)) {
					elementFound = true;
					qtyItem.click();
					break;
				}
			}
			Assert.assertTrue(elementFound, "No quantity option with text " + quantity);
			this.actions.Wait.wait("1");
		}
	}

	@When("i choose random quantity in add to bag dialog")
	public void i_choose_random_quantity_in_add_to_bag() throws Throwable {
		WebElement quantityElement = this.pdpPage.getAddToBag().getQuantityWebElement();
		if (quantityElement.getTagName().equalsIgnoreCase("select")) {
			this.actions.SelectText.selectRandomOption(this.pdpPage.getAddToBag().getQuantityListBox());
			this.actions.Wait.wait("1");
		} else if (quantityElement.getTagName().equalsIgnoreCase("md-select")) {
			quantityElement.click();
			this.actions.Wait.wait("2");
			List<WebElement> listQty = this.capabilities.getWebElementFactory()
					.getElements("pdp.addtobag.qty.list.item");
			int size = listQty.size();
			int randomValue = RandomUtil.getRandomNumber(1, size - 1);
			//listQty.get(randomValue).click();
			this.pdpPage.clickByJavaScript(listQty.get(randomValue));
			this.actions.Wait.wait("1");
		}
	}

	@Then("i click on add to bag button in add to bag dialog")
	public void clickAddToBagButton() throws NumberFormatException, InterruptedException {
		this.pdpPage.getAddToBag().clickAddToBag();
		this.actions.Wait.wait("5");
	}

	@Then("i should be able to see shopping bag updated")
	public void i_should_be_able_to_see_shopping_bag_with_selected() throws Throwable {
		this.actions.Wait.waitForPageReady();
		String newShoppingBagText = this.pdpPage.getGlobalNavigation().getShoppingBagText();
		String previousShoppingBagText = (String) this.capabilities.getScenarioContext()
				.getScenarioData("shoppingbagdata");
		Assert.assertNotEquals(newShoppingBagText, previousShoppingBagText, "shopping bag is not updated as expected");
		this.capabilities.reportStepInfo("shopping bag is update successfully. earlier it was "
				+ previousShoppingBagText + " and now it is " + newShoppingBagText);
	}

	@When("^(?:i|I) click on shopping bag icon at header$")
	public void i_click_on_shopping_bag_icon_at_header() {
		this.pdpPage.getAddToBag().clickShoppingBagIcon();
		this.capabilities.reportStepInfo("successfully clicked on shopping bag icon at header");
	}

	@Then("^(?:i|I) should be able to see viewbag overlay$")
	public void i_should_be_able_to_see_viewbag_overlay() {
		Assert.assertTrue(this.pdpPage.getAddToBag().getViewBagOverlay().isDisplayed(),
				"ViewBag overlay is not present");
		this.capabilities.reportStepInfo("ViewBag overlay is displayed");
	}

	@And("^(?:i|I) click on viewbag button on viewbag overlay$")
	public void i_click_on_viewbag_button_on_viewbag_overlay() {
		this.pdpPage.getAddToBag().clickViewBagButton();
		this.capabilities.reportStepInfo("successfully clicked on viewbag button on viewbag overlay");
	}

}
