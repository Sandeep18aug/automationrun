package steps.business.generic.pdp;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.RandomUtil;
import framework.shared.StringUtils;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.brandspecific.CWLoginAndRegistrationPage;
import unilever.pageobjects.platform.publish.common.CountrySelector;
import unilever.pageobjects.platform.publish.pdp.ProductDetailPage;

public class StepsProductDetailPage {

	UserActions actions;
	World capabilities;
	ProductDetailPage pdpPage;
	// String pr

	public static String personalisationtext;

	public StepsProductDetailPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.pdpPage = new ProductDetailPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@And("^(?:I|i)t should take me to (?:a|the) (?:product detail|pdp) page$")
	public void verifyProductDetailPageLoads() {
		actions.Wait.waitForPageReady();
//		String selectedProductListingV2ItemUrl = (String) capabilities.getScenarioContext()
//				.getScenarioData("selectedproductlistingv2itemurl");
//		Assert.assertTrue(capabilities.getWebDriver().getCurrentUrl().equalsIgnoreCase(selectedProductListingV2ItemUrl),
//				"page hasn't navigated to correct url. " + selectedProductListingV2ItemUrl);
//		this.capabilities.reportStepInfo("page is successfully navigated to url - " + selectedProductListingV2ItemUrl);
		Assert.assertTrue(
				this.pdpPage.getComponent("product-images") != null
						|| this.pdpPage.getComponent("product-overview") != null,
				"this is not a product detail page because 'productImages or product overview' components are not present in this page");
	}

	@And("^i see domestos dialog pops up and i close it$")
	public void domestospopup()
	{
		this.pdpPage.getCloseButtonForDomestosPopUp().click();
		this.capabilities.reportStepInfo("Domestos cookie popup is successfully closed");
	}
	
	
	@And("^i see cleanipedia dialog pops up and i close it$")
	public void cleanipediapopup()
	{
		this.pdpPage.getCloseButtonForCleanipediaPopUp().click();
		this.capabilities.reportStepInfo("Closed Cleanipedia Popup");
	}
		
	@And("^i see hellmanns signup dialog pops up and i close it$")
	public void hellmannssignuppopup()
	{
		this.pdpPage.getCloseButtonForHellmannsSignupPopUp().click();
		this.capabilities.reportStepInfo("Closed hellmanns signup Popup");
	}
	
	
	@And("^i see breeze cleanipedia dialog pops up and i close it$")
	public void breezecleanipediapopup()
	{
		this.pdpPage.getBreezeCleanipediaPopUp().click();
		this.capabilities.reportStepInfo("Closed Breeze Cleanipedia Popup");
		
	}
	
	@And("^i close cookie policy for canada locale$")
	public void canadacookiepopup()
	{
		this.pdpPage.getCanadacookiepopup().click();
		this.capabilities.reportStepInfo("Successfully closed cookie popup");
	}
	
	@And("^i see sunsilk quiz dialog pops up and i close it$")
	public void quizpopup()
	{
		this.pdpPage.getCloseButtonForquizPopUp().click();
		this.capabilities.reportStepInfo("closed quiz popup");
	}
		
	@And("^i see cookie popup for omo brand and i close it$")
	public void omocookiepopup()
	{
		this.pdpPage.getomocookiepopup().click();
		this.capabilities.reportStepInfo("omo cookie policy is successfully closed");
	}
	
	@And("^i see cookie popup for magnum brand and i close it$")
	public void magnumcookiepopup()
	{
		this.pdpPage.getmagnumcookiepopup().click();
		this.capabilities.reportStepInfo("magnum cookie policy is successfully closed");
	}
	
	@And("^I see evidon cookie popup and i close it$")
	public void evidoncookiepopup()
	{
		this.pdpPage.getevidonCookiepopup().click();
		this.capabilities.reportStepInfo("Closed Evidon Cookie Pop Up");
	}
	
	@And("^I see Evidon cookie popup and i close it$")
	public void Evidoncookiepopup()
	{
		this.pdpPage.getEvidonCookiepopup().click();
		this.capabilities.reportStepInfo("Closed Evidon Cookie Pop Up");
	}
	
	@And("^I see site cookie popup and i close it$")
	public void sitecookiepopupclose()
	{
		this.pdpPage.getsitecookiepopup().click();
		this.capabilities.reportStepInfo("Cookie Popup is successfully closed");
	}
	
	@And("^(?:i|I) click on Write a Review link$")
	public void clickWriteAReview() throws Throwable, Exception {
		this.actions.Scroll.scrollPublishedPageDownByPixels(100);
		this.actions.Wait.wait("5");
		this.pdpPage.clickWriteAReviewButton();
	}

	

	@And("^(?:i|I) click on Write a Review link for comments section$")
	public void clickWriteAReviewFromCommentSection() {
		// if()
		this.pdpPage.clickWriteAReviewButtonFromCommentSection();
	}

	@And("^(?:i|I) should be able to see write a review dialog$")
	public void verifyWriteReviewPopUp() {
		Assert.assertTrue(this.pdpPage.getWriteAReviewDialog().isPresent(),
				"Write a Review Dialog in not visible on page");

	}

	@And("^(?:i|I) select overall rating as (\\d+)$")
	public void selectRatingStars(int starsIndex) {
		this.pdpPage.getWriteAReviewDialog().selectRatingsStar(starsIndex);

	}

	@And("^(?:i|I) give review title as \"([^\"]*)\"$")
	public void enterReviewTitleAs(String title) {
		this.pdpPage.getWriteAReviewDialog().enterReviewTitle(title);

	}

	@And("^(?:i|I) give detailed review as \"([^\"]*)\"$")
	public void enterReviewDetailedAs(String detailedReview) {
		this.pdpPage.getWriteAReviewDialog().enterReviewDescriptionText(detailedReview);

	}

	@And("^(?:i|I) enter random value in nickname$")
	public void enterRandomValueInNickName() {
		String randomText = RandomStringUtils.random(10, true, true);
		this.pdpPage.getWriteAReviewDialog().enterNickName(randomText);
	}

	@And("^(?:i|I) enter random email id for notification purposes$")
	public void enterRandomEmailAddress() {
		String randomText = RandomStringUtils.random(10, true, true);
		this.pdpPage.getWriteAReviewDialog().enterEmail(randomText + "@gmail.com");
	}

	@And("^(?:i|I) check age checkbox$")
	public void checkAgeCheckBox() {
		this.pdpPage.getWriteAReviewDialog().checkAgeCheckBox();
	}

	@And("^(?:i|I) recommend this product to friend$")
	public void recommendProduct() {
		this.pdpPage.getWriteAReviewDialog().getRecommendedYesObject().click();
	}

	@And("^(?:i|I) donot recommend this product to friend$")
	public void donotRecommendProduct() {
		this.pdpPage.getWriteAReviewDialog().getRecommendedNoObject().click();
	}

	@And("^(?:i|I) agree to terms and conditions$")
	public void checkTermsAndConditionCheckbox() throws Throwable {
		this.pdpPage.getWriteAReviewDialog().checkTermsAndConditions();
	}

	@And("^(?:i|I) submit the review$")
	public void submitReview() throws Throwable {
		this.pdpPage.getWriteAReviewDialog().submitReview();
		this.actions.Wait.wait("5");
	}

	@And("^(?:i|I) should see that review is submitted with message \"([^\"]*)\"$")
	public void verifyReviewSubmitted(String message) throws Throwable {
		this.actions.Wait.waitForPageReady();
		String actualMessage = this.pdpPage.getWriteAReviewDialog().getReviewSubmitMessage();
		Assert.assertEquals(message, actualMessage, "Actual - " + actualMessage + " and Expected - " + message);
	}

		@Then("^(?:i|I) should be able to see Buy It Now functionality$")
	public void i_should_be_able_to_see_Buy_It_Now_functionality() {

		Assert.assertNotNull(this.pdpPage.getBuyItNowButton(), "Buy It Now functionality is not present");
		System.out.println("buy it now functionality is present on page");
		this.capabilities.getWebDriver().switchTo().defaultContent();
	}

	@When("^(?:i|I) click on Buy It Now Button$")
	public void i_click_on_Buy_It_Now_Button() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.pdpPage.getBuyItNowButton().click();
		System.out.println("successfully clicked on buy it now button");
		this.capabilities.getWebDriver().switchTo().defaultContent();
		this.actions.Wait.wait("3");
	}

	@And("^(?:i|I) should be able to see \"([^\"]*)\"$")
    public void verifyLocationMessage(String message) throws Throwable {
           this.actions.Wait.waitForPageReady();
           String actualMessage = this.pdpPage.getLocationMessage();
           Assert.assertEquals(message, actualMessage, "Actual - " + actualMessage + " and Expected - " + message);
    }

	
	@Then("^(?:i|I) should be able to see online store list$")
	public void i_should_be_able_to_see_online_store_list() throws Throwable {
		// capabilities.getWebDriver().switchTo().frame(this.pdpPage.getBuyItNowIFrame());
		List<WebElement> stores = this.pdpPage.getBuyItNowOnlineStoreList();
		Assert.assertTrue(stores.size() > 0, "buy it now online store are not present");
		stores.forEach(x -> System.out.println(x.getText()));
		this.capabilities.reportStepInfo("number of stores found " + stores.size());

	}
	
	@Then("^(?:i|I) should be able to see target store list$")
    public void i_should_be_able_to_see_target_store_list() throws Throwable {
           // capabilities.getWebDriver().switchTo().frame(this.pdpPage.getBuyItNowIFrame());
           List<WebElement> stores = this.pdpPage.getBuyItNowPricespiderStoreList();
           Assert.assertTrue(stores.size() > 0, "buy it now target store are not present");
           stores.forEach(x -> System.out.println(x.getText()));
           this.capabilities.reportStepInfo("number of stores found " + stores.size());
	}
	
	@Then("^(?:i|I) should be able to see online store dialog list$")
	public void i_should_be_able_to_see_online_store_dialog_list() throws Throwable {
		List<WebElement> stores = this.pdpPage.getBuyItNowOnlineStoreList1();
		Assert.assertTrue(stores.size() > 0, "Aucun distributeur n'a été trouvé pour votre localisation");
		stores.forEach(x -> x.getText());
	}
	
	
	@Then("^(?:i|I) should be able to see online store list for clear brand$")
	public void i_should_be_able_to_see_online_store_list_for_clear_brand() throws Throwable {
		List<WebElement> stores = this.pdpPage.getBuyItNowOnlineStoreList();
		Assert.assertTrue(stores.size() > 0, "buy it now online store are not present");
		stores.forEach(x -> x.getText());

	}

	@Then("^(?:i|I) should be able to see online store list for (?:knorr|rexona|Knorr|Rexona) brand$")
	public void i_should_be_able_to_see_online_store_list_for_knorr_brand() throws Throwable {
		List<WebElement> stores = this.pdpPage.getBuyItNowOnlineStoreListForSmartProductBasketFrame();
		Assert.assertTrue(stores.size() > 0, "buy it now online store are not present");
		stores.forEach(x -> x.getText());

	}

	@When("^(?:i|I) enter zipcode as \"([^\"]*)\"$")
	public void i_enter_zipcode_as(String zipCode) {
		// Write code here that turns the phrase above into concrete actions
		this.pdpPage.getWriteAReviewDialog().getZipCodeEditBox().sendKeys(zipCode);
	}

	@When("^(?:i|I) select \"([^\"]*)\" as age of majority$")
	public void i_select_as_age_of_majority(String majority) throws IOException {
		Select selectMajority = new Select(this.pdpPage.getWriteAReviewDialog().getMajorityListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(majority, selectMajority);
	}
	
	@When("^(?:i|I) select \"([^\"]*)\" as birth month$")
	public void i_select_as_birth_month(String month) throws IOException {
		Select selectMonth = new Select(this.pdpPage.getWriteAReviewDialog().getBirthMonthListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(month, selectMonth);
	}

	@When("^(?:i|I) select \"([^\"]*)\" as birth year$")
	public void i_select_as_birth_year(String year) throws IOException {
		Select selectYear = new Select(this.pdpPage.getWriteAReviewDialog().getBirthYearListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(year, selectYear);
	}

	@When("^(?:i|I) select \"([^\"]*)\" Are you over the age of majority$")
	public void selectAreYouOverTheAgeAs(String option) throws IOException {
		Select selectAreYouOverTheAge = new Select(this.pdpPage.getWriteAReviewDialog().getAreYourOverTheYearListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(option, selectAreYouOverTheAge);
	}
	
	@When("^(?:i|I) select \"([^\"]*)\" as Age Range$")
	public void selectAgeRange(String option) throws IOException {
		Select selectAgeRange = new Select(this.pdpPage.getWriteAReviewDialog().getAgeRangeListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(option, selectAgeRange);
	}
	
	@When("^(?:i|I) select \"([^\"]*)\" as gender$")
	public void i_select_as_gender(String gender) throws IOException {
		Select selectGender = new Select(this.pdpPage.getWriteAReviewDialog().getGenderListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(gender, selectGender);
	}

	@When("^(?:i|I) select \"([^\"]*)\" as usaully purchased vim product$")
	public void select_usually_purchased_vim_product(String option) throws IOException {
		Select VimProduct = new Select(this.pdpPage.getWriteAReviewDialog().getUsuallyPurchasedVimProduct());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(option, VimProduct);
	}
	
	@When("^(?:i|I) select \"([^\"]*)\" as surface typically used$")
	public void select_surface_typically_used(String option) throws IOException {
		Select surface = new Select(this.pdpPage.getWriteAReviewDialog().getSurfaceUsed());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(option, surface);
	}
	
	@When("^(?:i|I) select \"([^\"]*)\" for first time buying the product$")
	public void selectFirstTimeBuyingProduct(String option) throws IOException {
		Select FirstTimeBuyingProduct = new Select(this.pdpPage.getWriteAReviewDialog().getFirstTimeBuyingProduct());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(option, FirstTimeBuyingProduct);
	}
	
	@When("^(?:i|I) select \"([^\"]*)\" store for purchasing vim product$")
	public void selectVimProductPurchaseStore(String option) throws IOException {
		Select VimProductPurchaseStore = new Select(this.pdpPage.getWriteAReviewDialog().getVimProductPurchaseStore());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(option, VimProductPurchaseStore);
	}
	
	@When("^(?:i|I) select \"([^\"]*)\" as purchase location$")
	public void i_select_as_purchase_location(String purchaseLocation) throws IOException {
		Select selectPurchaseLocation = new Select(this.pdpPage.getWriteAReviewDialog().getPurchaseLocationListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(purchaseLocation, selectPurchaseLocation);
	}

	@When("^(?:i|I) select \"([^\"]*)\" as Frequency$")
	public void i_select_as_Frequency(String frequency) throws IOException {
		Select selectFrequnecy = new Select(this.pdpPage.getWriteAReviewDialog().getFrequencyListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(frequency, selectFrequnecy);
	}

	@When("^(?:i|I) select \"([^\"]*)\" as how long you have been purchasing$")
	public void i_select_as_how_long_you_have_been_purchasing(String purchaseDuration) throws IOException {
		Select selectPurchaseDuration = new Select(this.pdpPage.getWriteAReviewDialog().getHowLongHaveYouBeenListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(purchaseDuration, selectPurchaseDuration);
	}

	@When("^(?:i|I) select \"([^\"]*)\" as incentive for reviews$")
	public void i_select_as_incentive_for_reviews(String incentiveReviews) throws IOException {
		Select select = new Select(this.pdpPage.getWriteAReviewDialog().getIncentivizedReviewListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(incentiveReviews, select);
	}

	@When("^(?:i|I) select \"([^\"]*)\" as sign up for exciting offers$")
	public void i_select_as_sign_up_for_exciting_offers(String signUpExcitingOffers) throws IOException {
		Select select = new Select(this.pdpPage.getWriteAReviewDialog().getMarketingOptInListBox());
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(signUpExcitingOffers, select);
	}

	@When("^(?:i|I) select (\\d+) option for recommend brand to a friend$")
	public void i_select_option_for_recommend_brand_to_a_friend(int recommend) {
		this.pdpPage.getWriteAReviewDialog().selectRecommendScore(recommend);
	}

	@When("^(?:i|I) enter \"([^\"]*)\" ZipCode for store locator PDP$")
	public void i_select_ZipCode_for_storelocatorPDP(String ZipCode) {
		this.actions.Wait.waitForPageReady();
		this.pdpPage.getStoreSearchPDP().sendKeys(ZipCode);
	}

	@Then("^(?:i|I) should be able to see Shopalyst retailers list$")
	public void shopalystStoreList() throws Throwable {
		actions.Wait.waitForPageReady();
		capabilities.getWebDriver().switchTo().frame(this.pdpPage.getBuyItNowIFrame());
		List<WebElement> stores = this.pdpPage.getShopalystOnlineStoreList();
		Assert.assertTrue(stores.size() > 0, "Shopalyst online store are not present");
		stores.forEach(x -> System.out.println(x.getText()));
		this.capabilities.reportStepInfo("number of stores found " + stores.size());

	}

	@When("^(?:I|i) click on any of the shopalyst retailers$")
	public void clickOnRandomShopalystRetailer() throws Throwable {
		actions.Wait.waitForPageReady();
		List<WebElement> shopalystRetailersAll = this.pdpPage.getShopalystOnlineStoreList();
		Assert.assertNotNull(shopalystRetailersAll, "No retailers are appearing for shopalyst");
		int randomNumber = RandomUtil.getRandomNumber(1, shopalystRetailersAll.size() - 1);
		WebElement listItem = shopalystRetailersAll.get(randomNumber - 1);
		String href = listItem.getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("selectedshopalystretailerurl", href);
		listItem.click();
		this.capabilities.reportStepInfo("clicked on product listing item with href " + href);
	}

	@Then("^(?:I|i)t should take user to corresponding shopalyst retailer page$")
	public void correspondingShopalystRetailerPage() throws Throwable {
		this.actions.Wait.waitForPageReady();
		// String expectedUrl = (String)
		// this.capabilities.getScenarioContext().getScenarioData("selectedshopalystretailerurl");
		this.actions.SwitchWindow.switchTabByIndex(2);
		String currentUrl = this.capabilities.getWebDriver().getCurrentUrl();
		System.out.println("Navigate Retailer is: " + currentUrl);
	}

	@Then("^(?:i|I) should be able to see ConstantCommerce retailers list$")
	public void constantcommerceretailerslist() throws Throwable {
		actions.Wait.waitForPageReady();
		capabilities.getWebDriver().switchTo().frame(this.pdpPage.getBuyItNowIFrame());
		List<WebElement> stores = this.pdpPage.getConstantCommerceOnlineStoreList();
		Assert.assertTrue(stores.size() > 0, "ConstantCommerce online store are not present");
		stores.forEach(x -> System.out.println(x.getText()));
		this.capabilities.reportStepInfo("number of stores found " + stores.size());

	}

	@When("^(?:I|i) click on any of the ConstantCommerce retailers$")
	public void clickOnRandomConstantCommerceRetailer() throws Throwable {
		actions.Wait.waitForPageReady();
		List<WebElement> constantcommerceRetailersAll = this.pdpPage.getConstantCommerceOnlineStoreList();
		Assert.assertNotNull(constantcommerceRetailersAll, "No retailers are appearing for ConstantCommerce");
		int randomNumber = RandomUtil.getRandomNumber(1, constantcommerceRetailersAll.size() - 1);
		WebElement listItem = constantcommerceRetailersAll.get(randomNumber - 1);
		String retailerName = listItem.findElement(By.cssSelector("span[class='sr-only ng-binding']")).getText();
		capabilities.getScenarioContext().addScenarioData("selectedconstantcommerceretailername", retailerName);
		listItem.click();
	}

	@Then("^(?:I|i)t should open ConstantCommerce retailer window$")
	public void correspondingConstantCommerceRetailer() throws Throwable {
		this.actions.Wait.waitForPageReady();
		Object ccRetailer = this.capabilities.getScenarioContext()
				.getScenarioData("selectedconstantcommerceretailername");
		Assert.assertNotNull(ccRetailer);
	}

	@Then("^(?:i|I) should be able to see Cartwire retailers list$")
	public void Cartwireretailerslist() throws Throwable {
		actions.Wait.waitForPageReady();
		// capabilities.getWebDriver().switchTo().frame(this.pdpPage.getBuyItNowIFrame());
		List<WebElement> stores = this.pdpPage.getCartwireOnlineStoreList();
		Assert.assertTrue(stores.size() > 0, "Cartwire online store are not present");
		stores.forEach(x -> System.out.println(x.getText()));
		this.capabilities.reportStepInfo("number of stores found " + stores.size());

	}

	@When("^(?:I|i) click on any of the Cartwire retailers$")
	public void clickOnRandomCartwireRetailer() throws Throwable {
		actions.Wait.waitForPageReady();
		List<WebElement> CartwireRetailersAll = this.pdpPage.getCartwireOnlineStoreList();
		Assert.assertNotNull(CartwireRetailersAll, "No retailers are appearing for Cartwire");
		int randomNumber = RandomUtil.getRandomNumber(1, CartwireRetailersAll.size() - 1);
		WebElement listItem = CartwireRetailersAll.get(randomNumber - 1);
		// String retailerName =
		// listItem.findElement(By.cssSelector("span[class='sr-only
		// ng-binding']")).getText();
		// capabilities.getScenarioContext().addScenarioData("selectedCartwireRetailerName",
		// retailerName);
		listItem.click();
	}

	@Then("^(?:I|i)t should take user to corresponding Cartwire retailer page$")
	public void correspondingCartwireRetailerPage() throws Throwable {
		this.actions.Wait.waitForPageReady();
		// String expectedUrl = (String)
		// this.capabilities.getScenarioContext().getScenarioData("selectedshopalystretailerurl");
		this.actions.SwitchWindow.switchTabByIndex(2);
		String currentUrl = this.capabilities.getWebDriver().getCurrentUrl();
		System.out.println("Navigate Retailer is: " + currentUrl);
	}

	@When("^(?:i|I) click on add to (?:bag|basket) button$")
	public void i_click_on_add_to_bag_button() throws Throwable {
		this.pdpPage.clickAddToBagButton();
		this.capabilities.getScenarioContext().addScenarioData("shoppingbagdata",
				this.pdpPage.getGlobalNavigation().getShoppingBagText());
		this.capabilities.reportStepInfo("successfully clicked on add to bag button");
	}

	@And("^(?:i|I) click on the Share This button$")
	public void i_click_on_sharethis_button() {
		this.pdpPage.clickShareThisButton();
		System.out.println("successfully clicked on shared this button");

	}

	@And("^(?:i|I) verify that \"(.+)\" icon is present$")
	public void i_verify_iconpresent(String socialsSharingIcon) {
		actions.Wait.waitForPageReady();
		WebElement icon = capabilities.getWebDriver()
				.findElement(By.cssSelector("a[class*='" + socialsSharingIcon + "']"));
		Assert.assertTrue(icon.isDisplayed(), "social sharing icon: " + socialsSharingIcon + " is not present");
		if (icon.isDisplayed())
			this.capabilities.reportStepInfo(socialsSharingIcon + " icon is present");

	}

	@When("^(?:i|I) click the \"(.+)\" icon$")
	public void i_verify_socialsharingpopup(String socialIcon) {
		actions.Wait.waitForPageReady();
		WebElement icon = capabilities.getWebDriver().findElement(By.cssSelector("a[class*='" + socialIcon + "']"));
		Assert.assertTrue(icon.isDisplayed(), "social sharing icon: " + socialIcon + " is not present");
		if (icon.isDisplayed()) {
			this.capabilities.reportStepInfo("Clicking " + socialIcon + " icon");
			icon.click();
			actions.Wait.waitForPageReady();
		}
	}

	@And("^(?:i|I) expect to see Product image on page$")
	public void i_expect_product_image() {
		actions.Wait.waitForPageReady();
		WebElement productimage = capabilities.getWebDriver()
				.findElement(By.xpath("(//img[contains(@data-src,'ecomm_demo_resized')])[1]"));
		Assert.assertTrue(productimage.isDisplayed(), "Product image is not present");
		this.capabilities.reportStepInfo("Product Image is displayed");
	}

	@And("^(?:i|I) verify that Add to basket button is disabled$")
	public void disabledaddtobasketbutton() {
		WebElement disabledaddtobasket = capabilities.getWebDriver()
				.findElement(By.xpath("//button[contains(@class,'add-to-cart') and @disabled='true']"));
		Assert.assertTrue(disabledaddtobasket.isDisplayed(), "Disabled add to basket is not present");
		this.capabilities.reportStepInfo("Disabled add to basket button is displayed");
	}

	@Then("^(?:i|I) enter 11 digit or less personalization text \"(.+)\" in field and verify it appears on image component$")
	public void i_enter_pdp_personalisation(String personalisationtext) {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(personalisationtext.length() <= 11, " Personalisation text characters should be 11 or less");
		this.pdpPage.pdpPersonalisationText().sendKeys(personalisationtext + Keys.TAB);
		this.capabilities.getScenarioContext().addScenarioData("productPersonalisationText", personalisationtext);
		WebElement personalisedtext = capabilities.getWebDriver()
				.findElement(By.xpath("(//picture[contains(@data-tw-bind,'" + personalisationtext + "')])[1]"));
		Assert.assertTrue(personalisedtext.isDisplayed(), "Personalised text is not present in image component");
		this.capabilities.reportStepInfo("Personalised text is present in image component");
	}

	@And("^(?:i|I) verify that Add to basket button is Enabled$")
	public void enabledaddtobasketbutton() {
		WebElement enabledabledaddtobasket = capabilities.getWebDriver()
				.findElement(By.xpath("//button[contains(@class,'add-to-cart')]"));
		Assert.assertTrue(enabledabledaddtobasket.isDisplayed(), "Enabled add to basket is not present");
		this.capabilities.reportStepInfo("Enabled add to basket button is displayed");
		WebElement unitPrice = capabilities.getWebDriver().findElement(By.cssSelector("span[class*=label__price]"));
		String pdpproductprice = "";
		pdpproductprice = unitPrice.getAttribute("innerHTML");
		pdpproductprice = StringUtils.removeHTMLTag("\n", pdpproductprice).trim();
		System.out.println("Product per unit price on pdp page: " + pdpproductprice);
		this.capabilities.getScenarioContext().addScenarioData("pdpproductprice", pdpproductprice);
	}

	@And("^(?:i|I) verify that user is navigated to My basket page$")
	public void mybasketpage() throws InterruptedException {
		actions.Wait.waitForPageReady();
		System.out.println(capabilities.getWebDriver().getCurrentUrl());
		Assert.assertTrue(capabilities.getWebDriver().getCurrentUrl().contains("/ShopifyCheckout.html"),
				"Not on cart page");
		capabilities.getWebDriver().findElement(By.cssSelector("a[class*=btn--secondary]")).click();
		System.out.println("Clicked on continue shopping on my basket page");
		String productPagePersonalisationText = (String) capabilities.getScenarioContext()
				.getScenarioData("productPersonalisationText");
		System.out.println("text from cart page: " + productPagePersonalisationText);
		actions.Wait.waitForPageReady();
		this.pdpPage.pdpPersonalisationText().sendKeys(productPagePersonalisationText + Keys.TAB);
		this.pdpPage.clickAddToBagButton();
		System.out.println("clicked on add to bag button after navigating back to product page from my basket page");
		String productPagePerUnitProductPrice = (String) capabilities.getScenarioContext()
				.getScenarioData("pdpproductprice");
		System.out.println(productPagePerUnitProductPrice);
		actions.Wait.waitForPageReady();
		Thread.sleep(12);
		WebElement cartPagePerUnitPrice = capabilities.getWebDriver()
				.findElement(By.xpath("(//p[contains(@class,'cart__products')])[3]"));
		String cartProductPrice = cartPagePerUnitPrice.getText();
		String innerTextPrice = cartProductPrice.split(":")[0];
		String cartPerUnitProductPrice = cartProductPrice.replace(innerTextPrice + ":", "").trim();
		// String cartProductPrice = cartPagePerUnitPrice.getAttribute("innerHTML");
		System.out.println(cartProductPrice);
		Assert.assertTrue(cartPerUnitProductPrice.equalsIgnoreCase(productPagePerUnitProductPrice),
				"Price on PDP and its price on my basket page is not the same");
		String productquantity = "3";
		selectProductQuantityFilter(productquantity);

		// String cartPagePerUnitProductPrice = StringUtils.removeHTMLTag(tag, s)
		// System.out.println("Product per unit price on My basket page: " +
		// cartproductprice);
	}

	public void selectProductQuantityFilter(String quantity) throws InterruptedException {
		try {
			this.pdpPage.selectFilterByText(quantity);
			this.capabilities.reportStepInfo("successfully selected filter with text " + quantity);
			this.actions.Wait.wait(2);
		} catch (NoSuchElementException e) {
			System.out.println("there is not filter option with text " + quantity);
			e.printStackTrace();
		}
	}
	
	@And("i should be able to see product image in write a review dialog")
	public void verifyProductImageInWriteAReviewDialog() throws Throwable {
		WebElement image = this.pdpPage.getWriteAReviewDialog().getProductImage();
		this.actions.VerifyElementVisible.verifyImageLoaded(image);
	}	
}
