package steps.business.generic.pdp;

import java.io.IOException;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.CountrySelector;
import unilever.pageobjects.platform.publish.pdp.ProductDetailPage;
import unilever.pageobjects.platform.publish.pdp.ViewBagPage;
import unilever.pageobjects.platform.publish.pdp.WriteAReviewDialog;

public class StepsProductDetailPageViewBagPage {

	UserActions actions;
	World capabilities;
	ViewBagPage viewBagPage;
	// String pr

	public StepsProductDetailPageViewBagPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.viewBagPage = new ViewBagPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@When("^(?:I|i) click on continute shopping on view bag page$")
	public void i_click_on_continueshopping_button() {
	    this.viewBagPage.getContinueShoppingButton().click();
	}

	@When("^(?:I|i)t should take me to a view bag page$")
	public void verifyAddToBagPage() {
	    Assert.assertTrue(this.viewBagPage.isPresentOnPage(), "the current page is not view bag page");
	    this.capabilities.reportStepInfo("Successfully navigated to viewbag page");
	}	
	
	@And("^(?:I|i) click on edit link on viewbag page$")
	public void i_click_on_edit_link_on_viewbag_page() throws Throwable {
		this.viewBagPage.clickEditBagLink();
		this.capabilities.getScenarioContext().addScenarioData("shoppingbagdata", this.viewBagPage.getGlobalNavigation().getShoppingBagText());
		this.capabilities.reportStepInfo("Successfully clicked on edit link on viewbag page");
	}
	
	@Then("i should be able to see edit bag pop up")
	public void i_should_be_able_to_see_edit_bag_pop_up() {
		Assert.assertTrue(this.viewBagPage.isEditBagPopUpPresent(), "Edit Bag Pop Up is not present");
	}
	
	@When("i choose \"([^\"]*)\" quantity in edit bag pop up")
	public void i_choose_quantity_in_add_to_bag(String quantity) throws Throwable {
		this.actions.SelectText.selectByTextContainsTextIgnoreCase(quantity, this.viewBagPage.getQuantityEditBagPopUp());
		this.actions.Wait.wait("1");
	}
	
	@Then("i should be able to see shopping bag updated on viewbag page")
	public void i_should_be_able_to_see_shopping_bag_updated_on_viewbag_page() throws Throwable {
		this.actions.Wait.waitForPageReady();
		String newShoppingBagText = this.viewBagPage.getGlobalNavigation().getShoppingBagText();
		String previousShoppingBagText = (String) this.capabilities.getScenarioContext().getScenarioData("shoppingbagdata");
		Assert.assertNotEquals(newShoppingBagText, previousShoppingBagText,"shopping bag is not updated as expected");
		this.capabilities.reportStepInfo("shopping bag is update successfully after editing on viewbag page. earlier it was " + previousShoppingBagText + " and now it is " + newShoppingBagText);
	}
	
	@And("^(?:I|i) click on checkout button on viewbag page$")
	public void i_click_on_checkout_button_on_viewbag_page() {
		this.viewBagPage.clickCheckoutButton();
	    this.capabilities.reportStepInfo("successfully clicked on Checkout button");
	}

}
