package steps.business.generic.pdp;

import java.io.IOException;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.pdp.CheckOutPage;


public class StepsPdpAddToBagCheckOutPage {

	UserActions actions;
	World capabilities;
	CheckOutPage checkOutPage;
	// String pr
	

	public StepsPdpAddToBagCheckOutPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.checkOutPage = new CheckOutPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	

	@Then("^(?:I|i)t should take me to checkout page$")
	public void verifyAddToBagPage() {
	    Assert.assertTrue(this.checkOutPage.isCheckoutPage(), "the current page is not checkout page");
	    this.capabilities.reportStepInfo("Successfully navigated to checkout page");
	}	
	
	@And("^(?:I|i) should be able to see checkout form")
	public void verifyCheckOutForm() {
		this.checkOutPage.checkOutFormPresent();
	}
	
	@And("^(?:I|i) enter first name as \"([^\"]*)\" in checkout form$")
	public void enter_firstName(String name) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterFirstName(name);

	}
	
	@And("^(?:I|i) enter last name as \"([^\"]*)\" in checkout form$")
	public void enter_lastName(String name) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterLastName(name);

	}
	
	@And("^(?:I|i) enter addressLine1 as \"([^\"]*)\" in checkout form$")
	public void enter_addressLine1(String address1) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterAddressLine1(address1);

	}
	
	@And("^(?:I|i) enter addressLine2 as \"([^\"]*)\" in checkout form$")
	public void enter_addressLine2(String address2) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterAddressLine2(address2);

	}
	
	@And("^(?:I|i) enter city as \"([^\"]*)\" in checkout form$")
	public void enter_city(String city) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterCity(city);

	}
	
	@And("^(?:I|i) select State as \"([^\"]*)\" in checkout form$")
	public void select_State(String State) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.selectState(State);

	}
	
	@And("^(?:I|i) enter zip code as \"([^\"]*)\" in checkout form$")
	public void enter_zip(String zip) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterZipCode(zip);

	}
	
	@And("^(?:I|i) enter phone number as \"([^\"]*)\" in checkout form$")
	public void enter_phoneNumber(String num) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterPhoneNumber(num);

	}
	
	@And("^(?:I|i) enter email address as \"([^\"]*)\" in checkout form$")
	public void enter_emailAddress(String email) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterEmail(email);

	}
	
	@And("^(?:I|i) enter card holder name as \"([^\"]*)\" in checkout form$")
	public void enter_card_holder_name(String name) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterCardHolderName(name);

	}
	
	@And("^(?:I|i) enter card number as \"([^\"]*)\" in checkout form$")
	public void enter_card_number(String num) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterCardNumber(num);

	}
	
	@And("^(?:I|i) select expiry month as \"([^\"]*)\" in checkout form$")
	public void select_expiry_month(String expMonth) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.selectExpiryMonth(expMonth);

	}
	
	@And("^(?:I|i) select expiry year as \"([^\"]*)\" in checkout form$")
	public void select_expiry_year(String expYear) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.selectExpiryYear(expYear);

	}
	
	@And("^(?:I|i) enter verification code as \"([^\"]*)\" in checkout form$")
	public void enter_verification_code(String code) {
		this.actions.Wait.waitForPageReady();
		this.checkOutPage.enterVerificationCode(code);

	}
	
	@And("^(?:I|i) check information and offers checkbox$")
	public void check_information_and_offer_optin() throws Throwable {
		this.checkOutPage.clickInfoAndOfferOptin();
	}
	
	@And("^(?:I|i) click on place order button$")
	public void click_place_order() throws Throwable {
		this.checkOutPage.clickPlaceOrder();
		this.actions.Wait.wait("5");
	}
}
