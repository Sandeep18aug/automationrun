package steps.business.generic.pdp;

import java.io.IOException;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.CountrySelector;
import unilever.pageobjects.platform.publish.pdp.ProductDetailPage;
import unilever.pageobjects.platform.publish.pdp.WriteAReviewDialog;

public class StepsProductDetailPageBuyItNow {

	UserActions actions;
	World capabilities;
	ProductDetailPage pdpPage;
	// String pr

	public StepsProductDetailPageBuyItNow(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.pdpPage = new ProductDetailPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	

}
