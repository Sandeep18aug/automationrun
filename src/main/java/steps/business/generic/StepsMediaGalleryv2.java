package steps.business.generic;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import framework.shared.RandomUtil;
import steps.generic.keywords.World;
//import steps.generic.SetCapabilities;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.BackToTopPage;
import unilever.pageobjects.platform.publish.components.definitions.MediaGalleryV2;

public class StepsMediaGalleryv2 {
	UserActions actions;
	//SetCapabilities capabilities;
	World capabilities;
	PublishedPageCommon publishedPage;
	BackToTopPage backtotopPage;
	MediaGalleryV2 mediagalleryPage;

	public StepsMediaGalleryv2(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.backtotopPage = new BackToTopPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.mediagalleryPage = new MediaGalleryV2(capabilities.getWebDriver(),capabilities.getBrandName());
	}


	@Then("^(?:I|i) verify that media gallery component image is rendering$")
	public void i_verify_that_mediagallery_component_image_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement mediaGalleryV2Image = this.mediagalleryPage.getMediaGalleryV2Images();
		Assert.assertNotNull(mediaGalleryV2Image,
				"media gallery image is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("media gallery Image is present on page");
	}
	
	@And("^(?:I|i) click on the media gallery image$")
    public void click_related_article_image() throws Throwable {
           WebElement articleImage = this.mediagalleryPage.getMediaGalleryV2Images();
           Assert.assertNotNull(articleImage, "Pinterest Social Share button Tazo is not appearing for url "
                        + this.capabilities.getWebDriver().getCurrentUrl());
           this.capabilities.reportStepInfo("media gallery image has been clicked");
           this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
        		   articleImage.getAttribute("href"));
           articleImage.click();
    }
    
    @Then("^it should take me to the corresponding page of media gallery image$")
    public void verify_related_article_image() throws Throwable {
           this.actions.Wait.waitForPageReady();
           this.actions.SwitchWindow.switchTabByIndex(2);
           String exptitle = (String) this.capabilities.getScenarioContext().getScenarioData("relatedarticleimagehref");
           String acttitle = this.capabilities.getWebDriver().getCurrentUrl();
           System.out.println("###########");
	   		System.out.println("expectedUrl"+exptitle);
	   		if(acttitle!=null) {
	   			System.out.println("actualUrl"+acttitle);
	   		}
	   		System.out.println("###########");
           Assert.assertTrue(exptitle.equals(acttitle), "the page is not navigated to expected url. related article cta href was "
                        + exptitle + System.lineSeparator() + " actual title is " + acttitle);
    }

}
