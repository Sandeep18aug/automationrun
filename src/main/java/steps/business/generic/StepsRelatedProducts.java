package steps.business.generic;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.shared.RandomUtil;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.RelatedProduct;

public class StepsRelatedProducts {

	UserActions actions;
	World world;
	String magicFinderOptionWithPlaceholder = "//div[contains(@class,'que-active') and descendant::h2[contains(translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),translate('${heading}','ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'))]]//div[contains(translate(@data-res-text,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),translate('${option}','ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'))]";
	RelatedProduct relatedProduct = null;
	
	public StepsRelatedProducts(World world, UserActions getBasicKeywords) {
		this.actions = getBasicKeywords;
		this.world = world;
		this.relatedProduct = new RelatedProduct(world.getWebDriver(),world.getBrandName());
	}

	@And("^(?:I|i) verify that related products are present on the productpage$")
	public void verifyRelatedProductItems() {
		actions.Wait.waitForPageReady();
		Assert.assertTrue(this.relatedProduct.getRelatedProductsVisibleItems().size() > 0,
				"Related products are not present on page");
		this.world.reportStepInfo("Related products are present on page");	
	}
	
	@Then("^(?:I|i) verify that related product component image is rendering$")
	public void verifyRelatedProductComponentImage() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedproductImage = this.relatedProduct.getRelatedProductsVisibleItem();
		Assert.assertNotNull(relatedproductImage,
				"related article image is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
		this.world.reportStepInfo("related article Image is present on page");
	}
	
	@Then("^(?:I|i) verify that related product component heading for cif is rendering$")
	public void verifyRelatedProductComponentImageCif() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedproductImage = this.relatedProduct.getRelatedProductsVisibleItemCif();
		Assert.assertNotNull(relatedproductImage,
				"related article image is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
		this.world.reportStepInfo("related article Image is present on page");
	}

	
	@Then("^(?:I|i) verify that related product component heading is appearing$")
	public void verifyRelatedProductComponentHeading() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedproductHeading = this.relatedProduct.getRelatedProductHeading();
		Assert.assertNotNull(relatedproductHeading,
				"related article heading is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
		this.world.reportStepInfo("related article heading is present on page");
	}
	
	@Then("^(?:I|i) verify that related product component for axe heading is appearing$")
	public void verifyRelatedProductComponentHeadingAxe() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedproductHeading = this.relatedProduct.getRelatedProductHeadingAxe();
		Assert.assertNotNull(relatedproductHeading,
				"related article heading is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
		this.world.reportStepInfo("related article heading is present on page");
	}
	
	@And("^(?:I|i) click on the related-product component by indexing$")
    public void click_relatedproduct_magnum() {
           actions.Wait.waitForPageReady();
           WebElement cta = this.relatedProduct.getRelatedProductbyindexing();
           Assert.assertNotNull(cta,
                        "related product is not appearing for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.reportStepInfo("related product present on page");
           this.world.getScenarioContext().addScenarioData("relatedproducthref", cta.getAttribute("href"));
           cta.click();
           //cta.findElement(By.xpath(".//img")).click();
    }

		
	@Then("^(?:I|i) click on first related product item$")
	public void clickOnRandomRelatedProductImage() {
		// Write code here that turns the phrase above into concrete actions
		List<WebElement> relatedProductItems = this.relatedProduct.getRelatedProductsVisibleItems();
		Assert.assertTrue(relatedProductItems.size() > 0, "Related products are not present on page");
		relatedProductItems.get(0).click();
		this.world.reportStepInfo("clicked on first related product item");
	}
	
	
	// Check for related article click
	@And("^(?:I|i) click on the related-product component$")
	public void click_relatedproduct() {
		actions.Wait.waitForPageReady();
		WebElement cta = this.relatedProduct.getRelatedProductsVisibleItem();
		Assert.assertNotNull(cta,
				"related product is not appearing for url " + this.world.getWebDriver().getCurrentUrl());
		this.world.reportStepInfo("related product present on page");
		this.world.getScenarioContext().addScenarioData("relatedproducthref", cta.getAttribute("href"));
		cta.click();
		//cta.findElement(By.xpath(".//img")).click();
	}
	
	
//	// Check for Magnum related article click
//		@And("^(?:I|i) click on the related-product component by indexing$")
//		public void click_relatedproduct_magnum() {
//			actions.Wait.waitForPageReady();
//			WebElement cta = this.relatedProduct.getRelatedProductbyindexing();
//			Assert.assertNotNull(cta,
//					"related product is not appearing for url " + this.world.getWebDriver().getCurrentUrl());
//			this.world.reportStepInfo("related product present on page");
//			this.world.getScenarioContext().addScenarioData("relatedproducthref", cta.getAttribute("href"));
//			cta.click();
//			//cta.findElement(By.xpath(".//img")).click();
//		}
	
	@And("^(?:I|i) click on the related-product component image$")
	public void click_relatedproduct_image() {
		actions.Wait.waitForPageReady();
		WebElement imageClick = this.relatedProduct.getRelatedProductsVisibleItem();
		Assert.assertNotNull(imageClick,
				"related product is not appearing for url " + this.world.getWebDriver().getCurrentUrl());
		this.world.reportStepInfo("related product present on page");
		this.world.getScenarioContext().addScenarioData("relatedproducthref", imageClick.getAttribute("href"));
		//this.actions.Scroll.scrollPublishedPageDownByPixels(1000); 
		imageClick.click();
		//imageClick.findElement(By.xpath(".//img")).click();
	}
	
	@Then("^it should take me to the corresponding page of related product$")
	public void verifyrelatedproductUrlPage() throws Throwable {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.world.getScenarioContext().getScenarioData("c-related-products-item-image__link");
		this.actions.Wait.wait("3");
		String actualUrl = this.world.getWebDriver().getCurrentUrl();

		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related product href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);

	}
	
	@Then("^(?:I|i) verify that listing item component image is rendering$")
    public void i_verify_that_product_component_image_is_rendering() {
           // Write code here that turns the phrase above into concrete actions
           WebElement relatedarticleImage = this.relatedProduct.getRelatedListingItemImages();
           Assert.assertNotNull(relatedarticleImage,
                        "related product image is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.reportStepInfo("related product Image is present on page");
    }

	@Then("^(?:I|i) verify that listing item  component heading is appearing$")
    public void i_verify_that_product_component_heading_is_rendering() {
           // Write code here that turns the phrase above into concrete actions
           WebElement relatedarticleHeading = this.relatedProduct.getRelatedListingItemHeading();
           Assert.assertNotNull(relatedarticleHeading,
                        "related product heading is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.reportStepInfo("related product heading is present on page");
    }

	@And("^(?:I|i) click on the listing item image$")
    public void click_featured_products_image() {
           WebElement articleImage = this.relatedProduct.getRelatedListingItemImages();
           Assert.assertNotNull(articleImage,
                        "related product image is not appearing for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.getScenarioContext().addScenarioData("relatedarticleimagehref",
                        articleImage.getAttribute("href"));
           //articleImage.findElement(By.xpath(".//img")).click();
           articleImage.click();
           this.world.reportStepInfo("related product image has been clicked");
    }

	@Then("^(?:I|i) verify that product listing component image is rendering$")
    public void i_verify_that_productlisting_component_image_is_rendering() {
           // Write code here that turns the phrase above into concrete actions
           WebElement relatedarticleImage = this.relatedProduct.getRelatedArticleImagePLP();
           Assert.assertNotNull(relatedarticleImage,
                        "related article image is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.reportStepInfo("related article Image is present on page");
    }

	@Then("^(?:I|i) verify that product listing component heading is rendering$")
    public void i_verify_that_relatedarticle_component_image_is_rendering() {
           // Write code here that turns the phrase above into concrete actions
           WebElement productPageHeading = this.relatedProduct.getProductListingHeadingPLP();
           Assert.assertNotNull(productPageHeading,
                        "related article image is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.reportStepInfo("product Page heading is present on page");
    }

	@And("^(?:I|i) click on article image for product on product listing page$") //added on 17jan by Ritanshu
    public void clickproductlistingplp() {
           actions.Wait.waitForPageReady();
        WebElement relatedarticleplp = this.relatedProduct.getRelatedArticleImagePLP();
        Assert.assertNotNull(relatedarticleplp,
                        "article image is not appearing for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.reportStepInfo("article image clicked on plp page");
           this.world.getScenarioContext().addScenarioData("relatedarticleplplinkhref", relatedarticleplp.getAttribute("href"));
           relatedarticleplp.click();
    }
	@Then("^(?:I|i) verify that featured product component image is rendering$")
    public void i_verify_that_featured_product_component_image_is_rendering() {
           // Write code here that turns the phrase above into concrete actions
           WebElement relatedarticleImage = this.relatedProduct.getRelatedArticleImages();
           Assert.assertNotNull(relatedarticleImage,
                        "related article image is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.reportStepInfo("related article Image is present on page");
    }

	@Then("^(?:I|i) verify that featured product component heading is appearing$")
    public void i_verify_that_featured_product_component_heading_is_rendering() {
           // Write code here that turns the phrase above into concrete actions
           WebElement relatedarticleHeading = this.relatedProduct.getRelatedArticleHeading();
           Assert.assertNotNull(relatedarticleHeading,
                        "related article heading is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.reportStepInfo("related article heading is present on page");
    }
	
	@And("^(?:I|i) click on the featured product image$")
    public void click_featured_product_image() {
           WebElement articleImage = this.relatedProduct.getRelatedArticleImage();
           Assert.assertNotNull(articleImage,
                        "related article image is not appearing for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.getScenarioContext().addScenarioData("relatedarticleimagehref",
                        articleImage.getAttribute("href"));
           //articleImage.findElement(By.xpath(".//img")).click();
           articleImage.click();
           this.world.reportStepInfo("related article image has been clicked");
    }

	@And("^(?:I|i) verify that box-listing are present on the productpages$")
    public void verifyproductpresent() {
           actions.Wait.waitForPageReady();
           this.relatedProduct.getBoxListingItemvisible();
           Assert.assertTrue(this.relatedProduct.getBoxListingItemvisible().size() > 0,
                        "Related products are not present on page");
           this.world.reportStepInfo("Related products are present on page");      
    }

	@Then("^(?:I|i) verify that box-listing component image is rendering$")
    public void i_verify_that_related_article_component_image_is_rendering() {
           // Write code here that turns the phrase above into concrete actions
           WebElement relatedarticleImage = this.relatedProduct.getBoxListingImages();
           Assert.assertNotNull(relatedarticleImage,
                        "related article image is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.reportStepInfo("related article Image is present on page");
    }

	@Then("^(?:I|i) verify that box-listing component heading is appearing$")
    public void i_verify_that_relatedarticle_component_heading_is_rendering() {
           // Write code here that turns the phrase above into concrete actions
           WebElement relatedarticleHeading = this.relatedProduct.getBoxListingHeading();
           Assert.assertNotNull(relatedarticleHeading,
                        "related article heading is not rendering for url " + this.world.getWebDriver().getCurrentUrl());
           this.world.reportStepInfo("related article heading is present on page");
    }
}
