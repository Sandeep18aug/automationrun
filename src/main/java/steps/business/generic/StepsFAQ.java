package steps.business.generic;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.helpcenter.HelpCenter;

public class StepsFAQ {

	UserActions actions;
	World capabilities;
	HelpCenter helpCenter;

	public StepsFAQ(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.helpCenter = new HelpCenter(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@Then("^(?:i|I) verify FAQ exists on page$")
	public void faqExists() {
		this.actions.Wait.waitForPageReady();
		Assert.assertNotNull(this.helpCenter.getFaqHeadings(), "Live chat is not present on Help center page");
	}

	@When("^(?:i|I) expand all FAQs on page$")
	public void expandFQAs() {
		this.actions.Wait.waitForPageReady();
		List<WebElement> FaqItems = this.helpCenter.getFaqHeadings();
		for (WebElement individualQuestion : FaqItems) {
			individualQuestion.click();
		}

	}

	@And("^(?:i|I) verify that total number of questions in FAQ section is (\\d+)$")
	public void faqNumber(int question) {
		this.actions.Wait.waitForPageReady();
		int AllQuestion = this.helpCenter.getFaqHeadings().size();
		Assert.assertEquals(question, AllQuestion, "FAQ Count doesn't matches");
	}

	@And("^(?:i|I) verify All FAQ have answers for them$")
	public void faqAnswerToQuestionsNumber() {
		this.actions.Wait.waitForPageReady();
		int AllQuestion = this.helpCenter.getFaqHeadings().size();
		int AllAnswers = this.helpCenter.getFaqPanelAnswers().size();
		Assert.assertEquals(AllQuestion, AllAnswers, "Count of Questions and Answers doesn't matches");
	}
}
