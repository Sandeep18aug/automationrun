package steps.business.generic;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.CountrySelector;
import unilever.pageobjects.platform.publish.common.SiteMapPage;
import unilever.pageobjects.platform.publish.pdp.ProductDetailPage;
import unilever.pageobjects.platform.publish.pdp.WriteAReviewDialog;

public class StepsCountrySiteMap {

	UserActions actions;
	World capabilities;
	SiteMapPage siteMapPage;
	//String pr

	public StepsCountrySiteMap(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.siteMapPage = new SiteMapPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@Then("i should be able to see site map link in global footer")
	public void i_should_be_able_to_see_sitemap_link_in_global_footer() throws Throwable {
		this.actions.Wait.waitForPageReady();
	    Assert.assertNotNull(this.siteMapPage.getGlobalFooter().getSiteMapLink(), "Site map not present in global footer");
	}

	@When("i click on site map link in global footer")
	public void i_click_on_sitemap_link_in_global_footer() throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		WebElement siteMap = this.siteMapPage.getGlobalFooter().getSiteMapLink();
		this.actions.Click.click(siteMap);
		System.out.println("successfully clicked at site map link in global footer");
	}

	@Then("i should be taken to site map page")
	public void i_should_be_taken_to_country_selector_page() throws Throwable {
		this.actions.Wait.waitForPageReady();
	    Assert.assertTrue(this.siteMapPage.isPresent(),"current page is having site map component");
	}

}
