package steps.business.generic;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import steps.generic.keywords.SelectText;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.components.definitions.DiagnosticTool;
import unilever.pageobjects.platform.publish.components.definitions.FeaturedContent;
import unilever.pageobjects.platform.publish.homepage.HomePage;

public class StepsDiagnosticTool {

	UserActions actions;
	World capabilities;
	DiagnosticTool dt;

	public StepsDiagnosticTool(World capabilities, UserActions getBasicKeywords) {
		this.actions = getBasicKeywords;
		this.capabilities = capabilities;
		this.dt = new DiagnosticTool(capabilities.getWebDriver(),capabilities.getBrandName());

	}

	@And("^I click on Diagnostic Tool Start Button$")
	public void click_start_button() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement startButton = this.dt.getStartButton();
		Assert.assertNotNull(startButton,
				"Start Button is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Start Button is present on the page");
		actions.Wait.wait("2");
		startButton.click();

	}

	@Then("^(?:I|i) choose Gender from DT Dove Quiz as Dove Woman$")
	public void choose_gender() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement womenRadioButton = this.dt.getWomenRadioButton();
		Assert.assertNotNull(womenRadioButton, "Women Radio Button is not appearing for url ");
		this.capabilities.reportStepInfo("Women Radio Button is present on the page");
		womenRadioButton.click();

	}

	@Then("^(?:I|i) click on Next Button for Gender Quiz$")
	public void click_gender_next_button() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement genderNextButton = this.dt.getNextButtonGender();
		Assert.assertNotNull(genderNextButton, "Gender Next Button is not appearing for url ");
		this.capabilities.reportStepInfo("Gender Next Button is present on the page");
		genderNextButton.click();

	}

	@Then("^(?:I|i) choose Age Factor between 16-20$")
	public void select_age_transition() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement ageSector = this.dt.getAgeTransitionRadioButton();
		Assert.assertNotNull(ageSector, "Age Bars is not appearing for url ");
		this.capabilities.reportStepInfo("Age Bars is present on the page");
		ageSector.click();

	}

	@Then("^(?:I|i) click on Next Button for AgeFactor Quiz$")
	public void click_agefactor_next_button() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement ageFactorNextButton = this.dt.getNextButtonAgeFactor();
		Assert.assertNotNull(ageFactorNextButton, "AgeFactor Next Button is not appearing for url ");
		this.capabilities.reportStepInfo("AgeFactor Next Button is present on the page");
		ageFactorNextButton.click();

	}

	@Then("^(?:I|i) choose Children Age as 3 years$")
	public void select_children_age() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement childrenAge = this.dt.getAgeOfChildren();
		Assert.assertNotNull(childrenAge, "Children Age is not appearing for url ");
		this.capabilities.reportStepInfo("Children Age is present on the page");
		childrenAge.click();

	}

	@Then("^(?:I|i) click on Next Button for Children Age Quiz$")
	public void click_childrenAge_next_button() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement childrenAgeNextButton = this.dt.getNextButtonChildrenAgeFactor();
		Assert.assertNotNull(childrenAgeNextButton, "Children Age Next Button is not appearing for url ");
		this.capabilities.reportStepInfo("Children Age Next Button is present on the page");
		childrenAgeNextButton.click();

	}

	@Then("^(?:I|i) choose more than 1 Children$")
	public void more_than1_children() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement middleAgeChildren = this.dt.getMiddleAgeChildren();
		WebElement finalAgeChildren = this.dt.getFinalAgeChildren();
		Assert.assertNotNull(middleAgeChildren, "middleAgeChildren is not appearing for url ");
		this.capabilities.reportStepInfo("middleAgeChildren is present on the page");
		middleAgeChildren.click();
		Assert.assertNotNull(finalAgeChildren, "finalAgeChildren is not appearing for url ");
		this.capabilities.reportStepInfo("finalAgeChildren is present on the page");
		finalAgeChildren.click();

	}

	@Then("^(?:I|i) click on Next Button for Multiple Selected Children Age Quiz$")
	public void click_multiplechildrenAge_next_button() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement multipleChildrenAgeNextButton = this.dt.getNextButtonMultipleChildren();
		Assert.assertNotNull(multipleChildrenAgeNextButton,
				"Multiple Children Age Next Button is not appearing for url ");
		this.capabilities.reportStepInfo("Multiple Children Age Next Button is present on the page");
		multipleChildrenAgeNextButton.click();
		this.capabilities.reportStepInfo("Multiple Children Age Next Button is clicked");
	}

	@Then("^(?:I|i) choose more than 1 Interests$")
	public void more_than1_interest() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement interestDT1 = this.dt.getInterest_01();
		WebElement interestDT2 = this.dt.getInterest_02();
		Assert.assertNotNull(interestDT1, "Interest 1 is not appearing for url ");
		this.capabilities.reportStepInfo("Interest 1 is present on the page");
		interestDT1.click();
		Assert.assertNotNull(interestDT2, "Interest 2 is not appearing for url ");
		this.capabilities.reportStepInfo("Interest 2 is present on the page");
		interestDT2.click();

	}

	@Then("^(?:I|i) click on Next Button for Multiple Selected Interest Quiz$")
	public void click_allresult_next_button() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement allResult = this.dt.getNextButtonAllResult();
		Assert.assertNotNull(allResult, "Multiple Selected Interest Button is not appearing for url ");
		this.capabilities.reportStepInfo("Multiple Selected Interest Next Button is present on the page");
		allResult.click();

	}

	@Then("^(?:I|i) verify Thankyou message is present on DT Result Page$")
	public void i_verify_that_thankyou_msg_is_rendering() {
		WebElement thankyouMessage = this.dt.getThankYouMsg();
		Assert.assertNotNull(thankyouMessage, "Thankyou message is not rendering for url ");
		this.capabilities.reportStepInfo("Thankyou message is present on page");
	}

	@Then("^(?:I|i) verify Relationship message is present on DT Result Page$")
	public void i_verify_that_relationship_msg_is_rendering() {
		WebElement relationshipMessage = this.dt.getRelationShipMsg();
		Assert.assertNotNull(relationshipMessage, "RelationShip message is not rendering for url ");
		this.capabilities.reportStepInfo("RelationShip message is present on page");
	}

	@And("^(?:I|i) verify that RelationShip Article count is (\\d+)$")
	public void verifyrltnarticlecount(int count) {
		this.actions.Wait.waitForPageReady();
		int rltnArticlecount = this.dt.getRelationShipArticleCount();
		Assert.assertEquals(rltnArticlecount, count, "Component Count doesn't matches");

	}

	@Then("^(?:I|i) verify CTA is present for Relationship Message$")
	public void i_verify_CTA_relationship_msg_is_rendering() {
		WebElement relationshipCTA = this.dt.getRelationShipCTA();
		Assert.assertNotNull(relationshipCTA, "RelationShip CTA is not rendering for url ");
		this.capabilities.reportStepInfo("RelationShip CTA is present on page");
	}

	@And("^(?:I|i) click on the cta of the Relationship Message$")
	public void click_CTA_URL_relationship_msg() {
		WebElement ctarelationshipmsg = this.dt.getRelationShipCTA();
		Assert.assertNotNull(ctarelationshipmsg, "Relationship Message cta is not appearing for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Relationship Message cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("relationshipmsgctahref",
				ctarelationshipmsg.getAttribute("href"));
		ctarelationshipmsg.click();

	}

	@Then("^it should take me to the corresponding page of Relationship Message cta$")
	public void verify_CTA_URL_relationship_msg() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("relationshipmsgctahref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(actualUrl.equalsIgnoreCase(expectedUrl), "page hasn't navigated to expected url => "
				+ expectedUrl + ". instead it was navigated to => " + actualUrl);
		this.capabilities.reportStepInfo("page is successfully navigated to url - " + expectedUrl);
	}

	@Then("^(?:I|i) verify Female Stereotype message is present on DT Result Page$")
	public void i_verify_that_FemaleStereotype_msg_is_rendering() {
		WebElement fmlMessage = this.dt.getFemaleStereotypeMsg();
		Assert.assertNotNull(fmlMessage, "Female Stereotype message is not rendering for url ");
		this.capabilities.reportStepInfo("Female Stereotype message is present on page");
	}
	
	@And("^(?:I|i) verify that Female Stereotype Article count is (\\d+)$")
	public void verifyfmlarticlecount(int count) {
		this.actions.Wait.waitForPageReady();
		int fmlArticlecount = this.dt.getFemaleStereoArticleCount();
		Assert.assertEquals(fmlArticlecount, count, "Component Count doesn't matches");

	}
	@Then("^(?:I|i) verify CTA is present for Female Stereotype Message$")
	public void i_verify_CTA_fml_strotype_is_rendering() {
		WebElement fmlstereoCTA = this.dt.getfmlstereotypeCTA();
		Assert.assertNotNull(fmlstereoCTA, "Female Stereotype CTA is not rendering for url ");
		this.capabilities.reportStepInfo("Female Stereotype CTA is present on page");
	}

	@And("^(?:I|i) click on the cta of the Female Stereotype Message$")
	public void click_CTA_URL_FemaleStereotype_msg() {
		WebElement ctafmlsmsg = this.dt.getfmlstereotypeCTA();
		Assert.assertNotNull(ctafmlsmsg, "Female Stereotype Message cta is not appearing for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Female Stereotype Message cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("fmlstereotypemsgctahref",
				ctafmlsmsg.getAttribute("href"));
		ctafmlsmsg.click();

	}

	@Then("^it should take me to the corresponding page of Female Stereotype message cta$")
	public void verify_CTA_URL_FemaleStereotype_msg() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("fmlstereotypemsgctahref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(actualUrl.equalsIgnoreCase(expectedUrl), "page hasn't navigated to expected url => "
				+ expectedUrl + ". instead it was navigated to => " + actualUrl);
		this.capabilities.reportStepInfo("page is successfully navigated to url - " + expectedUrl);
	}

	@Then("^(?:I|i) verify Personal Goal message is present on DT Result Page$")
	public void i_verify_that_PersonalGoal_msg_is_rendering() {
		WebElement PersonalMessage = this.dt.getPersonalGoalMsg();
		Assert.assertNotNull(PersonalMessage, "Personal Goal message is not rendering for url ");
		this.capabilities.reportStepInfo("Personal Goal message is present on page");
	}

	@And("^(?:I|i) verify that Personal Goal Article count is (\\d+)$")
	public void verifypersonalgoalarticlecount(int count) {
		this.actions.Wait.waitForPageReady();
		int prsnlGoalArticlecount = this.dt.getPersonalGoalArticleCount();
		Assert.assertEquals(prsnlGoalArticlecount, count, "Component Count doesn't matches");

	}
	
	@Then("^(?:I|i) verify CTA is present for Personal Goal Message$")
	public void i_verify_CTA_personalgoal_is_rendering() {
		WebElement personalCTA = this.dt.getPersoanlGoalCTA();
		Assert.assertNotNull(personalCTA, "Personal Goal CTA is not rendering for url ");
		this.capabilities.reportStepInfo("Personal Goal CTA is present on page");
	}

	@And("^(?:I|i) click on the cta of the Personal Goal Message$")
	public void click_CTA_URL_PersonalGoal_msg() {
		WebElement ctapersonalmsg = this.dt.getRelationShipCTA();
		Assert.assertNotNull(ctapersonalmsg, "Personal Goal Message cta is not appearing for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Personal Goal Message cta is present on page");
		this.capabilities.getScenarioContext().addScenarioData("personalgoalmsgctahref",
				ctapersonalmsg.getAttribute("href"));
		ctapersonalmsg.click();

	}

	@Then("^it should take me to the corresponding page of Personal Goal Message cta$")
	public void verify_CTA_URL_PersonalGoal_msg() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("personalgoalmsgctahref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(actualUrl.equalsIgnoreCase(expectedUrl), "page hasn't navigated to expected url => "
				+ expectedUrl + ". instead it was navigated to => " + actualUrl);
		this.capabilities.reportStepInfo("page is successfully navigated to url - " + expectedUrl);
	}

	@And("^(?:i|I) enter random email id for DT$")
	public void enterRandomEmailAddressDT() throws Throwable {
		String randomText = RandomStringUtils.random(10, true, true);
		this.dt.enterEmailAddressDT(randomText + "@gmail.com");
	}

	@And("^(?:I|i) click on the Submit Button for email DT$")
	public void click_submitbutton_email_dt() {
		WebElement submitButton_DT = this.dt.getSubmitButtonDT();
		Assert.assertNotNull(submitButton_DT, "Submit Button for email DT is not appearing for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("Submit Button for email DT is present on page");
		this.capabilities.getScenarioContext().addScenarioData("personalgoalmsgctahref",
				submitButton_DT.getAttribute("href"));
		submitButton_DT.click();

	}

	@Then("^(?:I|i) verify Success Message is present for DT Email$")
	public void i_verify_email_success_msg() {
		WebElement emailSuccessMsg = this.dt.getSuccessMsgDT();
		Assert.assertNotNull(emailSuccessMsg, "Success Message is not rendering");
		this.capabilities.reportStepInfo("Success Message is present on page");
	}

	@Then("^(?:I|i) verify Social Share Link is present at bottom of DT Page$")
	public void i_verify_socia_share() {
		WebElement fbSocialIcon = this.dt.getFBIcon();
		Assert.assertNotNull(fbSocialIcon, "FB Icon is not rendering");
		this.capabilities.reportStepInfo("FB Icon is present on page");
		WebElement twitterSocialIcon = this.dt.getTwitterIcon();
		Assert.assertNotNull(twitterSocialIcon, "Twitter Icon is not rendering");
		this.capabilities.reportStepInfo("Twitter Icon is present on page");
	}

	@And("^(?:I|i) verify that options have following text \"([^\"]*)\"$")
	public void verifyAllContent(String optionText) {
		actions.Wait.waitForPageReady();
		String[] dtOptionNames1 = optionText.split(";");
		System.out.println(dtOptionNames1.length);
		for (int i = 0; i < dtOptionNames1.length; i++)
			System.out.println(dtOptionNames1[i]);
		this.dt.getDTOptionListItems(optionText);
		Assert.assertNotNull("Options are not appearing on page" + optionText);

	}

	@And("^(?:I|i) verify that options count is (\\d+)$")
	public void verifycount(int count) {
		this.actions.Wait.waitForPageReady();
		int Optcount = this.dt.getOptionsCountOnPage();
		Assert.assertEquals(Optcount, count, "Component Count doesn't matches");

	}

	@Then("^(?:I|i) verify \"([^\"]*)\" is rendering for Question$")
	public void verify_question_text(String questionText) {
		actions.Wait.waitForPageReady();
		this.dt.getDT_QuestionText(questionText);
		Assert.assertNotNull("Options are not appearing on page" + questionText);

	}

	@And("^(?:I|i) click on the Start Again Button for DT$")
	public void click_startAgainButton() {
		WebElement startAgainButton_DT = this.dt.getStartAgainButton();
		Assert.assertNotNull(startAgainButton_DT, "Start Again Button for email DT is not appearing for url");
		this.capabilities.reportStepInfo("Start Again Button for email DT is present on page");
		startAgainButton_DT.click();

	}

	@When("^(?:i|I) choose \"([^\"]*)\" as Gender$")
	public void choose_gender(String optionText) {
		actions.Wait.waitForPageReady();
		this.dt.getDTGender(optionText);
		this.capabilities.reportStepInfo("clicked");

	}
	@When("^(?:i|I) choose \"([^\"]*)\" for AgeFactor Quiz$")
	public void choose_agefactor(String agefactorText) {
		actions.Wait.waitForPageReady();
		this.dt.getDTAgeFactorSelection(agefactorText);
		this.capabilities.reportStepInfo("clicked");

	}
	@When("^(?:i|I) choose \"([^\"]*)\" as Children Age$")
	public void choose_childrenage(String childrenageText) {
		actions.Wait.waitForPageReady();
		this.dt.getDTChildrenAgeSelection(childrenageText);
		this.capabilities.reportStepInfo("clicked");

	}
	
	@When("^(?:i|I) choose \"([^\"]*)\" as Age of Children$")
	public void choose_age_of_children(String ageText) {
		actions.Wait.waitForPageReady();
		this.dt.getDTAgeSelection(ageText);
		this.capabilities.reportStepInfo("clicked");

	}
	
	@When("^(?:i|I) choose \"([^\"]*)\" as Interest$")
	public void choose_interest(String interestText) {
		actions.Wait.waitForPageReady();
		String[] dtinterestText = interestText.split(";");
		System.out.println(dtinterestText.length);
		for (int i = 0; i < dtinterestText.length; i++)
			System.out.println(dtinterestText[i]);
		this.dt.getDTInterestSelection(interestText);
		this.capabilities.reportStepInfo("clicked");

	}

}
