package steps.business.generic;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import framework.shared.RandomUtil;
import steps.generic.keywords.World;
//import steps.generic.SetCapabilities;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.BackToTopPage;
import unilever.pageobjects.platform.publish.components.definitions.RelatedProductsCitra;

public class StepsRelatedProductsCitra {
	UserActions actions;
	//SetCapabilities capabilities;
	World capabilities;
	PublishedPageCommon publishedPage;
	BackToTopPage backtotopPage;
	RelatedProductsCitra relatedproductPage;

	public StepsRelatedProductsCitra(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.backtotopPage = new BackToTopPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.relatedproductPage = new RelatedProductsCitra(capabilities.getWebDriver(),capabilities.getBrandName());

	}

	@And("^(?:I|i) click on the citra product image$")
	public void click_related_product_image() {
		WebElement articleImage = this.relatedproductPage.getRelatedProductImages();
		Assert.assertNotNull(articleImage,
				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
				articleImage.getAttribute("href"));
		articleImage.click();
		this.capabilities.reportStepInfo("related article image has been clicked");
	}


	@Then("^(?:I|i) verify that citra related product component image is rendering$")
	public void i_verify_that_relatedproduct_component_image_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleImage = this.relatedproductPage.getRelatedProductImages();
		Assert.assertNotNull(relatedarticleImage,
				"related article image is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article Image is present on page");
	}

	@Then("^(?:I|i) verify that citra related product component CTA is appearing$")
	public void i_verify_that_relatedproduct_component_description_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleDescription = this.relatedproductPage.getRelatedProductCTA();
		Assert.assertNotNull(relatedarticleDescription, "related article description is not rendering for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article description is present on page");
	}

}
