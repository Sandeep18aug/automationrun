package steps.business.generic.adp;


import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.util.Strings;
import cucumber.api.java.en.And;
import steps.business.generic.UserActions;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.alp.ArticleListingPage;

public class StepsArticleDetailedPage {

	UserActions actions;
	World capabilities;
	ArticleListingPage alpPage;

	public StepsArticleDetailedPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.alpPage = new ArticleListingPage(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@And("^(?:I|i)t should take me to (?:an|the) article detailed page$")
	public void verifyArticleDetailedPageLoads() {
		actions.Wait.waitForPageReady();
		String submenu = (String) capabilities.getScenarioContext().getScenarioData("submenutext");
		if(Strings.isNotNullAndNotEmpty(submenu))
			this.capabilities.reportStepInfo("(?:i|I)n previous step, following submenu was clicked - " + submenu);		
		String href = (String) capabilities.getScenarioContext().getScenarioData("submenuhref");
		String errorMessage = "";
		if(!Strings.isNullOrEmpty(href))
			errorMessage = "after clicking on " + submenu + " submenu, page hasn't navigated to correct url. " + href;
		else {
			href = (String) capabilities.getScenarioContext().getScenarioData("selectedpagelistingv2itemurl");
			errorMessage = "page hasn't navigated to correct url. " + href;
		}
		Assert.assertTrue(capabilities.getWebDriver().getCurrentUrl().equalsIgnoreCase(href),
				errorMessage);
		this.capabilities.reportStepInfo("page is successfully navigated to url - " + href);
	}
	
	@And("^(?:I|i) (?:hover over|select|click on) the tab1 of anchor link navigation$")
	public void clickanchorlink1() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement1 = alpPage.getanchorlink1();
		actions.Hover.hover(tabElement1);
		actions.Click.click(tabElement1);
		System.out.println("successfully clicked or hover on anchor link 1 ");
		actions.Wait.waitForPageReady();
	}
	
	@And("^(?:I|i) (?:hover over|select|click on) the tab2 of anchor link navigation$")
	public void clickanchorlink2() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement2 = alpPage.getanchorlink2();
		actions.Hover.hover(tabElement2);
		actions.Click.click(tabElement2);
		System.out.println("successfully clicked or hover on anchor link 2 ");
		actions.Wait.waitForPageReady();
	}
	
	@And("^(?:I|i) (?:hover over|select|click on) the tab3 of anchor link navigation$")
	public void clickanchorlink3() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement3 = alpPage.getanchorlink3();
		actions.Hover.hover(tabElement3);
		actions.Click.click(tabElement3);
		System.out.println("successfully clicked or hover on anchor link 1 ");
		actions.Wait.waitForPageReady();
	}
	
	@And("^(?:I|i) (?:hover over|select|click on) the tab4 of anchor link navigation$")
	public void clickanchorlink4() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement4 = alpPage.getanchorlink4();
		actions.Hover.hover(tabElement4);
		actions.Click.click(tabElement4);
		System.out.println("successfully clicked or hover on anchor link 1 ");
		actions.Wait.waitForPageReady();
	}
	
	@And("^(?:I|i) (?:hover over|select|click on) the tab5 of anchor link navigation$")
	public void clickanchorlink5() throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement tabElement5 = alpPage.getanchorlink5();
		actions.Hover.hover(tabElement5);
		actions.Click.click(tabElement5);
		System.out.println("successfully clicked or hover on anchor link 5 ");
		actions.Wait.waitForPageReady();
	}
	
}
