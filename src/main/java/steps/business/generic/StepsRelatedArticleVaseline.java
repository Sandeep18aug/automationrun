package steps.business.generic;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import framework.shared.RandomUtil;
//import steps.generic.SetCapabilities;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.BackToTopPage;
import unilever.pageobjects.platform.publish.components.definitions.RelatedArticleVaseline;

public class StepsRelatedArticleVaseline {
	UserActions actions;
	//SetCapabilities capabilities;
	World capabilities;
	PublishedPageCommon publishedPage;
	BackToTopPage backtotopPage;
	RelatedArticleVaseline relatedarticlevaselinePage;

	public StepsRelatedArticleVaseline(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.backtotopPage = new BackToTopPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.relatedarticlevaselinePage = new RelatedArticleVaseline(capabilities.getWebDriver(),capabilities.getBrandName());

	}


	@And("^(?:I|i) click on any of the items of vaseline article page$")
	public void clickOnAnyItemOfArticlePage() {
		actions.Wait.waitForPageReady();
		List<WebElement> relatedarticleItems = this.relatedarticlevaselinePage.getRelatedArticleVaselineItems();
		Assert.assertNotNull(relatedarticleItems, "articles are not appearing on page");
		int randomNumber = RandomUtil.getRandomNumber(1, relatedarticleItems.size() - 1);
		WebElement listItem = relatedarticleItems.get(randomNumber);
		String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("selectedrelatedarticleitemurl", href);
		listItem.click();
		this.capabilities.reportStepInfo("clicked on article item with href " + href);
	}

	@And("^(?:I|i) click on the vaseline article image$")
	public void click_related_article_image() {
		WebElement articleImage = this.relatedarticlevaselinePage.getRelatedArticleVaselineImages();
		Assert.assertNotNull(articleImage,
				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
				articleImage.getAttribute("href"));
		articleImage.click();
		this.capabilities.reportStepInfo("related article image has been clicked");
	}
	
	@Then("^it should take me to the corresponding page of vaseline article image$")
	public void verify_related_article_image() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext()
				.getScenarioData("relatedarticleimagehref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		System.out.println("###########");
		
		System.out.println("expectedUrl"+expectedUrl);
		if(actualUrl!=null) {
			System.out.println("actualUrl"+actualUrl);
		}
		System.out.println("###########");
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related article cta href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
		this.capabilities.reportStepInfo("You have been taken to the corresponding page");
	}


	@Then("^(?:I|i) verify that vaseline related article component image is rendering$")
	public void i_verify_that_relatedarticle_component_image_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleImage = this.relatedarticlevaselinePage.getRelatedArticleVaselineImages();
		Assert.assertNotNull(relatedarticleImage,
				"related article image is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article Image is present on page");
	}

	@Then("^(?:I|i) verify that vaseline related article component heading is appearing$")
	public void i_verify_that_relatedarticle_component_heading_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleHeading = this.relatedarticlevaselinePage.getRelatedArticleVaselineHeading();
		Assert.assertNotNull(relatedarticleHeading,
				"related article heading is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article heading is present on page");
	}

	@Then("^(?:I|i) verify that vaseline related article component description is appearing$")
	public void i_verify_that_relatedarticle_component_description_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleDescription = this.relatedarticlevaselinePage.getRelatedArticleVaselineDescription();
		Assert.assertNotNull(relatedarticleDescription, "related article description is not rendering for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article description is present on page");
	}

}
