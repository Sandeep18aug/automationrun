package steps.business.generic;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import framework.shared.RandomUtil;
import steps.generic.keywords.World;
//import steps.generic.SetCapabilities;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.BackToTopPage;
import unilever.pageobjects.platform.publish.components.definitions.MultipleRelatedArticlesSidebar;

public class StepsMultipleRelatedArticlesSidebar {
	UserActions actions;
	//SetCapabilities capabilities;
	World capabilities;
	PublishedPageCommon publishedPage;
	BackToTopPage backtotopPage;
	MultipleRelatedArticlesSidebar multiplerelatedarticlePage;

	public StepsMultipleRelatedArticlesSidebar(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.backtotopPage = new BackToTopPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.multiplerelatedarticlePage = new MultipleRelatedArticlesSidebar(capabilities.getWebDriver(),capabilities.getBrandName());

	}


	@Then("^it should take me to the corresponding page of multiple-related-articles-sidebar cta$")
	public void verifyrelatedarticleCtaUrlPage() throws Throwable {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext().getScenarioData("relatedarticlectahref");
		this.actions.Wait.wait("3");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();

		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related article cta href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);

	}

	@And("^(?:I|i) click on any of the items of multiple-related-articles-sidebar page$")
	public void clickOnAnyItemOfArticlePage() {
		actions.Wait.waitForPageReady();
		List<WebElement> relatedarticleItems = this.multiplerelatedarticlePage.getMultipleRelatedArticleItems();
		Assert.assertNotNull(relatedarticleItems, "articles are not appearing on page");
		int randomNumber = RandomUtil.getRandomNumber(1, relatedarticleItems.size() - 1);
		WebElement listItem = relatedarticleItems.get(randomNumber);
		String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("selectedrelatedarticleitemurl", href);
		listItem.click();
		this.capabilities.reportStepInfo("clicked on article item with href " + href);
	}

	@And("^(?:I|i) click on the multiple-related-articles-sidebar image$")
	public void click_multiple_related_article_image() {
		WebElement articleImage = this.multiplerelatedarticlePage.getMultipleRelatedArticleImages();
		Assert.assertNotNull(articleImage,
				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.getScenarioContext().addScenarioData("multiplerelatedarticleimagehref",
				articleImage.getAttribute("href"));
		articleImage.findElement(By.xpath(".//img")).click();
		//articleImage.click();
		this.capabilities.reportStepInfo("multiple related article sidebar image has been clicked");
	}
	

	@Then("^it should take me to the corresponding page of multiple-related-articles-sidebar image$")
	public void verify_multiple_related_article_image() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext()
				.getScenarioData("multiplerelatedarticleimagehref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related article cta href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
		this.capabilities.reportStepInfo("You have been taken to the corresponding page");
	}

	@Then("^(?:I|i) verify that multiple-related-articles-sidebar component image is rendering$")
	public void i_verify_that_relatedarticle_component_image_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleImage = this.multiplerelatedarticlePage.getMultipleRelatedArticleImages();
		Assert.assertNotNull(relatedarticleImage,
				"related article image is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("multiple related article sidebar Image is present on page");
	}

	@Then("^(?:I|i) verify that multiple-related-articles-sidebar component heading is appearing$")
	public void i_verify_that_relatedarticle_component_heading_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleHeading = this.multiplerelatedarticlePage.getMultipleRelatedArticleHeading();
		Assert.assertNotNull(relatedarticleHeading,
				"related article heading is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("multiple related article sidebar heading is present on page");
	}
	
	@Then("^(?:I|i) verify that multiple-related-articles-sidebar component description is appearing$")
	public void i_verify_that_relatedarticle_component_description_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement relatedarticleDescription = this.multiplerelatedarticlePage.getMultipleRelatedArticleDescription();
		Assert.assertNotNull(relatedarticleDescription, "related article description is not rendering for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article description is present on page");
	}



}
