package steps.business.generic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import steps.generic.keywords.World;

public class StepsMagicFinderFunctionality {

	UserActions actions;
	World world;
	String magicFinderOptionWithPlaceholder = "//div[contains(@class,'que-active') and descendant::h2[contains(translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),translate('${heading}','ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'))]]//div[contains(translate(@data-res-text,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),translate('${option}','ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'))]";

	public StepsMagicFinderFunctionality(World world, UserActions getBasicKeywords) {
		this.actions = getBasicKeywords;
		this.world = world;
	}

	@And("^I select option with text \"([^\"]*)\" of magic finder page with Heading \"([^\"]*)\"$")
	public void selectMagicFinder(String optionText, String magicFinderHeading) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		String currentMagicFinderOptionLocator = magicFinderOptionWithPlaceholder.replace("${heading}",
				magicFinderHeading);
		currentMagicFinderOptionLocator = currentMagicFinderOptionLocator.replace("${option}", optionText);
		System.out.println(currentMagicFinderOptionLocator);
		WebElement magicFinderOption = world.getWebElementFactory().getElement(By.xpath(currentMagicFinderOptionLocator));
		if (magicFinderOption != null)
			magicFinderOption.click();
		else
			Assert.fail("No Magic finder option contains text  " + optionText);
	}
}
