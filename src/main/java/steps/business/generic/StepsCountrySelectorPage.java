package steps.business.generic;

import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.RandomUtil;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.CountrySelector;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.homepage.HomePage;
import unilever.pageobjects.platform.publish.pdp.ProductDetailPage;
import unilever.pageobjects.platform.publish.pdp.WriteAReviewDialog;

public class StepsCountrySelectorPage {

	UserActions actions;
	World capabilities;
	CountrySelector countrySelectorPage;
	PublishedPageCommon publishedPageCommon;

	public StepsCountrySelectorPage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.countrySelectorPage = new CountrySelector(capabilities.getWebDriver(),capabilities.getBrandName());
		this.publishedPageCommon = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@Then("i should be able to see country selector link in global footer")
	public void i_should_be_able_to_see_country_selector_link_in_global_footer() throws Throwable {
		this.actions.Wait.waitForPageReady();
		Assert.assertNotNull(this.countrySelectorPage.getGlobalFooter().getCountrySelector(),
				"country selector not present in global footer");
	}

	@Then("i click link by text \"([^\']*)\" in global footer")
	public void clickGlobalFooterLinkHavingText(String text) throws Throwable {
		this.actions.Wait.waitForPageReady();
		this.actions.Scroll.scrollPublishedPageToBottom();
		this.countrySelectorPage.getGlobalFooter().selectFooterNavItemByText(text);
		this.actions.Wait.wait("3");
	}

	@When("i click on country selector link in global footer")
	public void i_click_on_country_selector_link_in_global_footer() throws Throwable {
		this.actions.Wait.waitForPageReady();
		WebElement country = this.countrySelectorPage.getGlobalFooter().getCountrySelector();
		this.actions.Click.clickByJavaScript(country);
		this.capabilities.reportStepInfo("successfully clicked at country selector link in global footer");

	}

	@Then("i should be taken to country selector page")
	public void i_should_be_taken_to_country_selector_page() throws Throwable {
		this.actions.Wait.waitForPageReady();
		actions.SwitchWindow.switchTabByIndex(2);
		this.capabilities.reportStepInfo("country selector page url = " + this.capabilities.getWebDriver().getCurrentUrl());
	}

	@When("i select \"([^\"]*)\" country from country selector page")
	public void selectCountryByText(String countryText) throws Throwable {
		this.actions.Wait.waitForPageReady();
		int totalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
//		List<WebElement> countryList = this.countrySelectorPage.getCountryList();
//		int countryListSize = RandomUtil.getRandomNumber(1, countryList.size());
		//WebElement country = countryList.get(countryListSize - 1);
		WebElement country = this.countrySelectorPage.getCountryObject(countryText);
		WebElement countryCategory = null;
		try {
			countryCategory = country.findElement(By.xpath("./ancestor::*[child::h4]/h4"));
			if (countryCategory != null) {
				countryCategory.findElement(By.xpath(".//a")).click();
				this.actions.Wait.wait("5");
			}
		} catch (Exception ex) {
		}

		WebElement countryLink = country.getTagName().equalsIgnoreCase("a") ? country
				: country.findElement(By.xpath(".//a"));
		String countryLinkUrl = countryLink.getAttribute("href");
		//this.capabilities.getWebElementFactory().focusElement(countryLink);
		this.actions.Wait.wait("3");
		//this.actions.Click.click(countryLink);
		countryLink.click();
		this.actions.Wait.wait("3");
		int currentTotalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		if (totalOpenedWindows < currentTotalOpenedWindows) {
			this.actions.SwitchWindow.switchTabByIndex(currentTotalOpenedWindows);
		}
		System.out.println("successfully clicked on country with url - " + countryLinkUrl);
		this.capabilities.getScenarioContext().addScenarioData("href", countryLinkUrl);
	}
	
	@When("i select any country from country selector page")
	public void i_select_any_country_from_country_selector_page() throws Throwable {
		this.actions.Wait.waitForPageReady();
		int totalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		List<WebElement> countryList = this.countrySelectorPage.getCountryList();
		int countryListSize = RandomUtil.getRandomNumber(1, countryList.size());
		WebElement country = countryList.get(countryListSize - 1);
		WebElement countryCategory = null;
		try {
			countryCategory = country.findElement(By.xpath("./ancestor::*[child::h4]/h4"));
			if (countryCategory != null) {
				countryCategory.findElement(By.xpath(".//a")).click();
				this.actions.Wait.wait("2");
			}
		} catch (Exception ex) {
		}

		WebElement countryLink = country.getTagName().equalsIgnoreCase("a") ? country
				: country.findElement(By.xpath(".//a"));
		String countryLinkUrl = countryLink.getAttribute("href");
		countryLink.click();
		this.actions.Wait.wait("3");
		int currentTotalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		if (totalOpenedWindows < currentTotalOpenedWindows) {
			this.actions.SwitchWindow.switchTabByIndex(currentTotalOpenedWindows);
		}
		System.out.println("successfully clicked on country with url - " + countryLinkUrl);
		this.capabilities.getScenarioContext().addScenarioData("href", countryLinkUrl);
	}

	@When("i select any country from (.+) country selector page")
	public void selectAnyCountryForBrandSpecific(String brandName) throws Throwable {
		this.actions.Wait.waitForPageReady();
		int totalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		List<WebElement> countryList = this.countrySelectorPage.getCountryList();
		int countryListSize = RandomUtil.getRandomNumber(1, countryList.size() - 1);
		WebElement country = countryList.get(countryListSize);
		WebElement countryCategory = null;
		try {
			countryCategory = country.findElement(By.xpath("./ancestor::*[child::h4]/h4"));
			if (countryCategory != null) {
				countryCategory.findElement(By.xpath(".//a")).click();
				this.actions.Wait.wait("2");
			}
		} catch (Exception ex) {
		}

		WebElement countryLink = country.getTagName().equalsIgnoreCase("a") ? country
				: country.findElement(By.xpath(".//a"));
		String countryLinkUrl = countryLink.getAttribute("href");
		countryLink.findElement(By.cssSelector(".c-language-label__name")).click();
		this.actions.Wait.wait("3");
		int currentTotalOpenedWindows = this.capabilities.getWebDriver().getWindowHandles().size();
		if (totalOpenedWindows < currentTotalOpenedWindows) {
			this.actions.SwitchWindow.switchTabByIndex(currentTotalOpenedWindows);
		}
		System.out.println("successfully clicked on country with url - " + countryLinkUrl);
		this.capabilities.getScenarioContext().addScenarioData("href", countryLinkUrl);
	}

	@Then("^i should be taken to corresponding page$")
	public void i_should_be_taken_to_corresponding_page() {
		this.actions.Wait.waitForPageReady();
		String expectedBaseUrl = publishedPageCommon.getBaseURLAndSubPath((String) this.capabilities.getScenarioContext().getScenarioData("href"));
		String currentBaseUrl = publishedPageCommon.getBaseURLAndSubPath(capabilities.getWebDriver().getCurrentUrl());
		Assert.assertTrue(currentBaseUrl.equalsIgnoreCase(expectedBaseUrl), "It is navigated to url - " + currentBaseUrl
				+ System.lineSeparator() + " but was expected to be navigated to - " + expectedBaseUrl + ".");
	}
	
	@When("i should be able to see country suggestion list")
	public void see_country_suggestion_list() throws Throwable {
		this.actions.Wait.waitForPageReady();
		int countrysuggestionlist = this.countrySelectorPage.getCountrySuggestionList().size();
		this.capabilities.reportStepInfo("country suggestion list after writing 3 text is " + countrysuggestionlist);
		Assert.assertTrue(countrysuggestionlist >= 0, "country suggestion list is not working properly");
	
	}
	
	@Then("i should not be able to see country suggestion list")
	public void not_able_see_country_suggestion_list() throws Throwable {
		this.actions.Wait.waitForPageReady();
		int countrysuggestionlist = this.countrySelectorPage.getCountrySuggestionList().size();
		this.capabilities.reportStepInfo("country suggestion list after writing incorrect or 2 text " + countrysuggestionlist);
		Assert.assertTrue(countrysuggestionlist <= 0, "country suggestion list is not working properly");
	
	}
	
	@And("^(?:i|I) search country with text \"([^\"]*)\"$")
	public void searchcountryas(String Countrysuggestion) throws ObjectNotFoundInORException {
		this.countrySelectorPage.enterCountrySuggestion(Countrysuggestion);

	}

}
