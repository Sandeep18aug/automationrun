package steps.business.generic;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import framework.shared.RandomUtil;
//import steps.generic.SetCapabilities;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;
import unilever.pageobjects.platform.publish.components.definitions.BackToTopPage;
import unilever.pageobjects.platform.publish.components.definitions.WhatsNewNonD2;

public class StepsWhatsNewNonD2 {
	UserActions actions;
	//SetCapabilities capabilities;
	World capabilities;
	PublishedPageCommon publishedPage;
	BackToTopPage backtotopPage;
	WhatsNewNonD2 whatsnewnond2Page;

	public StepsWhatsNewNonD2(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
		this.backtotopPage = new BackToTopPage(capabilities.getWebDriver(),capabilities.getBrandName());
		this.whatsnewnond2Page = new WhatsNewNonD2(capabilities.getWebDriver(),capabilities.getBrandName());

	}


	@And("^(?:I|i) click on any of the items of whats new page$")
	public void clickOnAnyItemOfArticlePage() {
		actions.Wait.waitForPageReady();
		List<WebElement> relatedarticleItems = this.whatsnewnond2Page.getWhatsNewNonD2Items();
		Assert.assertNotNull(relatedarticleItems, "articles are not appearing on page");
		int randomNumber = RandomUtil.getRandomNumber(1, relatedarticleItems.size() - 1);
		WebElement listItem = relatedarticleItems.get(randomNumber);
		String href = listItem.findElement(By.cssSelector("*[href]")).getAttribute("href");
		capabilities.getScenarioContext().addScenarioData("selectedrelatedarticleitemurl", href);
		listItem.click();
		this.capabilities.reportStepInfo("clicked on article item with href " + href);
	}

	@And("^(?:I|i) click on the whats new heading$")
	public void click_bwsLayout_wordpress_image() {
		WebElement bwsarticleImage = this.whatsnewnond2Page.getWhatsNewNonD2Heading();
		Assert.assertNotNull(bwsarticleImage,
				"related article image is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.getScenarioContext().addScenarioData("relatedarticleimagehref",
				bwsarticleImage.getAttribute("href"));
		bwsarticleImage.click();
		this.capabilities.reportStepInfo("sublayout image has been clicked");
	}
	

	@Then("^it should take me to the corresponding page of whats new image$")
	public void verify_bwsLayout_wordpress_image() {
		this.actions.Wait.waitForPageReady();
		String expectedUrl = (String) this.capabilities.getScenarioContext()
				.getScenarioData("relatedarticleimagehref");
		String actualUrl = this.capabilities.getWebDriver().getCurrentUrl();
		Assert.assertTrue(expectedUrl.equals(actualUrl),
				"the page is not navigated to expected url. related article cta href was " + expectedUrl
						+ System.lineSeparator() + " actual url is " + actualUrl);
		this.capabilities.reportStepInfo("You have been taken to the corresponding page");
	}


	@Then("^(?:I|i) verify that whats new component image is rendering$")
	public void i_verify_that_bwsLayout_wordpress_component_image_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement bwsLayoutWordpressImage = this.whatsnewnond2Page.getWhatsNewNonD2Images();
		Assert.assertNotNull(bwsLayoutWordpressImage,
				"related article image is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article Image is present on page");
	}

	@Then("^(?:I|i) verify that whats new component heading is appearing$")
	public void i_verify_that_bwsLayout_wordpress_component_heading_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement bwsLayoutWordpressHeading = this.whatsnewnond2Page.getWhatsNewNonD2Heading();
		Assert.assertNotNull(bwsLayoutWordpressHeading,
				"related article heading is not rendering for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article heading is present on page");
	}

	@Then("^(?:I|i) verify that whats new component description is appearing$")
	public void i_verify_that_bwsLayout_wordpress_component_description_is_rendering() {
		// Write code here that turns the phrase above into concrete actions
		WebElement bwsLayoutWordpressDescription = this.whatsnewnond2Page.getWhatsNewNonD2Description();
		Assert.assertNotNull(bwsLayoutWordpressDescription, "related article description is not rendering for url "
				+ this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("related article description is present on page");
	}

}
