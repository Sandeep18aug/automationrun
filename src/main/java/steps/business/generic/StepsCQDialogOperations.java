package steps.business.generic;

import cucumber.api.java.en.Given;
import framework.shared.FrameworkConstants;
import steps.generic.keywords.*;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class StepsCQDialogOperations {

	String TabText = "//a[translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=translate('"
			+ FrameworkConstants.OBJECT_DYNAMICVALUE_PLACEHOLDER
			+ "','ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')]";
	By byTab = By.cssSelector("form nav[role='tablist']>a");
	String DO_LI_Tag_ListItem_CQDialog = "//ul[contains(@class,'is-visible')]/li[translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')=translate('<<<<>>>>','ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')]";
	By CQDialogScrollableContentDiv = By.cssSelector(
			"coral-dialog.cq-dialog-floating[style*='display: block;'] div.cq-dialog-content,coral-dialog.coral-Dialog--fullscreen[style*='display: block;']");
	By selectableItemsForPathBrowserField = By
			.cssSelector("ul.is-below.is-visible>li[class*='SelectList-item--option']");
	String DO_CSS_QuickTip = "*[aria-label*='" + FrameworkConstants.OBJECT_DYNAMICVALUE_PLACEHOLDER + "']";
	By BTN_Expand_CQDialog = By.cssSelector("*[title='Toggle Fullscreen']>coral-icon");
	By btnConfigure = By.cssSelector("button[title='Configure']");
	By DIV_CQDialog_Loader = By.xpath("//div[contains(@class,'coral-Wait coral-Wait--center coral-Wait')]");
	By CQDialog = By.cssSelector("div[class*='cq-dialog']");
	By BTN_Done = By.cssSelector("button[title='Done']");
	By BTN_Save = By.xpath("//button[@form='cq-sites-properties-form']/coral-button-label");

	// #EditableToolBar
	By DIV_EditableToolBar = By.cssSelector("#EditableToolbar");
	By BTN_Configure_EditableToolBar = By.cssSelector("#EditableToolbar>button[data-action='CONFIGURE' i]");
	By BTN_Delete_EditableToolBar = By.cssSelector("#EditableToolbar>button[data-action='DELETE' i]");
	By BTN_Copy_EditableToolBar = By.cssSelector("#EditableToolbar>button[data-action='COPY' i]");
	By BTN_Move_EditableToolBar = By.cssSelector("#EditableToolbar>button[data-action='MOVE' i]");
	By BTN_Group_EditableToolBar = By.cssSelector("#EditableToolbar>button[data-action='GROUP' i]");
	By BTN_CancelInheritance_EditableToolBar = By
			.cssSelector("#EditableToolbar>button[data-action='MSM_CANCEL_INHERITANCE' i]");
	By BTN_MSM_RollOut_EditableToolBAr = By.cssSelector("#EditableToolbar>button[data-action='MSM_ROLLOUT' i]");

	UserActions actions;
	World world;

	public StepsCQDialogOperations(UserActions actions, World runner) {
		this.actions = actions;
		this.world = runner;
	}

	@Given("^I open CQDialog for AEM component configuration$")
	public void openCQDialogForComponent() throws Throwable {
		actions.Click.click("AEMComponentSearchListingV2");
		actions.Click.click(btnConfigure);
	}

	@Given("^I open CQDialog for \"([^\"]*)\"$")
	public void openCQDialogForComponent(String componentPlaceholder) throws Throwable {
		actions.Wait.waitForPageReady();
		actions.Click.click(componentPlaceholder);
		actions.VerifyElementVisible.verifyElementVisible(btnConfigure);
		actions.Click.click(btnConfigure);
		actions.Wait.waitForObjectToPerish(DIV_CQDialog_Loader);
		actions.VerifyElementVisible.verifyElementVisible(CQDialog);
		actions.Click.click(BTN_Expand_CQDialog);
	}
	
	public void openCQDialogForComponentUsingBy(By byComponentPlaceholder) throws Throwable {
		actions.Wait.waitForPageReady();
		actions.Click.click(byComponentPlaceholder);
		actions.VerifyElementVisible.verifyElementVisible(btnConfigure);
		actions.Click.click(btnConfigure);
		actions.Wait.waitForObjectToPerish(DIV_CQDialog_Loader);
		actions.VerifyElementVisible.verifyElementVisible(CQDialog);
		actions.Click.click(BTN_Expand_CQDialog);
	}

	@Given("^I open CQDialog for already configured component \"([^\"]*)\"$")
	public void openCQDialogForUpdatingComponent(String componentPlaceholder) throws Throwable {
		actions.Wait.waitForPageReady();
		WebElement component = world.getWebElementFactory().getElement(componentPlaceholder, false);
		Actions action = new Actions(world.getWebDriver());
		int x = component.getSize().getWidth() - (component.getSize().getWidth() * 90) / 100;
		int y = component.getSize().getHeight() - (component.getSize().getHeight() * 95) / 100;
		action.moveToElement(component, x, y).pause(Duration.ofSeconds(3)).click().build().perform();
		((JavascriptExecutor) world.getWebDriver()).executeScript("arguments[0].scrollIntoView();",
				world.getWebElementFactory().getElement(DIV_EditableToolBar, false));
		actions.VerifyElementVisible.verifyElementVisible(btnConfigure);
		actions.Click.click(btnConfigure);
		actions.Wait.waitForObjectToPerish(DIV_CQDialog_Loader);
		actions.VerifyElementVisible.verifyElementVisible(CQDialog);
		actions.Click.click(BTN_Expand_CQDialog);
	}

	public void scrollToTopOfCQDialogContent() {
		WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		WebElement divContentCQDialog = wait
				.until(ExpectedConditions.presenceOfElementLocated(CQDialogScrollableContentDiv));
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", divContentCQDialog, 0);
	}

	public void scrollToBottomOfCQDialogContent() {
		WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		WebElement divContentCQDialog = wait
				.until(ExpectedConditions.presenceOfElementLocated(CQDialogScrollableContentDiv));
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		Integer scrollHeight = (Integer) jsDriver.executeScript("arguments[0].scrollHeight;", divContentCQDialog);
		jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", divContentCQDialog, scrollHeight);
	}

	@Given("^I close CQDialog$")
	public void saveAndClose() throws Throwable {
		actions.Click.click(BTN_Done);
		actions.Wait.waitForPageReady();
	}

	public void saveAndCloseProperties() throws Throwable {
		actions.Click.click(BTN_Save);
		actions.Wait.waitForPageReady();
	}

	public void applyTagsToTextBox(By byObjectName, String strTagsList) throws Throwable {
		WebElement TagsEditBox = world.getWebElementFactory().getElement(byObjectName, true);
		applyTagsToTextBox(TagsEditBox, strTagsList);
	}

	public void applyTagsToTextBox(String strObjectName, String strTagsList) throws Throwable {
		WebElement TagsEditBox = world.getWebElementFactory().getElement(strObjectName, true);
		applyTagsToTextBox(TagsEditBox, strTagsList);
	}

	public void applyTagsToTextBox(WebElement TagsWebElementbject, String strTagsList) throws Throwable {
		if (null == strTagsList || strTagsList == "" || null == TagsWebElementbject)
			return;
		System.out.println(strTagsList);
		String strTagFromDynamicList = null;
		String[] arrTags = strTagsList.split(";");
		for (String tag : arrTags) {
			String tagStringToBeEntered = null;
			if (tag.contains("/")) {
				String[] tagParts = tag.split("/");
				int lengthTagParts = tagParts.length;
				tagStringToBeEntered = tagParts[lengthTagParts - 1];
			} else {
				tagStringToBeEntered = tag;
			}
			WebElement tagListItem = null;
			int tagApplicationCounter = 1;
			while(tagApplicationCounter <= 3) {
				TagsWebElementbject.clear();
				actions.Enter.enter(tagStringToBeEntered.trim(), TagsWebElementbject);
				strTagFromDynamicList = DO_LI_Tag_ListItem_CQDialog
						.replace(FrameworkConstants.OBJECT_DYNAMICVALUE_PLACEHOLDER, tag);
				WebDriverWait wd = new WebDriverWait(world.getWebDriver(), FrameworkConstants.SMALL_WAIT);
				try {
					tagListItem = wd.until(ExpectedConditions.elementToBeClickable(By.xpath(strTagFromDynamicList)));
				} catch (Exception e) {
					e.printStackTrace();
					tagListItem = null;
				}				
				if (tagListItem != null) {
					actions.Click.click(tagListItem);
					break;
				}				
				tagApplicationCounter++;
			}		
			
			if (tagListItem == null) {
				Assert.fail(tag + " tag is not available in the list");;
			}
			Thread.sleep(2000);
			strTagFromDynamicList = null;
		}
	}

	public void clickBelowPathBrowserOptionIfPresent() {
		WebElement selectableListItem = world.getWebElementFactory().getElement(selectableItemsForPathBrowserField, false);
		if (selectableListItem != null)
			selectableListItem.click();
	}

	public void verifyTabByText(String tabName) {
		scrollToTopOfCQDialogContent();
		String tab = TabText.replace(FrameworkConstants.OBJECT_DYNAMICVALUE_PLACEHOLDER, tabName);
		By tabBy = By.xpath(tab);
		WebElement tabElement = world.getWebElementFactory().getElement(tabBy, true);
		if (!(tabElement != null && tabElement.isDisplayed())) {
			Assert.fail(tabName + " tab is not present.");
		}
	}

	public void selectTabByText(String tabName) {
		scrollToTopOfCQDialogContent();
		String tab = TabText.replace(FrameworkConstants.OBJECT_DYNAMICVALUE_PLACEHOLDER, tabName);
		By tabBy = By.xpath(tab);
		WebElement tabElement = world.getWebElementFactory().getElement(tabBy, true);
		if (!(tabElement != null && tabElement.isDisplayed())) {
			Assert.fail(tabName + " tab is not present.");
		}
		tabElement.click();
	}

	/*
	 * public void verifyTabCount(int tabCount) { }
	 */
	public void verifyQuickTipExists(String quickTipText) {
		String quickTip = DO_CSS_QuickTip.replace(FrameworkConstants.OBJECT_DYNAMICVALUE_PLACEHOLDER, quickTipText);
		By byQuickTip = By.cssSelector(quickTip);
		WebElement objQuicktip = world.getWebElementFactory().getElement(byQuickTip, true);
		if (!(objQuicktip != null && objQuicktip.isDisplayed())) {
			Assert.fail(quickTipText + " quick tip is not present.");
		}
	}
	
	public void enterPath(String sData, String orObjectName) throws Throwable {
		actions.Enter.enter(sData, orObjectName);
		actions.Wait.wait("3");
		actions.Click.click(orObjectName);		
	}
	
	public void selectViewType(String strViewType) throws IOException {
		selectTabByText("View Type");
		actions.SelectText.select(strViewType, world.getWebElementFactory().getElement(By.name("./viewType"),false));
	}
	
}
