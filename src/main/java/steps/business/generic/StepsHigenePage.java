package steps.business.generic;


import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.And;
import steps.generic.keywords.World;
import unilever.pageobjects.platform.publish.common.PublishedPageCommon;

public class StepsHigenePage {

	UserActions actions;
	World capabilities;
	PublishedPageCommon publishedPage;

	public StepsHigenePage(World capabilities, UserActions actions) {
		this.actions = actions;
		this.capabilities = capabilities;
		this.publishedPage = new PublishedPageCommon(capabilities.getWebDriver(),capabilities.getBrandName());
	}

	@And("^(?:I|i)t should take me to the error message page$")
	public void verifyHigenePage() {
		this.actions.Wait.waitForPageReady();
		Assert.assertTrue(this.capabilities.getWebDriver().getTitle().contains("404"), "this is not the higene page " + this.publishedPage.driver.getCurrentUrl());
	}
	
	@And("^(?:T|t)he error message page should have text \"([^\"]*)\"$")
	public void verifyHigenePageMessage(String errorText) {
		this.actions.Wait.waitForPageReady();
		String actualErrorText = this.publishedPage.getErrorObject().getText();
		Assert.assertTrue(actualErrorText.equals(errorText), "the higene page error message mismatch. Actual - " + actualErrorText + " and Expected - "  + errorText);
	}
	
	@And("^(?:i|I) click on redirect home page button$")
	public void clickredirectbutton() throws Throwable {
		WebElement redirectbutton = this.publishedPage.getreturnHomepageButton();
		Assert.assertNotNull(redirectbutton,
				"redirect home page button is not appearing for url " + this.capabilities.getWebDriver().getCurrentUrl());
		this.capabilities.reportStepInfo("redirect home page button is present on page");
		this.capabilities.getScenarioContext().addScenarioData("redirectbuttonlinkhref", redirectbutton.getAttribute("href"));
		this.actions.Wait.wait("4");
		redirectbutton.click();
	}
}
