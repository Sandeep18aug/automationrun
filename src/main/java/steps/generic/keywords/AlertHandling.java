package steps.generic.keywords;

import org.openqa.selenium.Alert;
import org.testng.*;

import cucumber.api.java.en.And;

public class AlertHandling {
	World DriverHandle;

	public AlertHandling(World runner) {
		this.DriverHandle = runner;
	}

	@And("^I accept the alert$")
	public void acceptAlert() {
		try {
			DriverHandle.getWebDriver().switchTo().alert().accept();
		} catch (Exception e) {
			Assert.fail("Exception occurred in accepting alert");
		}
	}
	
	@And("^I dismiss the alert$")
	public void dismissAlert() {
		try {
			DriverHandle.getWebDriver().switchTo().alert().dismiss();
		} catch (Exception e) {
			Assert.fail("Exception occurred in dismissing alert");
		}
	}
	
	@And("^I expect alert text is equal to \"([^\"]*)\"$")
	public void verifyAlertTextEqualTo(String expectedText) {
		try {
			String actualText = DriverHandle.getWebDriver().switchTo().alert().getText();
			Assert.assertTrue(actualText.equals(expectedText), "Text mismatch - Actual text is - " + actualText + " and expected text is - " + expectedText);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	@And("^I expect alert text contains \"([^\"]*)\"$")
	public void verifyAlertTextContains(String expectedText) {
		try {
			String actualText = DriverHandle.getWebDriver().switchTo().alert().getText();
			Assert.assertTrue(actualText.contains(expectedText), "Text mismatch - Actual text is - " + actualText + " and expected text is - " + expectedText);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	@And("^I expect alert is not present$")
	public void verifyAlertNotPresent(String expectedText) {
		try {
			DriverHandle.getWebDriver().switchTo().alert();
			Assert.fail("Alert is present");
		} catch (Exception e) {}
	}
	
	@And("^I input text \"([^\"]*)\" in the alert textbox and accept the alert$")
	public void enterTextInAlert(String text) {
		try {
			 Alert alert  = DriverHandle.getWebDriver().switchTo().alert();
			 alert.sendKeys(text);
			 alert.accept();			
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
