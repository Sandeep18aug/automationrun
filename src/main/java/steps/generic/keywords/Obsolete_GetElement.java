package steps.generic.keywords;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import framework.shared.FrameworkConstants;

public class Obsolete_GetElement {
	By byProperty;
	Properties objectRepository;
	private World DriverHandle;
	private WebElement element;
	int iActualXCoordinate, iActualYCoordinate, iActualHeight, iActualWidth, iRelativeXCoordinate, iRelativeYCoordinate,
			iRelativeHeight, iRelativeWidth;
	
	public Obsolete_GetElement(World runner) {
		this.DriverHandle = runner;
	}
	
	public void loadObjectRepository(String sOR) {
		try {
			objectRepository = new Properties();
			File ORfile = new File(FrameworkConstants.OBJECT_REPOSITORY_FOLDER + sOR + ".properties");
			FileInputStream fs = new FileInputStream(ORfile);
			objectRepository.load(fs);
		} catch (Exception e) {
			System.out.println("Error while loading Object Repository");
		}
	}

	public By getLocator(String sOR, String sObject) {
		try {
			System.out.println("Get Locator OR name :: " + sOR);
			System.out.println("Get Locator Object name :: " + sObject);
			loadObjectRepository(sOR);
			String sProperty = objectRepository.getProperty(sObject);
			String sLocator = sProperty.replaceAll("\\=.*", "");
			String sLocator_Property = sProperty.substring(sLocator.length() + 1);
			switch (sLocator) {
			case "id":
				byProperty = By.id(sLocator_Property);
				break;
			case "css":
				byProperty = By.cssSelector(sLocator_Property);
				break;
			case "xpath":
				byProperty = By.xpath(sLocator_Property);
				break;
			case "linkText":
				byProperty = By.linkText(sLocator_Property);
				break;
			case "name":
				byProperty = By.name(sLocator_Property);
				break;
			case "partialLinkText":
				byProperty = By.partialLinkText(sLocator_Property);
				break;
			case "tagName":
				byProperty = By.tagName(sLocator_Property);
				break;
			}
			// element = DriverHandle.getBrowser().findElement(byProperty);
		} catch (Exception e) {
			System.out.println("Error while getting object locator :: " + sObject);
			Assert.fail("Error while getting object locator :: " + sObject);
		}
		return byProperty;
	}
	public By getDynamicLocator(String sOR, String sObject, String toBeReplaced, String replacedBy) {
		try {
			System.out.println("Get Locator OR name :: " + sOR);
			System.out.println("Get Locator Object name :: " + sObject);
			loadObjectRepository(sOR);
			String sProperty = objectRepository.getProperty(sObject);
			String sLocator = sProperty.replaceAll("\\=.*", "");
			String sLocator_Property = sProperty.substring(sLocator.length() + 1);
			sLocator_Property = sLocator_Property.replace(toBeReplaced, replacedBy);
			switch (sLocator) {
			case "id":
				byProperty = By.id(sLocator_Property);
				break;
			case "css":
				byProperty = By.cssSelector(sLocator_Property);
				break;
			case "xpath":
				byProperty = By.xpath(sLocator_Property);
				break;
			case "linkText":
				byProperty = By.linkText(sLocator_Property);
				break;
			case "name":
				byProperty = By.name(sLocator_Property);
				break;
			case "partialLinkText":
				byProperty = By.partialLinkText(sLocator_Property);
				break;
			case "tagName":
				byProperty = By.tagName(sLocator_Property);
				break;
			}
			// element = DriverHandle.getBrowser().findElement(byProperty);
		} catch (Exception e) {
			System.out.println("Error while getting object locator :: " + sObject);
			Assert.fail("Error while getting object locator :: " + sObject);
		}
		return byProperty;
	}
	
	public By getDynamicLocator(String sObject, String toBeReplaced, String replacedBy) {
		try {
			String sOR = DriverHandle.getOrName();
			System.out.println("Get Locator OR name :: " + sOR);
			System.out.println("Get Locator Object name :: " + sObject);
			loadObjectRepository(sOR);
			String sProperty = objectRepository.getProperty(sObject);
			String sLocator = sProperty.replaceAll("\\=.*", "");
			String sLocator_Property = sProperty.substring(sLocator.length() + 1);
			sLocator_Property = sLocator_Property.replace(toBeReplaced, replacedBy);
			switch (sLocator) {
			case "id":
				byProperty = By.id(sLocator_Property);
				break;
			case "css":
				byProperty = By.cssSelector(sLocator_Property);
				break;
			case "xpath":
				byProperty = By.xpath(sLocator_Property);
				break;
			case "linkText":
				byProperty = By.linkText(sLocator_Property);
				break;
			case "name":
				byProperty = By.name(sLocator_Property);
				break;
			case "partialLinkText":
				byProperty = By.partialLinkText(sLocator_Property);
				break;
			case "tagName":
				byProperty = By.tagName(sLocator_Property);
				break;
			}
			// element = DriverHandle.getBrowser().findElement(byProperty);
		} catch (Exception e) {
			System.out.println("Error while getting object locator :: " + sObject);
			Assert.fail("Error while getting object locator :: " + sObject);
		}
		return byProperty;
	}
	
	public void focusElement(WebElement element) {
		try {
			Point elementPoint = element.getLocation();
			iActualXCoordinate = elementPoint.x;
			iActualYCoordinate = elementPoint.y;
			iRelativeXCoordinate = iActualXCoordinate - 5;
			iRelativeYCoordinate = iActualYCoordinate - 5;
			// Getting Dimensions
			Dimension elementDimensions = element.getSize();
			iActualHeight = elementDimensions.getHeight();
			iActualWidth = elementDimensions.getWidth();
			iRelativeHeight = iActualHeight + 10;
			iRelativeWidth = iActualWidth + 10;
			// this is for Highlight div creation

			((JavascriptExecutor) DriverHandle.getWebDriver()).executeScript("arguments[0].focus()", element);
			Thread.sleep(50);
			((JavascriptExecutor) DriverHandle.getWebDriver()).executeScript("var div = document.createElement('div');"
					+ "div.style.width = '" + iRelativeWidth + "px';" + "div.style.height = '" + iRelativeHeight
					+ "px';" + "div.style.background = 'transparent';" + "div.style.border = '3px solid green';"
					+ "div.style.position = 'absolute';" + "div.style.left = '" + iRelativeXCoordinate + "px';"
					+ "div.style.top = '" + iRelativeYCoordinate + "px';" + "div.style.zIndex = '99999';"
					+ "div.id = 'highlightBorder';" + "document.body.appendChild(div);");
			Thread.sleep(50);
			((JavascriptExecutor) DriverHandle.getWebDriver()).executeScript(
					"var el = document.getElementById('highlightBorder');" + "el.scrollIntoView(false);");
			Thread.sleep(50);
			((JavascriptExecutor) DriverHandle.getWebDriver()).executeScript(
					"var child=document.getElementById('highlightBorder');" + "child.parentNode.removeChild(child);");

		} catch (Exception e) {
			System.out.println("Error while seting focus to element");
			System.out.println(e);
		}
	}

	public WebElement getElement(WebDriver driver, String sOR, String sObject) {
		WebElement element = null;
		try {
			String sPageInteractiveStatus = String
					.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"));
			for (int iPageStatusLoop = 0; iPageStatusLoop <= 100; iPageStatusLoop++) {
				if (sPageInteractiveStatus.equalsIgnoreCase("complete")
						|| sPageInteractiveStatus.equalsIgnoreCase("interactive")) {
					Thread.sleep(500);
					System.out.println("Page Status :: " + sPageInteractiveStatus);
					for (int iElementLoop = 0; iElementLoop <= 5; iElementLoop++) {
						if (driver.findElement(getLocator(sOR, sObject)).isEnabled()) {
							System.out.println("Element found and enabled :: " + sObject);
							element = driver.findElement(getLocator(sOR, sObject));
							Thread.sleep(100);
							focusElement(element);
							break;
						} else {
							Thread.sleep(500);
						}
					}
					break;
				} else {
					Thread.sleep(1000);
				}
			}
		} catch (Exception e) {
			System.out.println("Error while getting element :: " + sObject);
		}
		return element;
	}
	
	public WebElement getElement(String sObject, boolean highlight) {
		WebElement element = null;
		WebDriverWait webDriverWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.SMALL_WAIT);
		try { 
			element = webDriverWait.until(
					ExpectedConditions.presenceOfElementLocated(getLocator(DriverHandle.getOrName(), sObject)));
			if (highlight) {
				Thread.sleep(100);
				focusElement(element);
			}
		} catch (Exception e) {
			System.out.println("Element is not present :: " + sObject);
		}
		return element;
	}
	
	public WebElement getDynamicElement(String sObject, String toBeReplaced, String replacedBy, boolean highlight) {
		WebElement element = null;
		WebDriverWait webDriverWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.SMALL_WAIT);
		try {
			element = webDriverWait.until(
					ExpectedConditions.presenceOfElementLocated(getDynamicLocator(DriverHandle.getOrName(), sObject, toBeReplaced, replacedBy)));
			if (highlight) {
				Thread.sleep(100);
				focusElement(element);
			}
		} catch (Exception e) {
			System.out.println("Element is not present :: " + sObject);
		}
		return element;
	}

	public WebElement getNestedElement(By parentProperty, By childProperty) {
		WebElement element = null;
		WebDriverWait webDriverWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.SMALL_WAIT);
		try {
			element = webDriverWait.until(ExpectedConditions.presenceOfNestedElementLocatedBy
					(parentProperty, childProperty));
		} catch (StaleElementReferenceException ex) {
			int tryCounter = 0;
			do {
				try {
					element = webDriverWait.until(ExpectedConditions.presenceOfNestedElementLocatedBy
							(parentProperty, childProperty));
				} catch (Exception e) {
					tryCounter ++;
				}
			} while(tryCounter < 3 && element == null);
		} catch (TimeoutException e) {
			element = null;
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		return element;
	}
	
	public WebElement getNestedElement(WebElement parentElement, By childProperty) {
		WebElement element = null;
		WebDriverWait webDriverWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.SMALL_WAIT);
		try {
			element = webDriverWait.until(ExpectedConditions.presenceOfNestedElementLocatedBy
					(parentElement, childProperty));
		} catch (StaleElementReferenceException ex) {
			int tryCounter = 0;
			do {
				try {
					element = webDriverWait.until(ExpectedConditions.presenceOfNestedElementLocatedBy
							(parentElement, childProperty));
				} catch (Exception e) {
					tryCounter ++;
				}
			} while(tryCounter < 3 && element == null);
		} catch (TimeoutException e) {
			element = null;
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		return element;
	}
	
	public List<WebElement> getNestedElements(By parentProperty, By childProperty) {
		List<WebElement> nestedElements = null;
		WebDriverWait webDriverWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.SMALL_WAIT);
		try {
			nestedElements = webDriverWait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy
					(parentProperty, childProperty));	
		} catch (StaleElementReferenceException ex) {
			int tryCounter = 0;
			do {
				try {
					nestedElements = webDriverWait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy
							(parentProperty, childProperty));
				} catch (Exception e) {
					tryCounter ++;
				}
			} while(tryCounter < 3 && nestedElements == null);
		} catch (TimeoutException e) {
			nestedElements = new ArrayList<WebElement>();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		return nestedElements;
	}

	public List<WebElement> getNestedElements(WebElement parentElement, By childProperty) {
		List<WebElement> nestedElements = null;
		try {
			nestedElements = parentElement.findElements(childProperty);	
		} catch (StaleElementReferenceException ex) {
			int tryCounter = 0;
			do {
				try {
					nestedElements = parentElement.findElements(childProperty);
				} catch (Exception e) {
					tryCounter ++;
				}
			} while(tryCounter < 3 && nestedElements == null);
		} catch (TimeoutException e) {
			nestedElements = new ArrayList<WebElement>();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		return nestedElements;
	}
	
	public List<WebElement> getElements(By byProperties) {
		List<WebElement> elements = null;
		WebDriverWait webDriverWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.SMALL_WAIT);
		try {
			elements = webDriverWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(byProperties));			
		} catch (StaleElementReferenceException ex) {
			int tryCounter = 0;
			do {
				try {
					elements = webDriverWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(byProperties));
				} catch (Exception e) {
					tryCounter ++;
				}
			} while(tryCounter < 3 && elements == null);
		} catch (TimeoutException e) {
			elements = new ArrayList<WebElement>();
		} catch (Exception e) {
			System.out.println("Elements are not present :: " + byProperties);
		}
		return elements;
	}
	
	public WebElement getElement(String sObject) {
		return getElement(sObject, true);
	}

	public WebElement getElement(By byProperties, boolean highlight) {
		WebElement element = null;
		WebDriverWait webDriverWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.SMALL_WAIT);
		try {
			element = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(byProperties));
			if (highlight) {
				Thread.sleep(100);
				focusElement(element);
			}
		} catch (StaleElementReferenceException ex) {
			int tryCounter = 0;
			do {
				try {
					element = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(byProperties));
					if (highlight) {
						Thread.sleep(100);
						focusElement(element);
					}
				} catch (Exception e) {
					tryCounter ++;
				}
			} while(tryCounter < 3 && element == null);
		}  catch (Exception e) {
			System.out.println("Element is not present :: " + byProperties);
		}
		return element;
	}

	public int getYOffsetOfPage() {
		JavascriptExecutor js = (JavascriptExecutor)DriverHandle.getWebDriver();
		return ((Long)js.executeScript("return window.pageYOffset;")).intValue();
	}
	
	public WebElement getElement(By byProperties) {
		return getElement(byProperties, true);
	}

	public boolean isElementPresentOnPage(By name) {
		WebElement element = getElement(name);
		return element != null ? true : false;
	}

	public boolean isElementVisibleOnPage(String name) {
		WebElement element = null;
		try {
			WebDriverWait wdWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
			element = wdWait.until(ExpectedConditions.visibilityOf(getElement(name)));
		} catch (Exception ex) {
			element = null;
		}
		return element != null ? true : false;
	}
	

	public boolean isElementVisibleOnPage(By name) {
		WebElement element = null;
		try {
			WebDriverWait wdWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
			element = wdWait.until(ExpectedConditions.visibilityOf(getElement(name)));
		} catch (Exception ex) {
			element = null;
		}
		return element != null ? true : false;
	}

	
	public boolean isElementPresentOnPage(String name, boolean highlight) {
		WebElement element = getElement(name,highlight);
		return element != null ? true : false;
	}

	public boolean isElementPresentOnPage(By name, boolean highlight) {
		WebElement element = getElement(name,highlight);
		return element != null ? true : false;
	}

	public boolean isElementVisibleOnPage(String name, boolean highlight) {
		WebElement element = null;
		try {
			WebDriverWait wdWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
			element = wdWait.until(ExpectedConditions.visibilityOf(getElement(name,highlight)));
		} catch (Exception ex) {
			element = null;
		}
		return element != null ? true : false;
	}
	

	public boolean isElementVisibleOnPage(By name, boolean highlight) {
		WebElement element = null;
		try {
			WebDriverWait wdWait = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
			element = wdWait.until(ExpectedConditions.visibilityOf(getElement(name,highlight)));
		} catch (Exception ex) {
			element = null;
		}
		return element != null ? true : false;
	}

	public boolean isElementVisibleOnPage(By name, int maxTime) {
		WebElement element = null;
		try {
			WebDriverWait wdWait = new WebDriverWait(DriverHandle.getWebDriver(), maxTime);
			element = wdWait.until(ExpectedConditions.visibilityOf(getElement(name,true)));
		} catch (Exception ex) {
			element = null;
		}
		return element != null ? true : false;
	}
	
	public int getElementCount(String sObject) {
		By byProperty = getLocator(DriverHandle.getOrName(), sObject);		
		return getElementCount(byProperty);

	}
	public int getElementCount(By byProperty) {
		List<WebElement> elements = new ArrayList<WebElement>();
		try {
			elements = new WebDriverWait(DriverHandle.getWebDriver(), FrameworkConstants.MEDIUM_WAIT)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(byProperty));
			
		} catch (TimeoutException e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		return elements.size();
	}
}
