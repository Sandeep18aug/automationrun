package steps.generic.keywords;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import cucumber.api.java.en.And;
import framework.shared.FrameworkConstants;

public class Wait {

	public World world;

	public Wait(World world) {
		this.world = world;
	}

	@And("^I wait for (.*) seconds$")
	public void wait(String sData) throws NumberFormatException, InterruptedException {
		sData = sData + "000";
		Thread.sleep(Integer.parseInt(sData));
	}

	@And("^I wait for \"([^\"]*)\" object to disappear$")
	public void waitForObjectToPerish(String sObject) throws Throwable {
		WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.LARGE_WAIT);
		By byProperty = this.world.getWebElementFactory().getByLocator(sObject);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(byProperty));
	}

	public void waitForObjectToPerish(By byProperty) throws Throwable {
		WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.LARGE_WAIT);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(byProperty));
	}

	public void waitForObjectToVisible(By byProperty) throws Throwable {
		WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.LARGE_WAIT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(byProperty));
	}

	public void waitForObjectToVisible(WebElement element) throws Throwable {
		try {
			for (int i = 0; i < 60; i++) {
				if (element.isDisplayed()) {
					break;
				}
				Thread.sleep(1000);
			}
		} catch (Exception ex) {}
	}

	public void waitForPageReady(int seconds) {
		try {
			String sPageInteractiveStatus = "";
			for (int iPageStatusLoop = 0; iPageStatusLoop < seconds; iPageStatusLoop++) {
				if (sPageInteractiveStatus.equalsIgnoreCase("complete")
						|| sPageInteractiveStatus.equalsIgnoreCase("interactive")) {
					Thread.sleep(500);
					break;
				} else {
					Thread.sleep(1000);
					sPageInteractiveStatus = String.valueOf(((JavascriptExecutor) world.getWebDriver())
							.executeScript("return document.readyState"));
				}
			}
		} catch (Exception e) {
			System.out.println("error while getting page interactive status; " + e);
		}
	}

	@And("^I wait for page to load$")
	public void waitForPageReady() {
		waitForPageReady(FrameworkConstants.LARGE_WAIT);
	}
}
