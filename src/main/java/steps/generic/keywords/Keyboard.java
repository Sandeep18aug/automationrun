package steps.generic.keywords;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import cucumber.api.java.en.And;
import framework.exceptions.ObjectNotFoundInORException;
import framework.selenium.support.WebElementFactory;

public class Keyboard{
	World world;

	public Keyboard(World world) {
		this.world = world;
	}
	
	@And("^I press Enter key from keyboard on \"([^\"]*)\"$")
	public void pressEnter(String sObject) throws IOException, ObjectNotFoundInORException {
		WebElement element = this.world.getWebElementFactory().getElement(sObject);
		element.sendKeys(Keys.ENTER);
	}
}
