package steps.generic.keywords;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import cucumber.api.java.en.And;
import framework.selenium.support.WebElementFactory;

import org.testng.Assert;

public class Check {
	World world;

	public Check(World world) {
		this.world = world;
	}

	@And("^I check the \"([^\"]*)\" (?:checkbox|radiobutton)$")
	public void check(String sObject) throws Throwable {
		WebElement element;
		try {
			element = this.world.getWebElementFactory().getElement(sObject);
			if (!element.isSelected())
				element.click();
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}		
	}

	public void check(By byProperty) throws Throwable {
		WebElement element;
		try {
			element = this.world.getWebElementFactory().getElement(byProperty);
			if (!element.isSelected())
				element.click();
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());

		}	
	}
	
	public void check(WebElement element) throws Throwable {		
		try {
			if (!element.isSelected())
				element.click();
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}	
	}
	
	@And("^I uncheck the \"([^\"]*)\" (?:checkbox|radiobutton)$")
	public void unCheck(String sObject) throws Throwable {

		WebElement element;
		try {
			element = this.world.getWebElementFactory().getElement(sObject);
			if (element.isSelected())
				element.click();
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	public void unCheck(By byProperty) throws Throwable {
		WebElement element;
		try {
			element = this.world.getWebElementFactory().getElement(byProperty);
			if (element.isSelected())
				element.click();
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}	
	}
	
	public void unCheck(WebElement element) throws Throwable {
		try {
			if (element.isSelected())
				element.click();
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}	
	}
	
	@And("^I expect that (?:checkbox|radiobutton) \"([^\"]*)\" is checked$")
	public void verifyChecked(String sObject) throws Throwable {
		WebElement element;
		try {
			element = this.world.getWebElementFactory().getElement(sObject);
			Assert.assertTrue(element.isSelected(), sObject + " is not checked");
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@And("^I expect that (?:checkbox|radiobutton) \"([^\"]*)\" is not checked$")
	public void verifyNotChecked(String sObject) throws Throwable {
		WebElement element;
		try {
			element = this.world.getWebElementFactory().getElement(sObject);
			Assert.assertFalse(element.isSelected(),sObject + " is checked");
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
