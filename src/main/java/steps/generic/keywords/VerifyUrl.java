package steps.generic.keywords;

import org.testng.Assert;

import cucumber.api.java.en.And;


public class VerifyUrl{
	public World world;
	public Wait wait;

    public VerifyUrl(World world, Wait wait){
        this.world = world;
        this.wait = wait;
    }
	
	@And("^(?:I|i) expect page url as \"([^\"]*)\"$")
	public void verifyTitle(String sData) {
		try {
			wait.waitForPageReady();
			Assert.assertTrue(world.getWebDriver().getCurrentUrl().trim().contentEquals(sData.trim()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@And("^(?:I|i) expect page url contains \"([^\"]*)\"$")
	public void verifyUrlContains(String sData) {
			try {
				wait.waitForPageReady();				
				String currentUrl = world.getWebDriver().getCurrentUrl();
				Assert.assertTrue(currentUrl.toLowerCase().contains(sData.toLowerCase().toString()), 
						"Actual Url = " + currentUrl + " and Expected is = " + sData);
				System.out.println("current url " +currentUrl+" contains expected text " + sData);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}
	}
	
	@And("^(?:I|i) expect page url does not contains \"([^\"]*)\"$")
	public void verifyURLDoesNotContains(String sData) {
			try {
				wait.waitForPageReady();
				String currentUrl = world.getWebDriver().getCurrentUrl();
				Assert.assertFalse(currentUrl.toLowerCase().contains(sData.toLowerCase().toString()), 
						"Actual Url = " + currentUrl + " and Expected is = " + sData);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}
	}
}
