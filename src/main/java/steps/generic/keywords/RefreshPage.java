package steps.generic.keywords;

import cucumber.api.java.en.And;

public class RefreshPage {
	World world;
	
	public RefreshPage(World world) {
		this.world = world;
	}
	
	@And("^I refresh the (?:opened webpage|page)$")
	public void refreshPage() throws InterruptedException {
		world.getWebDriver().navigate().refresh();
	}
}
