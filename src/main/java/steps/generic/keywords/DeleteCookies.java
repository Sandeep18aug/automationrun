package steps.generic.keywords;

import org.testng.Assert;

import cucumber.api.java.en.And;

public class DeleteCookies{
	World world;

    public DeleteCookies(World world){
        this.world = world;
    }
	
	@And("^I delete all the existing cookies$")
	public void deleteCookies() {
			try {
				world.getWebDriver().manage().deleteAllCookies();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Assert.fail(e.getMessage());

			}
	}
}
