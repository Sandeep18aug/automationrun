
package steps.generic.keywords;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import cucumber.api.java.en.And;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.FrameworkConstants;

public class Enter {
	World world;

	public Enter(World runner) {
		this.world = runner;
	}

	@And("^I enter \"([^\"]*)\" in \"([^\"]*)\"$")
	public void enter(String sData, String sObject) throws ObjectNotFoundInORException {
		WebElement element = this.world.getWebElementFactory().getElement(sObject);
		if(sData.contains(FrameworkConstants.OBJECT_DYNAMICVALUE_PLACEHOLDER)) {
			String randomText = RandomStringUtils.random(10,true,true);
			sData = sData.replace(FrameworkConstants.OBJECT_DYNAMICVALUE_PLACEHOLDER, randomText);
		}
		enter(sData, element);
	}

	@And("^I enter \"([^\"]*)\" in \"([^\"]*)\" without highlighting$")
	public void enterWithoutHighlighting(String sData, String sObject) throws ObjectNotFoundInORException {
		WebElement element = this.world.getWebElementFactory().getElement(sObject, false);
		enter(sData, element);
	}

	@And("^I enter \"([^\"]*)\" in \"([^\"]*)\" without clearing$")
	public void enterWithOutClear(String sData, String sObject) {
		try {
			WebElement element = this.world.getWebElementFactory().getElement(sObject);
			element.sendKeys(sData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception occurred in enter value");
		}
	}

	public void enter(String sData, WebElement element) {
		try {
			element.clear();
			element.sendKeys(sData);
			Thread.sleep(100);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception occurred in enter value");
		}
	}

	public void enter(String sData, By byElement) throws Throwable {
		try {
			WebElement element = this.world.getWebElementFactory().getElement(byElement, true);
			enter(sData, element);
		} catch (Exception e) {
			Assert.fail("Exception occurred in enter value");
		}
	}

	@And("^I enter \"([^\"]*)\" in \"([^\"]*)\" rich text box$")
	public void enterRichText(String sData, String sObject) throws Throwable {
		try {
			WebElement Object = this.world.getWebElementFactory().getElement(sObject, true);
			Object.click();
			Thread.sleep(1000);
			Object.sendKeys(sData);
		} catch (Exception e) {
			Assert.fail("Exception occurred in enter value in richtext box : " + sObject);
		}
	}

	@And("^I enter \"([^\"]*)\" in \"([^\"]*)\" by clearing rich text box$")
	public void enterRichTextwithclearing(String sData, String sObject) throws Throwable {
		try {
			WebElement Object = this.world.getWebElementFactory().getElement(sObject, true);
			Object.click();
			Thread.sleep(1000);
			Object.clear();
			Thread.sleep(1000);
			Object.click();
			Object.sendKeys(sData);

		} catch (Exception e) {
			Assert.fail("Exception occurred in enter value in richtext box : " + sObject);
		}
	}

	@And("^I enter \"([^\"]*)\" in \"([^\"]*)\" from clipboard$")
	public void enterTextInObjectFromClipboard(String sData, String sObject) throws Throwable {
		try {
			By byproperties = this.world.getWebElementFactory().getByLocator(sObject);
			enterTextInObjectFromClipboard(sData, byproperties);
		} catch (Exception e) {
			Assert.fail("Exception occurred in enter value in richtext box : " + sObject);
		}
	}

	public void enterTextInObjectFromClipboard(String sData, By byProperties) throws Throwable {
		try {
			WebElement webElement = this.world.getWebElementFactory().getElement(byProperties, false);
			enterTextInObjectFromClipboard(sData, webElement);
		} catch (Exception e) {
			Assert.fail("Exception occurred in enter value in richtext box : " + byProperties);
		}
	}

	public void enterTextInObjectFromClipboard(String sData, WebElement webElement) throws Throwable {
		try {
			putDataIntoClipboard(sData);
			Thread.sleep(1000);
			Actions actions = new Actions(world.getWebDriver());
			world.getWebDriver().switchTo().activeElement();
			Thread.sleep(1000);
			actions.sendKeys(Keys.chord(Keys.CONTROL, "v")).perform();
			Thread.sleep(1000);
		} catch (Exception e) {
			Assert.fail("Exception occurred in enter value in richtext box : " + webElement);
		}
	}

	public void putDataIntoClipboard(String data) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Clipboard clipboard = toolkit.getSystemClipboard();
		StringSelection strSel = new StringSelection(data);
		clipboard.setContents(strSel, null);
	}
}
