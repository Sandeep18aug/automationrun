package steps.generic.keywords;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import cucumber.api.java.en.And;
import framework.selenium.support.WebElementFactory;
import framework.shared.FrameworkConstants;

public class Click {
	World world;

	public Click(World world) {
		this.world = world;
	}
	
    
@And("^(?:I|i) click on \"([^\"]*)\" if present$")
    public void clickIfPresent(String sObject) throws Throwable {
           System.out.println("clicking object  :: " + sObject);
           WebElement element = null;
           try {
                  //WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
                  element = this.world.getWebElementFactory().getClickableElement(sObject, false);
                  if(element != null) {
                        element.click();
                  }
           } catch (StaleElementReferenceException ex) {
                  try {
                        element = this.world.getWebElementFactory().getClickableElement(sObject, false);
                         if(element != null)
                               element.click();
                  } catch (Exception e) {
                        // TODO Auto-generated catch block
                        Assert.fail(e.getMessage());
                  }
           } catch (Exception e) {
                  e.printStackTrace();
                  Assert.fail(e.getMessage());
           }
    }


	@And("^I click (?:on a|on) \"([^\"]*)\"$")
	public void click(String sObject) throws Throwable {
		System.out.println("Driver in Click :: " + world.getWebDriver() + " :: " + sObject);
		WebElement element = null;
		try {
			WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
			element = wait.until(ExpectedConditions.elementToBeClickable(this.world.getWebElementFactory().getElement(sObject)));
			element.click();
		} catch (StaleElementReferenceException ex) {
			try {
				this.world.getWebElementFactory().getElement(sObject).click();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Assert.fail(e.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	


	public void click(WebElement element) throws Throwable {
		WebDriverWait webDriverWait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
		try {
			element = webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
			element.click();
		}  catch (StaleElementReferenceException ex) {
			int tryCounter = 0;
			do {
				try {
					element = webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
				} catch (Exception e) {
					tryCounter ++;
				}
			} while(tryCounter < 3 && element == null);
			element.click();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

		Thread.sleep(1000);
	}

	public void click(By byElement) throws Throwable {
		try {
			WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(byElement));
			element.click();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());

		}

		Thread.sleep(1000);
	}

	@And("^I click (?:on a|on) \"([^\"]*)\" at coordinates (\\d+) and (\\d+)$")
	public void clickAt(String sObject, int i, int j) throws Throwable {
		try {
			WebElement element = this.world.getWebElementFactory().getElement( sObject);
			WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
			element = wait.until(ExpectedConditions.elementToBeClickable(element));
			Actions build = new Actions(world.getWebDriver());
			build.moveToElement(element, i, j).pause(Duration.ofSeconds(3)).click().build().perform();
			System.out.println(sObject + " is successfully clicked at" + i + " and " + j);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());

		}
	}

	public void clickAt(By locator, int i, int j) throws Throwable {
		try {
			Actions build = new Actions(world.getWebDriver());
			WebElement element = this.world.getWebElementFactory().getElement(locator);
			build.moveToElement(element, i, j).pause(Duration.ofSeconds(3)).click().build().perform();
			System.out.println("Object is successfully clicked at" + i + " and " + j);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	public void clickAt(WebElement element, int i, int j) throws Throwable {
		try {
			Actions build = new Actions(world.getWebDriver());
			build.moveToElement(element, i, j).pause(Duration.ofSeconds(3)).click().build().perform();
			System.out.println("Object is successfully clicked at" + i + " and " + j);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@And("^I click (?:on a|on) \"([^\"]*)\" without highlight$")
	public void clickWithoutHighlight(String sObject) throws Throwable {
		System.out.println("Driver in Click :: " + world.getWebDriver() + " :: " + sObject);
		WebElement element = null;
		try {
			WebDriverWait wait = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT);
			element = wait.until(ExpectedConditions.elementToBeClickable(this.world.getWebElementFactory().getElement(sObject, false)));
			element.click();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	
	@And("^I click (?:on a|on) \"([^\"]*)\" by javascript$")
	public void clickByJavaScript(String sObjectName) throws Throwable{
		try {
			WebElement element = null;
			element = this.world.getWebElementFactory().getElement(sObjectName, false);
			performJSAction("click", element);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Exception occurred in clicking on " + sObjectName + " through javascript");
		}
	}
	
	public void clickByJavaScript(WebElement element) throws Throwable {
		try {
			performJSAction("click", element);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Exception occurred in clicking through javascript on :: " + element.toString());
		}
	}

	public void clickByJavaScript(By byLocator) throws Throwable {
		try {
			WebElement element = this.world.getWebElementFactory().getElement(byLocator);
			performJSAction("click", element);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Exception occurred in clicking through javascript on :: " + byLocator.toString());
		}
	}

	@And("^I double click (?:on a|on) \"([^\"]*)\" by actions class$")
	public void dblClickByActionsClass(String sObjectName) throws Throwable {
		try {
			Actions actions = new Actions(world.getWebDriver());
			WebElement element = this.world.getWebElementFactory().getElement(sObjectName);
			actions.moveToElement(element).pause(Duration.ofSeconds(2)).doubleClick(element).build().perform();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception occurred in double clicking through Actions class on :: " + sObjectName);
		}		
	}

	@And("^I click (?:on a|on) \"([^\"]*)\" by actions class$")
	public void clickByActionsClass(String sObjectName) throws Throwable {
		
			clickByActionsClass(this.world.getWebElementFactory().getElement(sObjectName));
	}

	public void clickByActionsClass(By locator) throws Throwable {
			clickByActionsClass(this.world.getWebElementFactory().getElement(locator));
	}
	
	public void clickByActionsClass(WebElement element) throws Throwable {
		try {
			Actions actions = new Actions(world.getWebDriver());
			actions.moveToElement(element).pause(Duration.ofSeconds(2)).click(element).pause(Duration.ofSeconds(2)).build().perform();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception occurred in clicking through Actions class on :: " + element);
		}		
	}
	
	public void dblClickByActionsClass(WebElement element) throws Throwable {
		try {
			Actions actions = new Actions(world.getWebDriver());
			actions.moveToElement(element).pause(Duration.ofSeconds(2)).doubleClick(element).build().perform();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception occurred in double clicking through Actions class on :: " + element.toString());
		}		
	}

	public void dblClickByActionsClass(By byLocator) throws Throwable {
		try {
			Actions actions = new Actions(world.getWebDriver());
			WebElement element = this.world.getWebElementFactory().getElement(byLocator);
			actions.moveToElement(element).pause(Duration.ofSeconds(2)).doubleClick(element).build().perform();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception occurred in double clicking through Actions class on :: " + byLocator.toString());
		}
	}

	@And("^I double click (?:on a|on) \"([^\"]*)\" by javascript$")
	public void dblClickByJavaScript(String sObjectName) throws Throwable {
		try {
			WebElement element = null;
			element = this.world.getWebElementFactory().getElement(sObjectName, false);
			performJSAction("dblclick", element);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Exception occurred while double clicking on " + sObjectName + " through javascript");
		}
	}

	public void dblClickByJavaScript(WebElement element) throws Throwable {
		try {
			performJSAction("dblClick", element);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Exception occurred while double clicking through javascript on:: " + element.toString());
		}
	}

	public void dblClickByJavaScript(By byLocator) throws Throwable {
		try {
			WebElement element = null;
			element = this.world.getWebElementFactory().getElement(byLocator);
			performJSAction("dblclick", element);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail("Exception occurred while double clicking through javascript on:: " + byLocator.toString());
		}
	}
	
	private void performJSAction(String action, WebElement element) throws Throwable {
			String jsString = getMouseEventsActionString("mouseover", 0);
			Thread.sleep(1000);
			JavascriptExecutor jsExecutor = (JavascriptExecutor)world.getWebDriver();
			jsExecutor.executeScript(jsString, element);
			jsString = getMouseEventsActionString(action, 1);
			Thread.sleep(1000);
			jsExecutor.executeScript(jsString, element);
	}
	
	private String getMouseEventsActionString(String action, int mouseClick) {
		return "var evObj = document.createEvent('MouseEvents');" +
				  "evObj.initMouseEvent(\""+ action +"\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, "+ mouseClick+", null);" +
				  "arguments[0].dispatchEvent(evObj);";
	}
	
	
}
