package steps.generic.keywords;

import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;

public class VerifyInnerHtml {
	World world;

	public VerifyInnerHtml(World world) {
		this.world = world;
	}

	@And("^I expect \"([^\"]*)\" object innerhtml contains \"([^\"]*)\"$")
	public void verifyInnerHtml(String sObject, String sData) throws IOException {
		WebElement Object;
		try {
			Object = this.world.getWebElementFactory().getElement( sObject);
			String actualInnerHtml = Object.getAttribute("innerHTML");
			Assert.assertTrue(actualInnerHtml.contains(sData),"Inner Html mismatch : Actual - " + actualInnerHtml + " and Expected - " + sData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	
	}
	
}
