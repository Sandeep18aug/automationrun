package steps.generic.keywords;



import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.exceptions.ObjectNotFoundInORException;

public class RichTextEdit {
	String NODE_INNER_HTML_ATTRIBUTE = "innerHTML";
	By byProperty;
	World world;

	public RichTextEdit(World world) {
		this.world = world;
	}
	String XPATH_RICH_TEXT_TOOLBAR = "//div[@class='richtext-container' and descendant::div[@style='display: block;']]//div[@class='coral-RichText-toolbar is-floating is-active']";
/*	function selectText(element) {
	    var doc = document;
	    var text = doc.getElementById(element);    

	    if (doc.body.createTextRange) { // ms
	        var range = doc.body.createTextRange();
	        range.moveToElementText(text);
	        range.select();
	    } else if (window.getSelection) { // moz, opera, webkit
	        var selection = window.getSelection();            
	        var range = doc.createRange();
	        range.selectNodeContents(text);
	        selection.removeAllRanges();
	        selection.addRange(range);
	    }
	}
	selectText("t");
*/	
	
      // @And("^I enter (.*) in \"([^\"]*)\" rich text box$")
	public void enterRichText(String sData, String sObject) throws Throwable  {
			try {
				WebElement element;
				element = this.world.getWebElementFactory().getElement(sObject);
				element.click();			
				WebElement rteParagraph = null;
				try {
					rteParagraph = element.findElement(By.xpath("./p"));
				}catch(NoSuchElementException e) {
					rteParagraph = null;
				}
				if(rteParagraph !=null) {
					JavascriptExecutor jsExecutor = ((JavascriptExecutor)world.getWebDriver());
					jsExecutor.executeScript("arguments[0].innerText = arguments[1]", rteParagraph,sData);
					Thread.sleep(500);	
				}else {
					element.sendKeys(sData);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Assert.fail("Exception occurred in enter value in richtext box : " + sObject);
			}			
	}	
	
	
     //  @And("^I expect RichText ToolBar is visible with all the controls$")
	public void verifyRTEToolbarWithAllControlsPresentFor(String Object) throws ObjectNotFoundInORException{
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.BOLD.getXpath()))!=null, "Bold button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.ITALIC.getXpath()))!=null,"Italic button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.UNDERLINE.getXpath()))!=null,"UnderLine button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.PARAFORMAT.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.IMAGEPROPS.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.MODIFYLINK.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.UNLINK.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.ANCHOR.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.SOURCEEDIT.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.CHECKTEXT.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.TABLE.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.SUBSCRIPT.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.SUPERSCRIPT.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.FONTSTYLES.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.UNDO.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.REDO.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.CUT.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.COPY.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.PASTEPLAINTEXT.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.PASTEWORDHTML.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.FIND.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.REPLACE.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.SPECIALCHARS.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.JUSTIFYLEFT.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.JUSTIFYCENTER.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.JUSTIFYRIGHT.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.UNORDERED.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.ORDERED.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.OUTDENT.getXpath()))!=null,"  button is not present");
		Assert.assertTrue(this.world.getWebElementFactory().getElement(Object,false).findElement(By.xpath(RTE_Button.INDENT.getXpath()))!=null,"  button is not present");
	}
	
	public void selectAllText() throws Throwable{
		Actions actions = new Actions(world.getWebDriver());
		world.getWebDriver().switchTo().activeElement();
		Thread.sleep(2000);
		actions.sendKeys(Keys.chord(Keys.CONTROL, "a")).perform();
		Thread.sleep(2000);
	}
	
	public void verifyTextIsBold (WebElement element){
		Assert.assertTrue(getRichTextAreaInnerHtml().contains("<b>"));
	}
	
	public void verifyTextIsItalic (WebElement element){
		Assert.assertTrue(getRichTextAreaInnerHtml().contains("<i>"));
	}
	
	public void verifyTextIsUnderlined (WebElement element){
		Assert.assertTrue(getRichTextAreaInnerHtml().contains("<u>"));
	}
	
	public void selectRTEButton(RTE_Button button){
		
		world.getWebDriver().findElement(By.xpath(button.getXpath())).click();
	}
	
	private String getRichTextAreaInnerHtml() {
		    String value = world.getWebDriver().switchTo().activeElement().getAttribute(NODE_INNER_HTML_ATTRIBUTE);
		    return value;
	}
	
	
       @When("^I select BOLD button of rich text toolbar for \"(.*?)\"$")
	public void i_select_BOLD_button_of_rich_text_toolbar_for(String objName) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions		
		//scroll.scrollDownElementIntoCQDialogViewPort(objName);
		WebElement rte = this.world.getWebElementFactory().getElement(objName, false);
		rte.click();
		selectAllText();
		rte.findElement(By.xpath(RTE_Button.BOLD.getXpath())).click();
		//this.world.getWebElementFactory().getElement(By.xpath(RTE_Button.BOLD.getXpath()),false).click();
		Thread.sleep(2000);	
	}

       @Then("^I select ITALIC button of rich text toolbar for \"(.*?)\"$")
	public void i_select_ITALIC_button_of_rich_text_toolbar_for(String objName) throws Throwable {
		WebElement rte = this.world.getWebElementFactory().getElement(objName, false);
		rte.click();
		selectAllText();
		rte.findElement(By.xpath(RTE_Button.ITALIC.getXpath())).click();
		Thread.sleep(2000);	
	}

       @Then("^I select UNDERLINE button of rich text toolbar for \"(.*?)\"$")
	public void i_select_UNDERLINE_button_of_rich_text_toolbar_for(String sObject) throws Throwable {
		WebElement rte = this.world.getWebElementFactory().getElement(sObject, false);
		rte.click();
		selectAllText();
		rte.findElement(By.xpath(RTE_Button.UNDERLINE.getXpath())).click();
		//this.world.getWebElementFactory().getElement(By.xpath(RTE_Button.BOLD.getXpath()),false).click();
		Thread.sleep(2000);	
	}

       @Then("^I select JUSTIFYLEFT button of rich text toolbar for \"(.*?)\"$")
	public void i_select_JUSTIFYLEFT_button_of_rich_text_toolbar_for(String sObject) throws Throwable {
		WebElement rte = this.world.getWebElementFactory().getElement(sObject, false);
		rte.click();
		selectAllText();
		rte.findElement(By.xpath(RTE_Button.JUSTIFYLEFT.getXpath())).click();
		Thread.sleep(2000);	
	}

       @Then("^I select JUSTIFYCENTER button of rich text toolbar for \"(.*?)\"$")
	public void i_select_JUSTIFYCENTER_button_of_rich_text_toolbar_for(String sObject) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
			//scroll.scrollDownElementIntoCQDialogViewPort(sObject);
/*			this.world.getWebElementFactory().getElement(sObject).click();
			selectAllText();
			this.world.getWebElementFactory().getElement(By.xpath(RTE_Button.JUSTIFYCENTER.getXpath()),false).click();
			Thread.sleep(2000);	*/
		WebElement rte = this.world.getWebElementFactory().getElement(sObject, false);
		rte.click();
		selectAllText();
		rte.findElement(By.xpath(RTE_Button.JUSTIFYCENTER.getXpath())).click();
		//this.world.getWebElementFactory().getElement(By.xpath(RTE_Button.BOLD.getXpath()),false).click();
		Thread.sleep(2000);	
	}

       @Then("^I select JUSTIFYRIGHT button of rich text toolbar for \"(.*?)\"$")
	public void i_select_JUSTIFYRIGHT_button_of_rich_text_toolbar_for(String sObject) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
			//scroll.scrollDownElementIntoCQDialogViewPort(sObject);
			/*this.world.getWebElementFactory().getElement(sObject).click();
			selectAllText();			
			this.world.getWebElementFactory().getElement(By.xpath(RTE_Button.JUSTIFYRIGHT.getXpath())).click();
			Thread.sleep(2000);*/	
		WebElement rte = this.world.getWebElementFactory().getElement(sObject, false);
		rte.click();
		selectAllText();
		rte.findElement(By.xpath(RTE_Button.JUSTIFYRIGHT.getXpath())).click();
		//this.world.getWebElementFactory().getElement(By.xpath(RTE_Button.BOLD.getXpath()),false).click();
		Thread.sleep(2000);	
	}


       @Then("^It should have BOLD impact on \"(.*?)\" rich text box$")
	public void it_should_have_BOLD_impact_on_rich_text_box(String sObject) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		verifyTextIsBold(this.world.getWebElementFactory().getElement(sObject,false));
	}

       @Then("^It should have ITALIC impact on \"(.*?)\" rich text box$")
	public void it_should_have_ITALIC_impact_on_rich_text_box(String sObject) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		verifyTextIsItalic(this.world.getWebElementFactory().getElement(sObject));
	}

       @Then("^It should have UNDERLINE impact on \"(.*?)\" rich text box$")
	public void it_should_have_UNDERLINE_impact_on_rich_text_box(String sObject) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		verifyTextIsUnderlined(this.world.getWebElementFactory().getElement(sObject));
	}

       @Then("^It should have JUSTIFYLEFT impact on \"(.*?)\" rich text box$")
	public void it_should_have_JUSTIFYLEFT_impact_on_rich_text_box(String sObject) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   WebElement element = this.world.getWebElementFactory().getElement(sObject);
	   Assert.assertTrue(element.findElement(By.xpath("./p")).getCssValue("text-align").equalsIgnoreCase("left"));
	}

       @Then("^It should have JUSTIFYCENTER impact on \"(.*?)\" rich text box$")
	public void it_should_have_JUSTIFYCENTER_impact_on_rich_text_box(String sObject) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		WebElement element = this.world.getWebElementFactory().getElement(sObject);
		   Assert.assertTrue(element.findElement(By.xpath("./p")).getCssValue("text-align").equalsIgnoreCase("center"));
	}

       @Then("^It should have JUSTIFYRIGHT impact on \"(.*?)\" rich text box$")
	public void it_should_have_JUSTIFYRIGHT_impact_on_rich_text_box(String sObject) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		WebElement element = this.world.getWebElementFactory().getElement(sObject);
		   Assert.assertTrue(element.findElement(By.xpath("./p")).getCssValue("text-align").equalsIgnoreCase("right"));
	}
	
	public enum RTE_Button {
		BOLD("format#bold"),
		ITALIC("format#italic"),
		UNDERLINE("format#underline"),
		PARAFORMAT("#paraformat"),
		IMAGEPROPS("image#imageProps"),
		MODIFYLINK("links#modifylink"),
		UNLINK("links#unlink"),
		ANCHOR("links#anchor"),
		SOURCEEDIT("misctools#sourceedit"),
		CHECKTEXT("spellcheck#checktext"),
		TABLE("table#table"),
		SUBSCRIPT("subsuperscript#subscript"),
		SUPERSCRIPT("subsuperscript#superscript"),
		FONTSTYLES("#styles"),
		UNDO("undo#undo"),
		REDO("undo#redo"), 
		CUT("edit#cut"),
		COPY("edit#copy"),
		DEFAULT("edit#paste-default"),
		PASTEPLAINTEXT("edit#paste-plaintext"),
		PASTEWORDHTML("edit#paste-wordhtml"),
		FIND("findreplace#find"),
		REPLACE("findreplace#replace"),
		SPECIALCHARS("misctools#specialchars"),
		JUSTIFYLEFT("justify#justifyleft"),
		JUSTIFYCENTER("justify#justifycenter"), 
		JUSTIFYRIGHT("justify#justifyright"),
		UNORDERED("lists#unordered"),
		ORDERED("lists#ordered"),
		OUTDENT("lists#outdent"),
		INDENT("lists#indent");
			
		private String xpath = "";

		  RTE_Button(String buttonText) {
		    //this.xpath = "//div[@class='richtext-container' and descendant::div[@style='display: block;']]//div[@class='coral-RichText-toolbar is-floating is-active']/button[@data-action='" + buttonText + "']";
		    //this.xpath = "//div[@class='richtext-container' and descendant::div[@style='display: block;']]//div[@class='coral-RichText-toolbar is-floating is-active']/button[@data-action='" + buttonText + "']";
		    
		    this.xpath = "./../div[contains(@class,'rte')]//coral-buttongroup[contains(@class,'active')]/button[@data-action='" + buttonText + "']";

		    
		  }

		/**
		 * @returns xpath of active richtext toolbar
		 */
		public String getXpath() {
		    return xpath;
		  }
		}
}
