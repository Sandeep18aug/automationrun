package steps.generic.keywords;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import cucumber.api.java.en.And;
import framework.exceptions.ObjectNotFoundInORException;

public class Hover {
	World world;

	public Hover(World world) {
		this.world = world;
	}

	@And("^I hover on \"([^\"]*)\"$")
	public void hover(String sObject) throws IOException, ObjectNotFoundInORException {
		WebElement webElement;
		webElement = this.world.getWebElementFactory().getElement(sObject);
		new Actions(world.getWebDriver()).moveToElement(webElement).build().perform();
	}

	public void hover(WebElement element) throws IOException {
		new Actions(world.getWebDriver()).moveToElement(element).build().perform();
	}
	
	public void hover(By byElement) throws IOException {
		WebElement webElement = this.world.getWebElementFactory().getElement(byElement,false);
		hover(webElement);
	}
	
	
}
