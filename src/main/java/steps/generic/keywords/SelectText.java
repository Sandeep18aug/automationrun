package steps.generic.keywords;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import cucumber.api.java.en.And;
import framework.exceptions.ObjectNotFoundInORException;
import framework.shared.RandomUtil;

public class SelectText {
	World world;

	public SelectText(World world) {
		this.world = world;
	}

	@And("^I expect to be able to choose (.*) for \"([^\"]*)\"$")
	public void select(String sData, String sObject) throws IOException {
		try {
			WebElement select = this.world.getWebElementFactory().getElement(sObject);
			new Select(select).selectByVisibleText(sData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	public void select(String sData, WebElement element) throws IOException {

		try {
			new Select(element).selectByVisibleText(sData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@And("^I select list option that contains text value \"([^\"]*)\" from \"([^\"]*)\"$")
	public void selectByTextContainsText(String sData, String sObject) throws IOException {
		try {
			WebElement list = this.world.getWebElementFactory().getElement(sObject);
			List<WebElement> options = new Select(list).getOptions();
			WebElement optionToBeSelected = null;
			optionToBeSelected = options.stream().filter(x -> x.getText().contains(sData)).findFirst().get();
			optionToBeSelected.click();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	public void selectByTextContainsText(String sData, By by) throws IOException {
		try {
			WebElement list = this.world.getWebElementFactory().getElement(by);
			List<WebElement> options = new Select(list).getOptions();
			WebElement optionToBeSelected = null;
			optionToBeSelected = options.stream().filter(x -> x.getText().contains(sData)).findFirst().get();
			if (optionToBeSelected == null) {
				Assert.fail("No option found for dropdown list object");
				return;
			}
			optionToBeSelected.click();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	public void selectByTextContainsText(String sData, Select select) throws IOException {
		try {
			List<WebElement> options = select.getOptions();
			WebElement optionToBeSelected = null;
			optionToBeSelected = options.stream().filter(x -> x.getText().contains(sData)).findFirst().get();
			if (optionToBeSelected == null) {
				Assert.fail("No option found for dropdown list object");
				return;
			}
			optionToBeSelected.click();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	public void selectByTextContainsTextIgnoreCase(String sData, Select select) throws IOException {
		try {
			List<WebElement> options = select.getOptions();
			WebElement optionToBeSelected = null;
			options.stream().forEach(x-> System.out.println(x.getText().toLowerCase()));
			System.out.println("Option to be selected is - " + sData.toLowerCase());
			optionToBeSelected = options.stream().filter(x -> x.getText().toLowerCase().contains(sData.toLowerCase()))
					.findFirst().get();
			if (optionToBeSelected == null) {
				Assert.fail("No option found for dropdown list object");
				return;
			}
			optionToBeSelected.click();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	public void selectByTextContainsTextIgnoreCase(String sData, WebElement select) throws IOException {
		try {
			
			List<WebElement> options = new Select(select).getOptions();
			WebElement optionToBeSelected = null;
			options.stream().forEach(x-> System.out.println(x.getText().toLowerCase()));
			System.out.println("Option to be selected is - " + sData.toLowerCase());
			optionToBeSelected = options.stream().filter(x -> x.getText().toLowerCase().contains(sData.toLowerCase()))
					.findFirst().get();
			if (optionToBeSelected == null) {
				Assert.fail("No option found for dropdown list object");
				return;
			}
			optionToBeSelected.click();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@And("^I select list option by value (.*) from \"([^\"]*)\"$")
	public void selectByValue(String sData, String sObject) throws IOException {
		try {
			new Select(this.world.getWebElementFactory().getElement(sObject))
					.selectByValue(sData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@And("^I select list option by (?:I|i)ndex (\\d+) from \"([^\"]*)\"$")
	public void selectByIndex(int sData, String sObject) throws IOException {
		try {
			new Select(this.world.getWebElementFactory().getElement(sObject))
					.selectByIndex(sData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@And("^I expect to be see (.*) Option under \"([^\"]*)\" Dropdown$")
	public void verifyListItem(String sData, String sObject) throws IOException, ObjectNotFoundInORException {
		WebElement element = this.world.getWebElementFactory().getElement(sObject);
		Select select = new Select(element);
		Assert.assertTrue(select.getOptions().stream().anyMatch(x -> x.getText().trim().equalsIgnoreCase(sData)));
	}

	@And("^I expect (.*) Option is selected for \"([^\"]*)\" Dropdown$")
	public void verifyListItemSelected(String sData, String sObject) throws IOException, ObjectNotFoundInORException {
		WebElement element = this.world.getWebElementFactory().getElement(sObject);
		Select select = new Select(element);
		Assert.assertTrue(select.getFirstSelectedOption().getText().trim().equals(sData.trim()));
	}

	public List<String> getAllOptionsAsStringList(String sObject) throws ObjectNotFoundInORException {
		WebElement element = this.world.getWebElementFactory().getElement(sObject);
		Select select = new Select(element);
		List<WebElement> options = select.getOptions();
		ArrayList<String> optionsStringList = new ArrayList<>();
		for (WebElement option : options) {
			optionsStringList.add(option.getText().trim());
		}
		return optionsStringList;
	}

	public boolean isSelectOptionEnabled(String sObject, String sOptionText) throws ObjectNotFoundInORException {
		WebElement element = this.world.getWebElementFactory().getElement(sObject);
		if (element == null)
			Assert.fail(sOptionText + " is not present in the list");
		WebElement option = element.findElement(By.xpath(".//option[text()='" + sOptionText + "']"));
		return option.isEnabled();
	}
	
	public void selectRandomOption(Select list) {
		int listSize = list.getOptions().size();
		int randomValue = RandomUtil.getRandomNumber(1, listSize-1);
		list.selectByIndex(randomValue );
	}
}
