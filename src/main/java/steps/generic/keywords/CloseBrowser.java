package steps.generic.keywords;

import cucumber.api.java.en.And;

public class CloseBrowser {
	World DriverHandle;

	public CloseBrowser(World runner) {
		this.DriverHandle = runner;
	}

	@And("^I close the open browser$")
	public void closeBrowser() {
		DriverHandle.closeDriver();
	}
}
