package steps.generic.keywords;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;


public class DragDrop{
	World world;

	public DragDrop(World world) {
		this.world = world;
	}
	
	public void dragAndDrop(String sSourceElement, String sDestinationElement){
		WebElement sourceElement, destinationElement;
		try {
			sourceElement = this.world.getWebElementFactory().getElement(sSourceElement);
			destinationElement = this.world.getWebElementFactory().getElement(sDestinationElement);
			Actions moveAndDrop = new Actions(world.getWebDriver());
	        moveAndDrop.clickAndHold(sourceElement).perform();
	        moveAndDrop.moveToElement(destinationElement).perform();
	        moveAndDrop.release(destinationElement).perform();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}		
	}
	
	
	public void dragAndDrop(WebElement SourceElement, WebElement DestinationElement){
		try {
			Actions moveAndDrop = new Actions(world.getWebDriver());
	        moveAndDrop.clickAndHold(SourceElement).perform();
	        moveAndDrop.moveToElement(DestinationElement).perform();
	        moveAndDrop.release(DestinationElement).perform();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		
	}
}





