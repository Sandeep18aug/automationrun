package steps.generic.keywords;

import java.util.ArrayList;
import java.util.Set;
import org.testng.Assert;
import cucumber.api.java.en.And;

public class WindowHandling{
	World world;

    public WindowHandling(World world){
        this.world = world;
    }
	
	
	@And("(?:I|i) switch to the newly opened tab (\\d+)$")
	public void switchTabByIndex(int sTabNumber) throws Throwable {
		try {
			ArrayList<String> tabs_windows = new ArrayList<String>(world.getWebDriver().getWindowHandles());
			String sTab = tabs_windows.get(sTabNumber - 1);
			world.getWebDriver().switchTo().window(sTab);
			Thread.sleep(2000);
			String title = world.getWebDriver().getTitle();
			System.out.println("Successfully switched to window with index " + sTabNumber + " and having title = " + title);											
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@And("(?:I|i) close the newly opened tab (\\d+)$")
	public void closeTabByIndex(int sTabNumber) throws Throwable {
			try {
				ArrayList<String> tabs_windows = new ArrayList<String>(world.getWebDriver().getWindowHandles());
				String sTab = tabs_windows.get(sTabNumber - 1);
				System.out.println(sTab);
//				world.getWebDriver().switchTo().window(sTab);
				Thread.sleep(2000);
				world.getWebDriver().close();
				System.out.println("Successfully closed newly opened tab " + sTabNumber);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}
	}

	@And("(?:I|i) expect window count is equal to (.*)$")
	public void verifyWindowCount(int sWindowCount) throws Throwable {
			try {
				ArrayList<String> tabs_windows = new ArrayList<String>(world.getWebDriver().getWindowHandles());
				int actualCount = tabs_windows.size();
				int expectedCount = Integer.valueOf(sWindowCount);
				System.out.println("Actual window count:: " + actualCount +" and Expected window count:: " + expectedCount);
				Assert.assertTrue(actualCount == expectedCount , "Actual window count:: " + actualCount +" and Expected window count:: " + expectedCount);
			} catch (Exception e) {
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}
	}	

	@And("(?:I|i) expect new (?:window|) tab opens with url having \"([^\"]*)\"$")
	public void verifyNewWindowWithUrlContains(String urlString) throws Throwable {
		String currentWindowHandle = null;
		try {
			currentWindowHandle = world.getWebDriver().getWindowHandle();
			boolean windowFoundHavingUrl = false;
			Set<String> windowHandles = world.getWebDriver().getWindowHandles();
			for(String windowHandle:windowHandles) {
				world.getWebDriver().switchTo().window(windowHandle);
				if(world.getWebDriver().getCurrentUrl().contains(urlString)) {
					windowFoundHavingUrl = true;
					break;
				}
			}
			Assert.assertTrue(windowFoundHavingUrl , "there is no new window having url " + urlString);
			} catch (Exception e) {
				e.printStackTrace();
				Assert.fail(e.getMessage());
			} finally {
				world.getWebDriver().switchTo().window(currentWindowHandle);
			}
	}	

}
