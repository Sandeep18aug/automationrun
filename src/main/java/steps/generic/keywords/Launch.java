package steps.generic.keywords;

import java.net.HttpURLConnection;
import java.net.URL;



import java.util.Properties;
import java.util.Random;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import framework.shared.GetUrl;

public class Launch {
	World world;
	Wait wait ;

	public Launch(World world, Wait wait) {
		this.world = world;
		this.wait = wait;
	}

	GetUrl getConfig = new GetUrl();

	// @Given("^I open the (.*) site$")
	public void launch(String sUrl) {
		try {
			// Properties globalConfig = getConfig.getConfiguration();
			// String Url = globalConfig.getProperty(sUrl, sUrl);
			// String Url = getConfig.getUrl(sUrl);
			world.setCapabilities(false);
			System.out.println("Launching proxy disabled browser");
			System.out.println(sUrl);
			world.getWebDriver().get(sUrl);
			maximizeBrowser(world.getWebDriver());
			world.getWebDriver().navigate().refresh();
			wait.waitForPageReady();
		} catch (Exception e) {
			System.out.println("Exception while launch :: " + e);
			Assert.fail(e.getMessage());
		}
	}

	public void launch(String sUrl, Boolean proxyEnabled) {
		try {
			Properties globalConfig = getConfig.getConfiguration();
			String Url = globalConfig.getProperty(sUrl, sUrl);
			world.setCapabilities(proxyEnabled);
			if (proxyEnabled)
				System.out.println("Launching proxy enabled browser");
			else
				System.out.println("Launching proxy disabled browser");
			System.out.println(Url);
			world.getWebDriver().get(Url);
			maximizeBrowser(world.getWebDriver());
			world.getWebDriver().navigate().refresh();
			wait.waitForPageReady();
			if(world.getWebDriver().getCurrentUrl().contains("")) {
				
			}
		} catch (Exception e) {
			System.out.println("Exception while launch :: " + e);
			Assert.fail(e.getMessage());
		}
	}
	
	public void httpresponsestatus(String sUrl) {
		try {
			URL url = new URL(sUrl);
			HttpURLConnection http = (HttpURLConnection)url.openConnection();
			int statusCode = http.getResponseCode();
			System.out.println("http response code for url: " + sUrl + " is> " + statusCode);
		} catch (Exception e) {
			System.out.println("Exception while launch :: " + e);
			Assert.fail(e.getMessage());
		}
		//return statusCode;
	}

	// @Given("^I navigate to (.*) page$")
	public void navigateTo(String sUrl) {
		try {
			Properties globalConfig = getConfig.getConfiguration();
			String Url = globalConfig.getProperty(sUrl, sUrl);
			System.out.println(Url);
			world.getWebDriver().navigate().to(Url);
			maximizeBrowser(world.getWebDriver());
			wait.waitForPageReady();
		} catch (Exception e) {
			System.out.println("Exception while Navigating page Url :: " + e);
			Assert.fail(e.getMessage());
		}
	}

	public void maximizeBrowser(WebDriver driver) {
		try {
			driver.manage().window().maximize();
		} catch (Exception ex) {
			System.out.println("Exception in maximizing browser window");			
		}
	}
	
	
	@Given("^(?:i|I) navigate to wrong url$")
	public void navigateToWrongUrl() throws Throwable {
		try {
			String url = world.getWebDriver().getCurrentUrl();
			url = url.toLowerCase().replace(".html","");
			Random rand = new Random();
			int  n = rand.nextInt(50) + 1;
			url = url + n + ".html";
			world.getWebDriver().navigate().to(url);
			wait.waitForPageReady();
			
		} catch (Exception e) {
			System.out.println("Exception while Navigating page Url :: " + e);
			Assert.fail(e.getMessage());
		}
	}
}
