package steps.generic.keywords;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import cucumber.api.java.en.And;
import framework.shared.FrameworkConstants;

public class VerifyElementVisible {
	World world;

	public VerifyElementVisible(World world) {
		this.world = world;
	}

	protected boolean isElementVisibleOnPage(By byProperty, int seconds) {
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(world.getWebDriver(), seconds);
		try {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(byProperty));
		} catch (org.openqa.selenium.TimeoutException ex) {
		} catch (Exception ex) {
			System.out.println("Caught Generic exception on Verify Element isElementVisibleOnPage ");
			ex.printStackTrace();
		}
		return element != null ? true : false;
	}
	

	@And("^I expect to see \"([^\"]*)\" as visible$")
	public void verifyElementVisible(String sObject) throws IOException {
		Boolean isElementPresent = this.world.getWebElementFactory().isElementVisibleOnPage(sObject, true);
		Assert.assertTrue(isElementPresent, sObject + " is not visible on page");
	}

	public void verifyElementVisible(By elementLocator) throws IOException {
		Boolean isElementPresent = this.world.getWebElementFactory().isElementVisibleOnPage(elementLocator, true);
		Assert.assertTrue(isElementPresent, elementLocator.toString() + " is not visible on page");
	}

	@And("^I expect not to see \"([^\"]*)\" as visible$")
	public void verifyElementNotVisible(String sObject) throws IOException {
		By elementLocator = this.world.getWebElementFactory().getByLocator(sObject);
		Boolean isElementPresent = isElementVisibleOnPage(elementLocator, FrameworkConstants.SMALL_WAIT);
		Assert.assertFalse(isElementPresent, sObject + " is visible on page");
	}

	public void verifyElementNotVisible(By elementLocator) throws IOException {
		Boolean isElementPresent = isElementVisibleOnPage(elementLocator, FrameworkConstants.SMALL_WAIT);
		Assert.assertFalse(isElementPresent, elementLocator.toString() + " is visible on page");
	}

	@And("^I expect \"([^\"]*)\" element count is \"([^\"]*)\" (\\d+)$")
	public void verifyRelation(String sObject, String relationalOperator, int value) throws IOException {
		By byProperty = this.world.getWebElementFactory().getByLocator(sObject);
		List<WebElement> elements = null;
		try {
			elements = new WebDriverWait(world.getWebDriver(), FrameworkConstants.MEDIUM_WAIT)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(byProperty));
		} catch (TimeoutException e) {
		}
		if (elements == null) {
			System.out.println("Actual Count :: 0 and Expected Count :: " + value);
			Assert.assertTrue(value == 0);
			return;
		} else {
			int actualSize = elements.size();
			String message = "Actual Count :: " + actualSize + " and Expected Count :: " + value;
			System.out.println(message);
			switch (relationalOperator.trim()) {
			case ">":
			case "greater than":
				Assert.assertTrue(actualSize > value, message);
				break;
			case "<":
			case "lesser than":
				Assert.assertTrue(actualSize < value, message);
				break;
			case ">=":
			case "greater than equal to":
				Assert.assertTrue(actualSize >= value, message);
				break;
			case "<=":
			case "lesser than equal to":
				Assert.assertTrue(actualSize <= value, message);
				break;
			case "==":
			case "=":
			case "equal to":
				Assert.assertTrue(actualSize == value, message);
				break;
			case "!=":
			case "<>":
			case "not equal to":
				Assert.assertTrue(actualSize != value, message);
				break;
			default:
				Assert.fail(
						"Relational Operator must be one of these (>,<,>=,<=,==,!=,greater than,lesser than,greater than equal to, lesser than equal to,equal to,not equal to)");
			}
		}

	}

	@And("^I expect that \"([^\"]*)\" image is loaded on paged$")
	public void verifyImageLoaded(String sObject) throws IOException {
		WebElement image = this.world.getWebElementFactory().getElement(sObject, true);
		verifyImageLoaded(image);
	}

	public void verifyImageLoaded(By byObject) throws IOException {
		WebElement image = this.world.getWebElementFactory().getElement(byObject, true);
		verifyImageLoaded(image);
	}

	public void verifyImageLoaded(WebElement imageObject) throws IOException {
		String script = "return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0";
		Boolean imageLoaded = false;
		for (int i = 1; i <= 15; i++) {
			imageLoaded = (Boolean) ((JavascriptExecutor) this.world.getWebDriver()).executeScript(script, imageObject);
			if (imageLoaded) {
				System.out.println("image is loaded on the page");
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		Assert.assertTrue(imageLoaded, "image is not loaded on the page.");
	}
}
