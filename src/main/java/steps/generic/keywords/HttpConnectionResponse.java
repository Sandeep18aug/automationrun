package steps.generic.keywords;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.testng.Assert;

import cucumber.api.java.en.Then;

public class HttpConnectionResponse {
	
	World capabilities;
	static String assertMessage = "";
	public HttpConnectionResponse(World capabilities) {
		this.capabilities = capabilities;
	}
	
	public static void concatenateMessages(String message) {
		assertMessage += message + "\n";
	}
	@Then("^(?:i|I) expect to see no broken links in the page$")
	public void getBrokenLinks() {
		Map<Integer, List<String>> map = capabilities.getWebDriver().findElements(By.xpath("//a[@href]")) 
	            .stream()
	            .map(ele -> ele.getAttribute("href"))
	            .map(String::trim)
	            .distinct()
	            .collect(Collectors.groupingBy(HttpConnectionResponse::getResponseCode));
		
		if (map.get(404)!=null) {
			map.get(404)
			   .stream()
			   .forEach(HttpConnectionResponse::concatenateMessages);
			Assert.fail("The page has " + map.get(404).size() + " broken links and they are as follows:\n" + assertMessage);
		} else
			capabilities.reportStepInfo("No broken links found for the page.");
	}
	public static int getResponseCode(String link) {
	    URL url;
	    HttpURLConnection con = null;
	    Integer responsecode = 0;
	    try {
	        url = new URL(link);
	        con = (HttpURLConnection) url.openConnection();
	        responsecode = con.getResponseCode();
	    } catch (Exception e) {
	    } finally {
	        if (null != con)
	            con.disconnect();
	    }
	    return responsecode;
	}
}
