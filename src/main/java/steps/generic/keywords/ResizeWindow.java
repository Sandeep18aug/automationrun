package steps.generic.keywords;


import org.openqa.selenium.Dimension;
import cucumber.api.java.en.And;

public class ResizeWindow{
	World world;

    public ResizeWindow(World world){
        this.world = world;
    }
	
	@And("^I resize the opened window for width \"([^\"]*)\" and height \"([^\"]*)\"$")
	public void resizeWindow(String sHeight, String sWidth) {
		world.getWebDriver().manage().window().setSize(new Dimension(Integer.parseInt(sWidth), Integer.parseInt(sHeight)));	
	}
}
