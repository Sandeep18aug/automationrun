package steps.generic.keywords;

import org.testng.Assert;
import cucumber.api.java.en.And;


public class VerifyTitle{
	World world;

    public VerifyTitle(World world){
        this.world = world;
    }
	
	@And("^I expect page title as \"([^\"]*)\"$")
	public void verifyTitle(String sData) {
		try {
			String title = world.getWebDriver().getTitle().trim();
			Assert.assertTrue(title.contentEquals(sData.trim()),"Actual title :: '" + title + "' and Expected title :: '" + sData + "'");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@And("^I expect page title contains \"([^\"]*)\"$")
	public void verifyTitleContains(String sData) {
			try {
				sData = sData.trim().replaceAll("^\"|\"$", "");
				String title = world.getWebDriver().getTitle().trim();
				Assert.assertTrue(title.toLowerCase().contains(sData.toLowerCase().toString()),"Actual title :: '" + title + "' doesn't contain Expected title :: '" + sData + "'");
			} catch (Exception e) {
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}
	}
	
	@And("^I expect page title starts with \"([^\"]*)\"$")
	public void verifyTitleStartWith(String sData) {
			try {
				String title = world.getWebDriver().getTitle().trim();
				Assert.assertTrue(title.toLowerCase().startsWith(sData.toLowerCase().toString()),"Actual title :: '" + title + "' doesn't starts with string :: '" + sData + "'");
	
				Assert.assertTrue(world.getWebDriver().getTitle().toLowerCase().startsWith(sData.toLowerCase().toString()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}
	}
}
