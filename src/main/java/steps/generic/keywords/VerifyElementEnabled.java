package steps.generic.keywords;

import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.And;

public class VerifyElementEnabled{
	World world;

	public VerifyElementEnabled(World world) {
		this.world = world;
	}
	
	@And("^I (?:expect to see|expect) \"([^\"]*)\" as (?:enabled|present)$")
	public void verifyElementEnabled(String sObject) throws IOException {
		try {
			WebElement webElement = null;
			webElement = this.world.getWebElementFactory().getElement(sObject,false);
			if(webElement == null) {
				Assert.fail(sObject + " is not present in webpage");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception occurred in verifying object's presence on webpage");
		}
	}
	
	@And("^I (?:expect not to see|donot expect) \"([^\"]*)\" as (?:enabled|present)$")
	public void verifyElementNotEnabled(String sObject) throws IOException {
		try {
			WebElement webElement = null;
			webElement = this.world.getWebElementFactory().getElement(sObject,false);
			if(webElement != null) {
				Assert.fail(sObject + " is present in webpage");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception occurred in verifying object's presence on webpage");
		}
	}
	
	@And("^I expect to see \"([^\"]*)\" as enabled without highlighting$")
	public void verifyElementEnabledWithoutHighlighting(String sObject) throws IOException {
		try {
			WebElement webElement = null;
			webElement = this.world.getWebElementFactory().getElement(sObject,false);
			if(webElement == null) {
				Assert.fail(sObject + " is not present in webpage");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception occurred in verifying object's presence on webpage");
		}
	}
	
	@And("^I expect to see \"([^\"]*)\" as enabled with highlighting$")
	public void verifyElementEnabledWithHighlighting(String sObject) throws IOException {
		try {
			WebElement webElement = null;
			webElement = this.world.getWebElementFactory().getElement(sObject,true);
			if(webElement == null) {
				Assert.fail(sObject + " is not present in webpage");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Exception occurred in verifying object's presence on webpage");
		}
	}
	
	
	@And("^I expect to see \"([^\"]*)\" as disabled$")
	public void verifyElementDisabled(String sObject) throws IOException {
			try {
				WebElement Object = null;
				Object = this.world.getWebElementFactory().getElement(sObject);				
				Assert.assertNotNull(Object.getAttribute("disabled"));
			} catch (Exception e) {
				e.printStackTrace();
				Assert.fail("Exception occurred in verifyElementDisabled method for :: " + sObject);
			}			
	}	
}











