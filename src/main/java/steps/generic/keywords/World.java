package steps.generic.keywords;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Properties;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import framework.selenium.support.WebDriverFactory;
import framework.selenium.support.WebElementFactory;
import framework.shared.FrameworkConstants;
import framework.shared.GetBrowserConfiguration;
import framework.shared.ScenarioContext;
import cucumber.api.Scenario;

public class World {
	private DesiredCapabilities capabilities;
	private WebDriver driver = null;
	Scenario scenario;
	private String sOrName;
	private String sFeatureName;
	private GetBrowserConfiguration getBrowserConfiguration = new GetBrowserConfiguration();
	private static volatile int nextID = 1;
	private String brandNameForScenarioContext = "";
	private String localeNameForScenarioContext = "";
	private String authorUrlForScenarioContext = "";
	private String pageNameToBeCreated = "";
	private ScenarioContext scenarioContext = new ScenarioContext();
	private String brandName = "";
	private String scenarioId = "";
	private WebElementFactory webElementFactory = null;

	private static class ThreadLocalID extends ThreadLocal<Integer> {
		protected synchronized Integer initialValue() {
			return nextID++;
		}
	}

	private static ThreadLocalID threadID = new ThreadLocalID();

	public static int getThreadNumber() {
		return threadID.get();
	}

	public static void set(int index) {
		threadID.set(index);
	}

	private void setOrName() {
		try {
			Properties orMapping = new Properties();
			File ORfile = new File(FrameworkConstants.SRC_MAIN_RESOURCES + "config/orMapping.properties");
			FileInputStream fs = new FileInputStream(ORfile);
			orMapping.load(fs);
			sOrName = orMapping.getProperty(sFeatureName, sFeatureName);
			//System.out.println("setting object repository as " + this.sOrName);
		} catch (FileNotFoundException ex) {
			System.out.println("orMapping.properties file is not found.");
			sOrName = sFeatureName;
		} catch (Exception e) {
			System.out.println("Exception occurred while loading orMapping.properties");
			sOrName = sFeatureName;
		}
	}
	
	public ScenarioContext getScenarioContext() {
		return this.scenarioContext;
	}
	@Before
	public void before(Scenario scenario) {
		this.scenario = scenario;
		System.out.println(scenario.getId());
		int iPos = scenario.getId().lastIndexOf(":");
		sFeatureName = scenario.getId().substring(0, iPos);
		sFeatureName = sFeatureName.trim();
		String[] paths = sFeatureName.split("/");
		int sizeOfPaths = paths.length;
		sFeatureName = paths[sizeOfPaths - 1];
		sFeatureName = sFeatureName.replaceAll("(?i).feature", "");		
		System.out.println("*******************************************************************************");
		System.out.println("Feature Name:: " + sFeatureName);
		System.out.println("Scenario Name:: " + scenario.getName());
		System.out.println("*******************************************************************************");
		setOrName();
		this.scenarioContext.resetScenarioContext();
		this.scenarioId = scenario.getName();
		if(this.scenarioId.contains("sid=")) {
			this.scenarioId = this.scenarioId.split("sid=")[1].trim();
		}
	}

	@After
	public void after(Scenario scenario) {
		this.scenarioContext.resetScenarioContext();
		if (null != driver) {
			try {
				if (scenario.isFailed()) {
					// This takes a screenshot from the driver at save it to the specified location
					TakesScreenshot ts = (TakesScreenshot) driver;
					byte[] screenshot = ts.getScreenshotAs(OutputType.BYTES);
					scenario.embed(screenshot, "image/png");

				}
				driver.quit();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

//	@BeforeStep
//	public void beforeStep(Scenario scenario) {
//		System.out.println("Executing step >>>>>>>>> ");
//	}
	
	
	public void addMessage(String strMessage) {
		Cookie cookie = new Cookie("zaleniumMessage", strMessage);
		driver.manage().addCookie(cookie);
	}

	public void setScenarioStatus(Boolean status) {
		Cookie cookie = new Cookie("zaleniumTestPassed", status ? "true" : "false");
		driver.manage().addCookie(cookie);
	}

	public void closeDriver() {
		if (null != driver) {
			try {
				driver.quit();
				System.out.println("Successfully quit Driver object");
			} catch (Exception e) {
				System.out.println("exception in closing browser");
			}
		}
	}

	public String getFeatureName() {
		return sFeatureName;
	}

	public String getOrName() {
		return sOrName;
	}

	public synchronized void setBrowserCapabilities(String sBrowser, String sVersion, String sLocation,
			boolean proxyEnabled) throws Exception {
		if (sLocation.equalsIgnoreCase("Local")) {
			if (sBrowser.equalsIgnoreCase("chrome")) {
				driver = null;
				try {
					driver = WebDriverFactory.getChromeDriver(proxyEnabled);
					System.out.println("Successfully launced chrome browser...");
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (driver == null) {
						System.out.println("Driver is still null. Trying to create driver again.");
						new WebDriverFactory();
						driver = WebDriverFactory.getChromeDriver(proxyEnabled);
					}
				}
			} else if (sBrowser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			} else {
				System.out
						.println("Browser for execution is not set properly. Choose option from 'chrome' or 'firefox'");
			}

		} else if (sLocation.equalsIgnoreCase("saucelab")) {
			capabilities = new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.BROWSER_NAME, sBrowser);
			capabilities.setCapability(CapabilityType.VERSION, sVersion);
			capabilities.setCapability("name", "Test :: " + sBrowser + "-" + sVersion);
			capabilities.setCapability("screenResolution", "1280x1024");
			capabilities.setCapability("maxDuration", 3000);
			capabilities.setCapability("build", "Build 6-Sep V-0.01");
			driver = new RemoteWebDriver(new URL("https://" + "UnileverBDDQA" + ":"
					+ "994fc7bf-a05b-4dfe-b911-898f83d79601" + "@ondemand.saucelabs.com:443" + "/wd/hub"),
					capabilities);
			((JavascriptExecutor) driver).executeScript("sauce:job-result=passed");
		} else if (sLocation.equalsIgnoreCase("docker")) {
			driver = WebDriverFactory.getDockerRemoteWebDriver("http://localhost:4444/wd/hub", scenario.getName(), sBrowser,
					proxyEnabled);
		}else if (sLocation.equalsIgnoreCase("grid")) {
			driver = WebDriverFactory.getGridRemoteWebDriver("http://"+FrameworkConstants.GRID_IP+":"+FrameworkConstants.GRID_PORT+"/wd/hub", scenario.getName(), sBrowser,
					proxyEnabled);
		}
		else
			System.out.println("Browserconfig.xml file - mention 'Local' or 'Docker' only in Destination tag");
		this.webElementFactory = new WebElementFactory(driver, brandName);
	}

	public synchronized void setCapabilities(Boolean proxyEnabled) throws Exception {
		int threadNumber = World.getThreadNumber();
		System.out.println("Thread Number:: " + threadNumber);
		getBrowserConfiguration.getBrowserData("CommonThread");
		if (getBrowserConfiguration.getRunStatus().equalsIgnoreCase("Yes")) {
			setBrowserCapabilities(getBrowserConfiguration.getBrowserName(),
					getBrowserConfiguration.getBrowserVersion(), getBrowserConfiguration.getDestination(),
					proxyEnabled);
		}
	}
	
	public void reportStepInfo(String text) {
		System.out.println(this.scenario.getName() + " >>>>>>> " + System.lineSeparator() +text);
		this.scenario.write(text);
	}

	public synchronized WebDriver getWebDriver() {
		return driver;
	}

	public String getBrandNameForScenarioContext() {
		return brandNameForScenarioContext;
	}

	public void setBrandNameForScenarioContext(String brandNameForScenarioContext) {
		this.brandNameForScenarioContext = brandNameForScenarioContext;
	}

	public String getLocaleNameForScenarioContext() {
		return localeNameForScenarioContext;
	}

	public void setLocaleNameForScenarioContext(String localeNameForScenarioContext) {
		this.localeNameForScenarioContext = localeNameForScenarioContext;
	}

	public String getAuthorUrlForScenarioContext() {
		return authorUrlForScenarioContext;
	}

	public void setAuthorUrlForScenarioContext(String authorUrlForScenarioContext) {
		this.authorUrlForScenarioContext = authorUrlForScenarioContext;
	}

	public String getPageNameToBeCreated() {
		return pageNameToBeCreated;
	}

	public void setPageNameToBeCreated(String pageNameToBeCreated) {
		this.pageNameToBeCreated = pageNameToBeCreated;
	}
	
	public String getBrandName() {
		return this.brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
		this.webElementFactory.setOrName(brandName);
	}
	public String getScenarioId() {
		return this.scenarioId;
	}
	public WebElementFactory getWebElementFactory() {
		return this.webElementFactory;
	}
}
