package steps.generic.keywords;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.google.common.base.Strings;

import cucumber.api.java.en.And;

public class VerifyText {
	World world;

	public VerifyText(World world) {
		this.world = world;
	}

	@And("^I expect to see text as (.*) for \"([^\"]*)\"$")
	public void verifyText(String sData, String sObject) throws IOException {
		WebElement Object;
		try {
			Object = this.world.getWebElementFactory().getElement( sObject);
			String actualText = Object.getText().trim();
			Assert.assertTrue(actualText.contentEquals(sData),"Text mismatch : Actual - " + actualText + " and Expected - " + sData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@And("^I expect \"([^\"]*)\" object has text equal to \"([^\"]*)\"$")
	public void TextVerify(String sObject, String sData) throws IOException {
		WebElement Object;
		try {
			Object = this.world.getWebElementFactory().getElement( sObject);
			String actualText = Object.getText().trim();
			Assert.assertTrue(actualText.contains(sData),"Text mismatch : Actual - " + actualText + " and Expected - " + sData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());

		}
		
	}
	
	@And("^I expect \"([^\"]*)\" object contains text \"([^\"]*)\"$")
	public void TextVerifyContains(String sObject, String sData) throws IOException {
		WebElement Object = null;
		try {
			Object = this.world.getWebElementFactory().getElement( sObject);
			String actualText = Object.getText().trim();
			Assert.assertTrue(actualText.contains(sData),"Text mismatch : Actual - " + actualText + " and Expected - " + sData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@And("^I don't expect \"([^\"]*)\" object contains text \"([^\"]*)\"$")
	public void TextVerifyNotContains(String sObject, String sData) throws IOException {
		WebElement Object = null;
		try {
			Object = this.world.getWebElementFactory().getElement( sObject);
			String actualText = Object.getText().trim();
			Assert.assertFalse(actualText.contains(sData),"Text : Actual - " + actualText + " and Expected - " + sData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());

		}
	}
	
	@And("^I expect \"([^\"]*)\" object has no text$")
	public void verifyObjectHasNoText(String sObject) throws IOException {
		WebElement Object;
		try {
			Object = this.world.getWebElementFactory().getElement( sObject);
			String actualText = Object.getText().trim();
			Assert.assertTrue(Strings.isNullOrEmpty(actualText), sObject + " has text " + actualText);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}		
	}
	
	@And("^I expect \"([^\"]*)\" object has some text$")
	public void verifyObjectHasText(String sObject) throws IOException {
		WebElement Object;
		try {
			Object = this.world.getWebElementFactory().getElement(sObject);
			String actualText = Object.getText().trim();
			Assert.assertTrue(!Strings.isNullOrEmpty(actualText), sObject + " has no text");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}		
	}
	
	@And("^I expect to see \"([^\"]*)\" text on the page$")
	public void verifyTextOnPage(String text) throws IOException {
		try {
			String pageText = world.getWebDriver().findElement(By.tagName("body")).getText();
			Assert.assertTrue(pageText.contains(text),text + " is not present on page");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}		
	}
	
	
	//I expect to see "${expectedresult}" on the page
	
}
