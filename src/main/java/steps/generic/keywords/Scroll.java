
package steps.generic.keywords;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.And;
import framework.exceptions.ObjectNotFoundInORException;


public class Scroll{
	public World world;
	public Wait wait;
	
	public Scroll(World world, Wait wait) {
		this.world = world;
		this.wait = wait;
	}

	public Boolean isElementInViewport(WebElement element) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("var rect = arguments[0].getBoundingClientRect();");
		stringBuilder.append(" return rect.bottom > 0 && rect.right > 0 &&");
		stringBuilder.append(" rect.left < (window.innerWidth || document.documentElement.clientWidth) &&");
		stringBuilder.append(" rect.top < (window.innerHeight || document.documentElement.clientHeight);");
		String js = stringBuilder.toString();
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		// System.out.println("
		// "+(String)jsDriver.executeScript("arguments[0].getBoundingClientRect();",
		// element));
		return (Boolean) jsDriver.executeScript(js, element);
	}
	
	public void scrollByYAxis(int yAxisPoint) {
		JavascriptExecutor js = (JavascriptExecutor) world.getWebDriver();
		String script = "javascript:window.scrollBy(0,"+yAxisPoint+")";
		js.executeScript(script);
	}
	
	/*
	 * This method can used to scroll to the top of static elements whose size is fixed in the page 
	 */
/*	public void scrollTillTopOfElement(WebElement scrollToElement) {
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		jsDriver.executeScript("javascript:window.scroll(0,0)");
		if (scrollToElement != null) {
			String script = "var viewportHeight = window.innerHeight;"
					+"var pos = arguments[0].getBoundingClientRect().top;"
					+"if(pos != viewportHeight/4){"
						+"window.scrollBy(0, pos-(viewportHeight/4));"
					+"}";
			jsDriver.executeScript(script, scrollToElement);
		} else {
			Assert.fail("The element doesn't exist.");
		}
	}*/
	public void scrollTillTopOfElement(WebElement scrollToElement) throws NumberFormatException, InterruptedException {
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		jsDriver.executeScript("javascript:window.scroll(0,0)");
		long pageHeightBefore = 0;
		long pageHeightAfter = 0;
		String offSetHeightScript = "return document.body.offsetHeight;";
		if (scrollToElement != null) {
			do {
				pageHeightBefore = (Long)jsDriver.executeScript(offSetHeightScript);
				String scrollToTopScript = "var viewportHeight = window.innerHeight;"
						+"var pos = arguments[0].getBoundingClientRect().top;"
						+"if(pos != viewportHeight/4){"
							+"window.scrollBy(0, pos-(viewportHeight/4));"
						+"}";
				jsDriver.executeScript(scrollToTopScript, scrollToElement);
				wait.wait("2");
				pageHeightAfter = (Long)jsDriver.executeScript(offSetHeightScript);
			} while (pageHeightBefore != pageHeightAfter);
		} else {
			Assert.fail("The element doesn't exist.");
		}
	}
	
	/*
	 * This method can be used to get the Y offset of a page having static elements and or dynamic 
	 * elements whose size increases dynamically in the page
	 */
	public int getYOffsetOfPage() throws NumberFormatException, InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) world.getWebDriver();
		long pageHeightBefore = 0;
		long pageHeightAfter = 0;
		String script = "return document.body.offsetHeight;";
		do {
			pageHeightBefore = (Long)js.executeScript(script);
			wait.wait("2");
			pageHeightAfter = (Long)js.executeScript(script);
		} while (pageHeightBefore != pageHeightAfter);
		return ((Long)js.executeScript("return window.pageYOffset;")).intValue();
	}
	
	public void scrollUntilInViewport(WebElement scrollToElement) throws NumberFormatException, InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) world.getWebDriver();
//		js.executeScript("javascript:window.scroll(0,0)");
		do {
			if (!isElementInViewport(scrollToElement)) {												
				if (!hasVerticalScrollingEnded()) {
					js.executeScript("javascript:window.scrollBy(0,300)");
					wait.wait("1");
				}
				else {
					Assert.fail("Page ended, element not found.");
					break;
				}
			}
			else {
				wait.wait("2");
				break;				
			}
		} while(true);
	}
	
	public boolean hasVerticalScrollingEnded() { 
		int yAxisScrollHeight = ((Number)((JavascriptExecutor) world.getWebDriver()).executeScript("return window.scrollY")).intValue();
		int heightSoFarScrolled = ((Number)((JavascriptExecutor) world.getWebDriver()).executeScript("return window.innerHeight")).intValue();
		int totalScrollableHeight = ((Number)((JavascriptExecutor) world.getWebDriver()).executeScript("return document.body.scrollHeight")).intValue();
		int totalScrolledHeight = yAxisScrollHeight + heightSoFarScrolled;
		return totalScrolledHeight >= totalScrollableHeight;
	}
	
	public void scrollDownElementIntoCQDialogViewPort(String elementName) throws ObjectNotFoundInORException {
		WebElement element = this.world.getWebElementFactory().getElement(elementName);
		scrollDownElementIntoCQDialogViewPort(element);
	}

	public void scrollDownElementIntoCQDialogViewPort(WebElement element) throws ObjectNotFoundInORException {
		WebElement divContentCQDialog = this.world.getWebElementFactory().getElement("CQDialogContent");
		scrollDownElementIntoViewPort(element, divContentCQDialog);
		/*
		 * JavascriptExecutor jsDriver = (JavascriptExecutor)driver; for(int i =
		 * 0;i<5;i++){ if(!isElementInViewport(element)){
		 * jsDriver.executeScript("arguments[0].scrollTop = arguments[1]",
		 * divContentCQDialog,300); }else{ break; } }
		 * jsDriver.executeScript("arguments[0].scrollTop = arguments[1]",
		 * divContentCQDialog,200);
		 */
	}

	public void scrollDownElementIntoViewPort(String toBeScrolledDownElementOrName,
			String scrollingWithinElementOrName) throws ObjectNotFoundInORException {
		WebElement elementToBeScrolled = this.world.getWebElementFactory().getElement(toBeScrolledDownElementOrName);
		WebElement elementWithInScrolled = this.world.getWebElementFactory().getElement(scrollingWithinElementOrName);
		scrollDownElementIntoViewPort(elementToBeScrolled, elementWithInScrolled);
	}

	public void scrollDownElementIntoViewPort(WebElement elementToBeScrolled, WebElement elementWithInScrolled) {
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", elementWithInScrolled, 0);
		int scrollHeight = ((Long) jsDriver.executeScript("return arguments[0].scrollHeight;", elementWithInScrolled))
				.intValue();
		int clientHeight = ((Long) jsDriver.executeScript("return arguments[0].clientHeight;", elementWithInScrolled))
				.intValue();
		int clientHeightCounter = 0;
		do {
			clientHeightCounter = clientHeightCounter + clientHeight;
			if (!isElementInViewport(elementToBeScrolled)) {
				jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", elementWithInScrolled,
						clientHeightCounter);
			} else {
				break;
			}
		} while (clientHeight < scrollHeight);
	}

	public void scrollUpElementIntoCQDialogViewPort(String elementName) throws ObjectNotFoundInORException {
		WebElement element = this.world.getWebElementFactory().getElement(elementName);
		scrollUpElementIntoCQDialogViewPort(element);
	}

	public void scrollUpElementIntoCQDialogViewPort(WebElement element) throws ObjectNotFoundInORException {
		WebElement divContentCQDialog = this.world.getWebElementFactory().getElement("CQDialogContent");
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		for (int i = 0; i < 5; i++) {
			if (!isElementInViewport(element)) {
				jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", divContentCQDialog, -200);
			} else {
				break;
			}
		}
	}

	public void scrollToTopOfCQDialogContent() throws ObjectNotFoundInORException {
		WebElement divContentCQDialog = this.world.getWebElementFactory().getElement("CQDialogContent");
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", divContentCQDialog, 0);
	}

	public void scrollToBottomOfCQDialogContent() throws ObjectNotFoundInORException {
		WebElement divContentCQDialog = this.world.getWebElementFactory().getElement("CQDialogContent");
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		Integer scrollHeight = (Integer) jsDriver.executeScript("arguments[0].scrollHeight;", divContentCQDialog);
		jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", divContentCQDialog, scrollHeight);
	}
	
	@And("^I scroll to the element \"([^\"]*)\"$")
	public void scrollToTopOfElement(String strElementORName) throws ObjectNotFoundInORException {
		WebElement element = this.world.getWebElementFactory().getElement(strElementORName);
		scrollToTopOfElement(element);
	}

	
	public void scrollToTopOfElement(WebElement element) {
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		//Integer scrollHeight = (Integer) jsDriver.executeScript("arguments[0].scrollHeight;", element);
		jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", element, 0);
	}

	@And("^I scroll published page to Top$")
	public void scrollPublishedPageToTop() {
		WebElement html = this.world.getWebElementFactory().getElement(By.tagName("html"), false);
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		//jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", html, 0);
		jsDriver.executeScript("window.scrollTo(0, 0)");
	}

	@And("^(?:I|i) scroll published page to Bottom$")
	public void scrollPublishedPageToBottom() throws Throwable {
		WebElement html = this.world.getWebElementFactory().getElement(By.tagName("html"), false);
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		int scrollHeight = ((Long) jsDriver.executeScript("return arguments[0].scrollHeight;", html))
				.intValue();
//		jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", html, scrollHeight);
		System.out.println("page scroll height is " + scrollHeight);
		jsDriver.executeScript("arguments[0].scrollTo(0,arguments[1])", html, scrollHeight);
		Thread.sleep(5000);
		jsDriver.executeScript("arguments[0].scrollTo(0,arguments[1])", html, scrollHeight);


//		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
//		jsDriver.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		Thread.sleep(2000);
	}
	
	@And("^(?:I|i) scroll published page up by (\\d+)$")
	public void scrollPublishedPageUpByPixels(int pixels) {
		WebElement html = this.world.getWebElementFactory().getElement(By.tagName("html"), false);
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		int scrollTop = ((Long) jsDriver.executeScript("return arguments[0].scrollTop;", html))
				.intValue();

		jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", html, scrollTop-pixels);

	}

	@And("^(?:I|i) scroll published page down by (\\d+)$")
	public void scrollPublishedPageDownByPixels(int pixels)  {
		try {
			WebElement html = this.world.getWebElementFactory().getElement(By.tagName("html"), false);
			JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
			int scrollTop = ((Long) jsDriver.executeScript("return arguments[0].scrollTop;", html))
					.intValue();
			jsDriver.executeScript("arguments[0].scrollTop = arguments[1]", html, scrollTop + pixels);
//			JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
//			jsDriver.executeScript("window.scrollBy(0,arguments[0])",pixels);
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@And("(?:I|i) expect page is scrolled to top$")
	public void verifyPageIsAtTop() {
		JavascriptExecutor jsDriver = (JavascriptExecutor) world.getWebDriver();
		int top = ((Long)jsDriver.executeScript("return window.scrollY")).intValue();
		Assert.assertTrue(top == 0, "page is not on the top");

	}

}
