package steps.generic.keywords;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.And;

public class FrameHandling{
	World world;
    public FrameHandling(World world ){
        this.world = world;
    }
	
	
	@And("^I switch to frame by index (.*)$")
	public void switchToFrame(int frameNumber) {
		
		 try {
			world.getWebDriver().switchTo().frame(frameNumber);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@And("^I switch to frame by name (.*)$")
	public void switchToFrame(String frameName) {
		
		 try {
			world.getWebDriver().switchTo().frame(frameName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@And("^I switch to frame by object (.*)$")
	public void switchToFrameByWebelement(String frameName) {		
		 try {
			 WebElement frameElement = this.world.getWebElementFactory().getElement(frameName, false);
			 world.getWebDriver().switchTo().frame(frameElement);
			 System.out.println("Successfully switched to frame :: " + frameName);
			 //System.out.println(world.getBrowser().getPageSource());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	public void switchToFrame(By property) {		
		 try {
			 WebElement frameElement = this.world.getWebElementFactory().getElement(property, false);
			 world.getWebDriver().switchTo().frame(frameElement);
			 System.out.println("Successfully switched to frame :: " + property);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	public void switchToFrame(WebElement frameElement) {		
		 try {
			 if (frameElement != null) {
				 world.getWebDriver().switchTo().frame(frameElement);
				 System.out.println("Successfully switched to frame :: " + frameElement);
			 } else
				 throw new NullPointerException();
		} catch (NullPointerException e) {
			Assert.fail("Frame doesn't exist.");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@And("^I switch to default$")
	public void switchToDefault() {		
		 world.getWebDriver().switchTo().defaultContent();
	}
		
}
