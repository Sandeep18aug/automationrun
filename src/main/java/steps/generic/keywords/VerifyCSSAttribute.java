package steps.generic.keywords;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import cucumber.api.java.en.And;

public class VerifyCSSAttribute {
	World world;

	public VerifyCSSAttribute(World world) {
		this.world = world;
	}

	@And("^I expect to see text as (.*) for (.*) attribute of \"([^\"]*)\"$")
	public void verifyCSSAttribute(String sData, String sAttribute, String sObject) throws IOException {
		try {
			WebElement Object;
			Object = this.world.getWebElementFactory().getElement( sObject);
			// Assert.assertTrue(Object.getAttribute(sAttribute).contentEquals(sData);
			String actualValue = Object.getAttribute(sAttribute);
			Assert.assertTrue(actualValue.equals(sData),
					"Mistmatch : Expected - " + sData + " and Actual - " + actualValue);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	/*
	 * @And("^I expect \"([^\"]*)\" attribute of \"([^\"]*)\" object is not having string \"([^\"]*)\"$"
	 * ) public void verifyCSSAttributeNotContains(String sAttribute, String
	 * sObject, String sData) throws IOException{ WebElement Object; try {
	 * this.world.getWebElementFactory().getElement(world.getBrowser(),
	 * world.getFeatureName(), sObject).isDisplayed(); } catch (Exception
	 * e) { ((JavascriptExecutor)
	 * world.getBrowser()).executeScript("sauce:job-result=failed");
	 * world.getBrowser().quit(); } Object =
	 * this.world.getWebElementFactory().getElement(world.getBrowser(),
	 * world.getFeatureName(), sObject);
	 * Assert.assertFalse(Object.getCssValue(sAttribute).contains(sData)); }
	 */

	@And("^I expect \"([^\"]*)\" attribute of \"([^\"]*)\" object is not having string \"([^\"]*)\"$")
	public void verifyAttributeNotContains(String sAttribute, String sObject, String sData) throws IOException {
		WebElement Object = null;
		try {
			Object = this.world.getWebElementFactory().getElement( sObject);
			String actualValue = Object.getAttribute(sAttribute);
			Assert.assertFalse(actualValue.contains(sData),
					"Mistmatch : Expected - " + sData + " and Actual - " + actualValue);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
			}
	}
	
	@And("^I expect \"([^\"]*)\" attribute of \"([^\"]*)\" object is having string \"([^\"]*)\"$")
	public void verifyAttributeContains(String sAttribute, String sObject, String sData) throws IOException {
		WebElement Object = null;
		try {
			Object = this.world.getWebElementFactory().getElement( sObject);
			String actualValue = Object.getAttribute(sAttribute);
			Assert.assertTrue(actualValue.contains(sData),
					"Mistmatch : Expected - " + sData + " and Actual - " + actualValue);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@And("^I expect \"([^\"]*)\" css attribute of \"([^\"]*)\" object is not having string \"([^\"]*)\"$")
	public void verifyCSSAttributeNotContains(String sAttribute, String sObject, String sData) throws IOException {
		WebElement Object = null;
		try {
			Object = this.world.getWebElementFactory().getElement( sObject);
			String actualValue = Object.getCssValue(sAttribute);
			Assert.assertFalse(actualValue.contains(sData),
					"Mistmatch : Expected - " + sData + " and Actual - " + actualValue);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@And("^I expect \"([^\"]*)\" css attribute of \"([^\"]*)\" object is having string \"([^\"]*)\"$")
	public void verifyCSSAttributeContains(String sAttribute, String sObject, String sData) throws IOException {
		WebElement Object = null;
		try {
			Object = this.world.getWebElementFactory().getElement(sObject);
			String actualValue = Object.getCssValue(sAttribute);
			Assert.assertTrue(actualValue.contains(sData),
					"Mistmatch : Expected - " + sData + " and Actual - " + actualValue);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
}
