package framework.shared;

public class StringUtils {
	public static boolean compareStrings(String str1, String str2) {
		return str1 != null && str2 != null && normalizeLineEnds(str1).equals(normalizeLineEnds(str2));
	}

	public static boolean compareStringsIgnoreCase(String str1, String str2) {
		return str1 != null && str2 != null && normalizeLineEnds(str1).equalsIgnoreCase(normalizeLineEnds(str2));
	}

	private static String normalizeLineEnds(String s) {
		return s.replace("\r\n", "\n").replace('\r', '\n');
	}
	
	public static String removeHTMLTag (String tag, String s) {
		s = s.replaceAll("<(?i)" + tag + ">", "").trim();
		s = s.replaceAll("</(?i)" + tag + ">", "").trim();
		return s;
	}
	public static String removeSpecialChar(String s) {
		s = s.replaceAll(s,"");
		return s;
	}
}
