package framework.shared;

import java.io.File;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.junit.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * @author agorka
 *
 */
public class TestData {

	private HashMap<String, String> testData = new HashMap<>();

	public TestData(String testDataFileName, String testDataConfigName)
	{
		try {
			File inputFile = new File(FrameworkConstants.TEST_DATA_FOLDER + testDataFileName
					+ (testDataFileName.toUpperCase().endsWith(".XML") ? "" : ".xml"));
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			XPath xpath = XPathFactory.newInstance().newXPath();
			NodeList commonNodeChildren = (NodeList) xpath.evaluate("//common/*",
					doc, XPathConstants.NODESET);
			for (int temp = 0; temp < commonNodeChildren.getLength(); temp++) {
				Element eElement = (Element) commonNodeChildren.item(temp);
				System.out.println(eElement.getNodeName() + " : " + eElement.getTextContent());
				this.testData.put(eElement.getNodeName(), eElement.getTextContent());
			}
			NodeList configNodeChildren = (NodeList) xpath.evaluate("//config[@TestData='" + testDataConfigName + "']/*",
					doc, XPathConstants.NODESET);
			if(configNodeChildren.getLength() == 0) {
				Assert.fail("TestData absent :: " + testDataConfigName + " configuration is missing.");
			}
			for (int temp = 0; temp < configNodeChildren.getLength(); temp++) {
				Element eElement = (Element) configNodeChildren.item(temp);
				System.out.println(eElement.getNodeName() + " : " + eElement.getTextContent());
				this.testData.put(eElement.getNodeName(), eElement.getTextContent());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
//	public String get(String testDataKey)
//	{ 
//		if(testData.containsKey(testDataKey)) {
//			String value =  this.testData.get(testDataKey).trim();
//			if(value.contains("$(brand)"))
//				value = value.replace("$(brand)", FrameworkConstants.Environment.getBrand());
//			if(value.contains("$(locale)"))
//				value = value.replace("$(locale)", FrameworkConstants.Environment.getAuthorlocale());
//				
//			return value;
//		}
//		else
//			return null;	
//	}
//	
//	public String getWithOutTrim(String testDataKey)
//	{
//		String value =  this.testData.get(testDataKey);
//		if(value.contains("$(brand)"))
//			value = value.replace("$(brand)", FrameworkConstants.Environment.getBrand());
//		if(value.contains("$(locale)"))
//			value = value.replace("$(locale)", FrameworkConstants.Environment.getAuthorlocale());
//		return value;
//	}

	
/*	public String get(String testDataKey)
	{ 
			if(testData.containsKey(testDataKey)) 
			return  this.testData.get(testDataKey).trim();
		else
			return null;	
	}
	
	public String getWithOutTrim(String testDataKey)
	{
		if(testData.containsKey(testDataKey)) 
			return  this.testData.get(testDataKey);
		else
			return null;
	}*/
	
	public boolean hasKey(String testDataKey)
	{
		return testData.containsKey(testDataKey);
	}
	
	public HashMap<String, String> getTestDataAsMap()
	{
		return testData;
	}
}
