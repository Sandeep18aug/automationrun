package framework.shared;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlHandler {

	private String url = "";

	public UrlHandler() {
	};

	public UrlHandler(String initialURL) {
		this.url = initialURL.endsWith("/") ? initialURL : initialURL + "/";
	}

	public UrlHandler append(String urlPart) {
		if (urlPart.toLowerCase().startsWith("http")) {
			this.url = urlPart.endsWith("/") ? urlPart : urlPart + "/";
		} else if (!urlPart.equals("/") && !urlPart.endsWith(".html")) {
			this.url = this.url
					+ (urlPart.startsWith("/") && urlPart.length() > 1 ? urlPart.substring(1) : urlPart) + "/";
		}else if (urlPart.endsWith(".html")) {
			this.url = this.url
					+ (urlPart.startsWith("/") && urlPart.length() > 1 ? urlPart.substring(1) : urlPart);
		}
		return this;
	}

	public UrlHandler appendExtension(String extension) {
		this.url = this.url.concat(extension);
		return this;
	}

	public String getUrl() {
		return this.url;
	}

	public String convertToEditorMode(String url) {
		Pattern pattern = Pattern.compile("siteadmin(\\.html#|#)", Pattern.CASE_INSENSITIVE);
		return pattern.matcher(url).replaceAll(FrameworkConstants.EDITOR_DOT_HTML_APPEND);
	}

	public String convertToSiteAdminMode(String url) {
		Pattern pattern = Pattern.compile("editor(\\.html#|#)", Pattern.CASE_INSENSITIVE);
		return pattern.matcher(url).replaceAll(FrameworkConstants.SITE_ADMIN_APPEND);
	}

	public String convertToAuthEnabledPublishedUrl(String sUrl) throws MalformedURLException {
		URL url = new URL(sUrl);
		if (url.getUserInfo() != null)
			return sUrl;
		Pattern pattern = Pattern.compile("^http[s]?://");
		Matcher m = pattern.matcher(sUrl);
		m.find();
		String initial = (m.group());
		sUrl = m.replaceAll(initial + FrameworkConstants.Environment.getPublisherAuthUserID() + ":"
				+ FrameworkConstants.Environment.getPublisherAuthPassword() + "@");
		System.out.println(sUrl);
		return sUrl;
	}

	public UrlHandler convertToAuthEnabledPublishedUrl() throws MalformedURLException {
		URL url = new URL(this.url);
		if (url.getUserInfo() != null)
			return this;
		Pattern pattern = Pattern.compile("^http[s]?://");
		Matcher m = pattern.matcher(this.url);
		m.find();
		String initial = (m.group());
		this.url = m.replaceAll(initial + FrameworkConstants.Environment.getPublisherAuthUserID() + ":"
				+ FrameworkConstants.Environment.getPublisherAuthPassword() + "@");
		System.out.println(this.url);
		return this;
	}

	public String removeAuthInfoFromPublishedUrl(String sUrl) throws MalformedURLException {
		URL url = new URL(sUrl);
		if (url.getUserInfo() != null) {
			String userInfo = url.getUserInfo();
			sUrl = sUrl.replace(userInfo + "@", "");
		}
		return sUrl;
	}

	public UrlHandler removeAuthInfoFromPublishedUrl() throws MalformedURLException {
		URL url = new URL(this.url);
		if (url.getUserInfo() != null) {
			String userInfo = url.getUserInfo();
			this.url = this.url.replace(userInfo + "@", "");
		}
		return this;
	}

	public String getHost() throws MalformedURLException {
		String host = new URL(this.url).getHost();
		int l = host.lastIndexOf("@");
		host = host.substring(l + 1);
		return host;
	}

	public String getProtocol() throws MalformedURLException {
		URL url = new URL(this.url);
		String protocol = url.getProtocol();
		return protocol;
	}

	public String getBaseUrl() throws MalformedURLException {
		return (this.url == "") ? this.url : getProtocol() + "://" + getHost();
	}

	public UrlHandler appendDotHtml() throws Exception {
		if (this.url == "")
			throw new Exception("UrlHandler object should not have empty URL");
		else if (!this.url.toLowerCase().endsWith(".html")) {
			if (this.url.endsWith("/")) {
				this.url = this.url.substring(0, this.url.length() - 1) + ".html";
			} else {
				this.url = this.url + ".html";
			}
		}
		return this;
	}
}
