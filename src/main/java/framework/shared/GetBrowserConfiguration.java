package framework.shared;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GetBrowserConfiguration {
	String sRun, sBrowser, sVersion, sDestination;
	
	public void getBrowserData(String ThreadNumber){
       
		try {	
	        File inputFile = new File(FrameworkConstants.SRC_MAIN_RESOURCES + "config/BrowserConfig.xml");
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        Document doc = dBuilder.parse(inputFile);
	        doc.getDocumentElement().normalize();
//	        System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
	        NodeList nList = doc.getElementsByTagName("config");
//	        System.out.println("----------------------------");
	        for (int temp = 0; temp < nList.getLength(); temp++) {
	           Node nNode = nList.item(temp);
//	           System.out.println("\nCurrent Element :" + nNode.getNodeName());
	           Element eElement = (Element) nNode;
//		       System.out.println("ConfigName : " + eElement.getAttribute("TestData"));
	           if(eElement.getAttribute("Thread").equalsIgnoreCase(ThreadNumber)){
	        	   sRun=eElement.getElementsByTagName("Run").item(0).getTextContent();
		    	   System.out.println("Run :: " + sRun);
		    	   
		    	   sBrowser=eElement.getElementsByTagName("Browser").item(0).getTextContent();
		    	   System.out.println("Browser :: " + sBrowser);
		    	   
		    	   sVersion=eElement.getElementsByTagName("Version").item(0).getTextContent();
		    	   //System.out.println("Version :: " + sVersion);
		    	   
		    	   sDestination=eElement.getElementsByTagName("Destination").item(0).getTextContent();
		    	   System.out.println("Destination :: " + sDestination);
		    	   
		    	   
	           }	   
		       
		    }
	     } catch (Exception e) {
	        e.printStackTrace();
	     }
	
	
	}

	public String getRunStatus() {
		return sRun;
	}
	
	public String getBrowserName() {
		return sBrowser;
	}
	public String getBrowserVersion() {
		return sVersion;
	}
	
	public String getDestination() {
		return sDestination;
	}
}
