package framework.shared;

import java.util.Random;

public class RandomUtil {
	
	
	public static int getRandomNumber(int min, int max) {

		if (min == max) {
			return min;
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

}
