package framework.shared;

import java.io.IOException;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.CommandLineArgs;

import com.google.common.base.Strings;

import framework.configuration.Environment;
import framework.configuration.EnvironmentDeserializer;
import framework.exceptions.EnvironmentNotSetupException;

public class FrameworkConstants {
       public static final String AEM_SUPERADMIN_USER_ID = "autoadmin";
       public static final String AEM_SUPERADMIN_PASSWORD_ID = "auto$admin89";
       public static final String SRC_MAIN_RESOURCES = "src/main/resources/";
       public static final String TEST_DATA_FOLDER = "src/main/resources/TestData/";
       public static final String TEST_CONFIG_FOLDER = "src/main/resources/config/";
       public static final String OBJECT_REPOSITORY_FOLDER = "src/main/resources/ObjectRepo/";
       public static final String DRIVERS_FOLDER = "src/main/resources/drivers/";
       public static final String REPORTS_FOLDER = "src/main/resources/Report/";
       public static final String CUCUMBER_HTML_REPORTS_FOLDER = "test-output/";

       public static final String TEST_BASEURL_MAPPING_FILE = TEST_DATA_FOLDER + "baseurlmapping.properties";

       public static final String ENVIRONMENT_JSON_FILE_PATH = TEST_CONFIG_FOLDER + "Environment.JSON";
       public static final String USERS_JSON_FILE_PATH = "Users.JSON";

       public static final String SITE_ADMIN_APPEND = "siteadmin#";
       public static final String EDITOR_DOT_HTML_APPEND = "editor.html";
       public static final String PROXY_IP = "172.31.43.13";
       public static final String PROXY_PORT = "3128";
       public static final String BRAND_NAME = "Whitelabel";
       public static final int SMALL_WAIT = 15;
       public static final int MEDIUM_WAIT = SMALL_WAIT * 3;
       public static final int LARGE_WAIT = SMALL_WAIT * 5;
       public static final int VERY_LARGE_WAIT = SMALL_WAIT * 8;
       public static Environment Environment = null;
       public static final String OBJECT_DYNAMICVALUE_PLACEHOLDER = "<<<<>>>>";
       public static final String CRX_SUPERADMIN_USER_ID = "autoadmin";
       public static final String CRX_SUPERADMIN_PASSWORD_ID = "auto$admin89";
       public static boolean SITE_ADMIN_APIEXECUTION_FLAG = false;
       public static boolean PROXY_ENABLED = true;
       public static final String PROXY_FILE = TEST_CONFIG_FOLDER + "proxy.properties";
       private static String EXECUTION_TIME_STAMP = null;
       public static String EXECUTION = "";
       public static String GRID_IP = "";
       public static String GRID_PORT = "";
       private static final String D2_COMMON_OR_NAME = "d2-common-or.properties";
       private static final String SK_COMMON_OR_NAME = "sk-common-or.properties";
       public static String COMMON_OR_NAME = D2_COMMON_OR_NAME;

       static {

              try {
                     String cucumberTags = System.getProperty("testtags");
                     if (!Strings.isNullOrEmpty(cucumberTags)) {
                           System.out.println("cucumber tags passed through cli --> " + System.getProperty("testtags"));
                     }
                     String cliEnvironmentVal = System.getProperty("env");
                     if (Strings.isNullOrEmpty(cliEnvironmentVal)) {
                           Environment = new EnvironmentDeserializer().getEnvironment();
                     } else {
                           System.out.println("Setting Environment :: " + cliEnvironmentVal);
                           Environment = new EnvironmentDeserializer().getEnvironment(cliEnvironmentVal);
                     }
                     if (Strings.isNullOrEmpty(Environment.getPublisherAuthUserID())
                                  && Strings.isNullOrEmpty(Environment.getPublisherAuthPassword())) {
                           PROXY_ENABLED = false;
                     }
                     String execution = System.getProperty("execution");
                     if(CommandLineVariable.isPresent(execution)) {
                           EXECUTION = execution;
                     }
                     String platform = System.getProperty("platform");
                     if(!(Strings.isNullOrEmpty(platform)) && platform.equalsIgnoreCase("starterkit")) {
                           COMMON_OR_NAME = SK_COMMON_OR_NAME;
                     }

              } catch (EnvironmentNotSetupException e) {
                     e.printStackTrace();
                     System.exit(0);
              } catch (IOException e) {
                     e.printStackTrace();
                     System.exit(0);
              } catch (Exception e) {
                     // TODO Auto-generated catch block
                     e.printStackTrace();
                     System.exit(1);
              }

              EXECUTION_TIME_STAMP = TimeUtil.getCurrentDate("yyyy-MM-dd-HH-mm-ss-SSS");

       }

       public static String getExecutionTimeStamp() {
              return EXECUTION_TIME_STAMP;
       }
}

