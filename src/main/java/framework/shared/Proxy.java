package framework.shared;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.net.InetAddresses;

import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.filters.RequestFilter;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;

public class Proxy {
	protected static final Logger logger = LoggerFactory.getLogger(Proxy.class);
	private static BrowserMobProxy proxy = null;
	// private static Proxy single_Instance = null;

	/*
	 * private Proxy() { proxy = new BrowserMobProxyServer();
	 * proxy.setTrustAllServers(true); proxy.setConnectTimeout(30,
	 * TimeUnit.SECONDS); proxy.addRequestFilter(new RequestFilter() {
	 * 
	 * @Override public HttpResponse filterRequest(HttpRequest request,
	 * HttpMessageContents contents, HttpMessageInfo messageInfo) { final String
	 * login = "fp" + ":" + "dove"; final String base64login = new
	 * String(Base64.encodeBase64(login.getBytes()));
	 * request.headers().add("Authorization", "Basic " + base64login); return
	 * null; } }); proxy.start(0); }
	 */
	public static void start(String userId, String password) {
		if (proxy == null) {
			proxy = new BrowserMobProxyServer();
			proxy.setTrustAllServers(true);
			proxy.setConnectTimeout(60, TimeUnit.SECONDS);
			proxy.addRequestFilter(new RequestFilter() {
				@Override
				public HttpResponse filterRequest(HttpRequest request, HttpMessageContents contents,
						HttpMessageInfo messageInfo) {
					final String login = userId + ":" + password;
					final String base64login = new String(Base64.encodeBase64(login.getBytes()));
					request.headers().add("Authorization", "Basic " + base64login);
					return null;
				}
			});
	        
			InetAddress localhost = getInetAddress();
	        if(localhost != null) {
	        	proxy.start(getAvailablePort(), localhost);
	        }else {
				proxy.start(0);	        	
	        }
			String message = "Proxy server is started at " + proxy.getClientBindAddress().getHostAddress() + ":"
					+ proxy.getPort();
			System.out.println(message);
			logger.info(message);
		}
	}

	private static int getAvailablePort() {
		ServerSocket socket0 = null;
		int port = 55555;
		try{
            socket0 = new ServerSocket(0);
            port = socket0.getLocalPort();
        }catch(IOException e){
            System.out.println(e.toString());
        }finally {
        	try {
        	socket0.close();}catch(Exception ex) {}
        }
		return port;
	}

	private static InetAddress getInetAddress() {
		InetAddress localhost = null;
		try {
			localhost = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			localhost = null;
		}
		return localhost;
	}
	public static BrowserMobProxy getInstance() {
		return proxy;
	}

	public static void stop() {
		if (null != proxy && proxy.isStarted()) {
			proxy.stop();
			proxy = null;
			System.out.println("Proxy is closed");
		}
	}
	
}
