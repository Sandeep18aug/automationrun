package framework.shared;
//package shared;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.ArrayList;
//import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
//import org.apache.poi.xssf.usermodel.XSSFRow;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//
//public class GenerateRunner {
//	static ArrayList<String> arrTestCaseID = new ArrayList<String>();
//	static ArrayList<String> arrPageName = new ArrayList<String>();
//	static ArrayList<String> arrDescription = new ArrayList<String>();
//	static ArrayList<String> arrCategory = new ArrayList<String>();
//	static ArrayList<String> arrRun = new ArrayList<String>();
//	static ArrayList<String> arrLocal = new ArrayList<String>();
//	static ArrayList<String> arrAuthorThreadOne = new ArrayList<String>();
//	static ArrayList<String> arrAuthorThreadTwo = new ArrayList<String>();
//	static ArrayList<String> arrAuthorThreadThree = new ArrayList<String>();
//	static ArrayList<String> arrAuthorThreadFour = new ArrayList<String>();
//	static ArrayList<String> arrAuthorThreadFive = new ArrayList<String>();
//	static ArrayList<String> arrAuthorThreadSix = new ArrayList<String>();
//	static ArrayList<String> arrAuthorThreadSeven = new ArrayList<String>();
//	static ArrayList<String> arrAuthorThreadEight = new ArrayList<String>();
//	static int iTCCount = 0;
//	static String sAuthorThreadOneTag, sAuthorThreadTwoTag,sAuthorThreadThreeTag,sAuthorThreadFourTag,
//	sAuthorThreadFiveTag,sAuthorThreadSixTag,sAuthorThreadSevenTag,sAuthorThreadEightTag;
//	
//	static ArrayList<String> arrPublisherThreadOne = new ArrayList<String>();
//	static ArrayList<String> arrPublisherThreadTwo = new ArrayList<String>();
//	static ArrayList<String> arrPublisherThreadThree = new ArrayList<String>();
//	static ArrayList<String> arrPublisherThreadFour = new ArrayList<String>();
//	static ArrayList<String> arrPublisherThreadFive = new ArrayList<String>();
//	static ArrayList<String> arrPublisherThreadSix = new ArrayList<String>();
//	static ArrayList<String> arrPublisherThreadSeven = new ArrayList<String>();
//	static ArrayList<String> arrPublisherThreadEight = new ArrayList<String>();
//	
//	static String sPublisherThreadOneTag, sPublisherThreadTwoTag,sPublisherThreadThreeTag,sPublisherThreadFourTag,
//	sPublisherThreadFiveTag,sPublisherThreadSixTag,sPublisherThreadSevenTag,sPublisherThreadEightTag;
//
//	public static void CreateRunner(String sRunnerName, String sThreadTag) {
//		try {
//			String sPackage = "package runner;";
//			String sImport = "import cucumber.api.CucumberOptions;import cucumber.api.testng.AbstractTestNGCucumberTests;";
//			String sCucumberOption = "@CucumberOptions(features={\"src/test/resources/FeatureFiles/AEM_Component/Quote.feature\"},"
//					+ "glue={\"basic_keyword\", \"business_keyword\"}," + "tags={\"@AuthoringOnly\"," + "\""+sThreadTag+ "\"" + "},"
//					+ "plugin={\"json:src/test/resources/Report/Automated_Test_Execution.json\"})" + "public class "
//					+ sRunnerName + " extends AbstractTestNGCucumberTests{}";
//			String content = sPackage + sImport + sCucumberOption;
//			File file = new File("src/test/java/runner/" + sRunnerName + ".java");
//			FileWriter fw = new FileWriter(file.getAbsoluteFile());
//			BufferedWriter bw = new BufferedWriter(fw);
//			bw.write(content);
//			bw.close();
//			System.out.println("Done");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	public static void ReadExcel() throws IOException, InvalidFormatException {
//		FileInputStream file = null;
//		file = new FileInputStream(new File("src/test/resources/TestData/Controller.xlsx"));
//		XSSFWorkbook workbook;
//		workbook = new XSSFWorkbook(file);
//		XSSFSheet sheet = workbook.getSheet("Author"); 
//		int iRowCount = sheet.getLastRowNum();
//		int iColCount = sheet.getRow(0).getLastCellNum();
////		System.out.println("Row Count:: " + iRowCount);
////		System.out.println("Column Count :: " + iColCount);
//
//		for (int iRowLoop = 0; iRowLoop <= iRowCount; iRowLoop++) {
//			XSSFRow sRow = sheet.getRow(iRowLoop);
//			arrTestCaseID.add(iTCCount, sRow.getCell(1).getStringCellValue());
////			System.out.println("arrTestCaseID :: " + arrTestCaseID.get(iRowLoop));
//			arrPageName.add(iTCCount, sRow.getCell(2).getStringCellValue());
////			System.out.println("arrPageName :: " + arrPageName.get(iRowLoop));
//			arrDescription.add(iTCCount, sRow.getCell(3).getStringCellValue());
////			System.out.println("arrDescription :: " + arrDescription.get(iRowLoop));
//			arrCategory.add(iTCCount, sRow.getCell(4).getStringCellValue());
////			System.out.println("arrCategory :: " + arrCategory.get(iRowLoop));
//			arrRun.add(iTCCount, sRow.getCell(5).getStringCellValue());
////			System.out.println("arrRun :: " + arrRun.get(iRowLoop));
//			arrLocal.add(iTCCount, sRow.getCell(6).getStringCellValue());
////			System.out.println("arrLocal :: " + arrLocal.get(iRowLoop));
//			arrAuthorThreadOne.add(iTCCount, sRow.getCell(7).getStringCellValue());
////			System.out.println("arrThreadOne :: " + arrThreadOne.get(iRowLoop));
//			arrAuthorThreadTwo.add(iTCCount, sRow.getCell(8).getStringCellValue());
////			System.out.println("arrThreadTwo :: " + arrThreadTwo.get(iRowLoop));
//			arrAuthorThreadThree.add(iTCCount, sRow.getCell(9).getStringCellValue());
////			System.out.println("arrThreadThree :: " + arrThreadThree.get(iRowLoop));
//			arrAuthorThreadFour.add(iTCCount, sRow.getCell(10).getStringCellValue());
////			System.out.println("arrThreadFour :: " + arrThreadFour.get(iRowLoop));
//			arrAuthorThreadFive.add(iTCCount, sRow.getCell(11).getStringCellValue());
////			System.out.println("arrThreadFive :: " + arrThreadFive.get(iRowLoop));
//			arrAuthorThreadSix.add(iTCCount, sRow.getCell(12).getStringCellValue());
////			System.out.println("arrThreadSix :: " + arrThreadSix.get(iRowLoop));
//			arrAuthorThreadSeven.add(iTCCount, sRow.getCell(13).getStringCellValue());
////			System.out.println("arrThreadSeven :: " + arrThreadSeven.get(iRowLoop));
//			arrAuthorThreadEight.add(iTCCount, sRow.getCell(14).getStringCellValue());
////			System.out.println("arrThreadEight :: " + arrThreadEight.get(iRowLoop));
//			iTCCount = iTCCount + 1;
//		}
//	}
//	
//	public static void ComposeTag() throws InvalidFormatException, IOException{
//		ReadExcel();
//		sAuthorThreadOneTag = "";
//		sAuthorThreadTwoTag = "";
//		sAuthorThreadThreeTag = "";
//		sAuthorThreadFourTag = "";
//		sAuthorThreadFiveTag = "";
//		sAuthorThreadSixTag = "";
//		sAuthorThreadSevenTag = "";
//		sAuthorThreadEightTag = "";
//		for(int iTagLoop=1; iTagLoop<arrTestCaseID.size(); iTagLoop++){
//			if(!(arrAuthorThreadOne.get(iTagLoop).isEmpty()) && arrCategory.get(iTagLoop).equalsIgnoreCase("Author")){
//				sAuthorThreadOneTag = sAuthorThreadOneTag + "@" + arrTestCaseID.get(iTagLoop) + ", ";
//			}
//			if(!(arrAuthorThreadTwo.get(iTagLoop).isEmpty()) && arrCategory.get(iTagLoop).equalsIgnoreCase("Author")){
//				sAuthorThreadTwoTag = sAuthorThreadTwoTag + "@" + arrTestCaseID.get(iTagLoop) + ", ";
//			}
//			if(!(arrAuthorThreadThree.get(iTagLoop).isEmpty()) && arrCategory.get(iTagLoop).equalsIgnoreCase("Author")){
//				sAuthorThreadThreeTag = sAuthorThreadThreeTag + "@" + arrTestCaseID.get(iTagLoop) + ", ";
//			}
//			if(!(arrAuthorThreadFour.get(iTagLoop).isEmpty()) && arrCategory.get(iTagLoop).equalsIgnoreCase("Author")){
//				sAuthorThreadFourTag = sAuthorThreadFourTag + "@" + arrTestCaseID.get(iTagLoop) + ", ";
//			}
//			if(!(arrAuthorThreadFive.get(iTagLoop).isEmpty()) && arrCategory.get(iTagLoop).equalsIgnoreCase("Author")){
//				sAuthorThreadFiveTag = sAuthorThreadFiveTag + "@" + arrTestCaseID.get(iTagLoop) + ", ";
//			}
//			if(!(arrAuthorThreadSix.get(iTagLoop).isEmpty()) && arrCategory.get(iTagLoop).equalsIgnoreCase("Author")){
//				sAuthorThreadSixTag = sAuthorThreadSixTag + "@" + arrTestCaseID.get(iTagLoop) + ", ";
//			}
//			if(!(arrAuthorThreadSeven.get(iTagLoop).isEmpty()) && arrCategory.get(iTagLoop).equalsIgnoreCase("Author")){
//				sAuthorThreadSevenTag = sAuthorThreadSevenTag + "@" + arrTestCaseID.get(iTagLoop) + ", ";
//			}
//			if(!(arrAuthorThreadEight.get(iTagLoop).isEmpty()) && arrCategory.get(iTagLoop).equalsIgnoreCase("Author")){
//				sAuthorThreadEightTag = sAuthorThreadEightTag + "@" + arrTestCaseID.get(iTagLoop) + ", ";
//			}
//		}
//		if(!(arrAuthorThreadOne.size()<1)){
//			CreateRunner("AuthorThreadOneRunner", sAuthorThreadOneTag);
//		}
//		if(!(arrAuthorThreadTwo.size()<1)){
//			CreateRunner("AuthorThreadTwoRunner", sAuthorThreadTwoTag);
//		}
//		if(!(arrAuthorThreadThree.size()<1)){
//			CreateRunner("AuthorThreadThreeRunner", sAuthorThreadThreeTag);
//		}
//		if(!(arrAuthorThreadFour.size()<1)){
//			CreateRunner("AuthorThreadFourRunner", sAuthorThreadFourTag);
//		}
//		if(!(arrAuthorThreadFive.size()<1)){
//			CreateRunner("AuthorThreadFiveRunner", sAuthorThreadFiveTag);
//		}
//		if(!(arrAuthorThreadSix.size()<1)){
//			CreateRunner("AuthorThreadSixRunner", sAuthorThreadSixTag);
//		}
//		if(!(arrAuthorThreadSeven.size()<1)){
//			CreateRunner("AuthorThreadSevenRunner", sAuthorThreadSevenTag);
//		}
//		if(!(arrAuthorThreadEight.size()<1)){
//			CreateRunner("AuthorThreadEightRunner", sAuthorThreadEightTag);
//		}
//	}
//
//	public static void main(String[] args) throws IOException, InvalidFormatException {
////		ReadExcel();
//		// CreateRunner("RajRunner");
//		ComposeTag();
//	}
//}
