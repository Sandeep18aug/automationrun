package framework.shared;

import com.google.common.base.Strings;

public class CommandLineVariable {

	public static Boolean isPresent(String parameterName) {
		return !Strings.isNullOrEmpty(parameterName);
	}
	
	public static String getValue(String parameterName) {
		return isPresent(parameterName)? System.getProperty(parameterName):null;
	}
}
