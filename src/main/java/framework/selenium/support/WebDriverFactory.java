package framework.selenium.support;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Level;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import framework.shared.FrameworkConstants;
import framework.shared.Proxy;
import framework.shared.TimeUtil;
import net.lightbody.bmp.client.ClientUtil;

public class WebDriverFactory {
	protected static final Logger logger = LoggerFactory.getLogger(WebDriverFactory.class);

	public static WebDriver getChromeDriver(boolean proxyEnabled) throws Exception {
		WebDriver driver = null;
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY,
					FrameworkConstants.DRIVERS_FOLDER + "chromedriver.exe");
		}
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-extensions");
		options.addArguments("--start-maximized");
		options.addArguments("--disable-infobars");		
		// options.addArguments("user-data-dir=" + "/Users/avigorka/Library/Application
		// Support/Google/Chrome/Default");
		options.setAcceptInsecureCerts(true);
		options.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
		options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		options.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors"));
		options.setCapability(CapabilityType.LOGGING_PREFS, getLoggingLevel());
		if (proxyEnabled && null != Proxy.getInstance() && Proxy.getInstance().isStarted()) {
			System.out.println("browser is being connected to browser-mob-proxy server at => "
					+ Proxy.getInstance().getClientBindAddress().getHostAddress() + ":"
					+ Proxy.getInstance().getPort());
			org.openqa.selenium.Proxy proxy = ClientUtil.createSeleniumProxy(Proxy.getInstance());
			proxy.setHttpProxy(
					Proxy.getInstance().getClientBindAddress().getHostAddress() + ":" + Proxy.getInstance().getPort());
			proxy.setFtpProxy(
					Proxy.getInstance().getClientBindAddress().getHostAddress() + ":" + Proxy.getInstance().getPort());
			proxy.setSslProxy(
					Proxy.getInstance().getClientBindAddress().getHostAddress() + ":" + Proxy.getInstance().getPort());
			options.setProxy(proxy);
		}
		driver = new ChromeDriver(options);
		return driver;
	}

	public static WebDriver getDockerRemoteWebDriver(String hubAddress, String scenarioName, String sBrowser,
            Boolean proxyEnabled) throws MalformedURLException {
        WebDriver driver = null;
//      String scenarioNameZalenium = FrameworkConstants.getExecutionId();
//      String buildJobName = System.getProperty("buildJobName");
//      if(!(Strings.isNullOrEmpty(buildJobName)))
//           scenarioNameZalenium = scenarioNameZalenium + buildJobName + "|END|";
//      String buildNumber = System.getProperty("buildNumber");
//      if(!(Strings.isNullOrEmpty(buildNumber)))
//           scenarioNameZalenium = scenarioNameZalenium + buildNumber + "|END|";
//      String buildBranch = System.getProperty("buildBranch");
//      if(!(Strings.isNullOrEmpty(buildBranch)))
//           scenarioNameZalenium = scenarioNameZalenium + buildBranch + "|END|";
        String scenarioNameZalenium = scenarioName + " TimeStamp - "
                 + TimeUtil.getCurrentDate("yyyy-MM-dd HH:mm:ss.SSS");
        System.out.println(scenarioNameZalenium);
        for (int i = 0; i < 5; i++) {
            try {
                 DesiredCapabilities capabilities = new DesiredCapabilities();
                 //if (FrameworkConstants.Environment.getEnvironment().equalsIgnoreCase("uat")) {
                      //String PROXY = FrameworkConstants.PROXY_IP + ":" + FrameworkConstants.PROXY_PORT;
                      //org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
                      //proxy.setHttpProxy(PROXY).setFtpProxy(PROXY).setSslProxy(PROXY);
                      //capabilities.setCapability(CapabilityType.PROXY, proxy);
                      //System.out.println("proxy server attached to selenium = " + PROXY);
//               } else if (proxyEnabled && null != Proxy.getInstance() && Proxy.getInstance().isStarted()) {
//                    System.out.println("browser is being connected to browser-mob-proxy server at => "
//                              + Proxy.getInstance().getClientBindAddress().getHostAddress() + ":"
//                              + Proxy.getInstance().getPort());
//                    org.openqa.selenium.Proxy proxy = ClientUtil.createSeleniumProxy(Proxy.getInstance());
//                    proxy.setHttpProxy(Proxy.getInstance().getClientBindAddress().getHostAddress() + ":"
//                              + Proxy.getInstance().getPort());
//                    proxy.setFtpProxy(Proxy.getInstance().getClientBindAddress().getHostAddress() + ":"
//                              + Proxy.getInstance().getPort());
//                    proxy.setSslProxy(Proxy.getInstance().getClientBindAddress().getHostAddress() + ":"
//                              + Proxy.getInstance().getPort());
//                    capabilities.setCapability(CapabilityType.PROXY, proxy);
//               }

                 capabilities.setCapability(CapabilityType.PLATFORM_NAME, Platform.LINUX);
                 capabilities.setCapability(CapabilityType.BROWSER_NAME, sBrowser);
                 capabilities.setCapability(CapabilityType.BROWSER_VERSION, "80.0");
                 //capabilities.setCapability("name", scenarioNameZalenium);
                 //capabilities.setCapability("build", FrameworkConstants.getExecutionId());
                 //capabilities.setCapability("tz", "Europe/Berlin");
                 //capabilities.setCapability("idleTimeout", 90);
//               if(sBrowser.equalsIgnoreCase("chrome")) {
//                    ChromeOptions options = new ChromeOptions();
//                    options.addArguments("--start-maximized");
//                    options.addArguments("--disable-dev-shm-usage");
//                    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//               }
                 FirefoxOptions fOptions = new FirefoxOptions();
                 fOptions.setHeadless(true);
                 ChromeOptions options = new ChromeOptions();
                 options.addArguments("--disable-dev-shm-usage");
                 options.setHeadless(true);
                 //options.addArguments("--no-sandbox");
                 options.addArguments("--start-maximized");
                 //capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                 driver = new RemoteWebDriver(new URL(hubAddress), options);
                 driver.manage().window().setSize(new Dimension(1440, 900));
            } catch (Exception e) {
                 e.printStackTrace();
                 driver = null;
            }
            if (driver != null)
                 break;
            System.out.println(
                      "WebDriver object creation attempt  - " + i + " failed. " + "Trying to create object again.");
        }
        return driver;
   }


	public static WebDriver getGridRemoteWebDriver(String hubAddress, String scenarioName, String sBrowser,
		Boolean proxyEnabled) throws MalformedURLException {
		WebDriver driver = null;
		for (int i = 0; i < 5; i++) {
			try {
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(CapabilityType.BROWSER_NAME, sBrowser);
				driver = new RemoteWebDriver(new URL(hubAddress), capabilities);
			} catch (Exception e) {
				e.printStackTrace();
				driver = null;
			}
			if (driver != null)
				break;
			System.out.println(
					"WebDriver object creation attempt  - " + i + " failed. " + "Trying to create object again.");
		}
		return driver;
	}
	
	private static LoggingPreferences getLoggingLevel() {
		LoggingPreferences logPrefs = new LoggingPreferences();
		logPrefs.enable(LogType.BROWSER, Level.ALL);
		logPrefs.enable(LogType.CLIENT, Level.OFF);
		logPrefs.enable(LogType.DRIVER, Level.OFF);
		logPrefs.enable(LogType.PERFORMANCE, Level.OFF);
		logPrefs.enable(LogType.PROFILER, Level.OFF);
		logPrefs.enable(LogType.SERVER, Level.OFF);
		return logPrefs;
	}
}
