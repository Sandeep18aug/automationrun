package framework.selenium.support.zalenium;

import static io.restassured.RestAssured.when;

import org.junit.Assert;
import io.restassured.response.Response;

public class ZaleniumHub {

	public synchronized Boolean isSlotFree(String hubAddress) {
		Response response = null;
		try {
			response = when().get("http://" + hubAddress + "/grid/api/hub");
			response.then().assertThat().statusCode(200);
		} catch (Exception e) {
			Assert.fail("grid is not running on address - " + hubAddress);
		}
		int freeSlot = response.jsonPath().getInt("slotCounts.free");
		System.out.println(freeSlot);
		if (freeSlot > 1)
			return true;
		else
			return false;
	}

	public synchronized int getSlotFreeCount(String hubAddress) {
		Response response = null;
		try {
			response = when().get("http://" + hubAddress + "/grid/api/hub");
			response.then().assertThat().statusCode(200);
		} catch (Exception e) {
			Assert.fail("grid is not running on address - " + hubAddress);
		}
		return response.jsonPath().getInt("slotCounts.free");
	}

	public Boolean isGridUpAndRunning(String hubAddress) {
		try {
			when().get("http://" + hubAddress + "/grid/api/hub").then().assertThat().statusCode(200);
		} catch (Exception e) {
			return false;
		}
		System.out.println("grid is up and running on address - " + hubAddress);
		return true;
	}
}
