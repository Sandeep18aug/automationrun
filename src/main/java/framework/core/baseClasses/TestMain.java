package framework.core.baseClasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.amazonaws.regions.AwsRegionProvider;
import com.amazonaws.regions.AwsRegionProviderChain;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import framework.reporting.CreateReport;
import framework.reporting.ExecutionDetail;
import framework.reporting.ExecutionDetailList;
import framework.reporting.Locale;
import framework.reporting.ProcessedReport;
import framework.reporting.ScenarioDetail;
import framework.reporting.aws.S3Handling;
import framework.shared.TimeUtil;

public class TestMain {

	public static void main(String[] args) throws JsonSyntaxException, JsonIOException, FileNotFoundException {
		
		CreateReport.createProcessedReport();
		//String expectedBaseUrl = "instagram.com/test/";
		//expectedBaseUrl = expectedBaseUrl.endsWith("/") ? expectedBaseUrl.substring(0, expectedBaseUrl.length() - 1) : expectedBaseUrl;
		//System.out.println(expectedBaseUrl);

//		String awsRegion = "eu-west-2";
//		String bucketName = "processedreportbucket";
//		String fileObjKeyName = "processedreport.json";
//		File file = FileUtils.getFile("/Users/avigorka/Documents/project-work/unilever/bitbucket/sitevalidation/site-production-validation/BDD_Automation_Framework/test-output/Report-2019-04-16-01-46-25/processedreport.json");
//		
//		S3Handling s3 = new S3Handling(awsRegion, bucketName, fileObjKeyName, file);
//		s3.uploadFileToS3();

//		System.out.println("abc.FEature".replaceAll("(?i).feature" , ""));
//
//		//System.out.println("abc.FEATURE".replaceAll("(?i).feature" , ""));
//		String url = "facebook.com/tresemme";
//		String url2 = "facebook.com/tresemme/";
//		
//		try {
//			System.out.println(new URL(url).equals(new URL(url2)));
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}/Users/avigorka/Downloads/Report/Automated_Test_Execution.json

		
//		TestMain t = new TestMain();
//		ProcessedReport pr = new ProcessedReport();
//		pr.setJobName("tresemme-uk");
//		pr.setBranchName("develop");
//		pr.setExecutionTime(Date.from(Instant.now()).toString());
//		pr.setReportUrl("http://www.report.com");
//		
//		ExecutionDetailList executionDetailList = ExecutionDetailList.getInstance();
//		
//		//List<ExecutionDetail> executionDetailList = new ArrayList<ExecutionDetail>();
//		//pr.setExecutionDetails(executionDetailList);
//		Gson gson = new Gson();
//		File jsonFile = Paths.get("src/main/resources/Report/Automated_Test_Execution.json").toFile();
//		JsonArray featureArray = gson.fromJson(new FileReader(jsonFile), JsonArray.class);
//		int totalScenario = 0;
//		int totalPassed = 0;
//		for (JsonElement featureElement : featureArray) {
//			JsonObject featureObject = (JsonObject) featureElement;
//			JsonArray scenarioArray = featureObject.getAsJsonArray("elements");
//			totalScenario += scenarioArray.size();
//			for (JsonElement scenElement : scenarioArray) {
//				JsonObject scenario = scenElement.getAsJsonObject();
//				ScenarioDetail scenarioDetail = new ScenarioDetail(scenario);
//				totalPassed = scenarioDetail.isPassed()? ++totalPassed:totalPassed;
//				executionDetailList.updateExecutionDetailList(scenarioDetail);
//				
//			}
//		}
//		pr.setExecutionDetails(executionDetailList.getList());
//		System.out.println(gson.toJson(pr));
//		System.out.println(totalScenario);
//		System.out.println(totalPassed);
		
//		System.out.println(TimeUtil.getEpochTime());
//		String t = TimeUtil.getCurrentDate("yyyy-MM-dd-HH-mm-ss-SSS-z");
//		System.out.println(t);
//		System.out.println(TimeUtil.getEpochTime(t));
		
		  
	}
	
}
 