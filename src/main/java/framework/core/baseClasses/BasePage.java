package framework.core.baseClasses;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import framework.selenium.support.WebElementFactory;
import framework.shared.FrameworkConstants;

public abstract class BasePage {
	public WebDriver driver;
	public WebDriverWait waitDriver;
	public WebElementFactory webElementFactory;
	public String brandName = ""; 
	
	
	public BasePage(WebDriver driver, String brandName) {
		this.driver = driver;
		this.waitDriver = new WebDriverWait(driver, FrameworkConstants.MEDIUM_WAIT);
		this.webElementFactory = new WebElementFactory(driver, brandName);
		//this.actions = new UserActions(driver, this.waitDriver);
		this.brandName = brandName;
	}
	
	public void waitForObjectToPerish(By byProperty) throws Throwable{
		try {
			WebDriverWait wait = new WebDriverWait(driver, FrameworkConstants.LARGE_WAIT);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(byProperty));
		} catch (TimeoutException e) {
			
		}		
	}
	
	public boolean waitForObjectToLoad(By byProperty) throws Throwable{
		return waitForObjectToLoad(byProperty, FrameworkConstants.MEDIUM_WAIT);	
	}
	
	
	public void waitForObjectToPerish(String object) throws Throwable{
		try {
			WebDriverWait wait = new WebDriverWait(driver, FrameworkConstants.LARGE_WAIT);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(webElementFactory.getByLocator(object)));
		} catch (TimeoutException e) {
			
		}		
	}
	
	public boolean waitForObjectToLoad(String object) throws Throwable{
		return waitForObjectToLoad(webElementFactory.getByLocator(object), FrameworkConstants.MEDIUM_WAIT);	
	}
	public boolean waitForObjectToLoad(By byProperty, int maxTime) throws Throwable{
		try {
			WebDriverWait wait = new WebDriverWait(driver, maxTime);
			WebElement elem =  wait.until(ExpectedConditions.presenceOfElementLocated(byProperty));
			return elem != null;
		} catch (TimeoutException e) {
			return false;
		}		
	}

	public void waitForPageReady(int seconds) {
		try {
			String sPageInteractiveStatus = "";
			for (int iPageStatusLoop = 0; iPageStatusLoop < seconds; iPageStatusLoop++) {
				if (sPageInteractiveStatus.equalsIgnoreCase("complete")||sPageInteractiveStatus.equalsIgnoreCase("interactive")) {
					Thread.sleep(500);
					break;
				} else {
					Thread.sleep(1000);
					sPageInteractiveStatus = String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"));
				}
			}
		} catch (Exception e) {
			System.out.println("error while getting page interactive status; " + e);
		}
	}

	public void waitForPageReady() {
		waitForPageReady(FrameworkConstants.MEDIUM_WAIT);
	}
	
	public void clickAt(WebElement element, int i, int j) throws Throwable {
		try {
			Actions build = new Actions(this.driver);
			build.moveToElement(element, i, j).pause(Duration.ofSeconds(3)).click().pause(Duration.ofSeconds(3)).build().perform();
			System.out.println("Object is successfully clicked at" + i + " and " + j);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	public void clickByJavaScript(WebElement element)
    {
           JavascriptExecutor executor = (JavascriptExecutor)driver;
           executor.executeScript("arguments[0].click();", element);
    }
	
}
