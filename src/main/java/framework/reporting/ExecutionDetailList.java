/**
 * 
 */
package framework.reporting;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

/**
 * @author agorka@sapient.com
 *
 */
public class ExecutionDetailList {

	private static ExecutionDetailList executionDetailList = null;
	private static Set<ExecutionDetail> listExecutionDetail = new HashSet<ExecutionDetail>();

	private ExecutionDetailList() {
	}
	
	public static ExecutionDetailList getInstance() {
		if (executionDetailList == null)
			executionDetailList = new ExecutionDetailList();
		return executionDetailList;
	}

		
	public boolean isEmpty() {
		return listExecutionDetail.size() == 0;
	}

	public boolean containsExecutionDetail(String brandName) {
		return listExecutionDetail.stream().anyMatch((executionDetail)->executionDetail.getBrandName().equals(brandName));
	}
	
	public ExecutionDetail getExecutionDetail(String brandName) {
		
		try {
			return listExecutionDetail.stream().filter((executionDetail)-> executionDetail.getBrandName().equals(brandName)).findFirst().get();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	public void addExecutionDetail(ScenarioDetail scenarioDetail) {
		ExecutionDetail executionDetail = new ExecutionDetail(scenarioDetail);
		listExecutionDetail.add(executionDetail);
	}

	public void updateExecutionDetailList(ScenarioDetail scenarioDetail){
		
		if(executionDetailList.isEmpty() || !executionDetailList.containsExecutionDetail(scenarioDetail.getBrandName())) {
			addExecutionDetail(scenarioDetail);
			return;
		}
		
		getExecutionDetail( scenarioDetail.getBrandName()).updateLocale(scenarioDetail);
		
		
//		 ListIterator<ExecutionDetail> iterator = listExecutionDetail.listIterator();
//	      while (iterator.hasNext()){
//	    	  ExecutionDetail executionDetail = iterator.next();
//	    	  if(executionDetail.getBrandName().equals(anObject))
//	          iterator.set(executionDetail);
//	      }


	}

	public Set<ExecutionDetail> getList() {
		// TODO Auto-generated method stub
		return listExecutionDetail;
	}

}
