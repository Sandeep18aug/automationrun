package framework.reporting;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Locale implements Serializable {

	@SerializedName("localeName")
	@Expose
	private String localeName;
	@SerializedName("total")
	@Expose
	private Integer total = 0;
	@SerializedName("pass")
	@Expose
	private Integer pass = 0;
	private final static long serialVersionUID = 6997248261085432320L;

	public String getLocaleName() {
		return localeName;
	}

	public void setLocaleName(String localeName) {
		this.localeName = localeName;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getPass() {
		return pass;
	}

	public void setPass(Integer pass) {
		this.pass = pass;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("localeName", localeName).append("total", total).append("pass", pass)
				.toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(localeName).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Locale) == false) {
			return false;
		}
		Locale rhs = ((Locale) other);
		return new EqualsBuilder().append(localeName, rhs.localeName).isEquals();
	}

	public Locale(String localeName) {
		this.localeName = localeName;
	}

	public Locale(ScenarioDetail scenarioDetail) {
		this.localeName = scenarioDetail.getLocaleName();
		if(scenarioDetail.isPassed()) {
			this.pass += 1;
		}
		this.total += 1;
	}
	
	public void incrementPass() {
		this.pass += 1;
		incrementTotal();
	}
	
	public void incrementTotal() {
		this.total += 1;
	}
}