package framework.reporting;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExecutionDetail implements Serializable {

	@SerializedName("brandName")
	@Expose
	private String brandName;
	@SerializedName("locales")
	@Expose
	private Set<Locale> locales = new HashSet<Locale>();
	private final static long serialVersionUID = -1416329458521714555L;

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public Set<Locale> getLocales() {
		return locales;
	}

	public void setLocales(Set<Locale> locales) {
		this.locales = locales;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("brandName", brandName).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(brandName).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ExecutionDetail) == false) {
			return false;
		}
		ExecutionDetail rhs = ((ExecutionDetail) other);
		return new EqualsBuilder().append(brandName, rhs.brandName).isEquals();
	}

	public ExecutionDetail(ScenarioDetail scenarioDetail) {
		this.brandName = scenarioDetail.getBrandName();
		Locale locale = new Locale(scenarioDetail.getLocaleName());
		if (scenarioDetail.isPassed()) {
			locale.incrementPass();
		} else {
			locale.incrementTotal();
		}
		this.locales.add(locale);
	}

	public void addLocale(Locale locale) {
		if (!isLocalePresent(locale.getLocaleName()))
			this.locales.add(locale);
	}

	public void addLocale(String localeName) {
		if (!isLocalePresent(localeName))
			this.locales.add(new Locale(localeName));
	}

	public boolean isLocalePresent(String localeName) {
		return locales.stream().anyMatch((locale) -> locale.getLocaleName().equals(localeName));
	}

	public Locale getLocale(String localeName) {
		try {
			return this.locales.stream().filter((locale) -> locale.getLocaleName().equals(localeName)).findFirst()
					.get();
		} catch (Exception e) {
			return null;
		}
	}

	public void updateLocale(ScenarioDetail scenarioDetail) {
		if (!isLocalePresent(scenarioDetail.getLocaleName())) {
			Locale locale = new Locale(scenarioDetail);
			addLocale(locale);
			return;
		}
		if (scenarioDetail.isPassed())
			getLocale(scenarioDetail.getLocaleName()).incrementPass();
		else
			getLocale(scenarioDetail.getLocaleName()).incrementTotal();
	}

}