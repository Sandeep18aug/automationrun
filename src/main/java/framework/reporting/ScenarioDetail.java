/**
 * 
 */
package framework.reporting;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author agorka@sapient.com
 *
 */
public class ScenarioDetail {

	private String brandName = "misc", localeName = "misc";
	private boolean isPassed = true;
	private Locale locale = null;
	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}
	/**
	 * @return the localeName
	 */
	public String getLocaleName() {
		return localeName;
	}
	/**
	 * @return the isPassed
	 */
	public boolean isPassed() {
		return isPassed;
	}
	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	/**
	 * @param localeName the localeName to set
	 */
	public void setLocaleName(String localeName) {
		this.localeName = localeName;
	}
	/**
	 * @param isPassed the isPassed to set
	 */
	public void setPassed(boolean isPassed) {
		this.isPassed = isPassed;
	}
	
	public ScenarioDetail(JsonObject scenario) {
		JsonArray stepsArray = scenario.getAsJsonArray("steps");
		JsonArray tagsArray = scenario.getAsJsonArray("tags");
		
		for(JsonElement stepElement : stepsArray) {
			JsonObject step = (JsonObject) stepElement;
			JsonObject result = step.getAsJsonObject("result");
			if(!result.get("status").getAsString().equalsIgnoreCase("passed")) {
				isPassed = false;
				break;
			}
		}

		for(JsonElement tagElement : tagsArray) {
			JsonObject tag = (JsonObject) tagElement;
			String tagName = tag.get("name").getAsString();
			if(tagName.toLowerCase().startsWith("@brand-")) {
				//this.brandName = tagName.split("-")[1];
				this.brandName = tagName.replace("@brand-", "");
			}
		}
		
		for(JsonElement tagElement : tagsArray) {
			JsonObject tag = (JsonObject) tagElement;
			String tagName = tag.get("name").getAsString();
			if(tagName.toLowerCase().startsWith("@locale-")) {
				//this.localeName = tagName.split("-")[1];
				this.localeName = tagName.replace("@locale-", "");
			}
		}
		if(this.brandName.equalsIgnoreCase("misc"))this.localeName = "misc";
		this.locale = new Locale(localeName);
	}
	
	public Locale getLocale() {
		return this.locale;
	}
	
}
