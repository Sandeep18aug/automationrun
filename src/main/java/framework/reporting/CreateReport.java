package framework.reporting;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.testng.util.Strings;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import framework.reporting.aws.DynamoDbHandling;
import framework.shared.CustomFileFilter;
import framework.shared.FrameworkConstants;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.reducers.ReducingMethod;

public class CreateReport {
	public static void createFinalReport() {
		try {
			String sInputFilePaths = FrameworkConstants.REPORTS_FOLDER;
			String sTrackName = "Unilever Digital Platform";
			File reportOutputDirectory = new File(FrameworkConstants.CUCUMBER_HTML_REPORTS_FOLDER + "Report-"
					+ FrameworkConstants.getExecutionTimeStamp());
			List<String> jsonFiles = new ArrayList<String>();
			File reportInputFolder = new File(sInputFilePaths);
			File[] filesInput = reportInputFolder.listFiles(new CustomFileFilter(new String[] { "json" }));
			List<File> listInputFiles = Arrays.asList(filesInput);
			for (File inputFile : listInputFiles) {
				jsonFiles.add(sInputFilePaths + inputFile.getName());
			}
			Configuration configuration = new Configuration(reportOutputDirectory, sTrackName);
			configuration.setBuildNumber(new Date().toString());
			configuration.addReducingMethod(ReducingMethod.MERGE_FEATURES_BY_ID);
			ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
			reportBuilder.generateReports();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Exception in generating final report");
		}
		createProcessedReport();
	}



	public static void main(String args[]) {
		createFinalReport();
	}

	public void deletePreviousReports() {
		File dir = new File(FrameworkConstants.REPORTS_FOLDER);
		for (File file : dir.listFiles()) {
			if (!file.isDirectory())
				file.delete();
		}
	}

	public static void createProcessedReport() {
		try {
			ProcessedReport processedReport = new ProcessedReport();
			String executionId = System.getProperty("executionId", "build-propagation");
			processedReport.setJobName(executionId);
			processedReport.setBranchName("develop");
			processedReport.setExecutionTime(getCurrentEpochTimeStamp());
			processedReport.setReportUrl(System.getProperty("reporturl", "http://zalenium.unileversolutions.com/share/BDD/"));

			ExecutionDetailList executionDetailList = ExecutionDetailList.getInstance();

			Gson gson = new Gson();
			File jsonFile = Paths.get(FrameworkConstants.REPORTS_FOLDER + "Automated_Test_Execution.json").toFile();
			JsonArray featureArray = gson.fromJson(new FileReader(jsonFile), JsonArray.class);
			int totalScenario = 0;
			int totalPassed = 0;
			for (JsonElement featureElement : featureArray) {
				JsonObject featureObject = (JsonObject) featureElement;
				JsonArray scenarioArray = featureObject.getAsJsonArray("elements");
				totalScenario += scenarioArray.size();
				for (JsonElement scenElement : scenarioArray) {
					JsonObject scenario = scenElement.getAsJsonObject();
					ScenarioDetail scenarioDetail = new ScenarioDetail(scenario);
					totalPassed = scenarioDetail.isPassed() ? ++totalPassed : totalPassed;
					executionDetailList.updateExecutionDetailList(scenarioDetail);
				}
			}

			processedReport.setExecutionDetails(executionDetailList.getList());
			processedReport.setTotal(totalScenario);
			processedReport.setPassed(totalPassed);
			String processedJson = gson.toJson(processedReport);
			System.out.println(processedJson);
			System.out.println(totalScenario);
			System.out.println(totalPassed);
			String uploadToAws = System.getProperty("uploadReport", null);
			if(Strings.isNotNullAndNotEmpty(uploadToAws) && uploadToAws.toLowerCase().contains("y")) {
				DynamoDbHandling dbHandling = new DynamoDbHandling();
				dbHandling.uploadReportThroughApi(processedJson);
			}
			File file = new File(FrameworkConstants.CUCUMBER_HTML_REPORTS_FOLDER + "Report-"
					+ FrameworkConstants.getExecutionTimeStamp() + "/processedreport.json");			
			FileWriter writer = new FileWriter(file);
			writer.write(processedJson);
			writer.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Exception in generating final report");
		}
	}

	public static long getCurrentEpochTimeStamp() {
		OffsetDateTime currentTimeInUtc = OffsetDateTime.now(ZoneOffset.UTC);
		System.out.println(currentTimeInUtc);
		long millisecondsSinceEpoch = currentTimeInUtc.toInstant().toEpochMilli();
		return millisecondsSinceEpoch / 1000;
	}

}