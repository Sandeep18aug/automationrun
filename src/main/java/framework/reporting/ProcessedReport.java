package framework.reporting;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessedReport implements Serializable {

	@SerializedName("jobName")
	@Expose
	private String jobName;
	@SerializedName("branchName")
	@Expose
	private String branchName;
	@SerializedName("executionTime")
	@Expose
	private long executionTime;
	@SerializedName("reportUrl")
	@Expose
	private String reportUrl;
	@SerializedName("executionDetails")
	@Expose
	private Set<ExecutionDetail> executionDetails = null;
	
	@SerializedName("total")
	@Expose
	private long total;
	
	@SerializedName("passed")
	@Expose
	private long passed;
	
	private final static long serialVersionUID = -7442472240660948048L;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}

	public String getReportUrl() {
		return reportUrl;
	}

	public void setReportUrl(String reportUrl) {
		this.reportUrl = reportUrl;
	}

	public Set<ExecutionDetail> getExecutionDetails() {
		return executionDetails;
	}

	public void setExecutionDetails(Set<ExecutionDetail> executionDetails) {
		this.executionDetails = executionDetails;
	}
	

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public long getPassed() {
		return passed;
	}

	public void setPassed(long passed) {
		this.passed = passed;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("jobName", jobName).append("branchName", branchName)
				.append("executionTime", executionTime).append("reportUrl", reportUrl)
				.append("executionDetails", executionDetails).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(reportUrl).append(executionTime).append(branchName).append(jobName)
				.append(executionDetails).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ProcessedReport) == false) {
			return false;
		}
		ProcessedReport rhs = ((ProcessedReport) other);
		return new EqualsBuilder().append(reportUrl, rhs.reportUrl).append(executionTime, rhs.executionTime)
				.append(branchName, rhs.branchName).append(jobName, rhs.jobName)
				.append(executionDetails, rhs.executionDetails).isEquals();
	}

}