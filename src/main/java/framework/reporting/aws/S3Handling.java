package framework.reporting.aws;

import java.io.File;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class S3Handling {
	String clientRegion = "";
	String bucketName = "";
	String stringObjKeyName = "";
	String fileObjKeyName = "";
	String fileName = "";
	File file = null;

	public S3Handling(String clientRegion, String bucketName, String fileObjKeyName, File fileTobeUploaded) {
		this.clientRegion = clientRegion;
		this.bucketName = bucketName;
		this.fileObjKeyName = fileObjKeyName;
		this.file = fileTobeUploaded;
	}
	
	public void uploadFileToS3() {
		try {
			AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(clientRegion)
					.withCredentials(new AWSCredentialsHandler().getCredentials()).build();

			// Upload a file as a new object with ContentType and title specified.
			PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName, file);
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType("application/json");
			metadata.addUserMetadata("x-amz-meta-title", "processedreport.json");
			request.setMetadata(metadata);
			s3Client.putObject(request);
		} catch (AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
			// it, so it returned an error response.
			e.printStackTrace();
		} catch (SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
			// couldn't parse the response from Amazon S3.
			e.printStackTrace();
		}
	}
}
