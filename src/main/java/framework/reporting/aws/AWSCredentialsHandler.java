package framework.reporting.aws;


import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;

public class AWSCredentialsHandler {
	
	public AWSCredentialsProvider getCredentials() {
		return new PropertiesFileCredentialsProvider(
				"AwsCredentials.properties");
	}
}
