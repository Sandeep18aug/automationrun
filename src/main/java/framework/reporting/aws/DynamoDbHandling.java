package framework.reporting.aws;


import static io.restassured.RestAssured.given;

public class DynamoDbHandling {
	
	String url = "https://ab6cgwns93.execute-api.eu-central-1.amazonaws.com/dev/report";

	public void uploadReportThroughApi(String json) {
		given().body(json).
		when().post(url).
		then().statusCode(200);
	}
}
