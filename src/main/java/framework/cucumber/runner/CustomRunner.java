package framework.cucumber.runner;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.google.common.base.Strings;

import cucumber.api.testng.AbstractTestNGCucumberTests;
import framework.cucumber.modify.support.MyTestNGCucumberTests;
import framework.reporting.CreateReport;
import framework.selenium.support.ObjectRepository;
import framework.shared.FrameworkConstants;
import framework.shared.Proxy;
import framework.shared.TimeUtil;

/**
 * @author agorka
 *
 */
public class CustomRunner extends MyTestNGCucumberTests {

	@BeforeSuite(alwaysRun = true)
	public void setUp() {
		System.out.println("Execution started at - " + TimeUtil.getCurrentDate("yyyy-MM-dd-HH-mm-ss"));
		deletePreviousJsonReports();
		String runProxyServer = System.getProperty("proxy");
		 if(!(Strings.isNullOrEmpty(runProxyServer)) && runProxyServer.equalsIgnoreCase("yes")) {
				Proxy.start(FrameworkConstants.Environment.getPublisherAuthUserID(),
						FrameworkConstants.Environment.getPublisherAuthPassword());
		 }
		 ObjectRepository.loadCommonOr(FrameworkConstants.COMMON_OR_NAME);
	}

	private void deletePreviousJsonReports() {
		File reportFolder = new File(FrameworkConstants.REPORTS_FOLDER);
		File[] files = reportFolder.listFiles();
		System.out.println("Deleting previous reports.");
		if (files.length == 0) {
			System.out.println("No previous report found in " + FrameworkConstants.REPORTS_FOLDER + " folder");
			return;
		}
		for (File f : files) {
			System.out.println("Deleting report ::> " + f.getName());
			FileUtils.deleteQuietly(f);
		}
	}

	@AfterSuite(alwaysRun = true)
	public void tearDown() {
		Proxy.stop();
		CreateReport.createFinalReport();		
		System.out.println(TimeUtil.getCurrentDate("yyyy-MM-dd-HH-mm-ss"));
		System.out.println("Execution completed at - " + TimeUtil.getCurrentDate("yyyy-MM-dd-HH-mm-ss"));
	}
	
	 @Override
	    @DataProvider(parallel = true)
	    public Object[][] scenarios() {
	        return super.scenarios();
	    }
}