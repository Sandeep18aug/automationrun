package framework.cucumber.runner;

import cucumber.api.CucumberOptions;
 

@CucumberOptions(
		features = { "src/main/resources/FeatureFiles" },
		glue = { "steps" },
 
		tags = {"@brand-sunsilk and @locale-in and @cxx and @cross"},
		
		//tags = {"@cxx and (@bin or @BIN) and not @functional"},
		plugin = { "json:src/main/resources/Report/Automated_Test_Execution.json" }
		)


public class ScriptRunner extends CustomRunner {
	
}

