package framework.cucumber.modify.support;

import cucumber.api.cli.Main;

public class MyRunnable implements Runnable {
	String[] bddArgs = null;
	
	MyRunnable(String[] bddArgs) {
		this.bddArgs = bddArgs;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		Main.main(this.bddArgs); 

	}

}
