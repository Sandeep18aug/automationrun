package framework.cucumber.modify.support;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.PickleEventWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import framework.shared.FrameworkConstants;
import framework.shared.TimeUtil;
import gherkin.events.PickleEvent;
import gherkin.pickles.Pickle;

public class MyTestNGCucumberTests {
	   private TestNGCucumberRunner testNGCucumberRunner;

	    @BeforeClass(alwaysRun = true)
	    public void setUpClass() throws Exception {
	        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
	    }

	    @Test(groups = "cucumber", description = "Runs Cucumber Scenarios", dataProvider = "scenarios")
	    public void runScenario(PickleEventWrapper pickleWrapper, CucumberFeatureWrapper featureWrapper) throws Throwable {
	        // the 'featureWrapper' parameter solely exists to display the feature file in a test report
	        
	    	PickleEvent pickleEvent = pickleWrapper.getPickleEvent();
	    	Pickle pickle = pickleEvent.pickle;
	    	Pickle newPickle = new Pickle(pickle.getName() + "     sid=" + getDynamicScenarioId(), pickle.getLanguage(), pickle.getSteps(), pickle .getTags(), pickle.getLocations());
	    	PickleEvent newPickleEvent = new PickleEvent(pickleEvent.uri, newPickle);
	    	testNGCucumberRunner.runScenario(newPickleEvent);
	    }

	    /**
	     * Returns two dimensional array of PickleEventWrapper scenarios with their associated CucumberFeatureWrapper feature.
	     *
	     * @return a two dimensional array of scenarios features.
	     */
	    @DataProvider
	    public Object[][] scenarios() {
	        if (testNGCucumberRunner == null) {
	            return new Object[0][0];
	        }
	        return testNGCucumberRunner.provideScenarios();
	    }

	    @AfterClass(alwaysRun = true)
	    public void tearDownClass() throws Exception {
	        if (testNGCucumberRunner == null) {
	            return;
	        }
	        testNGCucumberRunner.finish();
	    }
	    
	    private String getDynamicScenarioId() {
	    	return TimeUtil.getCurrentDate("yyyyMMddHHmmssSSS") + RandomStringUtils.random(10, true, true);
	    }
}
