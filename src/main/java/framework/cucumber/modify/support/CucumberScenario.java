package framework.cucumber.modify.support;

import gherkin.events.PickleEvent;

public class CucumberScenario {
	PickleEvent pickleEvent;

	public CucumberScenario(PickleEvent pickleEvent) {
		this.pickleEvent = pickleEvent;
	}

	public boolean run() {
		boolean result = true;
		try {
			String[] arguments = { this.pickleEvent.uri +":" };
			cucumber.api.cli.Main.main(arguments);
		} catch (Exception ex) {
			result = false;
		}
		return result;
	}

}
