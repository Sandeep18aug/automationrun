package framework.cucumber.modify.support;
//package cucumber.modify.support;
//
//import static java.util.Arrays.asList;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//import java.util.TreeSet;
//
//import cucumber.api.StepDefinitionReporter;
//import cucumber.runner.EventBus;
//import cucumber.runner.TimeService;
//import cucumber.runner.TimeServiceEventBus;
//import cucumber.runtime.ClassFinder;
//import cucumber.runtime.Runtime;
//import cucumber.runtime.RuntimeOptions;
//import cucumber.runtime.io.MultiLoader;
//import cucumber.runtime.io.ResourceLoader;
//import cucumber.runtime.io.ResourceLoaderClassFinder;
//import cucumber.runtime.model.CucumberFeature;
//import gherkin.events.PickleEvent;
//
//public class MyRuntime extends Runtime {
//
//	Set<String> featureFiles = new TreeSet<String>();
//	private EventBus bus;
//	private RuntimeOptions runtimeOptions;
//	private ResourceLoader resourceLoader;
//	private ClassLoader classLoader;
//	private List<PickleEvent> pe = new ArrayList<>();
//
//	public MyRuntime(ResourceLoader resourceLoader, ClassFinder classFinder, ClassLoader classLoader,
//			RuntimeOptions runtimeOptions) {
//		super(resourceLoader, classFinder, classLoader, runtimeOptions);
//		this.resourceLoader = resourceLoader;
//		this.runtimeOptions = runtimeOptions;
//		this.classLoader = classLoader;
//		bus = new TimeServiceEventBus(TimeService.SYSTEM);
//	}
//
//	public void run() {
//		// Make sure all features parse before initialising any reporters/formatters
//		List<CucumberFeature> features = runtimeOptions.cucumberFeatures(resourceLoader, bus);
//		StepDefinitionReporter stepDefinitionReporter = runtimeOptions.stepDefinitionReporter(classLoader);
//		reportStepDefinitions(stepDefinitionReporter);
//		for (CucumberFeature cucumberFeature : features) {
//			runFeature(cucumberFeature);
//			// featureFiles.add(cucumberFeature.getUri());
//		}
//		System.out.println("total scenarios are :" + pe.size());
//		System.out.println("total features are :" + featureFiles.size());
//		for (String featureFile : featureFiles) {
//			System.out.println(featureFile);
//		}
//	}
//	
//	
//	public List<PickleEvent> getAllScenarios() {
//		// Make sure all features parse before initialising any reporters/formatters
//		List<CucumberFeature> features = runtimeOptions.cucumberFeatures(resourceLoader, bus);
//		StepDefinitionReporter stepDefinitionReporter = runtimeOptions.stepDefinitionReporter(classLoader);
//		reportStepDefinitions(stepDefinitionReporter);
//		for (CucumberFeature cucumberFeature : features) {
//			runFeature(cucumberFeature);		
//		}
//		return this.pe;
//		
//		
//		
////		System.out.println("total scenarios are :" + pe.size());
////		System.out.println("total features are :" + featureFiles.size());
////		for (String featureFile : featureFiles) {
////			System.out.println(featureFile);
////		}
//	}
//
//	public void runFeature(CucumberFeature feature) {
//		List<PickleEvent> pickleEvents = compileFeature(feature);
//		for (PickleEvent pickleEvent : pickleEvents) {
//			if (matchesFilters(pickleEvent)) {
//				// runner.runPickle(pickleEvent);
//				//System.out.println(pickleEvent.pickle.getName());
//				//System.out.println(pickleEvent.uri + ":" +
//				//pickleEvent.pickle.getLocations().get(0).getLine()
//				 //+":"+pickleEvent.pickle.getName());
//				featureFiles.add(pickleEvent.uri.split(":")[0]);
//				pe.add(pickleEvent);
//			}
//		}
//	}
//
//	public List<PickleEvent> getTotalScenarios() {
//		return pe;
//	}
//
//	/**
//	 * Launches the Cucumber-JVM command line.
//	 *
//	 * @param argv
//	 *            runtime options. See details in the
//	 *            {@code cucumber.api.cli.Usage.txt} resource.
//	 * @param classLoader
//	 *            classloader used to load the runtime
//	 * @return 0 if execution was successful, 1 if it was not (test failures)
//	 */
//	public static byte run(String[] argv, ClassLoader classLoader) {
//		RuntimeOptions runtimeOptions = new RuntimeOptions(new ArrayList<String>(asList(argv)));
//		ResourceLoader resourceLoader = new MultiLoader(classLoader);
//		ClassFinder classFinder = new ResourceLoaderClassFinder(resourceLoader, classLoader);
//		Runtime runtime = new Runtime(resourceLoader, classFinder, classLoader, runtimeOptions);
//		runtime.run();
//		return runtime.exitStatus();
//	}
//}
