package framework.cucumber.modify.support;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class FetchSteps {

	public static Class[] getClassesInPackage(String pckgname) {
		File directory = getPackageDirectory(pckgname);
		if (!directory.exists()) {
			throw new IllegalArgumentException("Could not get directory resource for package " + pckgname + ".");
		}

		return getClassesInPackage(pckgname, directory);
	}

	private static Class[] getClassesInPackage(String pckgname, File directory) {
		List<Class> classes = new ArrayList<Class>();
		for (String filename : directory.list()) {
			if (filename.endsWith(".class")) {
				String classname = buildClassname(pckgname, filename);
				try {
					classes.add(Class.forName(classname));
				} catch (ClassNotFoundException e) {
					System.err.println("Error creating class " + classname);
				}
			}
		}
		return classes.toArray(new Class[classes.size()]);
	}

	private static String buildClassname(String pckgname, String filename) {
		return pckgname + '.' + filename.replace(".class", "");
	}

	private static File getPackageDirectory(String pckgname) {
		ClassLoader cld = Thread.currentThread().getContextClassLoader();
		if (cld == null) {
			throw new IllegalStateException("Can't get class loader.");
		}

		URL resource = cld.getResource(pckgname.replace('.', '/'));
		if (resource == null) {
			throw new RuntimeException("Package " + pckgname + " not found on classpath.");
		}

		return new File(resource.getFile());
	}

	public static void main(String[] args) {
		String[] packages = { "steps.business.generic", "steps.business.generic.adp", "steps.business.generic.listingpages","steps.business.generic.pdp","steps.business.generic.recipepage","steps.business.published.components","steps.generic"};
		Set<String> keywords = new TreeSet<String>();
		for (String p : packages) {
			int counter = 0;
			System.out.println(System.lineSeparator());
			System.out.println(System.lineSeparator());
			System.out.println("**Keywords of type : " + p + "  ********************************************");
			Class[] classes = getClassesInPackage(p);
			for (Class c : classes) {
				System.out.println(System.lineSeparator());
				System.out.println("Class: " + c.getCanonicalName() + " *******");
				Method[] methods = c.getMethods();
				for (Method method : methods) {
					// System.out.println(method.getName());
					if (method.isAnnotationPresent(cucumber.api.java.en.And.class)) {
						System.out.println(method.getAnnotation(cucumber.api.java.en.And.class).value());
						counter++;
					}
					else if (method.isAnnotationPresent(cucumber.api.java.en.When.class)) {
						System.out.println(method.getAnnotation(cucumber.api.java.en.When.class).value());
						counter++;
					}
					else if (method.isAnnotationPresent(cucumber.api.java.en.Given.class)) {
						System.out.println(method.getAnnotation(cucumber.api.java.en.Given.class).value());
						counter++;
					}
					else if (method.isAnnotationPresent(cucumber.api.java.en.Then.class)) {
						System.out.println(method.getAnnotation(cucumber.api.java.en.Then.class).value());
						counter++;
					}
				}
			}
			System.out.println(p + " keywords are " + counter);
			
		}
		 System.out.println(keywords);

	}
}
