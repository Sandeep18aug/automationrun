package framework.cucumber.hooks;

import cucumber.api.Scenario;
import cucumber.api.java.AfterStep;
import cucumber.api.java.BeforeStep;

public class Hooks {
	
	
	@BeforeStep
	public void beforeStep(Scenario scenario) {
		System.out.println("executing before Step");
		
	}

	@AfterStep
	public void afterStep(Scenario scenario) {
		System.out.println("executing after Step");
		
	}
}
