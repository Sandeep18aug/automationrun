package framework.configuration;

import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import framework.exceptions.UsersNotSetupException;
import framework.shared.FrameworkConstants;

public class UserFactory implements JsonDeserializer<User> {
	private static String JSON = null;
	private static List<User> usersList = null;

	public User deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		return null;
	}

	private static void loadUsersFromJson() throws UsersNotSetupException, Exception {
		JSON = new String(Files.readAllBytes(Paths.get(FrameworkConstants.USERS_JSON_FILE_PATH)));
		Type targetClassType = new TypeToken<ArrayList<User>>() {
		}.getType();
		usersList = new Gson().fromJson(JSON, targetClassType);		
	}

	public static synchronized User getRandomUserFromList(){
		try {
			if (usersList == null) {
				loadUsersFromJson();
			}
			int randomNumber = new Random().nextInt(usersList.size());
			return usersList.get(randomNumber);
		} catch (UsersNotSetupException e) {
			// TODO: handle exception
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
