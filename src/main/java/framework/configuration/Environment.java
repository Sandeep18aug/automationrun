package framework.configuration;

import com.google.gson.annotations.SerializedName;

import gherkin.deps.com.google.gson.annotations.Expose;

public class Environment {
//	@SerializedName("environment")
//	@Expose
//	private String environment;
//	@SerializedName("authorUserID")
//	@Expose
//	private String authorUserID;
//	@SerializedName("authorPassword")
//	@Expose
//	private String authorPassword;
//	@SerializedName("publisherAuthUserID")
//	@Expose
//	private String publisherAuthUserID;
//	@SerializedName("publisherAuthPassword")
//	@Expose
//	private String publisherAuthPassword;
//	@SerializedName("authorbaseUrl")
//	@Expose
//	private String authorbaseUrl;
//	@SerializedName("publishbaseUrl")
//	@Expose
//	private String publishbaseUrl;
//	@SerializedName("executionFlag")
//	@Expose
//	private Boolean executionFlag;
//	@SerializedName("brand")
//	@Expose
//	private String brand;
//	@SerializedName("authorlocale")
//	@Expose
//	private String authorlocale;
//	@SerializedName("publishedlocale")
//	@Expose
//	private String publishedlocale;
//
//	public String getEnvironment() {
//	return environment;
//	}
//
//	public void setEnvironment(String environment) {
//	this.environment = environment;
//	}
//
//	public String getAuthorUserID() {
//	return authorUserID;
//	}
//
//	public void setAuthorUserID(String authorUserID) {
//	this.authorUserID = authorUserID;
//	}
//
//	public String getAuthorPassword() {
//	return authorPassword;
//	}
//
//	public void setAuthorPassword(String authorPassword) {
//	this.authorPassword = authorPassword;
//	}
//
//	public String getPublisherAuthUserID() {
//	return publisherAuthUserID;
//	}
//
//	public void setPublisherAuthUserID(String publisherUserID) {
//	this.publisherAuthUserID = publisherUserID;
//	}
//
//	public String getPublisherAuthPassword() {
//	return publisherAuthPassword;
//	}
//
//	public void setPublisherAuthPassword(String publisherPassword) {
//	this.publisherAuthPassword = publisherPassword;
//	}
//
//	public String getAuthorbaseUrl() {
//	return authorbaseUrl;
//	}
//
//	public void setAuthorbaseUrl(String authorbaseUrl) {
//	this.authorbaseUrl = authorbaseUrl;
//	}
//
//	public String getPublishbaseUrl() {
//	return publishbaseUrl;
//	}
//
//	public void setPublishbaseUrl(String publishbaseUrl) {
//	this.publishbaseUrl = publishbaseUrl;
//	}
//
//	public Boolean getExecutionFlag() {
//	return executionFlag;
//	}
//
//	public void setExecutionFlag(Boolean executionFlag) {
//	this.executionFlag = executionFlag;
//	}
//
//	public String getBrand() {
//	return brand;
//	}
//
//	public void setBrand(String brand) {
//	this.brand = brand;
//	}
//
//	public String getAuthorlocale() {
//	return authorlocale;
//	}
//
//	public void setAuthorlocale(String authorlocale) {
//	this.authorlocale = authorlocale;
//	}
//
//	public String getPublishedlocale() {
//	return publishedlocale;
//	}
//
//	public void setPublishedlocale(String publishedlocale) {
//	this.publishedlocale = publishedlocale;
//	}
	
	@SerializedName("environment")
	@Expose
	private String environment;
	@SerializedName("publisherAuthUserID")
	@Expose
	private String publisherAuthUserID;
	@SerializedName("publisherAuthPassword")
	@Expose
	private String publisherAuthPassword;
	@SerializedName("executionflag")
	@Expose
	private Boolean executionflag;

	public String getEnvironment() {
	return environment;
	}

	public void setEnvironment(String environment) {
	this.environment = environment;
	}

	public String getPublisherAuthUserID() {
	return publisherAuthUserID;
	}

	public void setPublisherAuthUserID(String publisherAuthUserID) {
	this.publisherAuthUserID = publisherAuthUserID;
	}

	public String getPublisherAuthPassword() {
	return publisherAuthPassword;
	}

	public void setPublisherAuthPassword(String publisherAuthPassword) {
	this.publisherAuthPassword = publisherAuthPassword;
	}

	public Boolean getExecutionflag() {
	return executionflag;
	}

	public void setExecutionflag(Boolean executionflag) {
	this.executionflag = executionflag;
	}


}
