package framework.configuration;

public class User {
	private String userid=null;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}

	private String password=null;

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "Users [userid=" + userid + ", password=" + password + "]";
	}	
}
