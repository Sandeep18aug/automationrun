package framework.exceptions;

public class TestDataNotFoundException extends Exception {
	
	public TestDataNotFoundException(String message) {
		super(message);
	}

}
