package framework.exceptions;

public class InvalidPageException extends Exception {
	
	public InvalidPageException(String message) {
		super(message);
	}

}
